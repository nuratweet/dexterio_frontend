import React from 'react'
import { FooterLinksContainer,  FooterVerticalSpacing , FooterLinksWrapperMiddle , FooterLinksWrapperRight , FooterLinksWrapperLeft , FooterLinksItems, FooterLinkTitles , FooterLink, FooterSubText, SocialIcons, SocialIconLink } from './Footer.element';
import { Container } from '../../globalStyles';
import { FaInstagram , FaWhatsapp , FaLinkedinIn } from 'react-icons/fa';


const Footer = () => {
    return (
      <div>
        <Container>
          <FooterLinksContainer>
            <FooterLinksWrapperLeft>
              <FooterLinksItems>
                <FooterLinkTitles
                  style={{
                    fontFamily: "Montesserat",
                    fontStyle: "normal",
                    fontSize: "40px",
                    padding: "0px",
                    color: "black",
                    letterSpacing: "1.2px",
                  }}
                >
                  <img
                    src="/dexterio_logo.svg"
                    alt="D E X T E R I O"
                    style={{
                      width: "220px",
                      height: "69px",
                      transform: "translate(-12px,-7px)",
                    }}
                  />
                </FooterLinkTitles>
                <FooterVerticalSpacing />
                <FooterSubText>
                  Delivering beautiful
                  <br />
                </FooterSubText>
                <FooterSubText>
                  Interiors at budget
                  <br />
                </FooterSubText>
                <FooterSubText>
                  friendly prices!
                  <br />
                </FooterSubText>
                <FooterVerticalSpacing />
                <FooterSubText>Follow us</FooterSubText>
                <SocialIcons>
                  <SocialIconLink
                    to={{ pathname: "https://www.instagram.com/dexterio.in/" }}
                    target="_blank"
                    aria-label="Instagram"
                  >
                    <FaInstagram />
                  </SocialIconLink>
                  <SocialIconLink
                    to={{
                      pathname:
                        "https://api.whatsapp.com/send/?phone=918209194339&text=Hi%2C+I%27d+like+to+talk+regarding+your+interior+designing+services.&type=phone_number&app_absent=0",
                    }}
                    target="_blank"
                    aria-label="Youtube"
                  >
                    <FaWhatsapp />
                  </SocialIconLink>
                  <SocialIconLink
                    className="social-icon-link linkedin"
                    to={{
                      pathname: "https://www.linkedin.com/company/dexterio",
                    }}
                    target="_blank"
                    aria-label="LinkedIn"
                  >
                    <FaLinkedinIn />
                  </SocialIconLink>
                </SocialIcons>
              </FooterLinksItems>
            </FooterLinksWrapperLeft>
            <FooterLinksWrapperMiddle>
              <div
                style={{
                  width: "40%",
                  display: "flex",
                  justifyContent: "right",
                }}
              >
                <FooterLinksItems>
                  <FooterLinkTitles>Our Service</FooterLinkTitles>
                  <FooterVerticalSpacing />
                  <FooterLink to="/how-it-works">How it Works</FooterLink>
                  <FooterLink to="/portfolio">Projects</FooterLink>
                </FooterLinksItems>
              </div>
              <div
                style={{
                  width: "50%",
                  display: "flex",
                  justifyContent: "right",
                  marginLeft: "5%",
                }}
              >
                <FooterLinksItems>
                  <FooterLinkTitles>Company</FooterLinkTitles>
                  <FooterVerticalSpacing />
                  {/* <FooterLink to='/about'>About us</FooterLink> */}
                  <FooterLink to="/terms-conditions">Terms of Use</FooterLink>
                  <FooterLink to="/privacy">Privacy Policy</FooterLink>
                  <FooterLink to="/warranty-policy">Warranty Policy</FooterLink>
                </FooterLinksItems>
              </div>
            </FooterLinksWrapperMiddle>
            <FooterLinksWrapperRight>
              <div
                style={{
                  width: "100%",
                  display: "flex",
                  justifyContent: "right",
                }}
              >
                <FooterLinksItems>
                  <div>
                    <FooterLinkTitles>Contact</FooterLinkTitles>
                    <FooterVerticalSpacing />
                    <FooterSubText style={{ transform: "translateY(0px)" }}>
                      (+91) 82091-94339
                    </FooterSubText>
                    <FooterSubText style={{ transform: "translateY(0px)" }}>
                      150/21, Shipra Path
                    </FooterSubText>
                    <FooterSubText style={{ transform: "translateY(0px)" }}>
                      Mansarovar, Jaipur
                    </FooterSubText>
                    <FooterSubText style={{ transform: "translateY(0px)" }}>
                      Rajasthan 302020
                    </FooterSubText>
                  </div>
                </FooterLinksItems>
              </div>
            </FooterLinksWrapperRight>
          </FooterLinksContainer>
        </Container>
        <div
          style={{
            backgroundColor: "white",
            textAlign: "center",
            minHeight: "40px",
            alignItems: "center",
            borderTop: "2px solid grey",
          }}
        >
          <FooterSubText
            style={{
              textAlign: "center",
              lineHeight: "40px",
              fontFamily: "Poppins",
              fontStyle: "normal",
              fontWeight: "normal",
              fontSize: "20px",
              color: "rgba(0,0,0,1)",
              display: "flex",
              justifyContent: "center",
              marginTop: "1%",
            }}
          >
            <span>
              {" "}
              <svg
                className="Icon_material-copyright"
                viewBox="3 3 30 30"
                style={{ height: "30px", width: "30px" }}
              >
                {/* <path id="Icon_material-copyright" d="M 15.11999988555908 16.29000091552734 C 
                15.19499969482422 15.79500102996826 15.35999965667725 15.36000061035156 
                15.56999969482422 14.98500061035156 C 15.77999973297119 14.61000061035156 
                16.07999992370605 14.29500102996826 16.45499992370605 14.05500030517578 C 
                16.81500053405762 13.82999992370605 17.26499938964844 13.72500038146973 
                17.81999969482422 13.71000003814697 C 18.16499900817871 13.72500038146973 
                18.47999954223633 13.78499984741211 18.76499938964844 13.90499973297119 C 
                19.06499862670898 14.03999996185303 19.33499908447266 14.21999931335449 
                19.54500007629395 14.44499969482422 C 19.75500106811523 14.67000007629395
                 19.92000007629395 14.9399995803833 20.05500030517578 15.23999977111816 C
                  20.19000053405762 15.53999996185303 20.25 15.86999988555908 20.26499938964844
                   16.19999885559082 L 22.94999885559082 16.19999885559082 C 22.91999816894531 
                   15.49499893188477 22.78499794006348 14.84999847412109 22.52999877929688 14.26499938964844 
                   C 22.27499961853027 13.68000030517578 21.92999839782715 13.16999912261963 21.47999954223633 
                   12.74999904632568 C 21.03000068664551 12.32999897003174 20.48999977111816 11.99999904632568 
                   19.85999870300293 11.75999927520752 C 19.2299976348877 11.51999950408936 18.53999900817871 
                   11.41499900817871 17.77499771118164 11.41499900817871 C 16.79999732971191 11.41499900817871 
                   15.94499778747559 11.57999897003174 15.22499752044678 11.92499923706055 C 14.50499725341797 
                   12.26999950408936 13.90499782562256 12.71999931335449 13.42499732971191 13.30499935150146 C 
                   12.94499683380127 13.88999938964844 12.58499717712402 14.5649995803833 12.3599967956543 
                   15.34499931335449 C 12.13499641418457 16.125 12 16.93499946594238 12 17.80500030517578 L 
                   12 18.21000099182129 C 12 19.08000183105469 12.11999988555908 19.89000129699707 
                   12.34500026702881 20.67000198364258 C 12.57000064849854 21.45000267028809 12.93000030517578 
                   22.12500190734863 13.40999984741211 22.69500160217285 C 13.88999938964844 23.26500129699707 
                   14.48999977111816 23.73000144958496 15.21000003814697 24.06000137329102 C 15.93000030517578
                    24.39000129699707 16.78499984741211 24.57000160217285 17.76000022888184 24.57000160217285 
                    C 18.46500015258789 24.57000160217285 19.125 24.45000076293945 19.73999977111816 24.22500228881836 
                    C 20.35499954223633 24.00000381469727 20.89500045776367 23.68500137329102 21.36000061035156 
                    23.28000259399414 C 21.82500076293945 22.87500381469727 22.20000076293945 22.41000175476074 
                    .47000122070312 21.87000274658203 C 22.7400016784668 21.33000373840332 22.90500068664551 20.76000213623047 22.92000198364258 20.1450023651123 L 20.2350025177002 20.1450023651123 C 20.22000312805176 20.46000289916992 20.1450023651123 20.74500274658203 20.01000213623047 21.0150032043457 C 19.87500190734863 21.28500366210938 19.69500160217285 21.5100040435791 19.47000122070312 21.70500373840332 C 19.2450008392334 21.90000343322754 18.9900016784668 22.05000305175781 18.69000053405762 22.15500450134277 C 18.40500068664551 22.2600040435791 18.10500144958496 22.29000473022461 17.79000091552734 22.30500411987305 C 17.25 22.29000473022461 16.80000114440918 22.18500328063965 16.45500183105469 21.96000480651855 C 16.08000183105469 21.72000503540039 15.78000164031982 21.40500450134277 15.57000160217285 21.03000450134277 C 15.36000156402588 20.65500450134277 15.19500160217285 20.20500373840332 15.12000179290771 19.71000480651855 C 15.04500198364258 19.21500587463379 15.00000190734863 18.70500564575195 15.00000190734863 18.21000480651855 L 15.00000190734863 17.80500411987305 C 15.00000190734863 17.28000450134277 15.04500198364258 16.78500366210938 15.12000179290771 16.29000473022461 Z M 18 3 C 9.720000267028809 3 3 9.720000267028809 3 18 C 3 26.27999877929688 9.719999313354492 33 18 33 C 26.28000068664551 33 33 26.28000068664551 33 18 C 33 9.719999313354492 26.28000068664551 3 18 3 Z M 18 30 C 11.38500022888184 30 6 24.61499977111816 6 18 C 6 11.38500022888184 11.38500022888184 6 18 6 C 24.61499977111816 6 30 11.38500022888184 30 18 C 30 24.61499977111816 24.61499977111816 30 18 30 Z">
				</path> */}
              </svg>
            </span>
            <div
              style={{ marginLeft: "1%", marginTop: "10px", fontSize: "15px" }}
            >
              © 2023 Dexterio Private Limited.
            </div>{" "}
          </FooterSubText>
        </div>
      </div>
    );
}

export default Footer;