import styled from 'styled-components';
import {Link} from 'react-router-dom';

export const FooterVerticalSpacing = styled.div`
    height: 15px;
`;

export const FooterLinksContainer = styled.div`
    width: 100%;
    max-width: 100%;
    display: flex;
    margin-top: 60px;
    position: relative;
    font-family: Poppins;
    font-style: normal;
    font-weight: normal;
    font-size: 20px;
    color: rgba(0,0,0,1)

    @media screen and (max-width: 768px) {
        flex-direction: column;
        margin-top: 30px;
    }

    @media screen and (max-width: 425px) {
        flex-direction: column;
        margin-top: 15px;
        padding-bottom: 35px;
    }

`;

export const FooterLinksWrapperLeft = styled.div`
    display: flex;
    justify-content: flex-start;
    width: 40%;
    margin: 2px 15px 0px 0px;
    

    @media screen and (max-width: 768px) {
        flex-direction: column;
        width: 50%;
    }

`;

export const FooterLinksWrapperMiddle = styled.div`
    display: flex;
    width: 36%;

    @media screen and (max-width: 768px) {
        width: 100%;
    }
`;

export const FooterLinksWrapperRight = styled.div`
    display: flex;
    width: 24%;

    @media screen and (max-width: 768px) {
        width: 50%;
        top: 0px;
        right: 0px;
        position: absolute;
    }
`;

export const FooterLinksItems = styled.div`
    display: flex;
    flex-direction : column;
    align-items: flex-start;
    text-align: left;
    box-sizing: border-box;
    color: black;
    padding: 20px 0px;


    @media screen and (max-width: 768px) {
        margin: 20px;
        paddding: 10px;
        width: 100%;
        justify-content: center;
    }
`;

export const FooterLinkTitles = styled.h2`
    margin-bottom: 16px;
    padding-top:24px;
    font-family: Poppins;
    font-style: normal;
    font-weight: bold;
    font-size: 24px;
    color: rgba(0,0,0,1);

    @media screen and (max-width: 768px) {
        font-size: 20px;
        line-height: 1.1em;
    }
`;

export const FooterLink = styled(Link)`
    color: rgba(0,0,0,1);
    font-family: "Poppins";
    font-style:normal;
    font-size: 16px;
    font-weight: normal;
    
    text-decoration:none;
    margin: 0px 0px 5px 0px;

    @media screen and (max-width: 768px) {
        font-size: 13px;
        line-height: 1.3em;
    }

    &:hover {
        color: #0467fb;
        transition: 0.3s ease-out;
    }

`;

export const FooterSubText = styled.h3`
text-align: left;
font-family: Poppins;
font-style: normal;
font-weight: normal;
font-size: 16px;
transform:translateY(-30px);
color: rgba(0,0,0,1)
    text-decoration:none;
    margin: 0px 0px 5px 0px;

    @media screen and (max-width: 768px) {
        font-size: 13px;
        line-height: 1.3em;
    }

`;

export const SocialIcons = styled.div`
    display: flex;
    justify-content: flex-start;
    align-items: center;
    width: 100%;
`;

export const SocialIconLink = styled(Link)`
    color: #000000;
    font-size: 24px;
    margin-right: 20px;
`;