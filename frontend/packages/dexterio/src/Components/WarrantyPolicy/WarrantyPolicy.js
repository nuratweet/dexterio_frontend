import React from 'react';
import './WarrantyPolicy.css';
import wpData from '../../data/wp.json';
import { DesktopPane, WarrantyPolicyPage, MobilePane, WarrantyPolicyHeading, WarrantyPolicySubHeading , WarrantyPolicySubHeading1 , WarrantyPolicySubscriptSubHeading } from './WarrantyPolicy.element';
import { FaAngleDown , FaAngleUp } from 'react-icons/fa';
import { Helmet } from 'react-helmet';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import {Link} from 'react-scroll';

const WarrantyPolicy = () => {
	const [angleOpen, setAngleOpen] = React.useState(true);
	const [expanded, setExpanded] = React.useState(false);
	const [activeLink, setActiveLink] = React.useState('Dexterio warranty assures');

	const handleChange = (panel) => (event, isExpanded) => {
		setExpanded(isExpanded ? panel : false);
	};

	return (
		<>
		<Helmet>
			<title>Warranty Policy | Dexterio</title>
		</Helmet>
        <WarrantyPolicyPage>
			<DesktopPane width="30%" padding="10px" >
				<div style = {{"position":"fixed"}}>
					<div className="dexterio-wp-widget-container" style={{"minHeight": "0px"}}>
						<div className="dexterio-wp__header">
							<h2 className="dexterio-wp__header-title">Warranty Policy</h2>
							{
								angleOpen ? (
									<div className="dexterio-wp__toggle-button" onClick={()=> setAngleOpen(false)}>
										<FaAngleDown/>
									</div>
								) : 
								(
									<div className="dexterio-wp__toggle-button"  onClick={()=> setAngleOpen(true)}>
										<FaAngleUp/>
									</div>
								)
							}
						</div>
						<div className="dexterio-wp__body" style={{"--show": (angleOpen ? "block" : "none")}}>
							<ul className="dexterio-wp__list-wrapper">
								<li className={`dexterio-wp__list-item ${activeLink === 'Dexterio warranty assures' ? "active" : ""}`}>
									<div className="dexterio-wp__list-item-text-wrapper">
										<svg  x="0px" y="0px" width="10px" height="10px" viewBox="0 0 20 20" enableBackground="new 0 0 10 10"><circle cx="10" cy="10" r="5"/></svg>
										<Link  to="dexterio_warranty" spy={true} smooth={true} className="dexterio-wp__list-item-text dexterio-wp__top-level dexterio-item-active" onClick={()=>{setActiveLink('Dexterio warranty assures')}}>Dexterio warranty assures y...</Link>
									</div>
								</li>
								<li className={`dexterio-wp__list-item ${activeLink === 'Furniture' ? "active" : ""}`}>
									<div className="dexterio-wp__list-item-text-wrapper">
										<svg  x="0px" y="0px" width="10px" height="10px" viewBox="0 0 20 20" enableBackground="new 0 0 10 10"><circle cx="10" cy="10" r="5"/></svg>
										<Link  to="furniture" spy={true} smooth={true} className="dexterio-wp__list-item-text dexterio-wp__top-level" onClick={()=>{setActiveLink('Furniture')}}>Furniture</Link>
									</div>
								</li>
								<li className={`dexterio-wp__list-item ${activeLink === 'On-Site services' ? "active" : ""}`}>
									<div className="dexterio-wp__list-item-text-wrapper">
										<svg  x="0px" y="0px" width="10px" height="10px" viewBox="0 0 20 20" enableBackground="new 0 0 10 10"><circle cx="10" cy="10" r="5"/></svg>
										<Link  to="onSiteServices" spy={true} smooth={true} className="dexterio-wp__list-item-text dexterio-wp__top-level" onClick={()=>{setActiveLink('On-Site services')}}>On-Site services</Link></div>
									</li>
								<li className={`dexterio-wp__list-item ${activeLink === 'Painting' ? "active" : ""}`}>
									<div className="dexterio-wp__list-item-text-wrapper">
										<svg  x="0px" y="0px" width="10px" height="10px" viewBox="0 0 20 20" enableBackground="new 0 0 10 10"><circle cx="10" cy="10" r="5"/></svg>
										<Link  to="painting" spy={true} smooth={true} className="dexterio-wp__list-item-text dexterio-wp__top-level" onClick={()=>{setActiveLink('Painting')}}>Painting</Link>
									</div>
								</li>
								<li className={`dexterio-wp__list-item ${activeLink === 'Gypsum false ceiling' ? "active" : ""}`}>
									<div className="dexterio-wp__list-item-text-wrapper">
										<svg  x="0px" y="0px" width="10px" height="10px" viewBox="0 0 20 20" enableBackground="new 0 0 10 10"><circle cx="10" cy="10" r="5"/></svg>
										<Link  to="gypsum" spy={true} smooth={true} className="dexterio-wp__list-item-text dexterio-wp__top-level" onClick={()=>{setActiveLink('Gypsum false ceiling')}}>Gypsum false ceiling</Link>
									</div>
								</li>
								<li className={`dexterio-wp__list-item ${activeLink === 'Electrical' ? "active" : ""}`}>
									<div className="dexterio-wp__list-item-text-wrapper">
										<svg  x="0px" y="0px" width="10px" height="10px" viewBox="0 0 20 20" enableBackground="new 0 0 10 10"><circle cx="10" cy="10" r="5"/></svg>
										<Link  to="electrical" spy={true} smooth={true} className="dexterio-wp__list-item-text dexterio-wp__top-level" onClick={()=>{setActiveLink('Electrical')}}>Electrical</Link>
									</div>
								</li>
								<li className={`dexterio-wp__list-item ${activeLink === 'Plumbing' ? "active" : ""}`}>
									<div className="dexterio-wp__list-item-text-wrapper">
										<svg  x="0px" y="0px" width="10px" height="10px" viewBox="0 0 20 20" enableBackground="new 0 0 10 10"><circle cx="10" cy="10" r="5"/></svg>
										<Link  to="plumbing" spy={true} smooth={true} className="dexterio-wp__list-item-text dexterio-wp__top-level" onClick={()=>{setActiveLink('Plumbing')}}>Plumbing</Link>
									</div>
								</li>
								<li className={`dexterio-wp__list-item ${activeLink === 'Home improvement solutions' ? "active" : ""}`}>
									<div className="dexterio-wp__list-item-text-wrapper">
										<svg  x="0px" y="0px" width="10px" height="10px" viewBox="0 0 20 20" enableBackground="new 0 0 10 10"><circle cx="10" cy="10" r="5"/></svg>
										<Link  to="home_improvement" spy={true} smooth={true} className="dexterio-wp__list-item-text dexterio-wp__top-level" onClick={()=>{setActiveLink('Home improvement solutions')}}>Home improvement solutions</Link>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</DesktopPane>
			<DesktopPane width="70%" padding="0px 0px 0px 90px">
				<>
					<div style={{"height":"15px"}}></div>
					<WarrantyPolicyHeading>Warranty Policy</WarrantyPolicyHeading>
					{
						wpData.data.map((item, index) => {
							return(
								<div id={item.id} key={index} style={{"paddingTop":"60px"}}>
									<div>
									<WarrantyPolicySubHeading>{item.key}</WarrantyPolicySubHeading>
									<WarrantyPolicySubHeading1>{item.preValues}</WarrantyPolicySubHeading1>
									<WarrantyPolicySubHeading1><strong>{item.valueHeader}</strong></WarrantyPolicySubHeading1>
									<WarrantyPolicySubHeading1>
										<ol style={{"listStyle":"number inside"}}>
											{
												item.value.map((inner, innerIndex) => {
													return(
														<li key={innerIndex}>
															{inner.pre}
															<ol style={{"listStyleType":"disc", "paddingLeft": "20px"}}>
																{
																	inner.midlist.length > 0 && inner.midlist.map((innerInner, innerInnerIndex) => {
																		return (<li key={innerInnerIndex}>{innerInner}</li>);
																	})
																}
															</ol>
															{inner.post.length > 0 && inner.post}
														</li>)
												})
											}
										</ol>
									</WarrantyPolicySubHeading1>
									<WarrantyPolicySubHeading1><strong>{item.postValueHeader}</strong></WarrantyPolicySubHeading1>
									<WarrantyPolicySubHeading1>
										<ol style={{"listStyle":"number inside"}}>
											{
												item.postValues.map((inner, innerIndex) => {
													return(
														<li key={innerIndex}>
															{inner.pre}
															<ol style={{"listStyleType":"disc", "paddingLeft": "20px"}}>
																{
																	inner.midlist.length > 0 && inner.midlist.map((innerInner, innerInnerIndex) => {
																		return (<li key={innerInnerIndex}>{innerInner}</li>);
																	})
																}
															</ol>
															{inner.post.length > 0 && inner.post}
														</li>)
												})
											}
										</ol>
									</WarrantyPolicySubHeading1>
									<WarrantyPolicySubscriptSubHeading>{item.postSubscript}</WarrantyPolicySubscriptSubHeading>
									</div>
								</div>
							)
						})
					}
				</>
			</DesktopPane>
			<MobilePane>
				<div style={{"height":"45px"}}></div>
				<WarrantyPolicyHeading>Terms &amp; Conditions</WarrantyPolicyHeading>
				<div style={{"height":"45px"}}></div>
					{
						wpData.data.map((item, index) => {

						return(	
							<Accordion key={index} expanded={expanded === index} onChange={handleChange(index)} square>
								<AccordionSummary expandIcon={<FaAngleDown/>} >
									<WarrantyPolicySubHeading>{item.key}</WarrantyPolicySubHeading>
								</AccordionSummary>
								<AccordionDetails>
									<div style={{"display":"flex", "flexDirection":"column"}}>
										<WarrantyPolicySubHeading1>{item.preValues}</WarrantyPolicySubHeading1>
										<WarrantyPolicySubHeading1><strong>{item.valueHeader}</strong></WarrantyPolicySubHeading1>
										<WarrantyPolicySubHeading1>
										<ol style={{"listStyle":"number inside"}}>
											{
												item.value.map((inner, innerIndex) => {
													return(
														<li key = {innerIndex}>
															{inner.pre}
															<ol style={{"listStyleType":"disc", "paddingLeft": "20px"}}>
																{
																	inner.midlist.length > 0 && inner.midlist.map((innerInner, innerInnerIndex) => {
																		return (<li key={innerInnerIndex}>{innerInner}</li>);
																	})
																}
															</ol>
															{inner.post.length > 0 && inner.post}
														</li>)
												})
											}
											</ol>
											</WarrantyPolicySubHeading1>
											<WarrantyPolicySubHeading1><strong>{item.postValueHeader}</strong></WarrantyPolicySubHeading1>
											<WarrantyPolicySubHeading1>
												<ol style={{"listStyle":"number inside"}}>
													{
														item.postValues.map((inner, innerIndex) => {
															return(
																<li key={innerIndex}>
																	{inner.pre}
																	<ol style={{"listStyleType":"disc", "paddingLeft": "20px"}}>
																		{
																			inner.midlist.length > 0 && inner.midlist.map((innerInner, innerInnerIndex) => {
																				return (<li key={innerInnerIndex}>{innerInner}</li>);
																			})
																		}
																	</ol>
																	{inner.post.length > 0 && inner.post}
																</li>)
														})
													}
												</ol>
											</WarrantyPolicySubHeading1>
											<WarrantyPolicySubscriptSubHeading>{item.postSubscript}</WarrantyPolicySubscriptSubHeading>
										</div>
									</AccordionDetails>
							</Accordion>
							)
						})
					}
			</MobilePane>
		</WarrantyPolicyPage>
		</>
    );
};

export default WarrantyPolicy;
