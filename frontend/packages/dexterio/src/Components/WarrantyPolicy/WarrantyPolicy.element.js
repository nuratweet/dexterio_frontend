import styled from 'styled-components';
import { FontSizeHeader2 , FontSizeSubHeader1 } from '../../globalStyles';
import { StaticPage } from '../Privacy/Privacy.element';

export const WarrantyPolicyPage = styled(StaticPage)`
    max-width: 1300px;
    position: relative;
    display: flex;
    flex-direction: row;

`;


export const DesktopPane = styled.div`
    display: none;
    @media(min-width: 1024px) {
        display: block;
        width: ${({width}) => width};
        padding: ${({padding}) => padding};
    }
`;

export const MobilePane = styled.div`
    display : none;
    @media(max-width: 1023px){
        display: block;
        width: 100%;
        padding: 10px;
    }
`;

export const WarrantyPolicyHeading = styled(FontSizeHeader2)`
    font-size: 36px;
    display: flex;
    justify-content: center;
`;


export const WarrantyPolicySubHeading = styled(FontSizeSubHeader1)`
    display: flex;
    justify-content: center;
    padding: 20px 0px;
    @media(max-width: 1023px){
        font-size: 16px;
        padding: 0px;
    }
`;

export const WarrantyPolicySubHeading1 = styled(FontSizeSubHeader1)`
    font-family: "Open Sans", sans-serif;
    font-size: 18px;
    font-weight: 600;
    line-height: 3.1em;

    @media(max-width: 1023px){
        font-family: "Open Sans", sans-serif;
        font-size: 17px;
        font-weight: 400;
        line-height: 1.85714285714286;
    }
`;

export const WarrantyPolicySubscriptSubHeading = styled(FontSizeSubHeader1)`

    color: #000707B5;
    font-family: "Open Sans", sans-serif;
    font-size: 15px;
    font-weight: 600;
    line-height: 1.7em;
    letter-spacing: 0.8px;

    @media(max-width: 1023px){
        font-family: "Open Sans", sans-serif;
        font-size: 14px;
        font-weight: 400;
        line-height: 1.3;
    }
`;