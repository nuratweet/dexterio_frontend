import React, {useState} from 'react'
import './DesignQuiz2.css';
import home from '../../assets/home.png';
import renovate from '../../assets/renovate.png';
import confused from '../../assets/confused.png';
import lroom from '../../assets/livingRoom2.png';
import DiningRoom from '../../assets/DiningRoom.png';
import Bedroom from '../../assets/BedRoom.png';
import FamilyRoom from '../../assets/FamilyRoom.png';
import Nursery from '../../assets/Nursery.png';
import Office from '../../assets/Office.png';
import Kitchen from '../../assets/Kitchen.png';
import Bathroom from '../../assets/BathRoom.png';
import OutdoorSpace from '../../assets/OutdoorSpace.png';
import Entryway from '../../assets/EntryWay.png';
import Playroom from '../../assets/PlayRoom.png';
import AllOptions from '../../assets/AllOptions.png';
import { Box , TextField } from '@material-ui/core';
import { MaterialUiPhoneNumber } from "common";
import { Link } from 'react-router-dom'

const rooms = [
    {
        image:lroom,
        text: "Living Room"
    },
    {
        image:DiningRoom,
        text: "Dining Room"
    },
    {
        image:Bedroom,
        text: "Bed Room"
    },
    {
        image:FamilyRoom,
        text: "Family Room"
    },
    {
        image:Nursery,
        text: "Nursery"
    },
    {
        image:Office,
        text: "Office"
    },
    {
        image:Kitchen,
        text: "Kitchen"
    },
    {
        image:Bathroom,
        text: "Bathroom"
    },
    {
        image:OutdoorSpace,
        text: "Outdoor Space"
    },
    {
        image:Entryway,
        text: "Entryway"
    },
    {
        image:Playroom,
        text: "Play Room"
    },
    {
        image:AllOptions,
        text: "All Options"
    }

]

const designGrid = [
    [
        { 
            image: "https://s3.amazonaws.com/static.havenly.com/style-quiz-images/e36296d8-791f-491c-b452-fda65378f4a2.jpg",
            detail: "Conventional"
        },
        { 
            image: "https://s3.amazonaws.com/static.havenly.com/style-quiz-images/391dd983-7b7c-46f6-b6d5-300fa9afba0b.jpg",            
            detail: "Royal"
        },
        { 
            image: "https://s3.amazonaws.com/static.havenly.com/style-quiz-images/cd312296-6a92-4d99-835a-6868fbb2d9c8.jpg",
            detail: "Rajasthani"
        }
    ],[
        { 
            image: "https://s3.amazonaws.com/static.havenly.com/style-quiz-images/71b8b34e-1c1f-47fd-a086-ebf6a2fa11f5.jpg",
            detail: "Contemporary"
        },
        { 
            image: "https://s3.amazonaws.com/static.havenly.com/style-quiz-images/51ef90f4-8d77-4d11-a7fa-13a96a163067.jpg",
            detail: "Italian"
        },
        { 
            image: "https://s3.amazonaws.com/static.havenly.com/style-quiz-images/54b4b3e2-c862-4bc2-9071-49cacdfb7b40.jpg",
            detail: "Spanish"
        },
    ],[
        { 
            image: "https://s3.amazonaws.com/static.havenly.com/style-quiz-images/8e39d9ef-009a-4282-99cd-c70b56f8a913.jpg",
            detail: "Modern"
        },
        { 
            image: "https://s3.amazonaws.com/static.havenly.com/style-quiz-images/80cd484b-1b76-4c35-83b7-fb24d867f357.jpg",
            detail: "Chic"
        },
        { 
            image: "https://s3.amazonaws.com/static.havenly.com/style-quiz-images/528600ae-3f19-41c1-a78c-326ab89091d9.jpg",
            detail: "Absfdsgfdsg"
        }
    ]
]



function DesignQuiz2() {

    const [step, setStep] = useState(0);
    const [designState, setDesignState] = useState([]);
    const [roomState, setRoomState] = useState([]);
    const [clientStatus, setClientStatus] = useState("-1");
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [phone_number, setPhone] = useState("");
    const [error, setError] = useState(false);
    const [errorMessage, setErrorMessage] = useState("");

    const userDesignQuizCompletionState = ['translateX(16.6666%)', 'translateX(33.3332%)', 'translateX(50%)', 'translateX(66.6664%)', 'translateX(83.333%)', 'translateX(100%)'];
    const handleOnBlur = e => {
        let temp = e.target.value.replace("-","");
        temp = temp.replace(" ","");
        if (temp.length === 13) {
          setPhone(temp);
          setError(false);
          setErrorMessage("");
        } else {
          setError(true);
          setErrorMessage("Enter a valid phone number");
        }
      };
    
      const handleOnChange = value => {
        if (value.length === 13) {
          setError(false);
          setErrorMessage("");
        }
      };

    const handleDesignState = (arg) => {
        if (designState.includes(arg)) {
            var filtered = designState.filter(function(value, index, arr){ 
                return value !== arg;
            });

            setDesignState(filtered);
        } else {
            setDesignState([...designState, arg]);
        }
    }

    const handleRoomState = (arg) => {
        if (roomState.includes(arg)) {
            var filtered = roomState.filter(function(value, index, arr){ 
                return value !== arg;
            });

            setRoomState(filtered);
        } else {
            setRoomState([...roomState, arg]);
        }
    }


    const handleNext = (arg) => {
        setStep(arg);
    }


    return (
        <div className="_1GjESRq1rrvYHIYbAhhSaN">
            <div className={`_1m9bX7Zk23nTC7WgjGMHgl  ${step === 2 ? '_2NFAOdTlk1kVC0TaZ6N4G0' : ''} P4LgfjO-gQ4NC-eWV91wE`}>
                <div className="PATMCDStWMjK9OFKV2VXf _3pnd1OwRhVOLCz1-1C2Tz5">
                    <div className="BvXHMKaQegdwbQn4rqAMA" style={{"transform": userDesignQuizCompletionState[step]}}></div>
                </div>
                { step === 0 && 
                    (
                        <>
                            <div className="_1gv1Tk95AHOwjL115PTaO8">
                                <div className="_222TiOjpoo6mB1rUbWBeCp">
                                    <div className="undefined _3La2U282lPrSPieeityLah">
                                        <h3 className="_3sh4L7sP21DgNDoTCZcESC _1M8DfGn-ojI1PhFibM5aPx">What's your current situation?</h3>
                                    </div>
                                </div>
                                <div className="_3E6rG_Tt_qwLpsNghhXmVH">
                                    <ul className="zeMpUVtl5WAqLJGOW0qQp">
                                        <li className="zGpHBqm-PBif-8Uo_jgXM">
                                            <button className={`_3sRbR0H0E42xSXu0NBw6Nr ${clientStatus === 'Renovating' ? "active" : ""}`} data-automated-test="styleQuizChoiceBubble" type="button" onClick={() => setClientStatus('Renovating')}>
                                                <div className="_31Y8Yerqz3zfZLhcJ97B4U_step1">
                                                    <img alt="" className="_2KqV1hEn7H5083I2ENJ0qp" src={home}/>
                                                </div>
                                                <h3 className="nzFnMBBp9iqvVHz0LiJFr">Renovating</h3>
                                                <div className="_3OScaUBT4hjCuz8CQDhceC"><svg width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M13.508 1L6.752 13.011l-4.983-4.27L1 9.638l6.076 5.208L14.538 1.58z"></path>
                                                    </svg>
                                                </div>
                                            </button></li>
                                        <li className="zGpHBqm-PBif-8Uo_jgXM">
                                            <button className= {`_3sRbR0H0E42xSXu0NBw6Nr ${clientStatus === 'Bought a new Home' ? "active" : ""}`} data-automated-test="styleQuizChoiceBubble" type="button" onClick={() => setClientStatus('Bought a new Home')}>
                                                <div className="_31Y8Yerqz3zfZLhcJ97B4U_step1">
                                                    <img alt="" className="_2KqV1hEn7H5083I2ENJ0qp" src={renovate}/>
                                                </div>
                                                <h3 className="nzFnMBBp9iqvVHz0LiJFr">Bought a new Home</h3>
                                                <div className="_4L4i5yJMobACiAuOQEAIG _3OScaUBT4hjCuz8CQDhceC"><svg width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M13.508 1L6.752 13.011l-4.983-4.27L1 9.638l6.076 5.208L14.538 1.58z"></path>
                                                    </svg>
                                                </div>
                                            </button>
                                        </li>
                                        <li className="zGpHBqm-PBif-8Uo_jgXM">
                                            <button className={`_3sRbR0H0E42xSXu0NBw6Nr ${clientStatus === 'Not Sure yet' ? "active" : ""}`} data-automated-test="styleQuizChoiceBubble" type="button" onClick={() => setClientStatus('Not Sure yet')}>
                                                <div className="_31Y8Yerqz3zfZLhcJ97B4U_step1">
                                                    <img alt="" className="_2KqV1hEn7H5083I2ENJ0qp" src={confused}/>
                                                </div>
                                                <h3 className="nzFnMBBp9iqvVHz0LiJFr">Not Sure yet</h3>
                                                <div className="_3OScaUBT4hjCuz8CQDhceC">
                                                    <svg width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M13.508 1L6.752 13.011l-4.983-4.27L1 9.638l6.076 5.208L14.538 1.58z"></path>
                                                    </svg>
                                                </div>
                                            </button>
                                        </li>
                                    </ul>
                                    <div className="_1jqGzefjYSP-8u0Kwm08th _30sj_LV80UWzMsQRAUUANg">
                                        <button className="_2ieOSiz9QMnofmbzDKXxKc undefined _1AnEq6OSaO-7akm0dmfr30" disabled={`${clientStatus === "-1" ? "disabled" : ""}`} type="submit" onClick = {() => handleNext(1)}>Next</button></div>
                                </div>
                            </div>
                        </>
                    )
                }
                { step === 1 && 
                    (
                        <>
                            <div className="_1gv1Tk95AHOwjL115PTaO8">
                                <div className="_222TiOjpoo6mB1rUbWBeCp _3BdAWUGNWf8eDYGy7AxPK9">
                                    <div className="undefined _3La2U282lPrSPieeityLah">
                                        <h3 className="_3sh4L7sP21DgNDoTCZcESC _1M8DfGn-ojI1PhFibM5aPx">Select the rooms that make you swoon.</h3>
                                        <p className="_3sh4L7sP21DgNDoTCZcESC _1ox8cK5ZbRlLbM-gmkplG9">Decisions are hard. Pick as many as you want.</p>
                                        <div className="_3sh4L7sP21DgNDoTCZcESC _1by4cSjkdIOm2Yq5Fo24fe">
                                            <p>Already a member?</p><Link to="/login">Log in</Link>
                                        </div>
                                    </div>
                                </div>
                                <div className="_3E6rG_Tt_qwLpsNghhXmVH _3BdAWUGNWf8eDYGy7AxPK9">
                                    <div className="_1GL58ajZRXWF3gPslo0bAg">
                                        {
                                            designGrid.map((item) => {
                                                return (
                                                    <div className="_340a319TqMOr5Vpy5ABfFj">
                                                        <div>
                                                            {
                                                            item.map((innerItem) => {
                                                                return (
                                                                    <div className="_2O19xYRsBU6qAl-QPWI5O_">
                                                                        <button className={`_2N6Vxyrj2bGonm0c-7IbRB ${designState.includes(innerItem.detail) ? "active" : ""}`} data-automated-test="styleQuizImage" type="button" onClick={()=> {handleDesignState(innerItem.detail)}}>
                                                                            <img alt="" className="_2zH219UNsPmRAaFF1TNQA2" src={innerItem.image}/>
                                                                            <div className={`_2HSFNWHPAHJsI1ygOy3m2h _3OScaUBT4hjCuz8CQDhceC ${designState.includes(innerItem.detail) ? "active" : ""}`}>
                                                                                <svg width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
                                                                                    <path d="M13.508 1L6.752 13.011l-4.983-4.27L1 9.638l6.076 5.208L14.538 1.58z"></path>
                                                                                </svg>
                                                                            </div>
                                                                        </button>
                                                                    </div>                                                                    );
                                                                })
                                                            }
                                                        </div>
                                                    </div>
                                                )
                                            })
                                        }
                                    </div>
                                    <div className="_1FoNf0NT_554i8jrNCvOwd">
                                        <div className="_1jqGzefjYSP-8u0Kwm08th _30sj_LV80UWzMsQRAUUANg">
                                            <button className="_2ieOSiz9QMnofmbzDKXxKc undefined " disabled={`${designState.length > 0 ? "" : "disabled"}`} type="button" onClick = {() => handleNext(2)}>Next</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </>
                    )
                }
                { step === 2 && 
                    (
                        <>
                            <div className="_1gv1Tk95AHOwjL115PTaO8">
                                <div className="_222TiOjpoo6mB1rUbWBeCp">
                                    <div className="undefined _3La2U282lPrSPieeityLah">
                                        <h3 className="_3sh4L7sP21DgNDoTCZcESC _1M8DfGn-ojI1PhFibM5aPx">What should we call you, stranger?</h3>
                                        <p className="_3sh4L7sP21DgNDoTCZcESC _1ox8cK5ZbRlLbM-gmkplG9">Enough about us. Let’s talk about you.</p>
                                        <div className="_3sh4L7sP21DgNDoTCZcESC _1by4cSjkdIOm2Yq5Fo24fe">
                                            <p>Already a member?</p><Link href="/login">Log in</Link>
                                        </div>
                                    </div>
                                </div>
                                <div className="_3E6rG_Tt_qwLpsNghhXmVH">
                                    <form className="_2dUT8dqiuqx66XBgCjleMy" formnovalidate="" novalidate="">
                                        <Box component="div" sx={{'& .MuiTextField-root': { m: 1, width: '100%' },}} noValidate >
                                            <TextField
                                                variant="standard"
                                                margin="normal"
                                                size="small"
                                                fullWidth
                                                id="name"
                                                name="name"
                                                autoComplete="name"
                                                value={name}
                                                placeholder='type your name'
                                                onChange={(event) => setName(event.target.value)}
                                                inputProps={{style: {fontSize: 26}}}
                                                />
                                            </Box>

                                        <div className="_1jqGzefjYSP-8u0Kwm08th _2wyyxby8i9nYV9d8Hf2YUb _30sj_LV80UWzMsQRAUUANg">
                                            <button className="_2ieOSiz9QMnofmbzDKXxKc undefined " type="submit">Next</button>
                                        </div>
                                    </form>
                                    <div className="_1FoNf0NT_554i8jrNCvOwd">
                                        <div className="_1jqGzefjYSP-8u0Kwm08th _30sj_LV80UWzMsQRAUUANg">
                                            <button className="_2ieOSiz9QMnofmbzDKXxKc undefined " disabled={`${name.length > 0 ? "" : "disabled"}`} type="button" onClick = {() => handleNext(3)}>Next</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </>
                    )
                }
                { step === 3 && 
                    (
                        <>
                            <div className="_1gv1Tk95AHOwjL115PTaO8">
                                <div className="_222TiOjpoo6mB1rUbWBeCp">
                                    <div className="undefined _3La2U282lPrSPieeityLah">
                                        <h3 className="_3sh4L7sP21DgNDoTCZcESC _1M8DfGn-ojI1PhFibM5aPx">Which rooms are on your “It needs a little something” list?</h3>
                                        <p className="_3sh4L7sP21DgNDoTCZcESC _1ox8cK5ZbRlLbM-gmkplG9">Select as many as you like.</p>
                                    </div>
                                </div>
                                <div className="_3E6rG_Tt_qwLpsNghhXmVH">
                                    <ul className="_1LdlmQ-mgYNj78LZIHqEW">
                                        {
                                            rooms.map((item, index) => {
                                                return (
                                                        <li className="_1r6WkFwM0On9gRLpE7kr8R">
                                                            <button className={`_3sRbR0H0E42xSXu0NBw6Nr ${roomState.includes(item.text) ? "active" : ""}`} data-automated-test="styleQuizChoiceBubble" type="button" onClick={()=> {handleRoomState(item.text)}}>
                                                                <div className="_31Y8Yerqz3zfZLhcJ97B4U">
                                                                    <img alt="" className="_2KqV1hEn7H5083I2ENJ0qp" src={item.image}/>
                                                                </div>
                                                                <h3 className="nzFnMBBp9iqvVHz0LiJFr">{item.text}</h3>
                                                                <div className={`_3OScaUBT4hjCuz8CQDhceC ${roomState.includes(item.text) ? "active" : ""}`}>
                                                                    <svg width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
                                                                        <path d="M13.508 1L6.752 13.011l-4.983-4.27L1 9.638l6.076 5.208L14.538 1.58z"></path>
                                                                    </svg>
                                                                </div>
                                                            </button>
                                                        </li>
                                                    );
                                                })
                                            }
                                    </ul>
                                    <div className="_1jqGzefjYSP-8u0Kwm08th _30sj_LV80UWzMsQRAUUANg">
                                        <button className="_2ieOSiz9QMnofmbzDKXxKc undefined " disabled={`${roomState.length > 0 ? "" : "disabled"}`} type="button" onClick = {() => handleNext(4)}>Next</button>
                                    </div>
                                </div>
                            </div>
                        </>
                    )
                }
                { step === 4 && 
                    (
                        <>
                            <div className="_1gv1Tk95AHOwjL115PTaO8">
                                <div className="_222TiOjpoo6mB1rUbWBeCp">
                                    <div className="undefined _3La2U282lPrSPieeityLah">
                                        <h3 className="_3sh4L7sP21DgNDoTCZcESC _1M8DfGn-ojI1PhFibM5aPx">What’s your email and phone?</h3>
                                        <p className="_3sh4L7sP21DgNDoTCZcESC _1ox8cK5ZbRlLbM-gmkplG9">Please provide your email and phone to save your results and edit your profile.</p>
                                        <div className="_3sh4L7sP21DgNDoTCZcESC _1by4cSjkdIOm2Yq5Fo24fe">
                                            <p>Already a member?</p><Link href="/login">Log in</Link>
                                        </div>
                                    </div>
                                </div>
                                <div className="_3E6rG_Tt_qwLpsNghhXmVH">
                                    <form className="_2dUT8dqiuqx66XBgCjleMy" formnovalidate="" novalidate="">
                                        <Box component="div" sx={{'& .MuiTextField-root': { m: 1, width: '100%' },}} noValidate >
                                            <TextField
                                                variant="standard"
                                                margin="normal"
                                                fullWidth
                                                id="email"
                                                name="email"
                                                autoComplete="email"
                                                value={email}
                                                placeholder='type your email'
                                                onChange={(event) => setEmail(event.target.value)}
                                                inputProps={{style: {fontSize: 26}}}
                                            />
                                            <MaterialUiPhoneNumber
                                                variant="standard"
                                                margin="normal"
                                                value={phone_number}
                                                defaultCountry={"in"}
                                                onBlur={handleOnBlur}
                                                onChange={handleOnChange}
                                                fullWidth
                                                placeholder='type your phone number'
                                                error={error}
                                                helperText={errorMessage}
                                                inputProps={{style: {fontSize: 26}}}
                                            />
                                        </Box>
                                        <div className="_1jqGzefjYSP-8u0Kwm08th _2wyyxby8i9nYV9d8Hf2YUb _30sj_LV80UWzMsQRAUUANg">
                                            <button className="_2ieOSiz9QMnofmbzDKXxKc undefined " type="submit">Next</button>
                                        </div>
                                    </form>
                                    <div className="_1FoNf0NT_554i8jrNCvOwd">
                                        <div className="_1jqGzefjYSP-8u0Kwm08th _30sj_LV80UWzMsQRAUUANg">
                                            <button className="_2ieOSiz9QMnofmbzDKXxKc undefined " disabled={`${name.length > 0 ? "" : "disabled"}`} type="button" onClick = {() => handleNext(5)}>Next</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </>
                    )
                }
                { step === 5 && 
                    (
                        <>
                            <div className="_1gv1Tk95AHOwjL115PTaO8">
                                <div className="_222TiOjpoo6mB1rUbWBeCp">
                                    <div className="undefined _3La2U282lPrSPieeityLah">
                                        <h3 className="_3sh4L7sP21DgNDoTCZcESC _1M8DfGn-ojI1PhFibM5aPx">What’s your email and phone?</h3>
                                        <p className="_3sh4L7sP21DgNDoTCZcESC _1ox8cK5ZbRlLbM-gmkplG9">Please provide your email and phone to save your results and edit your profile.</p>
                                        <div className="_3sh4L7sP21DgNDoTCZcESC _1by4cSjkdIOm2Yq5Fo24fe">
                                            <p>Already a member?</p><Link href="/login">Log in</Link>
                                        </div>
                                    </div>
                                </div>
                                <div className="_3E6rG_Tt_qwLpsNghhXmVH">
                                    <form className="_2dUT8dqiuqx66XBgCjleMy" formnovalidate="" novalidate="">
                                        <Box component="div" sx={{'& .MuiTextField-root': { m: 1, width: '100%' },}} noValidate >
                                            <TextField
                                                variant="standard"
                                                margin="normal"
                                                fullWidth
                                                id="email"
                                                name="email"
                                                autoComplete="email"
                                                value={email}
                                                placeholder='type your email'
                                                onChange={(event) => setEmail(event.target.value)}
                                                inputProps={{style: {fontSize: 26}}}
                                            />
                                            <MaterialUiPhoneNumber
                                                variant="standard"
                                                margin="normal"
                                                value={phone_number}
                                                defaultCountry={"in"}
                                                onBlur={handleOnBlur}
                                                onChange={handleOnChange}
                                                fullWidth
                                                placeholder='type your phone number'
                                                error={error}
                                                helperText={errorMessage}
                                                inputProps={{style: {fontSize: 26}}}
                                            />
                                        </Box>
                                        <div className="_1jqGzefjYSP-8u0Kwm08th _2wyyxby8i9nYV9d8Hf2YUb _30sj_LV80UWzMsQRAUUANg">
                                            <button className="_2ieOSiz9QMnofmbzDKXxKc undefined " type="submit">Next</button>
                                        </div>
                                    </form>
                                    <div className="_1FoNf0NT_554i8jrNCvOwd">
                                        <div className="_1jqGzefjYSP-8u0Kwm08th _30sj_LV80UWzMsQRAUUANg">
                                            <button className="_2ieOSiz9QMnofmbzDKXxKc undefined " disabled={`${name.length > 0 ? "" : "disabled"}`} type="button" onClick = {() => handleNext(5)}>Next</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </>
                    )
                }
            </div>
        </div>
    )
}

export default DesignQuiz2;
