import React , {Fragment, useState} from 'react';
import home from '../../assets/home.png';
import renovate from '../../assets/renovate.png';
import confused from '../../assets/confused.png';
import lroom from '../../assets/livingRoom2.png';
import DiningRoom from '../../assets/DiningRoom.png';
import Bedroom from '../../assets/BedRoom.png';
import FamilyRoom from '../../assets/FamilyRoom.png';
import Nursery from '../../assets/Nursery.png';
import Office from '../../assets/Office.png';
import Kitchen from '../../assets/Kitchen.png';
import Bathroom from '../../assets/BathRoom.png';
import OutdoorSpace from '../../assets/OutdoorSpace.png';
import Entryway from '../../assets/EntryWay.png';
import Playroom from '../../assets/PlayRoom.png';
import AllOptions from '../../assets/AllOptions.png';
import styled1 from '/styled1.jpg';
import styled3 from '/styled3.jpg';
import styled4 from '/styled4.jpg';
import styled5 from '/styled5.jpg';
import styled6 from '/styled6.jpg';
import styled7 from '/styled7.jpg';
import styled8 from '/styled8.jpg';
import styled9 from '/styled9.jpg';
import styled10 from '../../assets/Design-Quiz-1.png';
import { Container } from '../../globalStyles';
import { MaterialUiPhoneNumber } from "common";
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box';
import { Link } from 'react-router-dom';



import './DesignQuiz.css';
import { Button } from '@material-ui/core';

const designStyles = [
    {
        image: styled1,
        text: "styled1"
    },
    {
        image: styled3,
        text: "styled3"
    },
    {
        image: styled4,
        text: "styled4"
    },
    {
        image: styled5,
        text: "styled5"
    },
    {
        image: styled6,
        text: "styled6"
    },
    {
        image: styled7,
        text: "styled7"
    },
    {
        image: styled8,
        text: "styled8"
    },
    {
        image: styled9,
        text: "styled9"
    },
    {
        image: styled10,
        text: "styled10"
    }
]

const rooms = [
    {
        image:lroom,
        text: "Living Room"
    },
    {
        image:DiningRoom,
        text: "Dining Room"
    },
    {
        image:Bedroom,
        text: "Bed Room"
    },
    {
        image:FamilyRoom,
        text: "Family Room"
    },
    {
        image:Nursery,
        text: "Nursery"
    },
    {
        image:Office,
        text: "Office"
    },
    {
        image:Kitchen,
        text: "Kitchen"
    },
    {
        image:Bathroom,
        text: "Bathroom"
    },
    {
        image:OutdoorSpace,
        text: "Outdoor Space"
    },
    {
        image:Entryway,
        text: "Entryway"
    },
    {
        image:Playroom,
        text: "Play Room"
    },
    {
        image:AllOptions,
        text: "All Options"
    }

]

function Copyright() {
    return (
        <>
            <Typography variant="body2" color="textSecondary" align="center">
                {'Already a User '}
                <Link color="inherit" href="/login">
                    Click Here
                </Link>{' '}
                {'.'}
            </Typography>
            <br/>
            <Typography variant="body2" color="textSecondary" align="center">
                {'By submitting this form, you agree to the '}
                <Link color="inherit" href="/privacy">
                    privacy policy
                </Link>{' & '}
                <Link color="inherit" href="/terms-conditions">
                    terms and conditions
                </Link>
            </Typography>
        </>
    );
}

function DesignQuiz(){

    const [clientStatus, setClientStatus] = useState(-1);
    const [roomState, setRoomState] = useState(
        new Array(rooms.length).fill(false)
    );

    const [designState, setDesignState] = useState(
        new Array(designStyles.length).fill(false)
    );


    
    const handleRoomOnChange = (position) => {
        const updatedCheckedState = roomState.map((item, index) =>
          index === position ? !item : item
        );
    
        setRoomState(updatedCheckedState);
        console.log(updatedCheckedState);
    };

    const handleDesignOnChange = (position) => {
        const updatedCheckedState = designState.map((item, index) =>
          index === position ? !item : item
        );
    
        setDesignState(updatedCheckedState);
    };
    
    const handleNext = (arg) => {
        setStep(arg);
    }

    const [step, setStep] = useState(0);

    const [username, setUsername] = useState("");
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    // eslint-disable-next-line no-unused-vars
    const [authenticationCode, setAuthenticationCode] = useState("");
    // eslint-disable-next-line no-unused-vars
    const [errors, setErrors] = useState({cognito: null,blankfield: false,passwordmatch: false});
    const [phone_number, setPhone] = useState("");
    const [error, setError] = useState(false);
    const [errorMessage, setErrorMessage] = useState("");
    
    async function confirmSignup(event){
        event.preventDefault();

    };

    async function handleSubmit(event) {
        event.preventDefault();

    };

    const handleOnBlur = e => {
        let temp = e.target.value.replace("-","");
        temp = temp.replace(" ","");
        if (temp.length === 13) {
          setPhone(temp);
          setError(false);
          setErrorMessage("");
        } else {
          setError(true);
          setErrorMessage("Enter a valid phone number");
        }
      };
    
      const handleOnChange = value => {
        if (value.length === 13) {
          setError(false);
          setErrorMessage("");
        }
      };
    
    return (
        <section className = "parent">
            <Container>
                <div className = "inner-wrapper2">
                    <div className = "form-wrapper">
                        { step === 0 && 
                            (
                                <>
                                    <div className = "child-1" id ="1">
                                        <div className = "page-1-wrapper">
                                            <label for = "form-field" style = {{ fontFamily: "sans-serif", fontSize: "26px",fontWeight: "500",lineHeight: "1.5em"}}>
                                                <h2 style = {{ color: "#01070B", fontFamily: "Poppins", fontSize: "33px",fontWeight: "500"}}>
                                                    What's your current situation?
                                                </h2>
                                            </label>
                                            <div className = "reno-images-wrapper" style = {{display:"flex"}}>
                                                <span className= {`reno-image ${clientStatus === '0' ? "active" : ""}`}>
                                                    <label for = "reno-image-label" onClick={() => setClientStatus('0')}>
                                                        <input type = "radio" value = "0" className = "radio-style" checked={clientStatus === '0'} />
                                                        <img alt="" src ={home} style = {{cursor: "pointer", width: "150px", margin: "40px"}} /><br/>Renovating
                                                    </label>
                                                </span>
                                                <span className= {`reno-image ${clientStatus === '1' ? "active" : ""}`}>
                                                    <label for = "reno-image-label" onClick={() => setClientStatus('1')}>
                                                        <input type = "radio" value = "1" className = "radio-style" checked={clientStatus === '1'} />
                                                        <img alt="" src ={renovate} style = {{cursor: "pointer",width: "150px", margin: "40px"}} /><br/>Bought a new home
                                                    </label>
                                                </span>
                                                <span className= {`reno-image ${clientStatus === '2' ? "active" : ""}`}>
                                                    <label for = "reno-image-label" onClick={() => setClientStatus('2')}>
                                                        <input type = "radio" value = "2" className = "radio-style" checked={clientStatus === '2'} />
                                                        <img alt="" src ={confused} style = {{cursor: "pointer",width: "150px", margin: "40px"}} /><br/>Not Sure yet
                                                    </label>
                                                </span>
                                            </div>
                                        </div>    
                                    </div>
                                    <div className = "button-wrapper">
                                        <div className = "outer-button-wrapper" id = "4">
                                            <button type = "submit" class = "next" onClick = {() => handleNext(1)}>Next</button>
                                        </div>                                        
                                    </div>
                                </>
                            )
                        }
                        { step === 1 && 
                            (
                                <>
                                    <div className= "child-2" id = "2">
                                        <div class = "text-field-wrapper">
                                            <label className = "label-text">
                                                What should we call you, stranger?
                                            </label>
                                            <Box component="div" sx={{'& .MuiTextField-root': { m: 1, width: '50%' },}} noValidate >
                                            <TextField
                                                variant="outlined"
                                                margin="normal"
                                                required
                                                fullWidth
                                                id="name"
                                                label="Name"
                                                name="name"
                                                autoComplete="name"
                                                value={name}
                                                onChange={(event) => setName(event.target.value)}
                                                />
                                            </Box>
                                        </div>
                                        <div className = "button-wrapper">
                                            <div className = "prev-wrapper">
                                                <button type = "submit" class = "prev" onClick = {() => handleNext(0)}>Previous</button>
                                            </div>
                                            <div className = "next-wrapper" id = "3">
                                                <button type = "submit" class = "next" onClick = {() => handleNext(2)}>Next</button>
                                            </div>
                                        </div>
                                    </div>
                                </>   
                            )
                        }
                        { step === 2 && 
                            (
                            <>
                                <div className = "child-3" id = "5">
                                    <div className = "child-3-wrapper">
                                        <label className ="furnished-options">
                                            <h2 style = {{
                                                color: "#01070B",
                                                fontFamily: "sans-serif",
                                                fontSize: "33px",
                                                fontWeight: "500"}}>Which rooms you want furnished
                                            </h2>
                                        </label>
                                        <div className = "room-type-wrapper">
                                            {
                                                rooms.map((item, index) => {
                                                    return (
                                                    <span className= {`check-wrapper ${roomState[index] ? "active" : ""}`} key = {index}>
                                                        <label className = "room-image-wrapper">
                                                            <input className = "hidden-checkbox" type="checkbox" id={`room-checkbox-${index}`}  name={item.text} checked={roomState[index]} onChange={() => handleRoomOnChange(index)} />
                                                            <img alt="" src = {item.image} style  = {{"cursor":"pointer", "width":"200px"}}/><br/>{item.text}
                                                        </label>
                                                    </span>
                                                    );
                                                })
                                            }
                                        </div>
                                        <div className = "button-wrapper">
                                            <div className = "prev-wrapper">
                                                <button type = "submit" class = "prev" onClick = {() => handleNext(1)}>Previous</button>
                                            </div>
                                            <div className = "next-wrapper" id = "3">
                                                <button type = "submit" class = "next" onClick = {() => handleNext(3)}>Next</button>
                                            </div>    
                                        </div>
                                    </div>
                                </div>
                            </>   
                            )
                        }
                        {  step === 3 && 
                            (
                                <div className = "child-4" id ="6">
                                    <div className = "image-option-wrapper">
                                        <label className = "image-selector">
                                            <h2 style = {{fontFamily:"Roboto", fontSize: "35px", lineHeight: "42px"}}>Choose Designs that fit your style</h2>
                                        </label>
                                        <div className = "images-wrap">
                                        {
                                            designStyles.map((item, index) => {
                                                return (
                                                <span className= "design-images" key = {index}>
                                                    <label className = "designs">
                                                        <input type="checkbox" id={`design-checkbox-${index}`}  name={item.text} checked={designState[index]} onChange={() => handleDesignOnChange(index)} />
                                                        <img alt="" src = {item.image} style  = {{"cursor":"pointer", "width":"200px"}}/><br/>
                                                    </label>
                                                </span>
                                                );
                                            })

                                        }
                                        </div>
                                    </div>
                                    <div className = "button-wrapper">
                                        <div className = "prev-wrapper">
                                            <button type = "submit" class = "prev"  onClick = {() => handleNext(2)}>Previous</button>
                                        </div>
                                        <div className = "next-wrapper" id = "3">
                                            <button type = "submit" class = "next" onClick = {() => handleNext(4)}>Next</button>
                                        </div>    
                                    </div>
                                </div>
                            )
                        }
                        {  step === 4 && 
                            (
                                <div id = "7" className = "child-5">
                                    <div  className ="photo-wrapper">
                                        <label className = "pic-label">Upload some photos of your home</label>
                                        <input type = "file" multiple = "multiple"/>
                                    </div>
                                    <div className = "floor-wrapper">
                                        <label className = "plan">Do you have a floor plan</label>
                                        <input type = "file" multiple = "multiple"/>
                                    </div>
                                    <div className = "button-wrapper">
                                        <div className = "prev-wrapper">
                                            <button type = "submit" class = "prev" onClick = {() => handleNext(3)}>Previous</button>
                                        </div>
                                        <div className = "next-wrapper" id = "3">
                                            <button type = "submit" class = "next" onClick = {() => handleNext(5)}>Next</button>
                                        </div>    
                                    </div>
                                </div> 
                            )
                        }
                        {  step === 5 && 
                            (
                                <form onSubmit={handleSubmit} noValidate>
                                    <TextField
                                    variant="outlined"
                                    margin="normal"
                                    required
                                    fullWidth
                                    id="username"
                                    label="User Name"
                                    name="username"
                                    autoComplete="username"
                                    value={username}
                                    onChange={(event) => setUsername(event.target.value)}
                                    />
                                    <TextField
                                    variant="outlined"
                                    margin="normal"
                                    required
                                    fullWidth
                                    id="email"
                                    label="Email Address"
                                    name="email"
                                    autoComplete="email"
                                    value={email}
                                    onChange={(event) => setEmail(event.target.value)}
                                    />
                                    <MaterialUiPhoneNumber
                                    variant="outlined"
                                    margin="normal"
                                    value={phone_number}
                                    defaultCountry={"in"}
                                    onBlur={handleOnBlur}
                                    onChange={handleOnChange}
                                    label="Phone Number"
                                    fullWidth
                                    error={error}
                                    helperText={errorMessage}
                                    />
                                    <Button
                                    type="submit"
                                    fullWidth
                                    variant="contained"
                                    color="primary"
                                    >
                                    Sign In
                                    </Button>
                                    <Box mt={5}>
                                    <Copyright />
                                    </Box>
                                </form>
                            )
                        }
                        { step === 6 && 
                            (
                                <form onSubmit={confirmSignup} noValidate>
                                    <TextField
                                    variant="outlined"
                                    margin="normal"
                                    required
                                    fullWidth
                                    id="username"
                                    label="User Name"
                                    name="username"
                                    autoComplete="username"
                                    onChange={(event) => setUsername(event.target.value)}
                                    value={username}
                                    autoFocus
                                    />
                                    <TextField
                                    variant="outlined"
                                    margin="normal"
                                    required
                                    fullWidth
                                    id="authenticationCode"
                                    label="Please check your email for authetication Code"
                                    name="authenticationCode"
                                    onChange={(event) => setAuthenticationCode(event.target.value)}
                                    autoFocus
                                    />
                                    <Button
                                    type="submit"
                                    fullWidth
                                    variant="contained"
                                    color="primary"
                                    >
                                    Sign In
                                    </Button>
                                    <Box mt={5}>
                                    <Copyright />
                                    </Box>
                                </form>
                            )
                        }
                    </div>       
                </div>    
            </Container>    
        </section>
    );
};

export default DesignQuiz;