import React from 'react';
import './style.css';

const BlogCard = (props) => {
  return(
    <div className="card" style={{ width: props.width ? props.width: '100%' }} {...props}>
        {props.children}
    </div>
   )

 }

export default BlogCard;