import React from "react";
import {NavList, NavWrapper, NavListWrapper, NavListItem, NavLink, BlogContainer, BlogMainContainer} from './BlogHome2.element';
import RecentPosts from './RecentPosts';
import Layout from './Layout';

function BlogHome2() {
  return (
    <>
      <NavWrapper>
        <NavListWrapper>
          <NavList>
            <NavListItem>
              <NavLink to="/decorating/">
                <span>View All</span>
              </NavLink>
            </NavListItem>
            <NavListItem>
              <NavLink to="/decorating/">
                <span>Bathroom</span>
              </NavLink>
            </NavListItem>
            <NavListItem>
              <NavLink to="/decorating/">
                <span>Bedroom</span>
              </NavLink>
            </NavListItem>
            <NavListItem>
              <NavLink to="/decorating/">
                <span>Homes</span>
              </NavLink>
            </NavListItem>
            <NavListItem>
              <NavLink to="/decorating/">
                <span>Kitchen</span>
              </NavLink>
            </NavListItem>
            <NavListItem>
              <NavLink to="/decorating/">
                <span>Living Room</span>
              </NavLink>
            </NavListItem>
            <NavListItem>
              <NavLink to="/decorating/">
                <span>Lookbook</span>
              </NavLink>
            </NavListItem>
            <NavListItem>
              <NavLink to="/decorating/">
                <span>Outdoor</span>
              </NavLink>
            </NavListItem>
            <NavListItem>
              <NavLink to="/decorating/">
                <span>Workspaces</span>
              </NavLink>
            </NavListItem>
          </NavList>
        </NavListWrapper>
      </NavWrapper>

      <BlogContainer>
        <BlogMainContainer>
          <Layout>
              <RecentPosts/>
          </Layout>
        </BlogMainContainer>
      </BlogContainer>
    </>
  );
}

export default BlogHome2;
