import React from 'react';
import {BlogMainContainer} from './BlogHome2.element';
import Sidebar from './Sidebar';

const Layout = (props) => {
  return(
        <React.Fragment>
            <BlogMainContainer>
                {props.children}
                <Sidebar />
            </BlogMainContainer>
        </React.Fragment>
    
   )

 }

export default Layout;