import React, { useState, useEffect } from "react";
import "./style.css";
import Card from "./BlogCard";
import { Link } from "react-router-dom";
import { BorderedEmptyCurvedButton } from "../../globalStyles";
import { FaArrowRight } from "react-icons/fa";
import { Helmet } from "react-helmet";
import blogData from "../../data/blog.json";
const config = require("../../config_prod.json");

const BlogPost = (props) => {
  const [post, setPost] = useState({
    blogImage: {},
    blogImageLink: "",
    blogTitle: "",
    slug: "",
    description: "",
    active: "0",
    blogTextArray: [
      {
        id: "",
        blogText: "",
      },
    ],
    date: "",
    month: "",
    subContentArray: [
      {
        id: "",
        subContentImage: {},
        subContentImageLink: "",
        subContentHeader: "",
        paragraphArray: [
          {
            id: "",
            paragraph: "",
          },
        ],
      },
    ],
  });
  // eslint-disable-next-line no-unused-vars
  const [slug, setSlug] = useState("");

  useEffect(() => {
    const slug = props.match.params.slug;
    blogData.data
      .find((data) => data.slug === slug)
      .then((data) => {
        if (data.Item) {
          console.log(data.Item);
          setPost(data.Item);
        }
      });
    setSlug(slug);
  }, [post, props.match.params.slug]);

  return (
    <>
      <Helmet>
        <title>{post.blogTitle} | Dexterio</title>
        <meta
          property="og:site_name"
          content="Dexterio - Redefining Interiors"
        />
        <meta property="og:type" content="article" />
        <meta property="og:title" content={post.blogTitle} />
        <meta property="og:description" content={post.description} />
        <meta
          property="og:url"
          content="https://dexterio.in/study-room-7-essentials-elements-to-consider-before-designing/"
        />
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:domain" content="dexterio.in" />
        <meta name="twitter:title" content={post.blogTitle} />
        <meta name="twitter:description" content={post.description} />
        <meta name="description" content={post.description} />
      </Helmet>
      <div className="blogPostContainer">
        <Card>
          <div className="postImageContainer">
            <img
              src={post.blogImageLink}
              alt="Blog "
              style={{ borderRadius: "12px" }}
            />
          </div>
          <div className="panels">
            <div className="leftPanel">
              <div className="widgetWrap">
                <div className="widgetButton">
                  <div className="widgetContainer">
                    <div className="buttonWrapper">
                      <Link to="/" className="button">
                        <span className="buttonContentWrapper">
                          <span className="buttonContent">{post.date}</span>
                        </span>
                      </Link>
                    </div>
                  </div>
                </div>
                <div className="widgetWrap">
                  <div className="widgetContainer">
                    <h5 className="monthHeading">{post.month}</h5>
                  </div>
                </div>
              </div>
            </div>
            <div className="rightPanel">
              <div className="blogHeader">
                <div className="postTitle">{post.blogTitle}</div>
              </div>
              <div className="postContent">
                {post.blogTextArray.map((paragraph, index) => {
                  return <p key={index}>{paragraph.blogText}</p>;
                })}
                {post.subContentArray.map((item, index) => {
                  return (
                    <>
                      {index % 4 === 1 && (
                        <div className="dexterioPromotion">
                          <div key={`Add${index}Heading`} className="addTitle">
                            Interiors for every budget
                          </div>
                          <div
                            key={`Add${index}SubHeading`}
                            className="addSubTitle"
                          >
                            {" "}
                            Assured quality, price match guarantee and a smooth
                            experience
                          </div>
                          <div
                            style={{
                              textAlign: "center",
                              paddingBottom: "10px",
                            }}
                          >
                            <Link to="/hello">
                              <BorderedEmptyCurvedButton
                                primary="#111121"
                                curve
                                light
                                hoverBackground="#111121"
                                hoverTextColor="#fff"
                              >
                                Get Started <FaArrowRight />
                              </BorderedEmptyCurvedButton>
                            </Link>
                          </div>
                        </div>
                      )}
                      <div className="subContentImageContainer">
                        {item.subContentImageLink &&
                          item.subContentImageLink.length > 0 && (
                            <img
                              src={item.subContentImageLink}
                              alt="Blog "
                              style={{ borderRadius: "12px" }}
                            />
                          )}
                      </div>

                      <div key={`SubTitle${index}`} className="postSubTitle">
                        {item.subContentHeader}
                      </div>
                      <div key={`Paragraph${index}Wrapper`}>
                        {item.paragraphArray.map((paragraph, innerIndex) => {
                          return (
                            <>
                              <p key={`Paragraph${index}${innerIndex}`}>
                                {paragraph.paragraph}
                              </p>
                            </>
                          );
                        })}
                        {/* <ol style={{"listStyle":"disc inside", "paddingLeft":"30px"}}>
                                                {
                                                    item.values.map((inner) => {
                                                        return(
                                                            <li>
                                                                {inner}
                                                            </li>)
                                                    })
                                                }
                                            </ol> */}
                      </div>
                    </>
                  );
                })}
              </div>
            </div>
          </div>
        </Card>
      </div>
    </>
  );
};

export default BlogPost;
