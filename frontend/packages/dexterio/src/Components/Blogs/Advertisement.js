import React, {useEffect, useState} from 'react'

function Advertisement() {

    const [imageIndex, setImageIndex] = useState(0);

    const advertisements = [
        "/ads/Ads1.png",
        "/ads/Ads2.png",
        "/ads/Ads3.png",
        "/ads/Ads4.png"
      ];
    
      const getRandomAd = () => {

        return advertisements[imageIndex];
      };
    
    useEffect(() => {
        setInterval(() => {
            setImageIndex(Math.floor(Math.random() * advertisements.length));
        }, 6000)
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <div style={{"margin":" 20px auto", "textAlign":"center"}}>
            <img src={getRandomAd()} style= {{"maxWidth":"100%"}} alt="Dexterio Advertisement"/>
        </div>
    )
}

export default Advertisement
