import React, {useEffect, useState} from 'react';
import CreateBlog from './CreateBlog';
import { v4 as uuidv4 } from 'uuid';

const config = require('../../config_prod.json');

function BlogCreation(props) {
    const month = [
        "Jan",
        "Feb",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "Sep",
        "Oct",
        "Nov",
        "Dec"];
      
    const d = new Date();
    
    const [inputFields, setInputFields] = useState(
        { 
            "blogImage": {},
            "blogImageLink": "",
            "blogTitle" : "",
            "slug": "",
            "description": "",
            "active": "0",
            "blogTextArray": [
              {
                  "id": uuidv4(),
                  "blogText": ""
              }
            ],
            "date": d.getDate().toString(),
            "month" : month[d.getMonth()],
            "subContentArray": [
                {
                    "id": uuidv4(),
                    "subContentImage": {},
                    "subContentImageLink": "",
                    "subContentHeader" : "",
                    "paragraphArray" : [
                        {
                          "id": uuidv4(),
                          "paragraph": ""
                        }
                      ]
                },
            ]
        },
      );
    // eslint-disable-next-line no-unused-vars
    const [slug, setSlug] = useState('');
    
    useEffect(() => {
        const slug = props.match.params.slug;
        if (slug !== 'new') {
            fetch(`${config.api.invokeUrl}blogdata/getblog/${slug}`)
            .then(resp => resp.json())
            .then(data => {
                    if (data.Item) {
                        setInputFields(data.Item);
                    }
                }
            )
        }
    }, [inputFields, props.match.params.slug]);

    return (
        <CreateBlog data={{inputFields , setInputFields}} />
    )
}

export default BlogCreation
