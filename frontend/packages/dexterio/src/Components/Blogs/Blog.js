import React from 'react';
import BlogPost from './BlogPost';
import Layout from './Layout';
import ConsultPage from '../ConsultPage/ConsultPage';

const Blog = (props) => {

  return(
    <>
      <div className="blogContainer" >
        <Layout>
          <BlogPost {...props} />
        </Layout>
      </div>
      <ConsultPage color="#faefe6"/>
    </>
   )
}

export default Blog;