import React, { useState } from "react";
import Container from "@material-ui/core/Container";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import { v4 as uuidv4 } from "uuid";
import { Link } from "react-router-dom";
import Card from "./BlogCard";
import Layout from "./Layout";
import { makeStyles } from "@material-ui/styles";
import axios from "axios";

const config = require("../../config_prod.json");

const useStyles = makeStyles((theme) => ({
  root: {
    "& .MuiTextField-root": {
      margin: theme.spacing(1),
    },
  },
  button: {
    margin: theme.spacing(1),
  },
}));

function CreateBlog(props) {
  const classes = useStyles();

  const [reRender, setRerender] = useState(false);
  const [step, setStep] = useState(0);

  const { inputFields, setInputFields } = { ...props.data };

  const changeHandler = (e) => {
    e.preventDefault();

    setInputFields((prev) => ({
      ...prev,
      [e.target.name]: e.target.value,
    }));
  };

  const blogImageUpload = (e) => {
    e.preventDefault();

    setInputFields((prev) => ({
      ...prev,
      [e.target.name]: e.target.files[0],
    }));
  };

  function validateEmptyObject(obj) {
    for (var i in obj) return false;
    return true;
  }
  const upload = async (image) => {
    console.log("upload start");

    const formData = new FormData();
    formData.append("image", image);
    formData.append("description", "ProjectImage");

    const data = await axios.post("/images", formData, {
      headers: {
        "Content-Type": "multipart/form-data",
        "Access-Control-Allow-Origin": "true",
      },
    });
    console.log(data);
    console.log("upload end");
    return data.data.imagePath.toString();
  };

  const handleSubmit = async () => {
    try {
      console.log("handleAddIDProfile start");
      const blogImageLink = await upload(inputFields.blogImage);
      inputFields.blogImageLink = blogImageLink;
      inputFields.blogImage = "";

      if (inputFields.subContentArray.length > 0) {
        for (const image of inputFields.subContentArray) {
          if (
            image !== null &&
            !validateEmptyObject(image) &&
            !validateEmptyObject(image.subContentImage)
          ) {
            image.subContentImageLink = await upload(image.subContentImage);
          }
          image.subContentImage = "";
        }
      }

      console.log(inputFields);
      console.log("handleAddIDProfile end");

      await axios
        .post(`${config.api.invokeUrl}blogdata`, inputFields)
        .then((data) => {})
        .catch((error) => {
          console.log(error);
        });
    } catch (error) {
      console.log(`Error occured: ${error}`);
    }
  };

  const handleBlogHeaderInput = (id, event) => {
    event.preventDefault();

    const newBlogTextArray = inputFields.blogTextArray.map((i) => {
      if (id === i.id) {
        i[event.target.name] = event.target.value;
      }
      return i;
    });

    inputFields.blogTextArray = newBlogTextArray;
    setInputFields(inputFields);
    setRerender(!reRender);
  };

  const handleBlogSubContentInput = (id, event) => {
    event.preventDefault();

    const newSubContentArray = inputFields.subContentArray.map((i) => {
      if (id === i.id) {
        i[event.target.name] = event.target.value;
      }
      return i;
    });

    inputFields.subContentArray = newSubContentArray;
    setInputFields(inputFields);
    setRerender(!reRender);
  };

  const handleBlogSubContentImage = (id, event) => {
    event.preventDefault();

    const newSubContentArray = inputFields.subContentArray.map((i) => {
      if (id === i.id) {
        i[event.target.name] = event.target.files[0];
      }
      return i;
    });

    inputFields.subContentArray = newSubContentArray;
    setInputFields(inputFields);
    setRerender(!reRender);
  };

  const handleParagraphInput = (sId, pId, event) => {
    event.preventDefault();

    const newSubContentArray = inputFields.subContentArray.map((i) => {
      if (sId === i.id) {
        i.paragraphArray.map((p) => {
          if (pId === p.id) {
            p[event.target.name] = event.target.value;
          }
          return p;
        });
      }
      return i;
    });

    inputFields.subContentArray = newSubContentArray;
    setInputFields(inputFields);
    setRerender(!reRender);
  };

  const handleAddBlogHeader = () => {
    const values = inputFields;
    values.blogTextArray.push({
      id: uuidv4(),
      blogText: "",
    });
    setInputFields(values);
    setRerender(!reRender);
  };

  const handleAddBlogSubHeader = () => {
    const values = inputFields;
    values.subContentArray.push({
      id: uuidv4(),
      subContentImage: "",
      subContentHeader: "",
      paragraphArray: [
        {
          id: uuidv4(),
          paragraph: "",
        },
      ],
      values: [],
    });
    setInputFields(values);
    setRerender(!reRender);
  };

  const handleAddBlogSubHeaderParagraph = (subContentId) => {
    const values = inputFields;
    values.subContentArray.map((subContent) => {
      if (subContentId === subContent.id) {
        subContent.paragraphArray.push({
          id: uuidv4(),
          paragraph: "",
        });
      }
      return subContent;
    });
    setInputFields(values);
    setRerender(!reRender);
  };

  const handleRemoveBlogHeaderText = (id) => {
    const values = inputFields;
    values.blogTextArray.splice(
      values.blogTextArray.findIndex((value) => value.id === id),
      1
    );
    setInputFields(values);
    setRerender(!reRender);
  };

  const handleRemoveSubContent = (id) => {
    const values = inputFields;
    values.subContentArray.splice(
      values.blogTextArray.findIndex((value) => value.id === id),
      1
    );
    setInputFields(values);
    setRerender(!reRender);
  };

  const handleRemoveSubContentParagraph = (sId, pId) => {
    const values = inputFields;
    values.subContentArray.map((subContent) => {
      if (sId === subContent.id) {
        subContent.paragraphArray.splice(
          subContent.paragraphArray.findIndex((value) => value.id === pId),
          1
        );
      }
      return subContent;
    });

    setInputFields(values);
    setRerender(!reRender);
  };

  return (
    <>
      {step === 0 && (
        <Container>
          <h1>Add a New Blog</h1>
          <form
            className={classes.root}
            onSubmit={(event) => {
              event.preventDefault();
              handleSubmit();
            }}
          >
            <label>
              <input
                id="btn-upload"
                name="blogImage"
                style={{ display: "none" }}
                type="file"
                onChange={(event) => blogImageUpload(event)}
              />
              <Button
                className={classes.button}
                variant="outlined"
                component="span"
              >
                Choose BLOG'S HERO IMAGE
              </Button>
            </label>
            <TextField
              variant="outlined"
              size="small"
              margin="normal"
              required
              fullWidth
              name="blogTitle"
              label="Blog Title"
              value={inputFields.blogTitle}
              onChange={(event) => changeHandler(event)}
            />
            <TextField
              variant="outlined"
              size="small"
              margin="normal"
              required
              fullWidth
              name="description"
              label="Metadata Description"
              value={inputFields.description}
              onChange={(event) => changeHandler(event)}
            />
            <TextField
              variant="outlined"
              size="small"
              margin="normal"
              required
              fullWidth
              name="slug"
              label="URL"
              value={inputFields.slug}
              onChange={(event) => changeHandler(event)}
            />
            <div
              style={{
                border: "1px solid #d0d0d0",
                marginLeft: "8px",
                borderRadius: "10px",
                padding: "10px",
              }}
            >
              {inputFields.blogTextArray.map((inputField) => (
                <div
                  key={inputField.id}
                  style={{ display: "flex", flexDirection: "row-reverse" }}
                >
                  <TextField
                    variant="outlined"
                    multiline
                    minRows={6}
                    maxRows={6}
                    size="small"
                    margin="normal"
                    required
                    fullWidth
                    name="blogText"
                    label="Blog Header Text"
                    value={inputField.blogText}
                    onChange={(event) =>
                      handleBlogHeaderInput(inputField.id, event)
                    }
                  />
                  <IconButton
                    disabled={inputFields.blogTextArray.length === 1}
                    onClick={() => handleRemoveBlogHeaderText(inputField.id)}
                  >
                    <img
                      src="/svg/remove2.svg"
                      alt="Add Icon"
                      style={{ width: "20px" }}
                    />
                  </IconButton>
                </div>
              ))}
            </div>
            <IconButton onClick={() => handleAddBlogHeader()}>
              <img
                src="/svg/add2.svg"
                alt="Add Icon"
                style={{ width: "20px" }}
              />
            </IconButton>

            {inputFields.subContentArray.map((subContent) => (
              <div
                key={subContent.id}
                style={{
                  border: "1px solid #d0d0d0",
                  marginLeft: "8px",
                  marginBottom: "10px",
                  borderRadius: "10px",
                  padding: "10px",
                  display: "flex",
                  flexDirection: "column",
                }}
              >
                <IconButton
                  disabled={inputFields.subContentArray.length === 1}
                  onClick={() => handleRemoveSubContent(subContent.id)}
                >
                  <img
                    src="/svg/remove2.svg"
                    alt="Add Icon"
                    style={{ width: "20px" }}
                  />
                </IconButton>
                <label>
                  <input
                    id="btn-upload"
                    name="subContentImage"
                    style={{ display: "none" }}
                    type="file"
                    onChange={(event) =>
                      handleBlogSubContentImage(subContent.id, event)
                    }
                  />
                  <Button
                    className={classes.button}
                    variant="outlined"
                    component="span"
                  >
                    Choose Sub Header Image
                  </Button>
                </label>
                <TextField
                  variant="outlined"
                  size="small"
                  margin="normal"
                  required
                  fullWidth
                  name="subContentHeader"
                  label="Sub Header"
                  value={subContent.subContentHeader}
                  onChange={(event) =>
                    handleBlogSubContentInput(subContent.id, event)
                  }
                />
                {subContent.paragraphArray.map((paragraphItem) => (
                  <div
                    key={paragraphItem.id}
                    style={{ padding: "30px", paddingRight: "0px" }}
                  >
                    <TextField
                      variant="outlined"
                      multiline
                      minRows={6}
                      maxRows={6}
                      size="small"
                      margin="normal"
                      required
                      fullWidth
                      name="paragraph"
                      label="Paragraph"
                      value={paragraphItem.paragraph}
                      onChange={(event) =>
                        handleParagraphInput(
                          subContent.id,
                          paragraphItem.id,
                          event
                        )
                      }
                    />
                    <IconButton
                      disabled={subContent.paragraphArray.length === 1}
                      onClick={() =>
                        handleRemoveSubContentParagraph(
                          subContent.id,
                          paragraphItem.id
                        )
                      }
                    >
                      <img
                        src="/svg/remove2.svg"
                        alt="Add Icon"
                        style={{ width: "20px" }}
                      />
                    </IconButton>
                    <IconButton
                      onClick={() =>
                        handleAddBlogSubHeaderParagraph(subContent.id)
                      }
                    >
                      <img
                        src="/svg/add2.svg"
                        alt="Add Icon"
                        style={{ width: "20px" }}
                      />
                    </IconButton>
                  </div>
                ))}
              </div>
            ))}
            <IconButton onClick={() => handleAddBlogSubHeader()}>
              <img
                src="/svg/add2.svg"
                alt="Add Icon"
                style={{ width: "20px" }}
              />
            </IconButton>
            <br />
            <Button
              className={classes.button}
              variant="contained"
              color="primary"
              onClick={() => {
                setStep(1);
              }}
            >
              Preview
            </Button>
            <Button
              className={classes.button}
              variant="contained"
              color="primary"
              type="submit"
            >
              Submit
            </Button>
          </form>
        </Container>
      )}

      {step === 1 && (
        <div className="blogContainer">
          <Button
            className={classes.button}
            variant="contained"
            color="primary"
            onClick={() => {
              setStep(0);
            }}
          >
            Go Back and Submit
          </Button>
          <Layout>
            <div className="blogPostContainer">
              <Card>
                <div className="postImageContainer">
                  {inputFields.blogImage &&
                    !validateEmptyObject(inputFields.blogImage) && (
                      <img
                        src={URL.createObjectURL(inputFields.blogImage)}
                        alt="Blog "
                        style={{ borderRadius: "12px" }}
                      />
                    )}
                  {inputFields.blogImageLink &&
                    !validateEmptyObject(inputFields.blogImageLink) && (
                      <img
                        src={inputFields.blogImageLink}
                        alt="Blog "
                        style={{ borderRadius: "12px" }}
                      />
                    )}
                </div>
                <div className="panels">
                  <div className="leftPanel">
                    <div className="widgetWrap">
                      <div className="widgetButton">
                        <div className="widgetContainer">
                          <div className="buttonWrapper">
                            <Link to="/" className="button">
                              <span className="buttonContentWrapper">
                                <span className="buttonContent">
                                  {inputFields.date}
                                </span>
                              </span>
                            </Link>
                          </div>
                        </div>
                      </div>
                      <div className="widgetWrap">
                        <div className="widgetContainer">
                          <h5 className="monthHeading">{inputFields.month}</h5>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="rightPanel">
                    <div className="blogHeader">
                      <div className="postTitle">{inputFields.blogTitle}</div>
                    </div>
                    <div className="postContent">
                      {inputFields.blogTextArray.map((paragraph, index) => {
                        return <p key={index}>{paragraph.blogText}</p>;
                      })}
                      {inputFields.subContentArray.map((item, index) => {
                        return (
                          <>
                            <div className="subContentImageContainer">
                              {item.subContentImage &&
                                !validateEmptyObject(item.subContentImage) && (
                                  <img
                                    src={URL.createObjectURL(
                                      item.subContentImage
                                    )}
                                    alt="Blog "
                                    style={{ borderRadius: "12px" }}
                                  />
                                )}
                              {item.subContentImageLink && (
                                <img
                                  src={item.subContentImageLink}
                                  alt="Blog "
                                  style={{ borderRadius: "12px" }}
                                />
                              )}
                            </div>

                            <div
                              key={`SubTitle${index}`}
                              className="postSubTitle"
                            >
                              {item.subContentHeader}
                            </div>
                            <div key={`Paragraph${index}Wrapper`}>
                              {item.paragraphArray.map(
                                (paragraph, innerIndex) => {
                                  return (
                                    <>
                                      <p key={`Paragraph${index}${innerIndex}`}>
                                        {paragraph.paragraph}
                                      </p>
                                    </>
                                  );
                                }
                              )}
                            </div>
                          </>
                        );
                      })}
                    </div>
                  </div>
                </div>
              </Card>
            </div>
          </Layout>
        </div>
      )}
    </>
  );
}

export default CreateBlog;
