import React from "react";
import "./style.css";
import blogPost from "../../data/blog.json";
import Card from "./BlogCard";
import { NavLink } from "react-router-dom";
import { BlogList, PostImageWrapper } from "./BlogHome2.element";
import { FlexRow, FlexColumn } from "common";
import {
  SubSectionHeadlines,
  SubSectionParagraph
} from "globalstyles";
import styled from 'styled-components';

const RecentPosts = (props) => {
  const [posts, setPosts] = React.useState([]);

  React.useEffect(() => {
    const posts = blogPost.data;
    setPosts(posts);
  }, [posts]);

  return (
    <BlogList>
      <Card style={{ marginBottom: "20px" }}>
        {posts.map((post) => {
          return (
            <HoverableRow>
              <PostImageWrapper>
                <img src={post.blogImage} alt="" style={{"borderRadius":"8px"}}/>
              </PostImageWrapper>
              <BlogColumn>
                <NavLink
                  key={post.id}
                  to={`/blog/${post.slug}`}
                  style={{ textDecoration: "none" }}
                >
                  <SubSectionHeadlines
                    style={{
                      marginBottom: "1.5vw",
                      marginTop: "1.5vw",  
                      width: "100%"
                    }}
                  >
                    {post.blogTitle}
                  </SubSectionHeadlines>
                </NavLink>
                <FlexRow
                  style={{
                    width: "20%",
                    borderBottom: "1px solid rgba(123,131,97,1)",
                    justifyContent: "left",
                  }}
                ></FlexRow>
                <SubSectionParagraph
                    style={{
                      marginBottom: "0",
                      marginTop: "1.5vw",  
                      width: "100%",
                      textAlign: "justify",
                      textJustify: "inter-word",
                    }}
                  >
                    {post.blogSnap}
                  </SubSectionParagraph>
              </BlogColumn>
            </HoverableRow>
          );
        })}
      </Card>
    </BlogList>
  );
};

export default RecentPosts;

const HoverableRow = styled(FlexRow)`

    grid-gap: 2rem;
    padding-bottom: 2rem;
    padding-top: 2rem;
    border-bottom: 1px solid rgba(123,131,97,1);

    &:hover {
        background: #f4f3ee;
    }

    &:first-child {
        padding-top: 0;
    }
`;

const BlogColumn = styled(FlexColumn)`
    width: 50%;
    padding: 10px;

    @media only screen and (max-width: 600px) {
        width: 100%;
    }
`;