import styled from 'styled-components';
import { Link } from 'react-router-dom';


export const NavWrapper = styled.div`
    display: flex;
    position: relative;
    width: 100%;
    -webkit-box-pack: center;
    justify-content: center;
    border-width: 1px 0px;
    border-style: solid;
    border-color: rgb(221, 221, 221);
    margin-bottom: 1rem;
`;

export const NavListWrapper = styled.div`
    --grid-gap: 1rem;
    --grid-margin: 1.5rem;
    display: flex;
    overflow-x: auto;
    padding-left: var(--grid-margin);
    line-height: 1;
    &::-webkit-scrollbar {
        display: none;
    }
`;

export const NavList = styled.ul`
    display: inline-flex;
    margin: 0px;
    padding-top: 0px;
    padding-bottom: 0px;
    padding-left: 0px;
    padding-right: var(--grid-margin);
    white-space: nowrap;
`;

export const NavListItem = styled.li`
    display: flex;
    position: relative;
    flex-shrink: 0;
    -webkit-box-align: center;
    align-items: center;
    transition: all 500ms ease 0s;
    height: 48px;
    margin-left: 1rem;

    &:first-child {
        margin-left: 0rem;
    }
`;

export const NavLink = styled(Link)`
    text-transform: uppercase;
    font-family: Lato, helvetica, sans-serif;
    font-feature-settings: normal;
    font-style: normal;
    letter-spacing: 0.133em;
    line-break: auto;
    line-height: 1.17em;
    font-size: 12px;
    font-weight: 700;
    overflow-wrap: normal;
    color: rgb(43, 43, 43);
    transition-property: color, background, text-shadow;
    transition-duration: 0.2s;
    transition-timing-function: ease-in-out;
    cursor: pointer;
    text-decoration: none;
    padding: 0.75rem 0px;
    line-height: 1;
    word-break: normal;
    color: rgb(43, 43, 43);
`;


export const BlogContainer = styled.div`
    margin: 0 auto;

    @media only screen and (min-width: 1024px) {
        width: 70%;
    }
    @media only screen and (max-width: 1023px) {
        width: 70%;
    }

    @media only screen and (max-width: 768px) {
        width: 90%;
    }

`;

export const BlogMainContainer = styled.div`
    display: flex;
    justify-content: space-between;
    margin: 20px 0;
`;

export const BlogList = styled.div`
    width: 70%;

    @media only screen and (max-width:1024px) {
        width: 100%;
    }
`;

export const PostImageWrapper = styled.div`
    
    width: 50%;
    overflow: hidden;

    & > img {
        width: 100%;
        max-height: 100%;
        object-fit: contain;
    }

    @media only screen and (max-width: 767px) {
        width: 100%;
    }
`;