import ClientSignUpForm from '../Common/ClientSignUpForm';
import React, { useState , useEffect } from 'react';
import blogPost from '../../data/blog.json';
import { NavLink } from 'react-router-dom';
import Card from './BlogCard';
import './style.css';

const Sidebar = (props) => {

    const [posts, setPosts] = useState([]);

    useEffect(() => {
        const posts = blogPost.data;
        setPosts(posts);
    }, [posts]);

    return(
      <div className="sidebarContainer" style={{
          width: props.width
      }}>
        <Card style={{ marginBottom: '20px', marginTop: '10px', borderRadius: '7px',  padding: '20px', boxSizing: 'border-box', border: '1px solid #e1e1e1'}}>
            <ClientSignUpForm headerText="Get Expert Advice"/>
        </Card>
        <Card style={{ marginBottom: '20px', padding: '20px', boxSizing: 'border-box' }}>
            <div className="cardHeader">
                <span>Recent Posts</span>
            </div>
            <div className="recentPosts">
                {
                    posts.map(post => {
                        return (
                            <NavLink key={post.id} to={`/blog/${post.slug}`} style={{"textDecoration": "none"}}>
                                <div className="recentPost">
                                    <h3>{post.blogTitle}</h3>
                                </div>
                            </NavLink>
                        );
                    })
                }
            </div>
        </Card>
      </div>
   )
 }

export default Sidebar;