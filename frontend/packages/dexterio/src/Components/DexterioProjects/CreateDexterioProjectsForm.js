import React, { useState } from 'react';

import { Step, Preview, validate} from 'common';
import axios from "axios";
import { createProjectFormData } from '../../data/CreateDexterioProjectsFormData';
const config = require('../../config_prod.json');

const CreateDexterioProjectsForm = ({ props , step , setStep }) => {
  const [formData, setFormData] = useState(createProjectFormData);
  const [errors, setErrors] = useState({});

  const dropdownChangeHandler = (event, step, name, newValue) => {
    event.persist();

    setFormData(prev => ({
      ...prev,
      [step]: {
        ...prev[step],
        [name]: {
          ...prev[step][name],
          value: newValue
        }
      }
    }));
  } 

  const changeHandler = (step, e) => {
    e.persist();

    setFormData(prev => ({
      ...prev,
      [step]: {
        ...prev[step],
        [e.target.name]: {
          ...prev[step][e.target.name],
          value: e.target.value
        }
      }
    }));
  }

  const fileChangeHandler = (name, file, step) => {
    setFormData(prev => ({
      ...prev,
      [step]: {
        ...prev[step],
        [name]: {
          ...prev[step][name],
          value: file,
          fileName: file.name ? file.name : 'No file chosen'
        }
      }
    }));
  }

  const stepChangeHandler = (values, e) => {
    e.preventDefault();
    const newErrors = validate(values);
    setErrors(newErrors);
    if(Object.keys(newErrors).length === 0) {
      setStep(step + 1);
    }
  }

  const submitHandler = async(e) => {
    e.preventDefault();

    try {
      console.log("Start uploading the profile Image");
      console.log(formData.stepTwo.image.value);
      const axiosForm = new FormData();
      axiosForm.append("image", formData.stepTwo.image.value)
      axiosForm.append("description", "ProjectImage")

      const data  = await axios.post('/images', axiosForm, 
                                  { 
                                      headers: {
                                          "Content-Type": "multipart/form-data", 
                                          "Access-Control-Allow-Origin": "true"
                                      }
                                  }
                                  );
      console.log(data);

      const params = {
        "username" : formData.stepOne.profilename.value,
        "name" : formData.stepOne.name.value,
        "addressLine1" : formData.stepOne.addressLine1.value,
        "addressLine2" : formData.stepOne.addressLine2.value,
        "city" : formData.stepOne.city.value,
        "state" : formData.stepOne.state.value,
        "country" : formData.stepOne.country.value,
        "pinCode" : formData.stepOne.pinCode.value,
        "email" : formData.stepOne.email.value,
        "phone" : formData.stepOne.phone.value,
        "briefDescription" : formData.stepTwo.briefDescription.value,
        "skills" : formData.stepTwo.skills.value,
        "themes" : formData.stepTwo.themes.value,
        "profileImage" : data.data.imagePath
      }
      await axios.post(`${config.api.invokeUrl}/designerprofile/${formData.stepOne.profilename.value}`,params)
              .then(data => {
                console.log(data.status);
                if(data.status === 201) {
                  setStep(step + 1);
                } else {
                  //TODO Forward to Error Screen
                }
              });

    } catch(error){
      console.log(`Error occured: ${error}`);
    }
  }

  return(
    <>
      <form onSubmit={submitHandler}>
        {step === 1 && <Step 
          data={formData.stepOne}
          onChange={changeHandler}
          onStepChange={stepChangeHandler}
          onFileChange={fileChangeHandler}
          dropdownChange={dropdownChangeHandler}
          errors={errors}
          stepKey="stepOne"
          stepName="Create Documentation"
          step={1}
        />}
        {step === 2 && <Preview 
          onPrevStep={(step) => setStep(step)}
          step={2}
          data={[
            { label: 'Project Name', value: formData.stepOne.projectName.value , width : '100%'},
            { label: 'Project Location', value: formData.stepOne.projectLocation.value , width : '100%'},
            { label: 'squareFoot', value: formData.stepOne.squareFoot.value , width : '100%'},
            { label: 'Milestones', value: formData.stepOne.milestones.value.join(', ') , width : '100%'},
            // { label: 'Profile Image', value: URL.createObjectURL(formData.stepTwo.image.value), image: true , width : '100%'},
          ]}
        />}
      </form>
    </>
  );
}

export default CreateDexterioProjectsForm;