import React, { Fragment , useState } from 'react';
import styled from 'styled-components';

import CreateDexterioProjectsForm from './CreateDexterioProjectsForm';

const CreateDexterioProjects = (props) => {
  const [step, setStep] = useState(1);

  return (
    <Fragment>
      <Container>
        <Columns>
          <Column>
            <CreateDexterioProjectsForm 
              step={step}
              setStep={setStep}
            />
          </Column>
        </Columns>
      </Container>
    </Fragment>
  );
}

const Container = styled.div`
  padding: 0.75rem 0 0 0;
  
  text-align: center;
  
  flex-grow: 1;
  position: relative;
`;

const Columns = styled.div`
    @media only screen and (min-width: 768px) {
      display: flex;
      justify-content: center;
      background-color: #f6f8fa;
    }

`;

const Column = styled.div`
    @media only screen and (min-width: 768px) {
      flex: none;
      width: 75%;
      
    }
    display: block;
`;

export default CreateDexterioProjects;