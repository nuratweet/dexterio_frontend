import React, { useState } from "react";
import AboutIntro from "./AboutUsIntro";
import { Welcome } from "../Projects/ProjectPage.element";
import {
  SectionHeadlines,
  SectionParagraph,
  VerticalSpacing,
  SubSectionHeadlines,
  SubSectionParagraph,
  Container,
} from "globalstyles";
import { FlexRow } from "common";
import BackGroundImageWithText from "../Common/BackGroundImageWithText";
import { FlexColumn } from "common";
import styled from 'styled-components';
import { Box } from "@material-ui/core";

import { useTheme } from "@material-ui/styles";
import SwipeableViews from "react-swipeable-views";
import { autoPlay } from "react-swipeable-views-utils";
import useMediaQuery from '@material-ui/core/useMediaQuery';

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

function AboutUs() {


  const values = [
    {
      desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Bibendum est",
      image: "/about/Priyanshu-300x300.png",
      value: "We handle everything",
    },
    {
      desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Bibendum est",
      image: "/about/Priyanshu-300x300.png",
      value: "We handle everything",
    },
    {
      desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Bibendum est",
      image: "/about/Priyanshu-300x300.png",
      value: "We handle everything",
    },
  ]

  const team = [
    {
      desc: "I consult the marketing at the Dexterio and run my marketing firm as well. My skills include planning, research, and marketing assistance.",
      image: "/about/Priyanshu-300x300.png",
      name: "Priyanshu Mahar",
    },
    {
      name: "Arpit Khattar",
      desc: "I am an interior designer responsible for the development part of the of Prefab Rooms and other operations.",
      image: "/about/Arpit-300x300.png",
    },
    {
      name: "Tarun Yadav",
      desc: "Technical Lead at Dexterio and looks after the product development and delivery.",
      image: "/about/Tarun-300x300.png",
    },
    {
      name: "Piyush Panikar",
      desc: "Front end Developer at Dexterio",
      image: "/about/Piyush-300x300.png",
    },
  ];

  const matches = useMediaQuery('(min-width:600px)');

  const theme = useTheme();
  const [activeStep, setActiveStep] = useState(0);

  const handleStepChange = (step) => {
    setActiveStep(step);
  };

  return (
    <div style={{ overflow: "hidden" }}>
      <AboutIntro />
      <Welcome>
        <SectionHeadlines style={{ textAlign: "center" }}>
          Welcome to a streamlined home renovation <br />
          platform to plan, design, and build
          <br /> <span>all in one place.</span>
        </SectionHeadlines>
      </Welcome>
      <div style={{ marginTop: "150px", "position":"relative" }}>
        <Overlay/>
        <Container style={{paddingBottom: "2vw"}}>
          <OurStory>
            <OurStoryColumn>
              <SectionHeadlines>
                Our <span>Story</span>
              </SectionHeadlines>
              <SectionParagraph>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua.
                Bibendum est ultricies integer quis. Iaculis urna id volutpat
                lacus laoreet. Mauris vitae ultricies leo integer malesuada. Ac
                odio tempor orci dapibus ultrices in. Egestas diam in arcu
                cursus euismod. Dictum fusce ut placerat orci nulla. Tincidunt
                ornare massa eget egestas purus viverra accumsan in nisl. Tempor
                id eu nisl nunc mi ipsum faucibus. Fusce id velit ut tortor
                pretium. Massa ultricies mi quis hendrerit dolor magna eget.
              </SectionParagraph>
            </OurStoryColumn>
            <OurStoryColumn
              style={{ transform: "translateY(-20%)" }}
            >
              <img
                src="/Rectangle_34.png"
                srcset="/Rectangle_34.png 1x, /Rectangle_34@2x.png 2x"
                alt=""
                style={{borderRadius: "8px"}}
              />
            </OurStoryColumn>
          </OurStory>
        </Container>
      </div>
      <Container>
        <FlexColumn>
          <FlexRow>
            <VerticalSpacing height="5vw" />
          </FlexRow>
          <FlexRow>
            <SectionHeadlines>
              Our <span>Values</span>
            </SectionHeadlines>
          </FlexRow>
          <FlexRow>
            <VerticalSpacing height="50px" />
          </FlexRow>

          {values.map((step, index) => (
            <>
              <FlexRow>
                <div
                  style={{
                    backgroundColor: "#f4f2ec",
                    borderRadius: "8px",
                    padding: "25px",
                    marginLeft: "5%",
                    marginRight: "5%",
                  }}
                >
                  <FlexRow>
                    <OurValues>
                      <Image
                        src="/icons/suitcase_do.png"
                        srcset="/icons/suitcase_do.png 1x, /icons/suitcase_do@2x.png 2x"
                        alt=""
                      />
                    </OurValues>
                    <FlexColumn style={{ flexGrow: "1" }}>
                      <SubSectionHeadlines style={{ color: "rgba(123,131,97,1)" }}>
                        {step.value}
                      </SubSectionHeadlines>
                      <SubSectionParagraph>
                        {step.desc}
                      </SubSectionParagraph>
                    </FlexColumn>
                  </FlexRow>
                </div>
              </FlexRow>
              <FlexRow>
                <VerticalSpacing height="50px" />
              </FlexRow>
            </>
          ))}
        </FlexColumn>
      </Container>
      <Container>
        <SectionHeadlines>
          Meet the <span style={{ color: "rgba(123,131,97,1)" }}> team </span>
        </SectionHeadlines>
        <VerticalSpacing height="50px" />
        <CarouselSection>
          <Box sx={{ width: matches ? "65%" : "100%", flexGrow: 1 }}>
            <AutoPlaySwipeableViews
              axis={theme.direction === "rtl" ? "x-reverse" : "x"}
              index={activeStep}
              onChangeIndex={handleStepChange}
              enableMouseEvents
              className="mui-div"
            >
              {team.map((step, index) => (
                <div key={index}>
                  {Math.abs(activeStep - index) <= 2 ? (
                    <FlexRow style={{ position: "relative" }}>
                      <FlexColumn className="swiper-wrapper">
                        <VerticalSpacing height="1vw" />
                        <SubSectionHeadlines style={{ color: "rgba(123,131,97,1)" }} >
                          {step.name}
                        </SubSectionHeadlines>
                        <WidthDefinedSubSectionParagraph>
                          {step.desc}
                        </WidthDefinedSubSectionParagraph>
                        <VerticalSpacing height="4vw" />
                      </FlexColumn>
                      <div className="swiper-image-wrapper">
                        <Box
                          component="img"
                          sx={{
                            height: "100%",
                            objectFit: "contain",
                            borderRadius: "8px",
                            zIndex: "9000"
                          }}
                          src={step.image}
                        />
                      </div>
                    </FlexRow>
                  ) : null}
                </div>
              ))}
            </AutoPlaySwipeableViews>
          </Box>
        </CarouselSection>
        <VerticalSpacing height="40" />
      </Container>
      <BackGroundImageWithText
        data={{ page: "career", imagePath: "CareersBanner.png" }}
      />
    </div>
  );
}

export default AboutUs;


const OurStoryColumn = styled(FlexColumn)`

   max-width: 50%;

   & > img {
    width: 100%
  }
   @media only screen and (max-width: 600px) {
    max-width: 100%;
    align-items: center;

    & > ${SectionHeadlines} {
      text-align: center;
    }
    & > img {
      width: 70%;
    }
   }
`;


const CarouselSection = styled.section`
  display: flex;
  flex-direction: column;
  justify-content: center;
  text-align: center;
  align-items: center;
`;

const WidthDefinedSubSectionParagraph = styled(SubSectionParagraph)`
  max-width: 50%;

  @media only screen and (max-width: 600px) {
    max-width: 90%
  }

`;

const OurValues = styled(FlexRow)`
  width: 40%;
  justify-content: center;
  align-items: center;

  @media only screen and (max-width: 600px) {
    width: 100%;
    justify-content: left;
    align-items: flex-start;
  }
`;

const Image = styled.img`
    width: 40%;
    @media only screen and (max-width: 600px) {
      width: 20%;
    }
  
`;

const OurStory = styled(FlexRow)`
  grid-gap: 3rem;
`;

const Overlay = styled.div`
  background-color: #f4f2ec;
  width: 75%;
  position: absolute;
  height: 100%;
  z-index: -100;

  &::after {
    content: " ";
    position: absolute;
    display: block;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    z-index: -1;

    background: #f4f2ec;

    transform: skew(-15deg, 0deg);
    transform-origin: bottom left;
  }

  @media only screen and (max-width: 600px) {
    width: 100%;
    position: absolute;
    height: 65%;
  
  }

`;