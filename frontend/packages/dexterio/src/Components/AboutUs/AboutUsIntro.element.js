import styled from "styled-components";

export const ImageWrapper = styled.div`
  max-height: 80vh;
  overflow: hidden;
  text-align: right;
  z-index: -100;
  @media screen and (max-width: 600px) {
    max-height: 100vh;
  }
`;

export const ImgDesktop = styled.img`
  z-index: -100;
  height: 100%;
  object-fit: fill;
  object-position: bottom;
  max-width: 100%;
  vertical-align: middle;
  display: inline-block;

  @media screen and (min-width: 1279px) {
    max-width: 70%;
    object-position: right;
  }

  @media screen and (max-width: 768px) {
    display: none;
  }
`;

export const ImgMobile = styled.img`
  z-index: -100;
  height: 100%;
  object-fit: fill;
  object-position: bottom;
  max-width: 100%;
  vertical-align: middle;
  display: inline-block;

  @media screen and (min-width: 1279px) {
    max-width: 70%;
    object-position: right;
  }
  @media screen and (min-width: 768px) {
    display: none;
  }
`;
