import React from "react";
import { ImageWrapper, ImgMobile, ImgDesktop } from "./AboutUsIntro.element";
import { FlexColumn, TextWrapper } from "common";
import { Link } from "react-router-dom";
import { IntroParagraph, IntroHeadlines, CurvedButton } from "globalstyles";
import { SvgWrapperRectangles } from "common";
import { LeftPanel } from "common";

function AboutUsIntro() {
  const heroImageArray = [
    "/estimate_2.webp",
    "/estimate_3.webp",
    "/estimate_4.webp",
    "/estimate_5.webp",
  ];

  const heroImageMobileArray = [
    "/projects_mobile_1.jpg",
    "/projects_mobile_2.jpg",
    "/projects_mobile_3.jpg",
    "/projects_mobile_4.png",
  ];

  const getRandomHeroImageDesktop = () => {
    return heroImageArray[Math.floor(Math.random() * heroImageArray.length)];
  };

  const getRandomHeroImageMobile = () => {
    return heroImageMobileArray[
      Math.floor(Math.random() * heroImageArray.length)
    ];
  };

  return (
    <div style={{ position: "relative", overflow: "hidden" }}>
      <LeftPanel>
        <div>
          <TextWrapper style={{ paddingBottom: "0" }}>
            <IntroHeadlines spanColor="rgba(123,131,97,1)">
              About <span>Us</span>
            </IntroHeadlines>
            <IntroParagraph>
              We are an interior designing firm that brings homeowners, interior
              designers, and vendors together <br />
              for an intrinsic and affordable renovation experience.
            </IntroParagraph>
            <Link to="/hello">
              <CurvedButton
                style={{
                  background: "rgba(255,92,92,1)",
                }}
                background="rgba(255,92,92,1)"
                light
                fontSize="20px"
                curve
                margin="0px"
              >
                Book Online Consultation
              </CurvedButton>
            </Link>
          </TextWrapper>
        </div>
      </LeftPanel>
      <FlexColumn style={{ zIndex: "-100" }}>
        <ImageWrapper>
          <ImgDesktop src={getRandomHeroImageDesktop()} alt="How it works" />
          <ImgMobile src={getRandomHeroImageMobile()} alt="How it works" />
        </ImageWrapper>
        <div style={{ height: "50px" }}></div>
      </FlexColumn>
      <SvgWrapperRectangles style={{ transform: "translateY(-50%)" }}>
        <img src="/Rectangles.svg" width="125" height="125" alt="" />
      </SvgWrapperRectangles>
    </div>
  );
}

export default AboutUsIntro;
