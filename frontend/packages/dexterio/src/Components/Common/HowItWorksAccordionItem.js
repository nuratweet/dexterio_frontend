
const AccordionItem = ({ item, active, onToggle}) => {
  const { heading, subheading, webpXMobile, webp2XMobile, jpgXMobile, jpg2XMobile, imgX, img2X } = item;

  return (
        <li className={`style__MobileAccordionWrap-sc-1fzc1wr-5 ${active ? "cLUgwi" : "hFgLoA"}`} onClick={onToggle}>
            <div className={`style__AccordionHeader-sc-1fzc1wr-6 ${active ? "ezWyUV" : "itLgeS"}`}>
                <span className="line"></span>
                <span className="check">
                    <svg viewBox="0 0 1024 1024" style={{"display": "inline-block", "stroke": "currentcolor", "fill": "currentcolor", "width": "10px", "height": "10px"}}>
                        <path d="M1024 267.832l-162.909-158.118-465.455 451.765-232.727-225.882-162.909 158.118 395.636 384z"></path>
                    </svg>
                </span>
                <h4>{heading} <span>→</span></h4>
                <p>{subheading}</p>
            </div>
            <div className="style__AccordionBody-sc-1fzc1wr-7 hWXIyu" style={active ? { "opacity": "1", height: "auto" } : { "opacity": "0", height: "0px" }}>
                <div className="img-wrap">
                    <picture>
                        <source media="(max-width: 767px)" srcSet={`${webpXMobile} 1x, ${webp2XMobile} 2x`} type="image/webp"/>
                        <source media="(max-width: 767px)" srcSet={`${jpgXMobile} 1x, ${jpg2XMobile} 2x`} type="image/jpeg"/>
                        <img src={imgX} srcSet={`${imgX} 1x, ${img2X} 2x`} sizes="100%" alt="How it works"/>
                    </picture>
                </div>
            </div>
        </li>
  );
};

export default AccordionItem;