import {
  SectionParagraph,
  VerticalSpacing,
  BannersHeadlines,
  CurvedButton,
} from "globalstyles";
import { TextWrapper } from "common";
import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { Link } from "react-scroll";
import ClientSignUpForm from "./ClientSignUpForm";

const BackGroundImageWithText = (props) => {
  const { page, imagePath } = { ...props.data };

  const [button, setbutton] = useState(true);
  const showButton = () => {
    if (window.innerWidth <= 768) {
      setbutton(false);
    } else {
      setbutton(true);
    }
  };
  useEffect(() => {
    showButton();
    return () => {
      window.removeEventListener("resize", () => {});
    };
  });

  window.addEventListener("resize", showButton);

  return (
    <Container>
      {page === "homecalltoaction" && <div id="client_signup"></div>}
      <BackgroundImage image={imagePath} />
      <Overlay></Overlay>
      <Content style={{ left: "0px" }}>
        <TextWrapper
          style={{
            paddingLeft:
              "calc(15px + (100vw - 700px) * (15 - 30) / (700 - 1000))",
            paddingTop:"9%"
          }}
        >
          <BannersHeadlines light>
            {page === "career" && (
              <>
                Like what
                <br />
                You see?
                <br />
                <span>Join Us</span>
              </>
            )}
            {page === "homegradient" && (
              <>
                Renovating is hard,
                <br />
                But we've got
                <br />
                <span style={{ color: "rgb(255,92,92)" }}>Your back</span>
              </>
            )}
            {page === "homecalltoaction" && (
              <>
                Get Expert Advise
                <br />
                <span style={{ color: "rgb(255,92,92)" }}>Right Now</span>
              </>
            )}
          </BannersHeadlines>
          <SectionParagraph light>
            {page === "career" && <></>}
            {page === "homegradient" && (
              <>
                You're on the way
                <br /> to the easiest renovation ever.{" "}
              </>
            )}
            {page === "homecalltoaction" && (
              <>Your dream interiors is just a click away.</>
            )}
          </SectionParagraph>
          <VerticalSpacing height="2.5vw" />
          <div style={{ zIndex: "1000" }}>
            {button ? (
              page !== "homecalltoaction" && (
                <Link to="client_signup" spy={true} smooth={true}> 
                  <CurvedButton background="#fff" curve fontBig margin="0px" style = {{padding:"6px 29px", fontSize:"1.10vw"}}>
                    Book Online Consultation
                  </CurvedButton>
                </Link>
              )
            ) : (
              <CurvedButton
                background="#fff"
                curve
                fontBig
                margin="0px"
                onClick={() => {
                  props.mobileLeadFormData.setMobileLeadFormData(true);
                }}
              >
                Book Online Consultation
              </CurvedButton>
            )}
          </div>
        </TextWrapper>
      </Content>
      {page === "homecalltoaction" && (
        <Content style={{ right: "0px" }}>
          <TextWrapper
            style={{
              paddingRight:
                "calc(15px + (100vw - 700px) * (15 - 30) / (700 - 1000))",
            }}
          >
            <LeadFormContainer>
              <LeadFormSection>
                <LeadFormWrapper>
                  <LeadFormArticle>
                    <ClientSignUpForm
                      headerText="Connect with the Team"
                      auth={props.auth}
                      {...props}
                    />
                  </LeadFormArticle>
                </LeadFormWrapper>
              </LeadFormSection>
            </LeadFormContainer>
          </TextWrapper>
        </Content>
      )}
    </Container>
  );
};

export default BackGroundImageWithText;

const Content = styled.div``;

const Container = styled.div`
  width: 100%;
  position: relative;

  @media screen and (min-width: 769px) {
    & > ${Content} {
      transform: translateY(22vh);
      z-index: 100;
      position: absolute;
      top: 0px;
    }
  }
  @media screen and (max-width: 768px) {
    & > ${Content} {
      position: absolute;
      top: 0px;
      z-index: 1000;
      transform: translateY(15vh);
    }
  }
`;

const LeadFormContainer = styled.div`
  max-width: 422px;

  @media only screen and (max-width: 768px) {
    display: none;
  }
`;

const LeadFormSection = styled.div``;

const LeadFormWrapper = styled.div`
  max-width: 422px;
`;

const LeadFormArticle = styled.article`
  max-width: 422px;
  padding: 2vw;
  border-radius: 8px;
  text-align: left;
  position: relative;
  margin-top: 0;
  background-color: rgba(244, 243, 237, 1);
`;

export const Overlay = styled.div`
  position: absolute;
  display: block;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background: linear-gradient(90deg, black, transparent);
  z-index: 2;
`;

export const BackgroundImage = styled.div`
  background: url(/${({ image }) => image});
  height: 100vh;
  max-width: 100%;
  background-size: cover;
  z-index: -100;

  @media screen and (max-width: 768px) {
    height: 80vh;
  }
`;
