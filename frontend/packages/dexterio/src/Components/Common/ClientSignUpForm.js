import {
  Box,
  Checkbox,
  FormControlLabel,
  TextField,
  Typography,
} from "@material-ui/core";
import React, { useState } from "react";
import saveClientLead from "../ClientLeads/saveClientLead";
import { MaterialUiPhoneNumber } from "common";
import styled,  {keyframes} from "styled-components";
import { IntroParagraph, CurvedButton } from "globalstyles";
import { FaSpinner } from "react-icons/fa";

function Copyright() {
  return (
    <>
      <Typography
        variant="body2"
        color="textSecondary"
        align="center"
        style={{
          fontSize: "12px",
          color: "rgba(0,0,0,1)",
          letterSpacing: "1.2px",
        }}
      >
        {
          "By submitting this form, you agree to the privacy policy, terms and conditions"
        }
      </Typography>
    </>
  );
}

function ClientSignUpForm(props) {
  const { headerText } = { ...props };

  // const [username, setUsername] = useState("");
  // const [propertyName, setPropertyName] = useState("");
  // const [name, setName] = useState("");
  // const [email, setEmail] = useState("");
  // const [authenticationCode, setAuthenticationCode] = useState("");
  // const [step, setStep] = useState(0);
  // const [errors, setErrors] = useState({
  //   cognito: null,
  //   blankfield: false,
  //   passwordmatch: false,
  // });
  // const [phone_number, setPhone] = useState("");
  // const [error, setError] = useState(false);
  // const [errorMessage, setErrorMessage] = useState("");
  // const [cognitoUser, setCognitoUser] = useState();

  let mailformat = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [emailFormatError, setEmailFormatError] = useState(false);
  const [emailFormatErrorMessage, setEmailFormatErrorMessage] = useState("");
  const [phoneNumber, setPhone] = useState("");
  const [propertyName, setPropertyName] = useState("");
  const [error, setError] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const [contactOnWhatsapp, setContactOnWhatsapp] = useState(true);
  const [submitting, setSubmitting] = useState(false);

  // function clearErrorState() {
  //   setErrors({
  //     cognito: null,
  //     blankfield: false,
  //     passwordmatch: false,
  //   });
  // }

  // async function confirmSignup(event) {
  //   event.preventDefault();

  //   clearErrorState();
  //   const error = Validate(event, username, email);
  //   if (error) {
  //     setErrors({
  //       cognito: error.cognito,
  //       blankfield: error.blankfield,
  //       passwordmatch: error.passwordmatch,
  //     });
  //   }
  //   const user = await Auth.sendCustomChallengeAnswer(
  //     cognitoUser,
  //     authenticationCode
  //   );
  //   try {
  //     await Auth.currentSession();
  //     let forward = props.forward;
  //     props.auth.setAuthStatus(true);
  //     props.auth.setUser(user);
  //     if (user) {
  //       props.history.push(forward);
  //     }
  //   } catch (err) {
  //     console.log(err);
  //     if (err === "No current user") {
  //       setErrors({
  //         cognito: "Wrong OTP, Please enter correct OTP",
  //         blankfield: error.blankfield,
  //         passwordmatch: error.passwordmatch,
  //       });
  //     }
  //     props.auth.setAuthStatus(false);
  //     props.auth.setUser(null);
  //     props.history.push("/hello");
  //   }
  // }

  // async function handleSubmit(event) {
  //   event.preventDefault();
  //   let userType = props.userType;

  //   try {
  //     const params = {
  //       username,
  //       password: getRandomString(7),
  //       attributes: {
  //         email: email,
  //         phone_number: phone_number,
  //         "custom:usertype": userType,
  //       },
  //     };
  //     await Auth.signUp(params);

  //     let temp = await Auth.signIn(username);
  //     setCognitoUser(temp);
  //     setStep(1);
  //   } catch (error) {
  //     console.log(error);
  //     if (error.message === "User already exists") {
  //       setErrors({
  //         cognito:
  //           "Username is not available. Consider using a different username",
  //         blankfield: error.blankfield,
  //         passwordmatch: error.passwordmatch,
  //       });
  //     }
  //   }
  // }

  async function handleSubmit(event) {
    event.preventDefault();
    let clientLeadStatus = await saveClientLead(
      username,
      email,
      phoneNumber,
      contactOnWhatsapp,
      propertyName
    );
    if (clientLeadStatus === "SUCCESS") {
      if (props.mobileLeadFormData) {
        props.mobileLeadFormData.setMobileLeadFormData(false);
      }
      if (props.history) {
        props.history.push("/thankyou");
      }
    } else {
    }
  }

  // function getRandomString(length) {
  //   var result = "";
  //   var characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
  //   var charactersLength = characters.length;
  //   for (var i = 0; i < length; i++) {
  //     result += characters.charAt(Math.floor(Math.random() * charactersLength));
  //   }
  //   return result;
  // }

  const handleOnBlur = (e) => {
    let temp = e.target.value.replace("-", "");
    temp = temp.replace(" ", "");
    if (temp.length === 13) {
      setPhone(temp);
      setError(false);
      setErrorMessage("");
    } else {
      setError(true);
      setErrorMessage("Enter a valid phone number");
    }
  };

  const handleOnChange = (value) => {
    if (value.length === 13) {
      setError(false);
      setErrorMessage("");
    }
  };
  return (
    <div>
      <LeadForm>
        <IntroParagraph style={{ fontWeight: "bold", marginBottom: "1vw" }}>
          {headerText}
        </IntroParagraph>
        {/* <FormErrors formerrors={errors} /> */}
        {/* {step === 0 && ( */}
        <form onSubmit={handleSubmit} noValidate>
          <TextField
            variant="outlined"
            size="small"
            margin="normal"
            required
            fullWidth
            id="name"
            label="Your Name"
            name="name"
            autoComplete="username"
            value={username}
            onChange={(event) => setUsername(event.target.value)}
          />
          <TextField
            variant="outlined"
            size="small"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            value={email}
            onChange={(event) => {
              if (!event.target.value.match(mailformat)) {
                setEmailFormatError(true);
                setEmailFormatErrorMessage("Enter a valid Email");
              } else {
                setEmailFormatError(false);
                setEmailFormatErrorMessage("");
              }
              setEmail(event.target.value);
            }}
            error={emailFormatError}
            helperText={emailFormatErrorMessage}
          />
          <MaterialUiPhoneNumber
            variant="outlined"
            size="small"
            margin="normal"
            value={phoneNumber}
            defaultCountry={"in"}
            onBlur={handleOnBlur}
            onChange={handleOnChange}
            label="Phone Number"
            fullWidth
            error={error}
            helperText={errorMessage}
          />
          <FormControlLabel
            control={
              <Checkbox
                checked={contactOnWhatsapp}
                onChange={(event) => setContactOnWhatsapp(event.target.checked)}
                sx={{
                  color: "rgba(123,131,97,1)",
                  "&.Mui-checked": { color: "rgba(123,131,97,1)" },
                }}
              />
            }
            label="Send me Whatsapp Updates"
          />
          <TextField
            variant="outlined"
            size="small"
            margin="normal"
            required
            fullWidth
            id="propert name"
            label="Property Name"
            name="property name"
            value={propertyName}
            onChange={(event) => setPropertyName(event.target.value)}
          />
          <div style={{ paddingTop: "20px" }}>
            <CurvedButton
              type="submit"
              background="rgba(255,92,92,1)"
              light
              curve
              fontBig
              margin="0px"
              
              onClick={() => {
                setSubmitting(true);
              }}
            >
              {submitting && (
                <SpinningFaSpinner
                  style={{ marginRight: "5px" }}
                />
              )}
              {submitting && <span>Get Started</span>}
              {!submitting && <span>Get Started</span>}
            </CurvedButton>
          </div>
          <Box mt={5}>
            <Copyright />
          </Box>
        </form>
        {/* )}

        {step === 1 && (
          <form onSubmit={confirmSignup} noValidate>
            <TextField
              variant="outlined"
              margin="normal"
              size="small"
              required
              fullWidth
              id="username"
              label="User Name"
              name="username"
              autoComplete="username"
              onChange={(event) => setUsername(event.target.value)}
              value={username}
              autoFocus
            />
            <TextField
              variant="outlined"
              size="small"
              margin="normal"
              required
              fullWidth
              id="authenticationCode"
              label="Please check your email for authetication Code"
              name="authenticationCode"
              onChange={(event) => setAuthenticationCode(event.target.value)}
              autoFocus
            />
            <CurvedButton
              type="submit"
              background="rgba(255,92,92,1)"
              light
              curve
              fontBig
              margin="0px"
            >
              Book Online Consultation
            </CurvedButton>
            <Box mt={5}>
              <Copyright />
            </Box>
          </form>
        )} */}
      </LeadForm>
    </div>
  );
}

export default ClientSignUpForm;

const LeadForm = styled.div`
  text-align: left;
`;

const spin = keyframes`

  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`;

const SpinningFaSpinner = styled(FaSpinner)`
  animation: ${spin} infinite 5s linear;
`;