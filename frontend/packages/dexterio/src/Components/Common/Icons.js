export const Logo = (props) => (
  <img alt="Logo" src="/static/logo.svg" {...props} />
);

export const ArrowRight = (props) => (
  <img alt="ArrowRight" src="/static/icons/arrow-right-solid.svg" {...props} />
);

export const DexterioHand = (props) => (
  <img
    alt="DexterioHand"
    src="/static/icons/dexterio_hand_35.svg"
    style={{ height: "35px", transform: "translateX(-5%)" , filter: "invert(1)"}}
    {...props}
  />
);

export const Home = (props) => (
  <img
    alt="DexterioHand"
    src="/static/icons/home.svg"
    style={{ height: "100%" }}
    {...props}
  />
);

export const Blog = (props) => (
  <img
    alt="Blog"
    src="/static/icons/magazine.svg"
    style={{ height: "100%" }}
    {...props}
  />
);

export const Projects = (props) => (
  <img
    alt="Projects"
    src="/static/icons/project-1.svg"
    style={{ height: "100%" }}
    {...props}
  />
);

export const HowItWorks = (props) => (
  <img
    alt="Blog"
    src="/static/icons/how-it-works.svg"
    style={{ height: "100%" }}
    {...props}
  />
);

export const GoldenStar = (props) => (
  <img
    alt="GoldenStar"
    src="/static/icons/3DStarYellow.svg"
    style={{ height: "20px" }}
    {...props}
  />
);
