import React from "react";
import { FlexRow } from "common";

function ThankYou() {
  return (
    <FlexRow style={{ backgroundColor: "rgb(245, 243, 237)", height: "100vh", justifyContent:"center"}}>
      <div style={{height: "100%" }}> 
        <img
          src="/thankyou.svg"
          alt="Interior Designing Client Renovation"
          style={{height: "100%"}}
        />
      </div>
    </FlexRow>
  );
}

export default ThankYou;
