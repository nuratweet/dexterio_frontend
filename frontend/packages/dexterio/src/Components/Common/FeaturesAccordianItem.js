import { FeatureMobileAccordian , FeatureMobileAccordianHeader, FeatureMobileAccordianCard,
    FeatureMobileAccordianCardImg, FeatureMobileAccordianCardImgWrap, FeatureMobileAccordianCardBody,
    FeatureMobileAccordianCardData, FeatureMobileAccordianCardWrap} from "../Home/FeaturesPage/FeaturesPage.element";

const FeaturesAccordionItem = ({ item, active, onToggle}) => {
    const { image, header, content, color } = item;

    return (
        <FeatureMobileAccordian >
            <FeatureMobileAccordianHeader onClick={onToggle} active = {active}>
                <span>{header}</span>
                <svg style={{"display":"inline-block","stroke":"currentColor","fill":"currentColor","width":"12px","height":"12px"}} viewBox="0 0 1024 1024">
                    <path d="M896 261.504l-384 384-384-384-90.496 90.496 429.248 429.248c24.992 24.984 65.504 24.984 90.496 0l429.248-429.248-90.496-90.496z">
                    </path>
                </svg>
            </FeatureMobileAccordianHeader>
            <FeatureMobileAccordianCard active = {active}>
                <FeatureMobileAccordianCardWrap>
                    <FeatureMobileAccordianCardData style={{"backgroundColor":{color}}}>
                        <FeatureMobileAccordianCardBody>
                            <FeatureMobileAccordianCardImgWrap>
                                <FeatureMobileAccordianCardImg src={image} alt="icon" width="76" height="80"/>
                            </FeatureMobileAccordianCardImgWrap>
                            <p>{content}</p>
                        </FeatureMobileAccordianCardBody>
                    </FeatureMobileAccordianCardData>
                </FeatureMobileAccordianCardWrap>
            </FeatureMobileAccordianCard>
        </FeatureMobileAccordian>
    );
  };
  
  export default FeaturesAccordionItem;