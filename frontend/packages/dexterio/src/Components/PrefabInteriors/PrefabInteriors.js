import React from 'react';

const PrefabInteriors = () => {
    return (
        <div className='parent-container'>
            <div className = "section-wrapper">
                <div className = "section-slider">
                    <section className='prefab-container'>
                        <div className='background-overlay'></div>
                        <div className='prefab-column'>
                            <div className='content-side'>
                                 <div className='content-wrapper'>
                                     
                                 </div>
                            </div>
                            <div className="image-side">

                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    );
};

export default PrefabInteriors;