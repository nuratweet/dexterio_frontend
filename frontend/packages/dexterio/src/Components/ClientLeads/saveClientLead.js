import axios from "axios";

const config = require("../../config_prod.json");

const saveClientLead = async (
  username,
  email,
  phonenumber,
  contactOnWhatsapp,
  property
) => {
  let clientLeadStatus = "";
  try {
    const params = {
      username: username,
      email: email,
      phoneNumber: phonenumber,
      contactOnWhatsapp: contactOnWhatsapp,
      property: property,
    };

    console.log(params);
    await axios
      .post(`${config.api.invokeUrl}/saveclientlead/`, params)
      .then((data) => {
        console.log(data.status);
        if (data.status === 201) {
          clientLeadStatus = "SUCCESS";
        } else {
          clientLeadStatus = "FAILURE";
        }
      });
  } catch (error) {
    console.log(`Error occured: ${error}`);
  }
  return clientLeadStatus;
};

export default saveClientLead;
