import styled,  {keyframes} from 'styled-components';
import { FontSizeHeader1 } from '../../../globalStyles';

const Base = styled.section`
    --element-start-at: 60px;
    --hero-img-height: 500px;
    --header-height: 80px;
`;

export const InfoSec = styled(Base)`

    padding: var(--element-start-at) 0;
    background-color: #f7f8fa;
    
    @media only screen and (max-width: 768px) {
        margin: -95px 12px 12px;
        padding: 32px 16px;
        border-radius: 10px;
        text-align: left;
    }
`;

export const InfoRow = styled.div`
    display: flex;
    margin: 0 -15px -15px -15px;
    flex-wrap: wrap;
    align-items: center;
    flex-direction: row;
`;

export const InfoColumn = styled.div`
    margin-bottom: 15px;
    padding-right: 15px;
    padding-left: 56px;
    flex: 1;
    max-width: ${({align}) => (align)};
    flex-basis: ${({align}) => (align)};
    display: flex;
    justify-content: ${({justify}) => (justify)};


    @media screen and (max-width: 1366px) {
        padding-left: 37px;
    }

    @media screen and (max-width: 1280px) {
        padding-left: 20px;
    }

    @media screen and (max-width: 1024px) {
        padding-left: 5px;
    }

    @media (max-width: 768px) {
        margin-bottom: 0px;
    }

    @media screen and (max-width: 768px) {
        max-width: 100%;
        flex-basis: 100%;
    }
`;

const spin_words = keyframes`

    15%{
        transform: translateY(-112%);
    }
    50%{
        transform: translateY(-100%);
    }
    65%{
        transform: translateY(-212%);
    }
    100%{
        transform: translateY(-200%);
    }
`;

export const Heading = styled(FontSizeHeader1)`
    
    text-align: left;
    font-family: Poppins;
    font-style: normal;
    font-weight: bold;
    font-size: 54px;
    color: rgba(0,0,0,1);
    line-height:80px;

    & > span {
        animation: ${spin_words} 4s 1s 1;
        animation-fill-mode: forwards;
    }

    & > div {
        box-sizing: content-box;
        height: 65px;
        display: flex;
        @media screen and (max-width: 768px) {
            height: 40px;
        }

        & > div {
            overflow: hidden;
            & > div {
                & >span {
                    color: #rgba(0,0,0,1);
                    white-space:nowrap;
                    display: block;
                    height: 100%;
                    font-size: 80px !important;
                    font-family: "Poppins", sans-serif;
                    font-weight: bold;
                    font-style:normal;
                    text-align:left;
                    line-height: 100px;
                    letter-spacing: 0px;
                    
                    padding-left: 0px;
                    animation: ${spin_words} 4s 1s 1;
                    animation-fill-mode: forwards;
         
                    @media screen and (max-width: 1024px) {
                        font-size: 46px;
                    }
                    
                    @media screen and (max-width: 768px) {
                        font-size: 40px;
                    }

                    @media screen and (max-width: 430px) {
                        font-size: 35px;
                    }

                    @media screen and (max-width: 375px) {
                        font-size: 30px;
                    }
                }
            }
        }
    }
`;

export const Subtitle = styled.p`
    margin-top:10px;
    max-width:440px;
    margin-top: 45px;
    margin-bottom: 55px;
    color: #000707DE;
    font-family: "Open Sans", sans-serif;
    font-size: 18px;
    font-weight: 600;
    line-height: 1.6em;

    @media screen and (max-width: 1024px) {
        font-size: 20px;
    }
    
    @media screen and (max-width: 768px) {
        font-size: 18px;
    }

    @media screen and (max-width: 430px) {
        font-size: 16px;
    }

    @media screen and (max-width: 375px) {
        font-size: 14px;
    }
`;

export const ImgWrapperMobile = styled.div`
    display: flex;
    justify-content: center;
    background-position: top;
    background-size: cover;
    
    @media only screen and (max-width: 425px) {
        max-width: 425px;
    }

    @media only screen and (max-width: 375px) {
        max-width: 375px;
    }

    @media only screen and (max-width: 360px) {
        max-width: 360px;
    }

    @media only screen and (min-width: 769px) {
        display: none;
    }
`;

export const ImgWrapperDesktop = styled.div`
    display: flex;
    justify-content: center;

    @media only screen and (max-width: 768px)
    {
        display: none;
        background-position: top;
        background-size: cover;
        height: 360px;
    }
`;

export const Img = styled.img`
    padding-right: 0;
    border: 0;
    max-width:100%;
    vertical-align: middle;
    display: inline-block;
    max-height: var(--hero-img-height);
    z-index: 10;

    @media only screen and (max-width: 1024px)
    {
        margin-right: 0px;
    }

    @media only screen and (max-width: 767px)
    {
        z-index: -1
    }
`;

export const HeroPattern = styled.span`
    width: 300px;
    height: 300px;
    background: url(/hero-pattern.svg) left 4em bottom 6em / 200px no-repeat;
    display: flex;
    position: absolute;
    right: calc(32%);
    top: calc(var(--element-start-at) + calc(var(--hero-img-height)*0.75) + var(--header-height));
    bottom: 40px;
    z-index: 2;
    opacity: 1;
    transform: none;

    @media(max-width: 2560px) {
        right: calc(40.81%);
    }

    @media(max-width: 1920px) {
        right: calc(37.9%);
    }

    @media(max-width: 1680px) {
        right: calc(36.1%);
    }

    @media(max-width: 1600px) {
        right: calc(35.3%);
    }

    @media(max-width: 1536px) {
        right: calc(34.8%);
    }

    @media(max-width: 1440px) {
        right: calc(33.8%);
    }

    @media(max-width: 1366px) {
        right: calc(34.4%);
    }

    @media(max-width: 1280px) {
        right: calc(34.8%);
    }

    @media(max-width: 1100px) {
        right: calc(29.7%);
    }

    @media(max-width: 1024px) {
        display: none;
    }

    &::before{
        box-sizing: border-box;
    }

    &::after{
        box-sizing: border-box;
    }
`;