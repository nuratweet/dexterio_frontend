import React from "react";
import { IntroParagraph, IntroHeadlines, CurvedButton } from "globalstyles";
import { Link } from "react-scroll";
import {
  SvgWrapperRectangles,
  LeftPanel,
  TextWrapper,
  FlexColumn,
  ImageWrapper,
  ImgDesktop,
  ImgMobile,
} from "common";

function IntroPageNew(props) {
  const [button, setbutton] = React.useState(true);

  const showButton = () => {
    if (window.innerWidth <= 768) {
      setbutton(false);
    } else {
      setbutton(true);
    }
  };

  React.useEffect(() => {
    showButton();
    return () => {
      window.removeEventListener("resize", () => {});
    };
  });

  window.addEventListener("resize", showButton);

  const heroImageArray = [
    "/estimate_2.webp",
    "/estimate_3.webp",
    "/estimate_4.webp",
    "/estimate_5.webp",
  ];

  const heroImageMobileArray = [
    "/projects_mobile_1.jpg",
    "/projects_mobile_2.jpg",
    "/projects_mobile_3.jpg",
    "/projects_mobile_4.png",
  ];

  const getRandomHeroImageDesktop = () => {
    return heroImageArray[Math.floor(Math.random() * heroImageArray.length)];
  };

  const getRandomHeroImageMobile = () => {
    return heroImageMobileArray[
      Math.floor(Math.random() * heroImageArray.length)
    ];
  };

  React.useEffect(() => {
    const typedTextSpan = document.querySelector(".typed-text");
    const cursorSpan = document.querySelector(".cursor");

    const textArray = ["easy", "fast", "affordable"];
    const typingDelay = 200;
    const erasingDelay = 100;
    const newTextDelay = 2000;
    let textArrayIndex = 0;
    let charIndex = 0;

    function type() {
      if (charIndex < textArray[textArrayIndex].length) {
        if (!cursorSpan.classList.contains("typing")) {
          cursorSpan.classList.add("typing");
        }

        typedTextSpan.textContent +=
          textArray[textArrayIndex].charAt(charIndex);
        charIndex++;
        setTimeout(() => {
          type();
        }, typingDelay);
      } else {
        cursorSpan.classList.remove("typing");
        setTimeout(() => {
          erase();
        }, newTextDelay);
      }
    }

    function erase() {
      if (charIndex > 0) {
        if (!cursorSpan.classList.contains("typing"))
          cursorSpan.classList.add("typing");
        typedTextSpan.textContent = textArray[textArrayIndex].substring(
          0,
          charIndex - 1
        );
        charIndex--;
        setTimeout(() => {
          erase();
        }, erasingDelay);
      } else {
        cursorSpan.classList.remove("typing");
        textArrayIndex++;
        if (textArrayIndex >= textArray.length) textArrayIndex = 0;
        setTimeout(() => {
          type();
        }, typingDelay + 1100);
      }
    }

    if (textArray.length)
      setTimeout(() => {
        type();
      }, newTextDelay);
  }, []);

  return (
    <div
      style={{
        position: "relative",
        overflow: "hidden",
      }}
    >
      <LeftPanel>
        <div>
          <TextWrapper style={{ paddingBottom: "0" }}>
            <IntroHeadlines spanColor="rgba(123,131,97,1)">
              Interiors <br />
              made
              <br />
              <span className="typed-text"></span>
              <span className="cursor">&nbsp;</span>
            </IntroHeadlines>
            <IntroParagraph>
              Welcome to the one stop solution for all your interior needs.
              <br />
              Unlock your dream home with Dexterio
            </IntroParagraph>
            {button ? (
              <Link to="client_signup" spy={true} smooth={true}>
                <CurvedButton
                  background="rgba(255,92,92,1)"
                  light
                  curve
                  fontBig
                  margin="0px"
                  style = {{padding:"6px 29px", fontSize:"1.10vw"}}
                >
                  Book Online Consultation
                </CurvedButton>
              </Link>
            ) : (
              <CurvedButton
                background="rgba(255,92,92,1)"
                light
                curve
                fontBig
                margin="0px"
                style={{ marginTop: "6vw" }}
                onClick={() => {
                  props.mobileLeadFormData.setMobileLeadFormData(true);
                }}
              >
                Book Online Consultation
              </CurvedButton>
            )}
          </TextWrapper>
        </div>
      </LeftPanel>
      <FlexColumn style={{ zIndex: "-100" }}>
        <ImageWrapper>
          <ImgDesktop src={getRandomHeroImageDesktop()} alt="How it works" />
          <ImgMobile src={getRandomHeroImageMobile()} alt="How it works" />
        </ImageWrapper>
        <div style={{ height: "50px" }}></div>
      </FlexColumn>
      <SvgWrapperRectangles style={{ transform: "translateY(-50%)" }}>
        <img src="/Rectangles.svg" width="125" height="125" alt="" />
      </SvgWrapperRectangles>
    </div>
  );
}

export default IntroPageNew;
