import styled from "styled-components";
import PropTypes from 'prop-types';
import React from "react";
import { CM_CENTER_CENTER, CM_TOP_LEFT,  CM_TOP_CENTER, CM_TOP_RIGHT} from '.';
import SignUpCard from "./SignUpCard";

const Model = styled.div`
    display: ${({show}) => (show ? 'block' : 'none')};
    position: fixed;
    top: 0;
    left: 0;
    height: 100vh;
    width:100vw;
    background: rgba(0,0,0,0.7);
`;

const Container = styled.div` 
    position:fixed;
    background: #fff;
    width: 40vw;
    height: auto;
    
    top: ${({openPos}) => (
    {
        [CM_CENTER_CENTER]: '50%',
        [CM_TOP_LEFT]: '10%',
        [CM_TOP_CENTER]: '10%',
        [CM_TOP_RIGHT]: '10%'
    }[openPos])};
    
    left: ${({openPos}) => (
    {
        [CM_CENTER_CENTER]: '50%',
        [CM_TOP_LEFT]: '5%',
        [CM_TOP_CENTER]: '50%',
        [CM_TOP_RIGHT]: '95%'
    }[openPos])};
  
    transform: ${({openPos}) => (
    {
        [CM_CENTER_CENTER]: 'translate(-50%,-50%)',
        [CM_TOP_LEFT]: 'translate(0,0)',
        [CM_TOP_CENTER]: 'translate(-50%,0)',
        [CM_TOP_RIGHT]: 'translate(-100%,0)'
    }[openPos])};
    border-radius: 8px;
    overflow: hidden;
    padding: 0rem;
`;

export default function ConfirmationModalImpl(props) {
    const {
        handleClose,
        show,
        openPos
    } = { ...props };

    return (
      <Model show={show}>
        <Container openPos={openPos}>
          <SignUpCard {...props} handleClose={handleClose} />
        </Container>
      </Model>
    );
}
ConfirmationModalImpl.propTypes = {
    handleClose: PropTypes.func.isRequired,
    show: PropTypes.bool.isRequired,
    openPos: PropTypes.symbol.isRequired
};