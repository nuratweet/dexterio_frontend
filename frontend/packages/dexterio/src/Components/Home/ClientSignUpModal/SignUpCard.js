import {
  Typography,
  Box,
  TextField,
  FormControlLabel,
  Checkbox,
} from "@material-ui/core";
import { FlexRow, FlexColumn, MaterialUiPhoneNumber } from "common";
import React, { useState } from "react";
import "./SignUpCard.css";
import { VerticalSpacing, CurvedButton, IntroParagraph } from "globalstyles";
import styled from "styled-components";
import saveClientLead from "../../ClientLeads/saveClientLead";
import { FaSpinner } from "react-icons/fa";


function Copyright() {
  return (
    <>
      <Typography
        variant="body2"
        color="textSecondary"
        align="center"
        style={{
          fontSize: "12px",
          color: "rgba(0,0,0,1)",
          letterSpacing: "1.2px",
        }}
      >
        {
          "By submitting this form, you agree to the privacy policy, terms and conditions"
        }
      </Typography>
    </>
  );
}

function SignUpCard(props) {
  const { handleClose } = { ...props };

  let mailformat = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [emailFormatError, setEmailFormatError] = useState(false);
  const [emailFormatErrorMessage, setEmailFormatErrorMessage] = useState("");
  const [phoneNumber, setPhone] = useState("");
  const [propertyName, setPropertyName] = useState("");
  const [error, setError] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const [contactOnWhatsapp, setContactOnWhatsapp] = useState(true);
  const [submitting, setSubmitting] = useState(false);


  async function handleSubmit(event) {
    event.preventDefault();
    let clientLeadStatus = await saveClientLead(
      username,
      email,
      phoneNumber,
      contactOnWhatsapp,
      propertyName
    );
    if (clientLeadStatus === "SUCCESS") {
      props.history.push("/thankyou");
    } else {
    }
  }

  const handleOnBlur = (e) => {
    let temp = e.target.value.replace("-", "");
    temp = temp.replace(" ", "");
    if (temp.length === 13) {
      setPhone(temp);
      setError(false);
      setErrorMessage("");
    } else {
      setError(true);
      setErrorMessage("Enter a valid phone number");
    }
  };

  const handleOnChange = (value) => {
    if (value.length === 13) {
      setError(false);
      setErrorMessage("");
    }
  };

  return (
    <FlexRow>
      <FlexColumn style={{ width: "40%" }}>
        <img
          src="/client_signup_modal.jpg"
          alt="Sign Up"
          style={{ objectFit: "contain" }}
        />
      </FlexColumn>
      <FlexColumn style={{ padding: "1.5vw", width: "60%" }}>
        <FlexRow
          style={{
            width: "100%",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <img
            src="/dexterio_hand.png"
            alt="Dexterio Hand"
            style={{ width: "40px", height: "40px" }}
          />
          <Logo style = {{marginleft:"5px"}}>DEXTERIO</Logo>
        </FlexRow>
        <VerticalSpacing height="1vw" />
        <FlexRow style={{ justifyContent: "center" }}>
          <IntroParagraph style={{ marginBottom: "0", width: "max-content" }}>
            Dreams made into reality
          </IntroParagraph>
        </FlexRow>
        <VerticalSpacing height="0.5vw" />
        <form onSubmit={handleSubmit} noValidate>
          <FlexColumn style={{ padding: "0 1.5vw" }}>
            <TextField
              variant="outlined"
              margin="dense"
              size="small"
              required
              id="username"
              label="User Name"
              name="username"
              fullWidth
              autoComplete="username"
              value={username}
              onChange={(event) => setUsername(event.target.value)}
            />
            <TextField
              variant="outlined"
              margin="dense"
              size="small"
              required
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              fullWidth
              value={email}
              onChange={(event) => {
                if (!event.target.value.match(mailformat)) {
                  setEmailFormatError(true);
                  setEmailFormatErrorMessage("Enter a valid Email");
                } else {
                  setEmailFormatError(false);
                  setEmailFormatErrorMessage("");
                }
                setEmail(event.target.value);
              }}
              error={emailFormatError}
              helperText={emailFormatErrorMessage}
            />
            <MaterialUiPhoneNumber
              variant="outlined"
              margin="dense"
              size="small"
              value={phoneNumber}
              defaultCountry={"in"}
              onBlur={handleOnBlur}
              onChange={handleOnChange}
              label="Phone Number"
              fullWidth
              error={error}
              helperText={errorMessage}
            />
            <FormControlLabel
              control={
                <Checkbox
                  checked={contactOnWhatsapp}
                  onChange={(event) =>
                    setContactOnWhatsapp(event.target.checked)
                  }
                  sx={{
                    color: "rgba(123,131,97,1)",
                    "&.Mui-checked": { color: "rgba(123,131,97,1)" },
                  }}
                />
              }
              label="Send me Whatsapp Updates"
            />
            <TextField
              variant="outlined"
              size="small"
              margin="normal"
              required
              fullWidth
              id="propert name"
              label="Property Name"
              name="property name"
              value={propertyName}
              onChange={(event) => setPropertyName(event.target.value)}
            />
          </FlexColumn>
          <VerticalSpacing height="0.5vw" />
          <FlexRow style={{ justifyContent: "center" }}>
            <CurvedButton
              background="rgba(255,92,92,1)"
              light
              curve
              margin="0px"
              type="submit"
              onClick={() => {
                setSubmitting(true);
              }}
            >
              {submitting && (
                <FaSpinner style={{ marginRight: "5px" }} className="spinner" />
              )}
              {submitting && <span>Get Started</span>}
              {!submitting && <span>Get Started</span>}
            </CurvedButton>
          </FlexRow>
          <Box mt={4}>
            <Copyright />
          </Box>
        </form>
      </FlexColumn>
      <div className="action">
        <button
          className="closeButton"
          onClick={() => {
            handleClose(true);
          }}
        >
          <span className="toggle-bar"></span>
          <span className="toggle-bar"></span>
        </button>
      </div>
    </FlexRow>
  );
}

export default SignUpCard;

const Logo = styled.div`
  text-decoration: none;
  display: flex;
  white-space: nowrap;
  text-align: left;
  font-family: "Montserrat";
  font-style: normal;
  font-weight: 600;

  font-size: 15px;
  color: black;
  letter-spacing: 6px;
`;
