import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { CM_CENTER_CENTER } from ".";
import ConfirmationModalImpl from "./ConfirmationModalImpl";

const ClientSignUpModal = (props) => {
  useEffect(() => {
    setup();
    return () => {
      window.removeEventListener("mousemove", () => {});
      window.removeEventListener("mousedown", () => {});
      window.removeEventListener("keypress", () => {});
      window.removeEventListener("DOMMouseScroll", () => {});
      window.removeEventListener("mousewheel", () => {});
      window.removeEventListener("touchmove", () => {});
      window.removeEventListener("MSPointerMove", () => {});
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  var timeoutID;

  function setup() {
    window.addEventListener("mousemove", resetTimer, false);
    window.addEventListener("mousedown", resetTimer, false);
    window.addEventListener("keypress", resetTimer, false);
    window.addEventListener("DOMMouseScroll", resetTimer, false);
    window.addEventListener("mousewheel", resetTimer, false);
    window.addEventListener("touchmove", resetTimer, false);
    window.addEventListener("MSPointerMove", resetTimer, false);

    startTimer();
  }

  function startTimer() {
    timeoutID = window.setTimeout(goInactive, 15000);
  }
  function resetTimer(e) {
    window.clearTimeout(timeoutID);
    goActive();
  }
  function goInactive() {
    if (!visited) {
      visited = true;
      setShow(true);
    }
  }
  function goActive() {
    startTimer();
  }

  let visited = false;
  const [show, setShow] = useState(false);

  const hideModal = (arg) => {
    setShow(false);
  };

  return (
    <>
      <SignUpModal>
        <ConfirmationModalImpl
          {...props}
          handleClose={hideModal}
          show={show}
          openPos={CM_CENTER_CENTER}
        />
      </SignUpModal>
    </>
  );
};

export default ClientSignUpModal;

export const SignUpModal = styled.div`
  position: fixed;
  bottom: 20%;
  left: 0;
  right: 0;
  z-index: 9998;
  @media only screen and (max-width: 600px) {
    display: none;
  }
`;
