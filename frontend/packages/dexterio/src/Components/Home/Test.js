import React from 'react'
import './Test.css'

function Test() {
    return (
        <div className="cmp cmp-c25-full-width-asset aem-GridColumn aem-GridColumn--default--12">
            <section className="b-fullWidthAsset" data-component="c25-full-width-asset" data-scroll-component="" style={{"min-height": "unset", "height": "100vh"}}>
                <div className="m-stickyMedia" data-component="m03-sticky-media" data-scroll-component="">
                    <div className="m-stickyMedia__container">
                        <div className="m-stickyMedia__background" data-sticky-background="" style={{"transform": "matrix(1, 0, 0, 1, 0, -208)"}}>
                            <div data-component="a01-image" className="a-image" data-enable-transition="false">
                                <picture className="a-image__picture" data-image-wrapper="">
                                    <source srcset="https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=250&amp;hei=170 250w,https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=345&amp;hei=235 345w,https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=500&amp;hei=340 500w,https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=690&amp;hei=470 690w" sizes="100vw" media="(max-width: 345px)"/>
                                    
                                    <source srcset="https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=240&amp;hei=163 240w,https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=360&amp;hei=245 360w,https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=479&amp;hei=326 479w,https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=480&amp;hei=327 480w,https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=720&amp;hei=490 720w,https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=958&amp;hei=652 958w" sizes="100vw" media="(max-width: 479px)"/>
                                    
                                    <source srcset="https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=384&amp;hei=261 384w,https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=540&amp;hei=368 540w,https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=767&amp;hei=522 767w,https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=768&amp;hei=523 768w,https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=1080&amp;hei=735 1080w,https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=1534&amp;hei=1044 1534w" sizes="100vw" media="(max-width: 767px)"/>
                                    
                                    <source srcset="https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=512&amp;hei=348 512w,https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=768&amp;hei=523 768w,https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=1023&amp;hei=696 1023w,https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=1024&amp;hei=697 1024w,https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=1536&amp;hei=1045 1536w" sizes="100vw" media="(max-width: 1023px)"/>
                                    
                                    <source srcset="https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=768&amp;hei=523 768w,https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=1024&amp;hei=697 1024w,https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=1130&amp;hei=769 1130w,https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=1239&amp;hei=843 1239w,https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=1536&amp;hei=1045 1536w" sizes="100vw" media="(max-width: 1239px)"/>
                                    
                                    <source srcset="https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=768&amp;hei=523 768w,https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=1024&amp;hei=697 1024w,https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=1240&amp;hei=844 1240w,https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=1439&amp;hei=979 1439w,https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=1536&amp;hei=1045 1536w" sizes="100vw" media="(max-width: 1439px)"/>
                                    
                                    <source srcset="https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=768&amp;hei=523 768w,https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=1024&amp;hei=697 1024w,https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=1240&amp;hei=844 1240w,https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=1440&amp;hei=980 1440w,https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=1536&amp;hei=1045 1536w,https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=1599&amp;hei=1088 1599w" sizes="100vw" media="(max-width: 1599px)"/>
                                    
                                    <source srcset="https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=768&amp;hei=523 768w,https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=1536&amp;hei=1045 1536w,https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=1600&amp;hei=1089 1600w,https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=1680&amp;hei=1143 1680w,https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=1920&amp;hei=1307 1920w" sizes="100vw"/>
                                <img className="a-image__img" data-image-img="" src="https://s7e5a.scene7.com/is/image/neomstage/C25%20-%20Full%20width%20asset?wid=1920&amp;hei=1307" alt="Find your team" loading="lazy"/>
                                </picture>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="b-fullWidthAsset__container" data-content-container="">
                    <div data-component="m04-component-header" className="m-componentHeader -center" data-alignment="center">
                        <div className="m-componentHeader__container">
                            <p className="a-eyebrow -large" data-component="a04-eyebrow" style={{"font-kerning": "none"}}>
                                FIND
                            </p>
                            <h2 className="a-heading -h4" data-component="a03-heading" style={{"font-kerning": "none"}}>
                                YOUR TEAM
                            </h2>
                            <p className="a-moustache -large " data-component="a05-moustache" style={{"font-kerning": "none"}}>
                                The most ambitious project in the world calls for the world’s most ambitious people. The work you do here will have a lasting impact not just on how we live our lives, but on the future of the planet itself.  If you believe in building a better world and have something to contribute toward it, then you could find yourself among the pioneers who are making their home with us in NEOM.
                            </p>
                        </div>
                    </div>
                    <div className="b-fullWidthAsset__buttons" data-buttons="" >
                        <a data-component="m02-button" href="https://careers.neom.com/search" rel="noopener"  className="m-button -primary -white -large -labelOnly " data-modal="null">
                            <span className="m-button__container" data-button-container="">
                            <span className="a-label -large" data-component="a07-label">
                                BROWSE ALL VACANCIES
                            </span>
                            <span className="ripple"></span></span>
                        </a>
                    </div>
                </div>
            </section>
        </div>
    )
}

export default Test
