import {
  GridWrapper,
  FeatureMobileWrapper,
  Grid,
  CardWrapper,
  FeatureMobileTab,
  FeatureMobilePanes,
  FeatureMobileContainer,
  FeatureMobileTabList,
  FeatureMobileTabs,
  FeatureMobileTabPanel,
  FeatureMobileTabPanelCardWrap,
  FeatureMobileTabPanelCard,
  FeatureMobileTabPanelCardBody,
  FeatureMobileTabPanelCardImgWrap,
  FeatureMobileAccordian,
} from "./FeaturesPage.element";
import React, { useEffect, useState } from "react";
import FeaturesAccordionItem from "../../Common/FeaturesAccordianItem";
import Lounger from "../../../assets/Lounger.svg";
import ZeroDelay from "../../../assets/Zero_Delay.svg";
import quality from "../../../assets/Quality.svg";
import priceTag from "../../../assets/Price_match.svg";
import consulting from "../../../assets/Design.svg";
import warranty from "../../../assets/Warranty.svg";
import { Link } from "react-scroll";
import {
  Container,
  VerticalSpacing,
  SectionHeadlines,
  SubSectionParagraph,
  CurvedButton,
  SubSectionHeadlines,
} from "globalstyles";

const FeaturesPage = (props) => {
  // eslint-disable-next-line no-unused-vars
  const [button, setbutton] = useState(true);
  const showButton = () => {
    if (window.innerWidth <= 768) {
      setbutton(false);
    } else {
      setbutton(true);
    }
  };
  useEffect(() => {
    showButton();
  });

  window.addEventListener("resize", showButton);

  const [stepActive, setStepActive] = useState("react-tabs-0");
  const [stepClicked, setStepClicked] = useState("0");

  const cardContent = [
    {
      image: Lounger,
      header: "We handle everything",
      content:
        "From designing through execution, selection of quality materials to getting vetted contractors. Sit back and relax. Let dexterio do the rest.",
      color: "#f2f8ff",
    },
    {
      image: ZeroDelay,
      header: "Zero delay",
      content:
        "We ensure quality execution, while keeping to timelines. Our delight is in living on your expectations.",
      color: "#f7faf5",
    },
    {
      image: quality,
      header: "Quality assured",
      content:
        " In house Quality monitoring system for Interiors - QMSIR. We provide the highest quality products from the best brands and manufacturers.",
      color: "#f2f8ff",
    },
    {
      image: consulting,
      header: "Design consultancy",
      content:
        "Collaborate with the industry's best interior designers to bring your dream interior to life.",
      color: "#fffbf2",
    },
    {
      image: warranty,
      header: "10 years warranty",
      content:
        "Unlike traditional constructors who went missing after the project ends, we take care of your interior even after our service ends.",
      color: "#f7faf5",
    },
    {
      image: priceTag,
      header: "Price guarantee",
      content:
        "No hidden costs. We deliver our services at a pre estimated quote. If not, we return the surplus.",
      color: "#f2f8ff",
    },
  ];

  return (
    <Container style={{ overflow: "hidden" }}>
      <SectionHeadlines
        style={{ textAlign: "center" }}
        spanColor="rgba(123,131,97,1)"
      >
        Come join India's <br />{" "}
        <span>
          Renovation Revolution
        </span>
      </SectionHeadlines>
      <VerticalSpacing height="4vw" />
      <FeatureMobileContainer>
        <FeatureMobileTabs>
          <div className="react-tabs" data-tabs="true">
            <FeatureMobileWrapper>
              <FeatureMobilePanes>
                <div>
                  <FeatureMobileTabList>
                    <FeatureMobileTab
                      id="react-tabs-0"
                      active={stepActive === "react-tabs-0" ? "true" : "false"}
                      onMouseEnter={() => setStepActive("react-tabs-0")}
                    >
                      <div style={{ opacity: "1", transform: "none" }}>
                        We Handle Everything
                      </div>
                    </FeatureMobileTab>
                    <FeatureMobileTab
                      id="react-tabs-2"
                      active={stepActive === "react-tabs-2" ? "true" : "false"}
                      onMouseEnter={() => setStepActive("react-tabs-2")}
                    >
                      <div style={{ opacity: "1", transform: "none" }}>
                        Zero delay
                      </div>
                    </FeatureMobileTab>
                    <FeatureMobileTab
                      id="react-tabs-4"
                      active={stepActive === "react-tabs-4" ? "true" : "false"}
                      onMouseEnter={() => setStepActive("react-tabs-4")}
                    >
                      <div style={{ opacity: "1", transform: "none" }}>
                        Quality assured
                      </div>
                    </FeatureMobileTab>
                    <FeatureMobileTab
                      id="react-tabs-6"
                      active={stepActive === "react-tabs-6" ? "true" : "false"}
                      onMouseEnter={() => setStepActive("react-tabs-6")}
                    >
                      <div style={{ opacity: "1", transform: "none" }}>
                        Design Consultancy
                      </div>
                    </FeatureMobileTab>
                    <FeatureMobileTab
                      id="react-tabs-8"
                      active={stepActive === "react-tabs-8" ? "true" : "false"}
                      onMouseEnter={() => setStepActive("react-tabs-8")}
                    >
                      <div style={{ opacity: "1", transform: "none" }}>
                        10 years warranty
                      </div>
                    </FeatureMobileTab>
                    <FeatureMobileTab
                      id="react-tabs-10"
                      active={stepActive === "react-tabs-10" ? "true" : "false"}
                      onMouseEnter={() => setStepActive("react-tabs-10")}
                    >
                      <div style={{ opacity: "1", transform: "none" }}>
                        Price match Guarantee
                      </div>
                    </FeatureMobileTab>
                  </FeatureMobileTabList>
                </div>
              </FeatureMobilePanes>
              <FeatureMobilePanes>
                <div style={{ opacity: "1", transform: "none" }}>
                  <FeatureMobileTabPanel
                    active={stepActive === "react-tabs-0" ? "true" : "false"}
                  >
                    <FeatureMobileTabPanelCardWrap>
                      <FeatureMobileTabPanelCard
                        style={{ backgroundColor: "#E0EBED" }}
                      >
                        <div className="card-body">
                          <FeatureMobileTabPanelCardImgWrap>
                            <img
                              src="/Tree2.png"
                              alt="icon"
                              width="76"
                              height="80"
                            />
                          </FeatureMobileTabPanelCardImgWrap>
                          <p>
                            From designing to execution, selection of quality
                            materials to getting vetted contractors. Sit back
                            and relax. Let dexterio do the rest.
                          </p>
                        </div>
                      </FeatureMobileTabPanelCard>
                    </FeatureMobileTabPanelCardWrap>
                  </FeatureMobileTabPanel>
                  <FeatureMobileTabPanel
                    active={stepActive === "react-tabs-2" ? "true" : "false"}
                  >
                    <FeatureMobileTabPanelCardWrap>
                      <FeatureMobileTabPanelCard
                        style={{ backgroundColor: "#ffebde" }}
                      >
                        <FeatureMobileTabPanelCardBody>
                          <FeatureMobileTabPanelCardImgWrap>
                            <img
                              src="/Clock2.png"
                              alt="icon"
                              width="68"
                              height="79"
                            />
                          </FeatureMobileTabPanelCardImgWrap>
                          <p>
                            We ensure superior quality execution, while keeping
                            to timelines. Our satisfaction is in living on your
                            expectations.
                          </p>
                        </FeatureMobileTabPanelCardBody>
                      </FeatureMobileTabPanelCard>
                    </FeatureMobileTabPanelCardWrap>
                  </FeatureMobileTabPanel>
                  <FeatureMobileTabPanel
                    active={stepActive === "react-tabs-4" ? "true" : "false"}
                  >
                    <FeatureMobileTabPanelCardWrap>
                      <FeatureMobileTabPanelCard
                        style={{ backgroundColor: "#E0EBED" }}
                      >
                        <FeatureMobileTabPanelCardBody shifted>
                          <FeatureMobileTabPanelCardImgWrap>
                            <img
                              src="/Ribbon2.png"
                              alt="icon"
                              width="68"
                              height="79"
                            />
                          </FeatureMobileTabPanelCardImgWrap>
                          <p>
                            In house Quality monitoring system for Interiors -
                            QMSIR. We provide the highest quality products from
                            the best brands and manufacturers.
                          </p>
                        </FeatureMobileTabPanelCardBody>
                      </FeatureMobileTabPanelCard>
                    </FeatureMobileTabPanelCardWrap>
                  </FeatureMobileTabPanel>
                  <FeatureMobileTabPanel
                    active={stepActive === "react-tabs-6" ? "true" : "false"}
                  >
                    <FeatureMobileTabPanelCardWrap>
                      <FeatureMobileTabPanelCard
                        style={{ backgroundColor: "#ebedfc" }}
                      >
                        <FeatureMobileTabPanelCardBody>
                          <FeatureMobileTabPanelCardImgWrap>
                            <img
                              src="/Pallette2.png"
                              alt="icon"
                              width="68"
                              height="79"
                            />
                          </FeatureMobileTabPanelCardImgWrap>
                          <p>
                            Collaborate with the industry's best interior
                            designers to bring your dream interior to life.
                          </p>
                        </FeatureMobileTabPanelCardBody>
                      </FeatureMobileTabPanelCard>
                    </FeatureMobileTabPanelCardWrap>
                  </FeatureMobileTabPanel>
                  <FeatureMobileTabPanel
                    active={stepActive === "react-tabs-8" ? "true" : "false"}
                  >
                    <FeatureMobileTabPanelCardWrap>
                      <FeatureMobileTabPanelCard
                        style={{ backgroundColor: "#f7faf5" }}
                      >
                        <FeatureMobileTabPanelCardBody>
                          <FeatureMobileTabPanelCardImgWrap>
                            <img
                              src="/Verified2.png"
                              alt="icon"
                              width="68"
                              height="79"
                            />
                          </FeatureMobileTabPanelCardImgWrap>
                          <p>
                            Unlike traditional constructors who went missing
                            after the project ends, we take care of your
                            interior even after our service ends.
                          </p>
                        </FeatureMobileTabPanelCardBody>
                      </FeatureMobileTabPanelCard>
                    </FeatureMobileTabPanelCardWrap>
                  </FeatureMobileTabPanel>
                  <FeatureMobileTabPanel
                    active={stepActive === "react-tabs-10" ? "true" : "false"}
                  >
                    <FeatureMobileTabPanelCardWrap>
                      <FeatureMobileTabPanelCard
                        style={{ backgroundColor: "#f2f8ff" }}
                      >
                        <FeatureMobileTabPanelCardBody>
                          <FeatureMobileTabPanelCardImgWrap>
                            <img
                              src="/Clipboard2.png"
                              alt="icon"
                              width="68"
                              height="79"
                            />
                          </FeatureMobileTabPanelCardImgWrap>
                          <p>
                            No hidden costs. We deliver our services at a pre
                            estimated quote. If not, we return the surplus.
                          </p>
                        </FeatureMobileTabPanelCardBody>
                      </FeatureMobileTabPanelCard>
                    </FeatureMobileTabPanelCardWrap>
                  </FeatureMobileTabPanel>
                </div>
              </FeatureMobilePanes>
            </FeatureMobileWrapper>
          </div>
        </FeatureMobileTabs>
        <FeatureMobileAccordian>
          <div style={{ opacity: "1", transform: "none" }}>
            {cardContent.map((faq, index) => (
              <div key={"Accordian_" + index}>
                <FeaturesAccordionItem
                  onToggle={() => setStepClicked(index)}
                  active={stepClicked === index}
                  item={faq}
                />
              </div>
            ))}
          </div>
        </FeatureMobileAccordian>
      </FeatureMobileContainer>
      <GridWrapper>
        <Grid>
          {cardContent.map((card, index) => (
            <CardWrapper
              bgColor=" rgba(245,243,237,1)"
              key={"CardWrapper_" + index}
              margin="0px 0px"
              padding="22px 22px"
            >
              <img
                width="25%"
                src={card.image}
                className="attachment-full size-full"
                alt=""
                loading="lazy"
              />
              <SubSectionHeadlines
                style={{ marginBottom: "2vw", width: "100%", textAlign:"left !important" }}
              >
                {card.header}
              </SubSectionHeadlines>
              <SubSectionParagraph
                style={{
                  marginBottom: "1vw",
                  marginTop: "0",
                  width: "100%",
                  textAlign: "left",
                  textJustify: "inter-word",
                }}
              >
                {card.content}
              </SubSectionParagraph>
            </CardWrapper>
          ))}
        </Grid>
      </GridWrapper>
      <div
        style={{ textAlign: "center", marginBottom: "0px", marginTop: "55px" }}
      >
        {button ? (
          <Link to="client_signup" spy={true} smooth={true}>
            <CurvedButton
           background="rgba(255,92,92,1)"
              light
              curve
              fontBig
              margin="0px"
              style = {{padding:"6px 29px", fontSize:"1.10vw"}}
            >
              Book Online Consultation
            </CurvedButton>
          </Link>
        ) : (
          <CurvedButton
            background="rgba(255,92,92,1)"
            light
            curve
            fontBig
            margin="0px"
            onClick={() => {
              props.mobileLeadFormData.setMobileLeadFormData(true);
            }}
          >
            Book Online Consultation
          </CurvedButton>
        )}
      </div>
    </Container>
  );
};

export default FeaturesPage;
