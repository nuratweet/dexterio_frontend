import styled from 'styled-components';
import {
    Container,
    SubSectionParagraph,
    SubSectionHeadlines
  } from "globalstyles";

export const FeaturesCarousel = styled.div`
    text-align : left;
    display: flex;
    flex-wrap: nowrap;
    overflow-x: auto;
  
    & > div {
      flex: 0 0 auto;
    }

    @media screen and (min-width: 1024px) {
        display:none;
    }
`;

export const GridWrapper = styled.div`
    margin: 0px 0px 30px 0px;

    @media screen and (max-width: 1366px) {
        margin: 0px 65px 30px 75px;
    }

    @media(max-width: 1280px) {
        margin: 0px 15px 30px 35px;
    }
    
    @media only screen and (max-width:1023px) {
        display:none;
    }
`;

export const Grid = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    grid-template-rows: max-content;
    row-gap: 3em;
    column-gap: 1em;
`;

export const FeatureMobileContainer = styled.div`
    width: 100%;
    max-width: 1200px;
    margin-left: auto;
    margin-right: auto;
    padding-left: 20px;
    padding-right: 20px;
    
    @media only screen and (min-width: 1024px) {
        display: none;
    }
`;

export const FeatureMobileTabs = styled.div`
    position: relative;
    @media only screen and (max-width: 767px) {
        display: none;
    }
`;

export const FeatureMobileAccordian = styled.div`
    position: relative;
    @media only screen and (min-width: 768px) {
        display: none;
    }
`;

export const FeatureMobileAccordianHeader = styled.h3 `

    margin-bottom: 1.25rem;
    margin-top: 0px;
    color: rgb(164, 172, 185);
    font-size: 3rem;
    font-weight: bold;
    line-height: 1.08;
    letter-spacing: -0.8px;
    transition: all 0.2s ease-in-out 0s;
    display: flex;
    -webkit-box-align: center;
    align-items: center;
    -webkit-box-pack: justify;
    justify-content: space-between;
    cursor: pointer;

    @media only screen and (max-width: 1199px) {
        font-size: calc(15.27px + 2.73vw);
    } 

    & > span {
        display: block;
        padding-right: 10px;
        color: rgba(28, 33, 31, 0.6);
    }

    & > svg {
        transition: all 0.2s ease-in-out 0s;
        transform: ${({active}) => active === true ? 'none' : 'rotate(-180deg)'};
    }
`;

export const FeatureMobileAccordianCard = styled.div`
    position: relative;
    
    opacity: ${({active})=> active === true ? 1 : 0};
    height: ${({active})=> active  === true ? 'auto' : '0px'};
    transition: height 1s;

`;

export const FeatureMobileAccordianCardWrap = styled.div`
    @media only screen and (max-width: 767px) {
        padding-bottom: 2.5rem;
    }
    position: relative;
`;

export const FeatureMobileAccordianCardData = styled.div`
    @media (max-width: 767px) {
        margin-left: auto;
        margin-right: auto;
    }

    border: none;
    border-radius: 0px;
    background-color: rgb(238, 238, 238);
    max-width: 460px;
    position: relative;
    z-index: 2;

`;

export const FeatureMobileAccordianCardBody = styled.div `
    
    @media (min-width: 400px) {
        font-size: 1.5rem;
        padding: 50px;
    }
    
    padding: 30px;
    font-size: 1.375rem;
    font-weight: 500;
    line-height: 1.33;
    letter-spacing: -0.4px;
`;

export const FeatureMobileAccordianCardImgWrap  = styled.div `

    @media (max-width: 767px){
        max-width: 60px;
        height: 60px;
        margin-bottom: 20px;
    }

    height: 80px;
    margin-bottom: 30px;
`;

export const FeatureMobileAccordianCardImg = styled.img`

    max-width: 100%;
    height: auto;

`;

export const FeatureMobileWrapper = styled.div`
    display: flex;
    flex-wrap: wrap;
    margin-right: -20px;
    margin-left: -20px;

    @media only screen and (min-width: 576px) {
        -webkit-box-align: center !important;
        align-items: center !important;
    }
`;

export const FeatureMobilePanes = styled.div`
    position: relative;
    width: 100%;
    min-height: 1px;
    padding-right: 20px;
    padding-left: 20px;

    @media only screen and (min-width: 576px) {
        flex: 0 0 50%;
        max-width: 50%;
    }
`;

export const FeatureMobileTabList = styled.ul`
    list-style: none;
    padding: 0px;
    margin: 0px;
    position: relative;
    z-index: 5;
`;

export const FeatureMobileTab = styled.li`
    position: relative;
    margin-bottom: 1.25rem;
    color: ${({active}) => active === 'true' ? '#111121' : '#a4acb9'};
    font-size: 3rem;
    font-weight: bold;
    line-height: 1.08;
    letter-spacing: -0.8px;
    transition: all 0.2s ease-in-out 0s;
    cursor: pointer;

    @media only screen and (max-width: 1199px) {
        font-size: calc(15.27px + 2.73vw);
    }

    &::after {
        content: "";
        position: absolute;
        inset: 0px -100vw;
    }
`;

export const FeatureMobileTabPanel = styled.div`
    display: ${({active}) => active === 'true' ? 'block' : 'none'};
    animation: 0.3s linear 0s 1 normal none running jBcSpD;
`;

export const FeatureMobileTabPanelCardWrap = styled.div`
    position:relative;
`;

export const FeatureMobileTabPanelCard = styled.div`
    border: none;
    border-radius: 0px;
    background-color: rgb(238, 238, 238);
    max-width: 460px;
    position: relative;
    z-index: 2;

    @media (min-width: 768px) {
        min-height: 396px;
    }
`;

export const FeatureMobileTabPanelCardBody = styled.div`

    @media (min-width: 400px){
        font-size: 1.5rem;
        padding: 50px;
    }

    padding: 30px;
    font-size: 1.375rem;
    font-weight: 500;
    line-height: 1.33;
    letter-spacing: -0.4px;
     & > p {
        margin-top: -20px;
     }
`;

export const FeatureMobileTabPanelCardImgWrap = styled.div`

    @media (min-width: 400px){
        font-size: 1.5rem;
        padding: 50px;
    }

    padding: 30px;
    font-size: 1.375rem;
    font-weight: 500;
    line-height: 1.33;
    letter-spacing: -0.4px;

    & > img {
        max-width: 100%;
        height: auto;
    }
`;

export const FeatureContainer = styled(Container)`
    margin-bottom: 55px;
    

    @media screen and (max-width: 450px) {
        margin-top: 50px;
        margin-bottom: 32px;
    }

`;

export const CardHeading = styled.div`
    font-family: Poppins;
    font-style: normal;
    font-weight: bold;
    font-size: 32px;
    color: rgba(0,0,0,1);
    line-height: 1.2em;
    letter-spacing: 0px;
    margin: 8% 0% 08% 0%;

    &: hover{
    font-family: Poppins;
    font-style: normal;
    font-weight: bold;
    font-size: 32px;
    color: rgba(245,243,237,1) !important;
    }

    @media(max-width: 768px) {
        margin-left: 0px;
    }
`;

export const CardWrapper = styled.div`
    width: 100%;
    overflow-x:hidden;
    border-radius: 9px;
    margin: ${({margin}) => margin};
    padding: ${({padding}) => padding};
    background: ${({bgColor}) => bgColor};
    font-family: Poppins;
    font-style: normal;
    font-weight: bold;
    font-size: 32px;
    cursor:pointer;


    &:hover {
            ${SubSectionHeadlines}{
                color: rgba(245,243,237,1) !important;
            }
    
            ${SubSectionParagraph}{
                color:white;
            }
        background: rgba(123,131,97,1);

        & > img {
            filter: invert(1) ;
        }
    }


`;

