import React from "react";
import {
  SectionHeadlines,
  DexterioEdgeContainer,
  SubSectionParagraph,
  DexterioEdgeSubSectionheadlines,
  CompareSubText,
  VerticalSpacing,
} from "globalstyles";
import {
  FlexColumn,
  FlexAlwaysRow,
  HideCompareFlexColumn,
  FlexRow,
  TypicalExperienceFlexColumn,
} from "common";
import styled from "styled-components";

const withDexterio = [
  {
    value: "On Time Delivery",
    compare: "Project Timeline",
  },
  {
    value: "10 Yrs Warranty",
    compare: "Warranty",
  },
  {
    value: "Branded",
    compare: "Quality",
  },
  {
    value: "Competitive Prices",
    compare: "Price",
  },
  {
    value: "Personalized/Prefab",
    compare: "Design",
  },
  {
    value: "Eco-friendly",
    compare: "Sustainability",
  },
  {
    value: "5:4:1 secure payment",
    compare: "Payment Terms",
  },
];

const parameters = [
  {
    point: "Project Timeline",
  },
  {
    point: "Warranty",
  },
  {
    point: "Quality",
  },
  {
    point: "Price",
  },
  {
    point: "Design",
  },
  {
    point: "Sustainability",
  },
  {
    point: "Payment Terms",
  },
];

const typicalExperience = [
  {
    exp: "Unreliable timelines",
    compare: "Project Timeline",
  },
  {
    exp: "No warranty",
    compare: "Warranty",
  },
  {
    exp: "Local inferior",
    compare: "Quality",
  },
  {
    exp: "Spiked prices",
    compare: "Price",
  },
  {
    exp: "Outdated designs",
    compare: "Design",
  },
  {
    exp: "Chemical infused materials",
    compare: "Sustainability",
  },
  {
    exp: "Insecure",
    compare: "Payment Terms",
  },
];

const DexterioEdge = () => {
  return (
    <div style={{ backgroundColor: "#f4f2ec" }}>
      <DexterioEdgeContainer style={{ textAlign: "center" }}>
        <FlexColumn style = {{paddingBottom:"6vw"}}>
          <SectionHeadlines>
            Dexterio <span style={{ color: "rgba(123,131,97,1)" }}>Edge</span>
          </SectionHeadlines>
          <VerticalSpacing height="2vw" />
          <FlexRow style={{ alignItems: "center", justifyContent: "center" }}>
            <FlexAlwaysRow style={{ width: "100%" }}>
              <HideCompareFlexColumn style={{ flex: 1 }}>
                <TypicalExperienceFlexColumn background="rgb(244, 242, 236, 1)">
                  <DexterioEdgeSubSectionheadlines
                    style={{
                      textAlign: "center",
                    }}
                  >
                    Compare Experience
                  </DexterioEdgeSubSectionheadlines>
                  {parameters.map((item, index) => {
                    return (
                      <SubSectionParagraphWrapper key={index}>
                        <SubSectionParagraph
                          style={{
                            textAlign: "left",
                            marginTop: "0",
                            marginBottom: "0",
                            width: "100%",
                          }}
                        >
                          {item.point}
                        </SubSectionParagraph>
                      </SubSectionParagraphWrapper>
                    );
                  })}
                </TypicalExperienceFlexColumn>
              </HideCompareFlexColumn>
              <FlexColumn
                style={{
                  flex: "1",
                  position: "relative",
                }}
              >
                <FlexColumn
                  style={{
                    position: "absolute",
                    justifyContent: "center",
                    alignItems: "center",
                    top: "3%",
                    bottom: "3%",
                    left: 0,
                    right: 0,
                  }}
                >
                  <TypicalExperienceFlexColumn background="rgba(222,226,214,1)">
                    <DexterioEdgeSubSectionheadlines
                      style={{
                        textAlign: "center",
                      }}
                    >
                      Typical Experience
                    </DexterioEdgeSubSectionheadlines>
                    {typicalExperience.map((item, index) => {
                      return (
                        <SubSectionParagraphWrapper key={index}>
                          <CompareSubText background="rgb(222, 226, 214)">
                            {item.compare}&ensp;
                          </CompareSubText>
                          <SubSectionParagraph
                            style={{
                              textAlign: "left",
                              marginTop: "0",
                              marginBottom: "0",
                              width: "100%",
                            }}
                          >
                            {item.exp}
                          </SubSectionParagraph>
                        </SubSectionParagraphWrapper>
                      );
                    })}
                  </TypicalExperienceFlexColumn>
                </FlexColumn>
              </FlexColumn>
              <FlexColumn
                style={{ background: "#fff", borderRadius: "8px", flex: "1" }}
              >
                <TypicalExperienceFlexColumn background="#fff">
                  <DexterioEdgeSubSectionheadlines
                    style={{
                      textAlign: "center",
                    }}
                  >
                    Dexterio Experience
                  </DexterioEdgeSubSectionheadlines>
                  {withDexterio.map((item, index) => {
                    return (
                      <SubSectionParagraphWrapper key={index}>
                        <CompareSubText background="#fff">
                          {item.compare}&ensp;
                        </CompareSubText>
                        <SubSectionParagraph
                          style={{
                            textAlign: "left",
                            marginTop: "0",
                            marginBottom: "0",
                            width: "100%",
                          }}
                        >
                          <span className="icon-wrapper">&#x2714;</span>{" "}
                          {item.value}
                        </SubSectionParagraph>
                      </SubSectionParagraphWrapper>
                    );
                  })}
                </TypicalExperienceFlexColumn>
              </FlexColumn>
            </FlexAlwaysRow>
          </FlexRow>
        </FlexColumn>
      </DexterioEdgeContainer>
    </div>
  );
};

export default DexterioEdge;

const SubSectionParagraphWrapper = styled.div`
  display: flex;
  height: 5vw;
  justify-content: center;
  align-items: center;
  width: 100%;
  border-bottom: 1px solid rgba(123, 131, 97, 1);
  flex-direction: column;
  position: relative;

  @media only screen and (max-width: 600px) {
    height: 15vw;
  }
`;
