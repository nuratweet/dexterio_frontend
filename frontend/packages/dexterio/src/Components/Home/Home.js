import BackGroundImageWithText from "../Common/BackGroundImageWithText";
import ClientSignUpModal from './ClientSignUpModal/ClientSignUpModal';
import SampleProjects from './SampleProjects/SampleProjects';
import FeaturesPage from './FeaturesPage/FeaturesPage';
import DexterioEdge  from './DexterioEdge/DexterioEdge';
import CaseStudies from './CaseStudies/CaseStudies';
import HowItWorks from './HowItWorks/HowItWorks';
import IntroPage from './IntroPage/IntroPageNew';
import { Helmet } from 'react-helmet';
import React from 'react';

const Home  = (props) =>  {

    const { mobileLeadFormData, setMobileLeadFormData } = {...props.mobileLeadFormData};

    return (
      <section style={{ overflow: "hidden" }}>
        <Helmet>
          <title>
            Dexterio: One stop solution for all your home interior needs.
          </title>
          <meta name="author" content="Dexterio" />
          <meta name="twitter:domain" content="dexterio.in" />
          <meta name="robots" content="index,follow" />
          <meta
            property="og:description"
            content="Home renovation made easy fast affordable. Welcome to the one stop solution for all your home interior needs. Get expert interior designers and create a beautiful- living room, toilet, kitchen, dining, bedrooms &amp; more."
          />
          <meta property="og:locale" content="en_IN" />
          <meta
            property="og:title"
            content="Dexterio: One stop solution for all your home interior needs."
          />
          <meta
            property="og:site_name"
            content="Dexterio - Redefining Interiors"
          />
          <meta property="og:url" content="https://dexterio.in/" />
          <meta property="og:type" content="website" />
          <meta
            name="twitter:description"
            content="Home renovation made easy fast affordable. Welcome to the one stop solution for all your home interior needs. Get expert interior designers and create a beautiful- living room, toilet, kitchen, dining, bedrooms &amp; more."
          />
          <meta
            name="twitter:title"
            content="Dexterio: One stop solution for all your home interior needs."
          />
          <meta name="twitter:card" content="summary" />
          {/* <meta
            name="description"
            content="Home renovation made easy fast affordable. Welcome to the one stop solution for all your home interior needs. Get expert interior designers and create a beautiful- living room, toilet, kitchen, dining, bedrooms &amp; more."
          /> */}
          <meta name="title" content="Dexterio Interiors - Top Architect & Interior designer in Jaipur"/>
<meta name="description" content="Looking to hire an interior designer in Jaipur to design your new home or for renovation of your house or office? If yes, Dexterio Interiors, interior design company will help you make the right choice."/>
<meta name="keywords" content="top interior designers in jaipur, interior designer in jaipur, interior design studio in jaipur, interior design firms in jaipur, interior design company in jaipur, interior decorators in jaipur, home interior in jaipur, home interior designer in jaipur, home designer in jaipur, best interior designers in jaipur, architect and interior designer in jaipur, modern bedroom interior design, simple bedroom interior design, luxurious bedroom interior design, living room home interior design, modern house interior design, interior design company"/>
<meta name="language" content="English"></meta>
        </Helmet>
        <ClientSignUpModal {...props} />
        <IntroPage
          mobileLeadFormData={{ mobileLeadFormData, setMobileLeadFormData }}
        />
        <FeaturesPage
          mobileLeadFormData={{ mobileLeadFormData, setMobileLeadFormData }}
        />
        <BackGroundImageWithText
          mobileLeadFormData={{ mobileLeadFormData, setMobileLeadFormData }}
          data={{ page: "homegradient", imagePath: "estimate_1.jpg" }}
        />
        <HowItWorks />
        <CaseStudies />
        <DexterioEdge />
        <SampleProjects />
        <BackGroundImageWithText
          {...props}
          mobileLeadFormData={{ mobileLeadFormData, setMobileLeadFormData }}
          data={{ page: "homecalltoaction", imagePath: "estimate_5.webp" }}
        />
      </section>
    );
};

export default Home;