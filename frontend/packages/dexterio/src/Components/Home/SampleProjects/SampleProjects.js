import React, { useRef, useEffect } from "react";
import "./SampleProjects.css";
import {
  CarouselInner,
  Track,
  CarouselContainer,
  WidgetCardWrap,
  WidgetCardImageWrapper,
  WidgetCardContainer,
  WidgetCardImage,
  SampleProjectsGlobalStyle,
} from "./SampleProjects.element";
import {
  SectionHeadlines,
  SubSubSectionHeadlines,
  SubSectionHeadlines,
  SubSectionParagraph,
  Container,
  VerticalSpacing,
} from "globalstyles";
import {
  FlexColumn,
  FlexRow,
  DesktopView,
  MobileView,
  CarouselSnapper,
  Carousel,
  CarouselSlide,
  CarouselViewPort,
} from "common";
import { FaAngleRight, FaAngleLeft } from "react-icons/fa";
import { Link } from "react-router-dom";

const data = [
  {
    image: "/2021/09/living-room-interior-1510173-1024x683.jpg",
    heading: "6 Trending Design Elements for Home Architecture",
    footer:
      "If this year you want to make a bold statement then aim to introduce 2022’s hottest home decor trends.6 trending designs for your dream home.",
    slug: "/blog/6-trending-design-elements-for-home-architecture",
  },
  {
    image: "/2021/09/123.jpg",
    heading: "Start Designing your Home, Tips from our Designers",
    footer:
      "Whether or not the wish list is a mile long, the decor cycle will deliver just what you want. Read valuable tips from Dexterio's experienced designers.",
    slug: "/blog/start-designing-your-home-tips-from-our-designers",
  },
  {
    image: "/2021/09/slava-keyzman-qr4d407hSjo-unsplash-scaled-1-1024x683.jpg",
    heading: "7 Essentials elements to design Study Room",
    footer:
      "A study room or home office should be an inviting ,efficient and promoting imagination, too. Understand few essential elements for your study room.",
    slug: "/blog/study-room-7-essentials-elements-to-consider-before-designing",
  },
  {
    image: "/2021/09/Blog-4-1-1024x683.png",
    heading: "10 Living room Design Mistakes everyone makes.",
    footer:
      "Just like a beautifully directed film, a well-decorated living room is captivating and impressive, familiar, and safe at once.",
    slug: "/blog/10-living-room-design-mistakes-everyone-makes",
  },
  {
    image: "/2021/09/dghdh.jpg",
    heading: "Choosing a Perfect Wallpaper",
    footer:
      "Wallpapering has become increasingly common to the everyday homeowner nowadays, which was once the road was less taken.",
    slug: "/blog/choosing-a-perfect-wallpaper",
  },
  {
    image: "/2021/09/7-1024x683.png",
    heading: "10 Small Bedroom Ideas that are Big in Style",
    footer:
      "If you need a makeup bedroom but don’t think you’ve got enough bedroom to work with, then you’re in the right place.",
    slug: "/blog/10-small-bedroom-ideas-that-are-big-in-style",
  },
  {
    image: "/2021/09/2-2-1024x683.png",
    heading: "6 Modern Living Rooms for ",
    footer:
      "We’ve put together this round-up of our favorite examples of modern living rooms to help you sort it all out.",
    slug: "/blog/6-modern-living-rooms-for-everyone",
  },
  {
    image: "/2021/09/Difference-Blog-1-1024x683.png",
    heading: "What is the difference between Living and Drawing Room!",
    footer:
      "Even though they both designate a seating type, they do not both designate the same space. Read on to understand more.",
    slug: "/blog/what-is-the-difference-between-living-and-drawing-room",
  },
];

const SampleProjects = () => {
  const prev = useRef(null);
  const next = useRef(null);
  const track = useRef(null);
  const carouselInner = useRef(null);

  useEffect(() => {
    let carouselWidth = 0;
    if (carouselInner.current) {
      carouselWidth = carouselInner.current.offsetWidth;
    }

    window.addEventListener("resize", () => {
      if (carouselInner.current) {
        carouselWidth = carouselInner.current.offsetWidth;
      }
    });

    let index = 0;

    if (next.current) {
      next.current.addEventListener("click", () => {
        index++;
        prev.current.classList.add("show");
        track.current.style.transform = `translateX(-${
          index * carouselWidth
        }px)`;

        if (index === 2) {
          next.current.classList.add("hide");
        }
      });
    }

    if (prev.current) {
      prev.current.addEventListener("click", () => {
        index--;
        next.current.classList.remove("hide");
        if (index === 0) {
          prev.current.classList.remove("show");
        }
        track.current.style.transform = `translateX(-${
          index * carouselWidth
        }px)`;
      });
    }

    return () => {
      if (prev.current != null) {
        prev.current.removeEventListener("click", () => {});
      }
      if (next.current != null) {
        next.current.removeEventListener("click", () => {});
      }
      window.removeEventListener("resize", () => {});
    };
  });

  return (
    <Container style={{ textAlign: "center" }}>
      <SampleProjectsGlobalStyle />
      <FlexColumn style={{ position: "relative" }}>
        <FlexColumn style={{ flex: "1" }}>
          <SectionHeadlines>
            Dexterio&nbsp;
            <span style={{ color: "rgba(123,131,97,1)" }}>Blog</span>
          </SectionHeadlines>
        </FlexColumn>
        <FlexColumn style={{ alignItems: "flex-end", marginBottom: "2vw" }}>
          <SubSubSectionHeadlines>
            <Link to="/blogs" style={{ textDecoration: "none" }}>
              <span style={{ color: "rgba(255,92,92,1)" }}>View All > </span>
            </Link>
          </SubSubSectionHeadlines>
        </FlexColumn>
      </FlexColumn>
      <DesktopView>
        <CarouselContainer>
          <CarouselInner ref={(node) => (carouselInner.current = node)}>
            <Track ref={(node) => (track.current = node)}>
              {data.map((item, index) => {
                return (
                  <WidgetCardWrap key={index}>
                    <WidgetCardImageWrapper>
                      <WidgetCardContainer>
                        <WidgetCardImage
                          src={item.image}
                          className="attachment-large size-large"
                          alt=""
                        />
                      </WidgetCardContainer>
                    </WidgetCardImageWrapper>
                    <SubSectionHeadlines
                      style={{
                        marginBottom: "1vw",
                        textAlign: "left",
                        flexGrow: 2,
                      }}
                    >
                      {item.heading}
                    </SubSectionHeadlines>
                    <SubSectionParagraph
                      style={{
                        marginBottom: "0.5vw",
                        textAlign: "left",
                      }}
                    >
                      {item.footer}
                    </SubSectionParagraph>
                    <FlexRow style={{ justifyContent: "end" }}>
                      <Link to={item.slug} style={{ textDecoration: "none" }}>
                        <SubSubSectionHeadlines style={{ fontSize: "1vw" }}>
                          <span
                            style={{
                              fontWeight: "bold",
                              color: "rgba(255,92,92,1)",
                            }}
                          >
                            Read Story
                          </span>
                        </SubSubSectionHeadlines>
                      </Link>
                    </FlexRow>
                  </WidgetCardWrap>
                );
              })}
            </Track>
          </CarouselInner>
          <div className="nav">
            <button className="prev" ref={(node) => (prev.current = node)}>
              <i className="material-icons">
                <FaAngleLeft size={28} />
              </i>
            </button>
            <button className="next" ref={(node) => (next.current = node)}>
              <i className="material-icons">
                <FaAngleRight size={28} />
              </i>
            </button>
          </div>
        </CarouselContainer>
      </DesktopView>
      <MobileView>
        <div style={{ display: "flex", justifyContent: "center" }}>
          <Carousel style={{ width: "100%" }}>
            <CarouselViewPort>
              {data.map((item1, stepIndex) => {
                return (
                  <CarouselSlide key={stepIndex}>
                    <CarouselSnapper>
                      <WidgetCardWrap>
                        <WidgetCardImageWrapper>
                          <WidgetCardContainer>
                            <WidgetCardImage
                              src={item1.image}
                              className="attachment-large size-large"
                              alt=""
                            />
                          </WidgetCardContainer>
                        </WidgetCardImageWrapper>
                        <VerticalSpacing height="2vw" />
                        <SubSectionHeadlines
                          style={{
                            marginTop: "1vw",
                            marginBottom: "1vw",
                            textAlign: "left",
                            flexGrow: 2,
                          }}
                        >
                          {item1.heading}
                        </SubSectionHeadlines>
                        <VerticalSpacing height="1vw" />
                        <SubSectionParagraph
                          style={{
                            marginBottom: "0.5vw",
                            textAlign: "left",
                          }}
                        >
                          {item1.footer}
                        </SubSectionParagraph>
                        <VerticalSpacing height="2vw" />
                        <FlexRow style={{ alignItems: "end" }}>
                          <Link
                            to={item1.slug}
                            style={{ textDecoration: "none" }}
                          >
                            <SubSubSectionHeadlines style={{ fontSize: "1vw" }}>
                              <span
                                style={{
                                  fontWeight: "bold",
                                  color: "rgba(255,92,92,1)",
                                }}
                              >
                                Read Story
                              </span>
                            </SubSubSectionHeadlines>
                          </Link>
                        </FlexRow>
                      </WidgetCardWrap>
                    </CarouselSnapper>
                  </CarouselSlide>
                );
              })}
            </CarouselViewPort>
          </Carousel>
        </div>
      </MobileView>
    </Container>
  );
};

export default SampleProjects;

// margin: 0px 125px 30px 125px;
