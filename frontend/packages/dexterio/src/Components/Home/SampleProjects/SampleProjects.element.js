import styled, { createGlobalStyle } from "styled-components";
import { Container } from "../../../globalStyles";

export const SampleProjectsGlobalStyle = createGlobalStyle`
  :root {
    --carousel-container-width: 90%;
  }
`;

export const CarouselContainer = styled(Container)`
  position: relative;
  margin: 0 auto;

  @media screen and (max-width: 1439px) {
    width: var(--carousel-container-width);
  }
`;

export const CarouselInner = styled.div`
  overflow: hidden;
`;

export const Track = styled.div`
  display: flex;
  transition: transform 0.5s;
`;

export const WidgetCardWrap = styled.div`
  display: flex;
  flex-direction: column;
  padding: 1%;
  background-color: rgba(244, 243, 237, 1);
  border-radius: 8px;

  width: 32%;
  flex-shrink: 0;
  margin-right: 2%;

  &:nth-child(3n) {
    margin-right: 0%;
  }

  @media only screen and (max-width: 600px) {
    width: 98%;
    padding: 3%;
    &:nth-child(n) {
      margin-right: 2%;
      margin-left: 2%;
    }
  }
`;

export const WidgetCardImageWrapper = styled.div`
  margin-bottom: 0px;
  width: 100%;
  text-align: center;
  position: relative;
`;

export const WidgetCardContainer = styled.div`
  transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s,
    -webkit-border-radius 0.3s, -webkit-box-shadow 0.3s;
`;

export const WidgetCardImage = styled.img`
  width: 100%;
  height: 235px;
  object-fit: cover;
  border-radius: 10px 10px 10px 10px;
  vertical-align: middle;
  display: inline-block;
`;
