import React, { useState } from "react";
import { useTheme, makeStyles } from "@material-ui/styles";
import { Step, Stepper, StepLabel } from "@material-ui/core";
import Box from "@material-ui/core/Box";
import SwipeableViews from "react-swipeable-views";
import { autoPlay } from "react-swipeable-views-utils";
import styled from "styled-components";
import "./HowItWorks.css";
import { Link } from "react-router-dom";
import {FlexRow, FlexColumn} from 'common';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import {
  Container,
  VerticalSpacing,
  SectionHeadlines,
  SubSectionParagraph,
  SubSectionHeadlines,
  CurvedButton,
} from "globalstyles";

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

const useStyles = makeStyles((theme) => ({
  heading: {
    fontFamily: '"Poppins", sans-serif',
    fontSize: "21px",
    fontWeight: 600,
    [theme.breakpoints.down("lg")]: {
      fontSize: "16px",
    },
  },

  subtitle: {
    fontFamily: '"Poppins", sans-serif',
    fontSize: "15px",
    fontWeight: 500,
    [theme.breakpoints.down("lg")]: {
      fontSize: "12px",
    },
  },
  iconContainer: {
    transform: "scale(1.5)",
  },
  icon: {
    color: "rgba(122,131,96,1) !important",
  },
  stepper: {
    backgroundColor: "#fff",
  },
  paper: {
    marginTop: "-35px",
    zIndex: 9991,
    borderRadius: "9px",
  },
}));

const images = [
  {
    label: "Design Quiz",
    desc: "Take dexterio's online Design Quiz to help us understand your personalized choices.",
    imgPath: "/img/home-howitworks-step1-1.jpg",
  },
  {
    label: "Virtual Consultation",
    desc: "Discuss your ideas, requirements and budget with our design consultants.",
    imgPath: "/img/home-howitworks-step1-2.jpg",
  },
  {
    label: "Confirm your Order",
    desc: "Happy with proposals, Pay 50% of agreed upon amount to kickstart the execution.",
    imgPath: "/img/home-howitworks-step1-2.jpg",
  },
  {
    label: "Installation",
    desc: "Civil work and installation initiates on the site. Relax and let our designers take the lead.",
    imgPath: "/img/home-howitworks-step3-1.jpg",
  },
  {
    label: "Settle In",
    desc: "Your property is ready to move in. Dexterio provides after sales support & 10 yrs warranty.",
    imgPath: "/img/home-howitworks-step3-2.jpg",
  },
];

const HowItWorks = () => {
  const matches = useMediaQuery('(min-width:600px)');

  const theme = useTheme();
  const classes = useStyles();
  const [activeStep, setActiveStep] = useState(0);

  const handleStepChange = (step) => {
    setActiveStep(step);
  };

  return (
    <Container style={{"borderBottom": "2px dashed rgb(226, 226, 226)"}}>
      <SectionHeadlines style={{ textAlign: "center" }}>
        How it <span style={{ color: "rgba(123,131,97,1)" }}> works </span>
      </SectionHeadlines>
      <VerticalSpacing height="4vw" />
      <CarouselSection>
        <Box sx={{ width: matches ? "60%" : "100%", flexGrow: 1 }}>
          <Stepper activeStep={activeStep} className={classes.stepper}>
            {images.map((step, index) => (
              <Step key={index}>
                <StepLabel
                  StepIconProps={{
                    classes: { active: classes.icon, completed: classes.icon },
                  }}
                  classes={{ iconContainer: classes.iconContainer }}
                />
              </Step>
            ))}
          </Stepper>
        </Box>
        <Box sx={{ width: matches ? "65%" : "100%", flexGrow: 1 }}>
          <AutoPlaySwipeableViews
            axis={theme.direction === "rtl" ? "x-reverse" : "x"}
            index={activeStep}
            onChangeIndex={handleStepChange}
            enableMouseEvents
            className="mui-div"
          >
            {images.map((step, index) => (
              <div key={index}>
                {Math.abs(activeStep - index) <= 2 ? (
                  <FlexRow style= {{position:"relative"}}>
                    <FlexColumn className="swiper-wrapper">
                      <VerticalSpacing height="1vw" />
                      <SubSectionHeadlines style={{ color: "rgba(123,131,97,1)" }} >
                        {step.label}
                      </SubSectionHeadlines>
                      <WidthDefinedSubSectionParagraph>
                        {step.desc}
                      </WidthDefinedSubSectionParagraph>
                      <VerticalSpacing height="4vw" />
                      <Link to="/how-it-works">
                        <CurvedButton
                            background= "rgba(255,92,92,1)"
                            light
                            curve
                            fontBig
                            margin="0px"
                          >
                          Learn More
                        </CurvedButton>
                      </Link>
                      <VerticalSpacing height="4vw" />
                    </FlexColumn>
                    <div className="swiper-image-wrapper">
                      <Box
                        component="img"
                        sx={{
                          height: "100%",
                          objectFit: "contain",
                          borderRadius: "8px",
                          zIndex: "9000"
                        }}
                        src={step.imgPath}
                      />
                    </div>
                  </FlexRow>
                ) : null}
              </div>
            ))}
          </AutoPlaySwipeableViews>
        </Box>
      </CarouselSection>
      <VerticalSpacing height="40" />
    </Container>
  );
};

const CarouselSection = styled.section`
  display: flex;
  flex-direction: column;
  justify-content: center;
  text-align: center;
  align-items: center;
`;

export default HowItWorks;


const WidthDefinedSubSectionParagraph = styled(SubSectionParagraph)`
  max-width: 50%;

  @media only screen and (max-width: 600px) {
    max-width: 90%
  }

`;