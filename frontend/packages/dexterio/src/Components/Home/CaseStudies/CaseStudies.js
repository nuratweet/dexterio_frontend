import styled from "styled-components";
import React from "react";
import "./CaseStudies.css";
import {
  FlexAlwaysRow,
  FlexColumn,
  FlexRow,
  CarouselSnapper,
  Carousel,
  CarouselSlide,
  CarouselViewPort,
  DesktopView,
  MobileView,
} from "common";
import {
  Container,
  VerticalSpacing,
  SectionHeadlines,
  SubSectionParagraph,
} from "globalstyles";

const caseData = [
  {
    profilePic: "/Sandeep.jpg",
    image: "/case_study_1.jpg",
    name: "Sandeep Sharma",
    address: "Malviya Nagar, Jaipur",
    content:
      "I had a budget limit but I wanted contemporary and European theme. Well, Dexterio made it possible with an amazing team of employees and their professionalism.",
    rating: 5,
  },
  {
    profilePic: "/Vaibhav.jpg",
    image: "/case_study_2.jpg",
    name: "Vaibhav Bhatnagar",
    address: "3 BHK Apartment, Jaipur",
    content:
      "Vaibhav loves the look of his new place and thoroughly enjoyed the whole process with Dexterio. He is very happy with the work and has already recommended Dexterio to friends",
    rating: 5,
  },
  {
    profilePic: "/bharat.jpg",
    image: "/case_study_3.jpg",
    name: "Bharat Azad",
    address: "Cape Town, Jaipur",
    content:
      "I had no idea of what I actually wanted so I left everything on the designer and I am so happy and satisfied with the affordable services and rustic warm touch to my cafe.",
    rating: 5,
  },
];

const CaseStudies = () => {
  return (
    <Container>
      <SectionHeadlines style={{ textAlign: "center", maxWidth: "100%" }}>
        Hear from our <br />
        <span style={{ color: "rgba(123,131,97,1)" }}> customers </span>
      </SectionHeadlines>
      <VerticalSpacing height="4vw" />
      <DesktopView style={{ width: "90%", margin: "0 auto" }}>
        <FlexAlwaysRow style={{ gridGap: "1.5vw", flex: "0 1 auto" }}>
          {caseData.map((item, index) => {
            return (
              <FlexColumn
                style={{
                  background: "rgba(245,243,237,1)",
                  borderRadius: "8px",
                }}
                key={index}
              >
                <FlexColumn style={{ width:"90%", margin: "0 auto" , height: "100%"}}>
                  <FlexRow
                    style={{ transform: "translateY(-15px)" }}
                  >
                    <img
                      src="/svg/Inverted.svg"
                      alt=""
                      height="60"
                      width="44"
                      style={{ transform: "translateY(-15px)" }}
                    />
                  </FlexRow>
                  <FlexRow
                    style={{ transform: "translateY(-15px)" }}
                  >
                    <img
                      src={item.image}
                      alt=""
                      style={{
                        maxWidth: "100%",
                        height: "100%",
                        borderRadius: "8px",
                        objectFit: "contain",
                      }}
                    />
                  </FlexRow>
                  <SubSectionParagraph
                    style={{ textAlign: "center", flexGrow: 2 }}
                  >
                    {item.content}
                  </SubSectionParagraph>
                  <VerticalSpacing
                    height="1vw"
                    style={{
                      borderBottom: "2px solid rgba(123,131,97,1)"
                    }}
                  />
                  <SubSectionParagraph
                    style={{
                      textAlign: "center",
                      fontWeight: "bold"
                    }}
                  >
                    {item.name}
                  </SubSectionParagraph>
                  <SubSectionParagraph
                    style={{
                      textAlign: "center",
                      marginTop: "0px"
                    }}
                  >
                    {item.address}
                  </SubSectionParagraph>
                </FlexColumn>
              </FlexColumn>
            );
          })}
        </FlexAlwaysRow>
      </DesktopView>
      <MobileView>
        <div style={{ display: "flex", justifyContent: "center" }}>
          <Carousel style={{ width: "90%" }}>
            <CarouselViewPort>
              {caseData.map((item1, stepIndex) => {
                return (
                  <CarouselSlide key={stepIndex}>
                    <CarouselSnapper>
                      <FlexColumn
                        style={{
                          background: "rgba(245,243,237,1)",
                          borderRadius: "8px",
                          flex: "1",
                          marginLeft: "0.5vw",
                          marginRight: "0.5vw"
                        }}
                      >
                        <img
                          src="/svg/Inverted.svg"
                          alt=""
                          height="45"
                          width="33"
                          style={{ paddingLeft: "5px" }}
                        />
                        <FlexColumn style={{ width: "90%", margin: "0 auto" }}>
                          <FlexColumn>
                            <FlexRow>
                              <img
                                src={item1.image}
                                alt=""
                                style={{
                                  maxWidth: "100%",
                                  height: "100%",
                                  borderRadius: "8px",
                                  objectFit: "contain",
                                }}
                              />
                            </FlexRow>
                            <SubSectionParagraph
                              style={{ textAlign: "center" }}
                            >
                              {item1.content}
                            </SubSectionParagraph>
                            <VerticalSpacing
                              height="1vw"
                              style={{
                                borderBottom: "2px solid rgba(123,131,97,1)",
                              }}
                            />
                            <SubSectionParagraph
                              style={{
                                textAlign: "center",
                                fontWeight: "bold",
                              }}
                            >
                              {item1.name}
                            </SubSectionParagraph>
                            <SubSectionParagraph
                              style={{
                                textAlign: "center",
                                marginTop: "0px",
                              }}
                            >
                              {item1.address}
                            </SubSectionParagraph>
                          </FlexColumn>
                        </FlexColumn>
                      </FlexColumn>
                    </CarouselSnapper>
                  </CarouselSlide>
                );
              })}
            </CarouselViewPort>
          </Carousel>
        </div>
      </MobileView>
    </Container>
  );
};

export default CaseStudies;

export const InvertedImg = styled.img`
  transform: "translateY(-30px)";
`;
