import React from 'react'
import './ProgressUpdate.css'
import { Avatar } from '@material-ui/core';
  

function ProgressUpdate({message, timestamp, userImage, user}) {
    return (
        <div className="progress_update">
            <div>
            <Avatar
                src={userImage}
                sx={{
                    cursor: 'pointer',
                    width: 64,
                    height: 64
                }}
                to="/c/account"
                />
            </div>
            <div className="update_info">
                <h4>
                    {user} <span className="update_timestamp">{timestamp}</span>
                </h4>
                <p>{message}</p>
            </div>

        </div>
    )
}

export default ProgressUpdate
