import React , { useState, useEffect } from 'react'
import './Progress.css';
import {
    Box,
    Container,
  } from '@material-ui/core';
// import { StarBorderOutlined, InfoOutlined } from '@material-ui/icons'
import { Helmet } from 'react-helmet';
import ProgressUpdate from './ProgressUpdate';
const config = require('../../../../config_prod.json');

const progressUpdates = [
    {
        message: "Celing Done",
        timestamp: "2015-03-25T12:00:00Z"
    },
    {
        message: "Kitchen Top done",
        timestamp: "2015-03-25T12:00:00Z"
    },
    {
        message: "Hello Hi there",
        timestamp: "2015-03-25T12:00:00Z"
    },
]
const Progress = (props) => {

    const [name, setName] = useState("");
    const [profileImage, setProfileImage] = useState();

    useEffect(() => {
        fetch(`${config.api.invokeUrl}/designerprofile/arpitkhatter`)
        .then(resp => resp.json())
        .then(data => {
               if (data.Item) {
                   setName(data.Item.name)
                   setProfileImage(data.Item.profileImage)
               }
           }
        )
    }, []);
    
    return (
        <>
        <Helmet>
            <title>Login | Material Kit</title>
        </Helmet>
        <div classname="progres">
            <Box
            sx={{
                backgroundColor: 'background.default',
                display: 'flex',
                flexDirection: 'column',
                height: '100%',
                justifyContent: 'left'
            }}
            >
                <Container>
                <div className="progress_header">
                    <div className="headerLeft">
                        <h4 className="projectName">
                            <strong>#malviya Nagar</strong>
                            {/* <StarBorderOutlined /> */}
                        </h4>
                    </div>
                    <div className="headerRight">
                        <p>
                            {/* <InfoOutlined /> Details */}
                        </p>
                    </div>
                </div>
                <div className="progress_messages">
                   
                    {progressUpdates.map((update) => (
                        <ProgressUpdate 
                        message={update.message}
                        timestamp={update.timestamp}
                        userImage={profileImage}
                        user={name}/>
                        ))
                    }
                </div>
            </Container>
        </Box>
        </div>
        </>
    )
}

export default Progress
