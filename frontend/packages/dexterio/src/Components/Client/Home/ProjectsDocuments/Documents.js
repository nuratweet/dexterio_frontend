import React, {useEffect} from 'react';
import { PDFViewer } from '@react-pdf/renderer';
// import HomeRenovationAgreementTemplate from './HomeRenovationAgreementTemplate';
import SalesQuote from './SalesQuote';
import ReactDOM from 'react-dom';

function Documents() {
    useEffect(() => {
        var agreementPage = document.getElementById('agreementPage');
        console.log(agreementPage);
    });

    return (
        <div id="agreementPage">
            <PDFViewer>
                <SalesQuote />
            </PDFViewer>
        </div>
    )
}

export default Documents

ReactDOM.render(<Documents />, document.getElementById('root'));
