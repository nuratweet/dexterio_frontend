import styled from 'styled-components';

export const FixedDrawer = styled.div`

    outline: 0;
    left: 0;
    border-right: 1px solid rgba(0, 0, 0, 0.12);
    width: 256px;
    top: 64px;
    height: calc(100% - 64px);
    background-color: #ffffff;
    color: #172b4d;
    transition: box-shadow 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
    box-shadow: none;
    overflow-y: auto;
    display: flex;
    flex-direction: column;
    flex: 1 0 auto;
    z-index: 1200;
    position: fixed;
`;