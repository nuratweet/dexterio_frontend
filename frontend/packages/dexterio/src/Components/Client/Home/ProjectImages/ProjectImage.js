import React from 'react'
import {
    Box,
    Container,
  } from '@material-ui/core';
import '../../../DesignQuiz/DesignQuiz2.css';
import { Helmet } from 'react-helmet';


const designGrid = [
    [
        { 
            image: "https://s3.amazonaws.com/static.havenly.com/style-quiz-images/e36296d8-791f-491c-b452-fda65378f4a2.jpg",
            detail: "Conventional"
        },
        { 
            image: "https://s3.amazonaws.com/static.havenly.com/style-quiz-images/391dd983-7b7c-46f6-b6d5-300fa9afba0b.jpg",            
            detail: "Royal"
        },
        { 
            image: "https://s3.amazonaws.com/static.havenly.com/style-quiz-images/cd312296-6a92-4d99-835a-6868fbb2d9c8.jpg",
            detail: "Rajasthani"
        }
    ],[
        { 
            image: "https://s3.amazonaws.com/static.havenly.com/style-quiz-images/71b8b34e-1c1f-47fd-a086-ebf6a2fa11f5.jpg",
            detail: "Contemporary"
        },
        { 
            image: "https://s3.amazonaws.com/static.havenly.com/style-quiz-images/51ef90f4-8d77-4d11-a7fa-13a96a163067.jpg",
            detail: "Italian"
        },
        { 
            image: "https://s3.amazonaws.com/static.havenly.com/style-quiz-images/54b4b3e2-c862-4bc2-9071-49cacdfb7b40.jpg",
            detail: "Spanish"
        },
    ],[
        { 
            image: "https://s3.amazonaws.com/static.havenly.com/style-quiz-images/8e39d9ef-009a-4282-99cd-c70b56f8a913.jpg",
            detail: "Modern"
        },
        { 
            image: "https://s3.amazonaws.com/static.havenly.com/style-quiz-images/80cd484b-1b76-4c35-83b7-fb24d867f357.jpg",
            detail: "Chic"
        },
        { 
            image: "https://s3.amazonaws.com/static.havenly.com/style-quiz-images/528600ae-3f19-41c1-a78c-326ab89091d9.jpg",
            detail: "Absfdsgfdsg"
        }
    ]
]

function Image() {
    return (
        <>
        <Helmet>
            <title>Login | Material Kit</title>
        </Helmet>
            <Box
            sx={{
                backgroundColor: 'background.default',
                display: 'flex',
                flexDirection: 'column',
                height: '100%',
                justifyContent: 'left'
            }}
            >
                <Container>
                <div className="_1GL58ajZRXWF3gPslo0bAg">
                                        {
                                            designGrid.map((item) => {
                                                return (
                                                    <div className="_340a319TqMOr5Vpy5ABfFj">
                                                        <div>
                                                            {
                                                            item.map((innerItem) => {
                                                                return (
                                                                    <div className="_2O19xYRsBU6qAl-QPWI5O_">
                                                                        <img alt="" className="_2zH219UNsPmRAaFF1TNQA2" src={innerItem.image}/>
                                                                    </div>                                                                    );
                                                                })
                                                            }
                                                        </div>
                                                    </div>
                                                )
                                            })
                                        }
                                    </div>            </Container>
        </Box>
        </>            
    )
}

export default Image


