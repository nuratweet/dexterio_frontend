import { useEffect } from 'react';

import { Link as RouterLink, useLocation } from 'react-router-dom';

import PropTypes from 'prop-types';

import {
  Avatar,
  Box,
  Button,
  Divider,
  Drawer,
  Hidden,
  List,
  Typography
} from '@material-ui/core';

import {
  Image as ImageIcon,
  BarChart as BarChartIcon,
  Lock as LockIcon,
  Settings as SettingsIcon,
  PenTool as DrawingIcon,
  TrendingUp as ProgressIcon,
  FileText as WorkIcon
} from 'react-feather';

import NavItem from './NavItem';
import styled from 'styled-components';

const user = {
  avatar: '/static/images/avatars/avatar_6.png',
  jobTitle: 'Senior Developer',
  name: 'Katarina Smith'
};

const items = [
  {
    href: '/c/dashboard',
    icon: BarChartIcon,
    title: 'Dashboard'
  },
  {
    href: '/c/documents',
    icon: WorkIcon,
    title: 'Documents'
  },
  {
    href: '/c/account',
    icon: SettingsIcon,
    title: 'Account'
  },
  {
    href: '/c/drawings',
    icon: DrawingIcon,
    title: 'Drawings'
  },
  {
    href: '/c/expense',
    icon: LockIcon,
    title: 'Expense Sheet'
  },
  {
    href: '/c/progress',
    icon: ProgressIcon,
    title: 'Progress'
  },
  {
    href: '/c/images',
    icon: ImageIcon,
    title: 'Project Images'
  }
];

const DashboardSidebar = ({ onMobileClose, openMobile }) => {
  const location = useLocation();

  useEffect(() => {
    if (openMobile && onMobileClose) {
      onMobileClose();
    }
  }, [location.pathname, onMobileClose, openMobile]);

  const content = (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
        height: '100%'
      }}
    >
      <Box
        sx={{
          alignItems: 'center',
          display: 'flex',
          flexDirection: 'column',
          p: 2
        }}
      >
        <Avatar
          component={RouterLink}
          src={user.avatar}
          sx={{
            cursor: 'pointer',
            width: 64,
            height: 64
          }}
          to="/c/account"
        />
        <Typography
          color="textPrimary"
          variant="h5"
        >
          {user.name}
        </Typography>
        <Typography
          color="textSecondary"
          variant="body2"
        >
          {user.jobTitle}
        </Typography>
      </Box>
      <Divider />
      <Box sx={{ p: 2 }}>
        <List>
          {items.map((item) => (
            <NavItem
              href={item.href}
              key={item.title}
              title={item.title}
              icon={item.icon}
            />
          ))}
        </List>
      </Box>
      <Box sx={{ flexGrow: 1 }} />
      <Box
        sx={{
          backgroundColor: 'background.default',
          m: 2,
          p: 2
        }}
      >
        <Typography
          align="center"
          gutterBottom
          variant="h4"
        >
          Need more?
        </Typography>
        <Typography
          align="center"
          variant="body2"
        >
          Upgrade to PRO version and access 20 more screens
        </Typography>
        <Box
          sx={{
            display: 'flex',
            justifyContent: 'center',
            pt: 2
          }}
        >
          <Button
            color="primary"
            component="a"
            href="https://react-material-kit.devias.io"
            variant="contained"
          >
            See PRO version
          </Button>
        </Box>
      </Box>
    </Box>
  );

  return (
    <>
      <Hidden mdUp>
        <Drawer
          anchor="left"
          onClose={onMobileClose}
          open={openMobile}
          variant="temporary"
          PaperProps={{
            sx: {
              width: 256
            }
          }}
        >
          {content}
        </Drawer>
      </Hidden>
      <Hidden mdDown>
        <FixedDrawer>
          {content}
        </FixedDrawer>
      </Hidden>
    </>
  );
};

DashboardSidebar.propTypes = {
  onMobileClose: PropTypes.func,
  openMobile: PropTypes.bool
};

DashboardSidebar.defaultProps = {
  onMobileClose: () => {
  },
  openMobile: false
};

export default DashboardSidebar;


export const FixedDrawer = styled.div`

    outline: 0;
    left: 0;
    border-right: 1px solid rgba(0, 0, 0, 0.12);
    width: 256px;
    top: 64px;
    height: calc(100% - 64px);
    background-color: #ffffff;
    color: #172b4d;
    transition: box-shadow 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
    box-shadow: none;
    overflow-y: auto;
    display: flex;
    flex-direction: column;
    flex: 1 0 auto;
    z-index: 1200;
    position: fixed;
`;