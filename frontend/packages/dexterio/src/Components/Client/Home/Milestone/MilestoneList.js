import { Helmet } from 'react-helmet';
import { Accordion, AccordionDetails, AccordionSummary, 
                    Box, Container, Typography } from '@material-ui/core';
import { FcExpand } from 'react-icons/fc';
import profile from '../../../../assets/profilepic.jpg';

const MilestoneList = () => {
  const fields = ['Accordion 1', 'Accordion 2', 'Accordion 3', 'Accordion 4', 'Accordion 5'];

  return (
  <>
    <Helmet>
      <title>Milestones | Dexterio</title>
    </Helmet>
    <Box
      sx={{
        backgroundColor: 'background.default',
        minHeight: '100%',
        py: 3
      }}
    >
      <Container maxWidth={false}>
        <Box sx={{ pt: 3 }}>
          {
          fields.map((field) => {
            return(
              <Accordion TransitionProps={{ unmountOnExit: true }}>
                <AccordionSummary
                  expandIcon={<FcExpand />}
                  aria-controls="panel1a-content"
                  id="panel1a-header"
                >
                  <Typography>{field}</Typography>
                </AccordionSummary>
                <AccordionDetails>
                  <Typography>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
                    malesuada lacus ex, sit amet blandit leo lobortis eget.
                  </Typography>
                  {<img src={profile} style={{ width: "500px" }} alt="" />}
                </AccordionDetails>
              </Accordion>
              )
            })
          }
        </Box>
      </Container>
    </Box>
  </>
  );
};

export default MilestoneList;
