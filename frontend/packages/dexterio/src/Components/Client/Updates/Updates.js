import React from 'react';
import styled from 'styled-components';
import UpdateLeftSide from './UpdateLeftSide';
import UpdateMain from './UpdateMain';
import UpdateRightSide from './UpdateRightSide';

const Updates = (props) => {
    const passToMain = {
    }
    
    return (
        <Container>
            <Layout>
                <UpdateLeftSide/>
                <UpdateMain {...props} data={passToMain}/>
                <UpdateRightSide/>
            </Layout>
        </Container>
    );    
}

const Container = styled.div`
    padding-top: 52px;
    max-width: 100%;
    padding-left: 25px;
    padding-right: 25px;
    background-color: #f3f2ef;
`;

const Layout = styled.div`
    display: grid;
    grid-template-areas: "leftside main rightside";
    grid-template-columns: minmax(200px, 200px) minmax(0, 14fr) minmax(200px, 200px) ;
    column-gap: 25px;
    row-gap: 25px;
    grid-template-rows: auto;
    margin: 25px 0;

    @media (max-width: 768px) {
        display: flex;
        flex-direction: column;
        padding: 0 5px;
    }
`;

export default Updates;