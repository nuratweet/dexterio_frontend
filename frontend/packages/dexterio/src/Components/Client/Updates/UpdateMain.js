import { Img , ImgWrapper , InfoColumn , InfoRow , Heading , Subtitle } from './Updates.element';
import styled from "styled-components";
import designImage from '../../../assets/work_in_progress.svg';


const UpdateMain = (props) => {

  return (
    <Container>
        <InfoColumn>
            <InfoRow>
                <ImgWrapper>
                    <Img src={designImage} alt="Image"/>
                </ImgWrapper>
            </InfoRow>
            <InfoRow>
                <Heading>
                    Our designers are working on your project
                </Heading>
                <Subtitle>
                        No updates to share yet. <br/>
                    You can find any recent project updates in here.
                </Subtitle>
            </InfoRow>
        </InfoColumn>

    </Container>
  );
};

const Container = styled.div`
  grid-area: main;
  display: flex;
  justify-content: center;
`;

export default UpdateMain;