import styled from "styled-components";

export const ImgWrapper = styled.div`
    max-width:555px;
    display: flex;
    justify-content: center;
`;

export const Img =styled.img`
    padding-right: 0;
    border: 0;
    max-width:100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 500px;
    z-index: 10;
`;

export const InfoColumn = styled.div`
    flex-direction: column;
    
    @media (max-width: 768px) {
        max-width:100%;
        flex-basis: 100%;
        display: flex;
        justify-content: center;
    }
`;

export const InfoRow = styled.div`
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    flex-direction: column;
`;

export const Heading = styled.div`
    margin-bottom: 24px;
    font-size: 24px;
    font-weight: 600;
    line-height: 1.33em;
    letter-spacing: 0px;
    color: #111121;
    font-family: "Poppins", sans-serif;
`;

export const Subtitle = styled.p`
    margin-bottom: 50px;
    color: #000707DE;
    font-family: "Open Sans", sans-serif;
    font-size: 18px;
    font-weight: 500;
    line-height: 1.33em;
`;
