import React from "react";

import "react-datasheet/lib/react-datasheet.css";
import _ from 'lodash';
import * as mathjs from 'mathjs';
import Datasheet from 'react-datasheet/lib/DataSheet'
import './styles.css';
// import Select from 'react-select'

const createInitialGridState = (state) => {
    _.range(1, state.items + 1).map((row, i) => 
        columns(state, row)
      );
  }

const columns = (state, row) => {
    [ 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O' , 'P'].map((col, j) => {
        let key = `${col}${row}`;
        let value = {key: `${col}${row}`, value: '', expr: ''}
        state.updates[key] = value;
        return null;
    })
}

class VendorManagementView extends React.Component {
  constructor(props) {
    super(props);
    this.options = [
        { label: 'Bread', value: 2.35 },
        { label: 'Berries', value: 3.05 },
        { label: 'Milk', value: 3.99 },
        { label: 'Apples', value: 4.35 },
        { label: 'Chicken', value: 9.95 },
        { label: 'Yoghurt', value: 4.65 },
        { label: 'Onions', value: 3.45 },
        { label: 'Salad', value: 1.55 }
      ]

    this.state = {
        updates : {},
        items: 3
    }

    createInitialGridState(this.state);
    this.onCellsChanged = this.onCellsChanged.bind(this);
  }

  onCellsChanged(changes) {
    const state = _.assign({}, this.state)
    changes.forEach(({cell, row, col, value}) => {
        this.cellUpdate(state, cell, value)
    })
    this.setState(state)
  }

  generateGrid() {
    // const groceryValue = (id) => {
    //     if (this.state.grocery[id]) {
    //       const {label, value} = this.state.grocery[id]
    //       return `${label} (${value})`
    //     } else {
    //       return ''
    //     }
    //   }
    //   const component = (id) => {
    //     return (
    //       <Select
    //         autofocus
    //         openOnFocus
    //         value={this.state && this.state.grocery[id]}
    //         onChange={(opt) => this.setState({grocery: _.assign(this.state.grocery, {[id]: opt})})}
    //         options={this.options}
    //       />
    //     )
    //   }
      
      let rows = [
        [
          { readOnly: true,
            colSpan: 17,
            value: 'Add Items',
            component: (
              <div className={'add-grocery'}>
                <div className={'add-button'} onClick={() => {
                    this.setState({items: this.state.items + 1}) 
                    columns(this.state, this.state.items + 1)}}> Add more Items</div>
              </div>
            ),
            forceComponent: true
          }]
      ]

    rows = rows.concat(_.range(0, this.state.items + 1).map((row, i) => 
            ['', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O' , 'P'].map((col, j) => {
                if(i === 0 && j === 0) {
                    return {readOnly: true, value: ''}
                    }
                    if(row === 0) {
                    return {readOnly: true, value: col}
                    } 
                    if(j === 0) {
                    return {readOnly: true, value: row}
                    }
                    return this.state.updates[col + row]
            })
        ))
    return rows;
  }

  validateExp(trailKeys, expr) {
    let valid = true;
    const matches = expr.match(/[A-Z][1-9]+/g) || [];
    matches.map(match => {
      if(trailKeys.indexOf(match) > -1) {
        valid = false
      } else {
        valid = this.validateExp([...trailKeys, match], this.state.updates[match].expr)
      }
      return null;
    })
    return valid
  }

  computeExpr(key, expr, scope) {
    let value = null;
    if(expr.charAt(0) !== '=') {
      return {className: '', value: expr, expr: expr};
    } else {
      try {
        value = mathjs.evaluate(expr.substring(1), scope)
      } catch(e) {
        value = null
      }

      if(value !== null && this.validateExp([key], expr)) {
        return {className: 'equation', value, expr}
      } else {
        return {className: 'error', value: 'error', expr: ''}
      }
    }
  }

  cellUpdate(state, changeCell, expr) {
    const scope = _.mapValues(state, (val) => isNaN(val.value) ? 0 : parseFloat(val.value))
    const updatedCell = _.assign({}, changeCell, this.computeExpr(changeCell.key, expr, scope))
    state.updates[changeCell.key] = updatedCell

    _.each(state.updates, (cell, key) => {
      if(cell.expr.charAt(0) === '=' && cell.expr.indexOf(changeCell.key) > -1 && key !== changeCell.key) {
        state = this.cellUpdate(state, cell, cell.expr)
      }

    })
    return state
  }

  render() {
    return (
        <div className={'container'} >
          <div className={'sheet-container'}>
            <Datasheet
                data={this.generateGrid()}
                valueRenderer={cell => cell.value}
                dataRenderer={(cell) => cell.expr}
                onCellsChanged={this.onCellsChanged}
            />
          </div>
        </div>
    );
  }
}

export default VendorManagementView;