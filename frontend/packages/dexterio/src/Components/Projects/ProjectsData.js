import React, { useState } from "react";
import styled from "styled-components";
import {
  SubSectionHeadlines,
  SubSectionParagraph,
} from "globalstyles";
import { FlexColumn } from "common";
import { Link } from "react-router-dom";

function ProjectsData() {
  // eslint-disable-next-line no-unused-vars
  const [projectData, setProjectData] = useState([
    {
      image: "/projects/2021/09/Culture-Shock/Snap/IMG_1704.JPG",
      header: "Culture Shock",
      description:
        "Client asked for a place with warmth and lots of lights and wood.",
      slug: "/project/culture-shock",
    },
    {
      image: "/projects/2021/09/Krishnatuvam/Snap/Krishnatuvam_Main_1.jpeg",
      header: "Krishnatuvam",
      description:
        "Our client wanted their place to be very decent and budget friendly.",
      slug: "/project/krishnatuvam",
    },
    {
      image: "/projects/2021/09/Jasmine/Snap/PB-M2-1-1536x1024.jpg",
      header: "Jasmine-105",
      description:
        "Our client wanted a cozy interior, so we made sure to give them a French styled bedroom in midnight blue",
      slug: "/project/jasmine",
    },
    {
      image: "/projects/2021/09/On_the_Way/On-the-way-19-2048x1365-1-1536x1024.jpeg",
      header: "On the way",
      description:
        "This project included a total refurbishment and the client wanted a very bold yet calm feel for his cafe.",
      slug: "/project/on-the-way",
    },
    {
      image: "/projects/2021/09/The_Capetown/Snap/IMG_8933.JPG",
      header: "The Capetown",
      description:
        "Our client wanted a bit rustic and industrial feel with an urbane touch.",
      slug: "/project/capetown",
    },
    {
      image: "/projects/2021/09/nandpuri/Nandpuri_Main.jpg",
      header: "Nandpuri",
      description:
        "This project needed a refurbishment of our client’s dining and living area.",
      slug: "/project/nandpuri",
    },
    {
      image: "/projects/2021/09/Balaji_Tower/IMG_6693.jpg",
      header: "Balaji Tower",
      description:
        "Our client wanted their place to be very decent and budget friendly.",
      slug: "/project/balaji",
    },
    {
      image: "/projects/2021/09/SarayuMarg/DSC_0859.jpg",
      header: "54/28, Saryu Marg",
      description: "Client wanted a Luxurios and Royal Feel, couples with ambient lighting and subtle use of colors, so we came up with the perfect combination",
      slug: "/project/sarayumarg"
    }
  ]);

  return (
    <Container>
      <ProjectsDataView>
        {projectData.map((item, index) => (
          <FlexColumn
            style={{
              margin: "25px",
              borderRadius: "10px",
              justifyContent: "start",
              backgroundColor: "rgba(245,243,237,1)",
            }}
            key={index}
          >
            <FlexColumn
              style={{
                padding: "10px",
                "justify-content": "space-around",
                "align-items": "stretch",
                "maxWidth": "400px",
                "height": "100%"
              }}
            >
              <div style={{ flexGrow: "1" }}>
                <img
                  src={`${item.image}`}
                  srcSet={`${item.image}`}
                  alt={item.room}
                  style={{
                    maxWidth: "100%",
                    height: "100%",
                    cursor: "pointer",
                    objectFit: "contain",
                    objectPosition: "center",
                    borderRadius: "10px",
                  }}
                  loading="lazy"
                />
              </div>
              <SubSectionHeadlines style={{ flexGrow: "1" }}>
                {item.header}
              </SubSectionHeadlines>
              <SubSectionParagraph
                style={{ marginTop: "0px", flexGrow: "1" }}
              >
                {item.description}
              </SubSectionParagraph>
              <div style={{ flexGrow: "2" }}></div>
              <div style={{ flexGrow: "1" }}>
                <Link to={item.slug} style={{ textDecoration: "none" }}>
                  <div
                    style={{
                      whiteSpace: "nowrap",
                      textAlign: "right",
                      fontFamily: "Poppins",
                      fontStyle: "normal",
                      fontWeight: "bold",
                      fontSize: "20px",
                      color: "rgba(255,92,92,1)",
                    }}
                  >
                    Read More
                  </div>
                </Link>
              </div>
            </FlexColumn>
          </FlexColumn>
        ))}
      </ProjectsDataView>
    </Container>
  );
}

export default ProjectsData;

export const Container = styled.div`
  max-width: 90vw;
  margin: 0 auto;
  overflow-x: hidden;
  margin-bottom: 30px;
  @media only screen and (max-width: 419px) {
    max-width: 98vw;
  }
`;

export const ProjectsDataView = styled.div`
  @media only screen and (max-width: 767px) {
    flex-direction: column;
  }
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: center;
`;
