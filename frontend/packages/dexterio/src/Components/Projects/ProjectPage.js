import React, { useState, useEffect } from "react";
import {
  Container,
  FontSizeHeader2,
  FontSizeSubHeader1,
} from "globalstyles";
import {
  Heading,
  Img,
  ImgWrapperDesktop,
  Subtitle,
} from "../Home/IntroPage/IntroPage.element";
import  { TextWrapper } from 'common';
import {
  PreDexterioStatus,
  ProjectSec,
  ProjectRow,
  ProjectVerticalSpacing,
  ProjectColumn,
  ProjectImages,
  ProjectImagesColumn,
} from "./ProjectPage.element";
import ConsultPage from "../ConsultPage/ConsultPage";
import Rating from "@material-ui/lab/Rating";
import { useTheme } from "@material-ui/styles";
import SwipeableViews from "react-swipeable-views";
import { autoPlay } from "react-swipeable-views-utils";
import { Box } from "@material-ui/core";
import Pagination from "../Common/Pagination";
import projectsData from "../../data/projectsData.json";

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

function ProjectPage(props) {
  const [project, setProject] = useState({
    id: "",
    projectTitle: "",
    slug: "",
    projectImage: "",
    projectText: "",
    beforeDexterio: "",
    results: "",
    resultImages: "",
    accolades: "",
  });
  // eslint-disable-next-line no-unused-vars
  const [slug, setSlug] = useState("");

  useEffect(() => {
    const slug = props.match.params.slug;
    const projectData = projectsData.data.find(
      (project) => project.slug === slug
    );
    setProject(projectData);
    setSlug(slug);
  }, [project, props.match.params.slug]);

  const theme = useTheme();
  const [activeStep, setActiveStep] = useState(0);

  const handleStepChange = (step) => {
    setActiveStep(step);
  };

  if (project.projectImage === "") return null;

  return (
    <div style={{ overflow: "hidden" }}>
      <ProjectSec backgroundcolor="#f7f8fa">
        <Container style={{ paddingTop: "50px", paddingBottom: "50px" }}>
          <ProjectRow>
            <ProjectColumn justify="left">
              <ImgWrapperDesktop>
                <Img
                  style={{ borderRadius: "10px" }}
                  src={project.projectImage}
                  alt="Image"
                />
              </ImgWrapperDesktop>
            </ProjectColumn>
            <ProjectColumn style={{ alignItems: "center" }} justify="right">
              <TextWrapper style={{ padding: "0px" }}>
                <Heading style={{ fontSize: "42px", maxWidth: "80%" }}>
                  {project.projectTitle}
                </Heading>
                <Subtitle>{project.projectText}</Subtitle>
              </TextWrapper>
            </ProjectColumn>
          </ProjectRow>
          <ProjectVerticalSpacing width="90" />
          <PreDexterioStatus backgroundcolor="#ffffff">
            <FontSizeHeader2
              style={{ display: "flex", "justify-content": "center" }}
            >
              Before Dexterio
            </FontSizeHeader2>
            <ProjectVerticalSpacing width="20" />
            <FontSizeSubHeader1
              style={{
                display: "flex",
                "alignItems": "center",
                color: "#000707B5",
                fontFamily: '"Open Sans", sans-serif',
                fontSize: "15px",
                fontWeight: "600",
                lineHeight: "1.7em",
                letterSpacing: "0.8px",
              }}
            >
              {project.beforeDexterio}
            </FontSizeSubHeader1>
          </PreDexterioStatus>
        </Container>
      </ProjectSec>
      <ProjectSec backgroundcolor="#ffffff">
        <Container>
          <ProjectVerticalSpacing width="30" />
          <PreDexterioStatus backgroundcolor="#ffffff">
            <FontSizeHeader2
              style={{ display: "flex", "justifyContent": "center" }}
            >
              Take a look at the results
            </FontSizeHeader2>
            <ProjectVerticalSpacing width="30" />

            <FontSizeSubHeader1
              style={{
                color: "#000707B5",
                fontFamily: '"Open Sans", sans-serif',
                fontSize: "15px",
                fontWeight: "600",
                lineHeight: "1.7em",
                letterSpacing: "0.8px",
              }}
            >
              {project.results}
            </FontSizeSubHeader1>
          </PreDexterioStatus>

          <ProjectImages backgroundcolor="#ffffff">
            <ProjectImagesColumn>
              <div>
                <img
                  style={{
                    borderRadius: "10px",
                    width: "100%",
                    objectFit: "fill",
                    objectPosition: "center",
                  }}
                  src={project.resultImages.fullHeight.src}
                  srcSet={project.resultImages.fullHeight.srcSet}
                  sizes={project.resultImages.fullHeight.sizes}
                  alt=""
                />
              </div>
            </ProjectImagesColumn>
            <ProjectImagesColumn>
              <div>
                <img
                  style={{
                    maxHeight: "260px",
                    borderRadius: "10px",
                    width: "100%",
                    objectFit: "fill",
                    objectPosition: "center",
                  }}
                  src={project.resultImages.divided[0].src}
                  srcSet={project.resultImages.divided[0].srcSet}
                  sizes={project.resultImages.divided[0].sizes}
                  alt=""
                />
              </div>
              <div>
                <img
                  style={{
                    maxHeight: "260px",
                    borderRadius: "10px",
                    width: "100%",
                    objectFit: "fill",
                    objectPosition: "center",
                  }}
                  src={project.resultImages.divided[1].src}
                  srcSet={project.resultImages.divided[1].srcSet}
                  sizes={project.resultImages.divided[1].sizes}
                  alt=""
                />
              </div>
            </ProjectImagesColumn>
          </ProjectImages>

          <PreDexterioStatus
            backgroundcolor="#f7f8fa"
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <Box
              component="fieldset"
              mb={3}
              borderColor="transparent"
              style={{ transform: "translateY(50%)" }}
            >
              <Rating name="read-only" value={5} readOnly />
            </Box>
            <ProjectVerticalSpacing width="30" />
            <Subtitle
              style={{ display: "flex", alignItems: "center", maxWidth: "90%" }}
            >
              {project.accolades.text}
            </Subtitle>
            <ProjectVerticalSpacing width="20" />
            <FontSizeSubHeader1
              style={{
                color: "#000707B5",
                fontFamily: '"Open Sans", sans-serif',
                fontSize: "15px",
                fontWeight: "600",
                lineHeight: "1.7em",
                letterSpacing: "0.8px",
              }}
            >
              {project.accolades.customername}
            </FontSizeSubHeader1>
          </PreDexterioStatus>

          <PreDexterioStatus
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              flexDirection: "column",
              position: "relative",
            }}
          >
            <AutoPlaySwipeableViews
              axis={theme.direction === "rtl" ? "x-reverse" : "x"}
              index={activeStep}
              onChangeIndex={handleStepChange}
              enableMouseEvents
            >
              {project.carouselImages.map((step, index) => (
                <div key={step.label}>
                  {Math.abs(activeStep - index) <= 2 ? (
                    <Box
                      component="img"
                      sx={{
                        display: "block",
                        alignItems: "center",
                        overflow: "hidden",
                        width: "95%",
                        objectFit: "contain",
                        objectPosition: "center",
                        borderRadius: "10px",
                      }}
                      src={step}
                      alt={index}
                    />
                  ) : null}
                </div>
              ))}
            </AutoPlaySwipeableViews>
            <Pagination
              dots={project.carouselImages.length}
              index={activeStep}
              onChangeIndex={handleStepChange}
            />
          </PreDexterioStatus>
        </Container>
      </ProjectSec>
      <ConsultPage color="#faefe6" />
    </div>
  );
}

export default ProjectPage;
