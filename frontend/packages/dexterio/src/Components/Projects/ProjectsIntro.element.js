import styled from 'styled-components';
import { FontSizeHeader1 } from '../../globalStyles';

export const Heading = styled(FontSizeHeader1)`
    margin-bottom: 24px;
    font-size: 80px;

    @media screen and (max-width: 1024px) {
        font-size: 46px;
    }
    
    @media screen and (max-width: 768px) {
        font-size: 40px;
    }

    @media screen and (max-width: 430px) {
        font-size: 35px;
    }

    @media screen and (max-width: 375px) {
        font-size: 30px;
    }

`;

export const Subtitle = styled.p`
    max-width:440px;
    margin-top: auto;
    margin-bottom: 35px;
    color: #000707DE;
    font-family: "Open Sans", sans-serif;
    font-size: 18px;
    font-weight: 600;
    line-height: 1.6em;

    @media screen and (max-width: 1024px) {
        font-size: 20px;
    }
    
    @media screen and (max-width: 768px) {
        font-size: 18px;
        max-width:320px;
    }

    @media screen and (max-width: 430px) {
        font-size: 16px;
    }

    @media screen and (max-width: 375px) {
        font-size: 14px;
    }
`;





