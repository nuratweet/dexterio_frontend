import React from 'react';
import BackGroundImageWithText from "../Common/BackGroundImageWithText";
import ProjectsIntro from './ProjectsIntro';
import ProjectsData from './ProjectsData';



const Projects3 = (props) => {
    return (
        <section style={{"overflow":"hidden"}}>
            <ProjectsIntro />
            <ProjectsData />
            <BackGroundImageWithText data={{ page: "homecalltoaction", imagePath: "estimate_5.webp" }} />
        </section>
    )
}

export default Projects3;