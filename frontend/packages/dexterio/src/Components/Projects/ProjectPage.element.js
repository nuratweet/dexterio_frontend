import styled from 'styled-components';
import { InfoColumn, InfoRow, InfoSec } from '../Home/IntroPage/IntroPage.element';
import {
    FlexRow,
  } from "common";
  

export const ProjectSec = styled(InfoSec)`
    background-color: ${({backgroundcolor}) => backgroundcolor};
    padding: 0px;

    @media only screen and (max-width: 767px) {
        margin: 0px;;
        
    }
`;

export const ProjectRow = styled(InfoRow)`
    flex-direction: row;
    align-items: center;
    @media only screen and (max-width: 767px) {
        flex-direction: column;
    }
`;

export const ProjectColumn = styled(InfoColumn)`
    display: flex;
    justify-content: ${({justify}) => (justify)};
    margin: 0px;
    padding: 0px;
    padding-left: calc(15px + (100vw - 700px) * (15 - 30) / (700 -  1000));
`;

export const ProjectVerticalSpacing = styled.div`
    height: ${({width}) => width}px;

    @media only screen and (max-width: 767px) {
        --mobilewidth = calc(width/4);
        height: {--mobilewidth}px;
    }
`;


export const PreDexterioStatus = styled.div`
    background:  ${({backgroundcolor}) => backgroundcolor};;
    border-radius: 8px;

    margin-left: 8%;
    margin-right: 8%;
    padding: calc(15px + (100vw - 700px) * (15 - 30) / (700 -  1000));
    padding-top: 0px;
    padding-bottom: 0px;

    @media only screen and (max-width: 600px) {
        margin-left: 0;
        margin-right: 0;
    }
`;

export const ProjectImages = styled.div`
    margin: 0px 90px 0px 90px;
    margin-left: calc(15px + (100vw - 700px) * (15 - 30) / (700 - 1000));
    background:  ${({backgroundcolor}) => backgroundcolor};;
    display:flex;
    flex-direction:row;
    gap: 6rem;
    margin-bottom: 70px;
`;

export const ProjectImagesColumn = styled.div`
    display:flex; 
    flex-direction: column;
    width: 50%;
    justify-content: space-between;
`;

export const Welcome = styled.div`
    width: 100%;
    max-width: 1300px;
    margin-left: auto;
    margin-right: auto;
    padding-left: 20px;
    padding-right: 20px;
    margin-bottom: 20px;

    &::after {
        border-bottom: 2px dashed rgb(226, 226, 226);
    }
`;

export const WelcomeHeading = styled.h2`
    color: rgb(28, 33, 31);
    margin-bottom: 1.25rem;
    font-size: 2rem;
    line-height: 1.25;
    font-weight: bold;
    letter-spacing: -0.1px;

    @media (max-width: 1199px) {
        font-size: calc(23.82px + 0.68vw);
    }

`;

export const ProjectImagesContainer = styled.div`
    --base-individual-project-max-width: 1300px;
    max-width: var(--base-individual-project-max-width);
    display: flex;
    grid-gap: 1rem;
    justify-content: center;
    align-items: center;
    margin: 20px auto;
    flex-direction: column;
`;

export const InvertedQoutes = styled.img`
    z-index: 1000;
    transform: translateY(-50%) !important;

    @media only screen and (max-width: 600px) {
        height: 4.5vh;
        padding-left: 15px;
    }
`;

export const ProjectFlexRow = styled(FlexRow) `
    margin-top: 2vw;
    margin-bottom: 5vw;
    width: 100%; 
    align-items: center;

    @media only screen and (max-width: 600px) {
        margin-top: 15vw;
        margin-bottom: 5vw;
    }
`;

export const ProjectSnap = styled.div`
    margin-bottom: 15vw;

    @media only screen and (max-width: 600px) {
        margin-bottom: 30vw;
    }
`;