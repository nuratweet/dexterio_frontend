import {
  FlexRow,
  FlexAlwaysRow,
  CarouselImages,
  ProfileImageWrapper,
  ProfileImage
} from "common";
import React, { useState, useEffect } from "react";
import BackGroundImageWithText from "../Common/BackGroundImageWithText";
import { PreDexterioStatus, InvertedQoutes , ProjectFlexRow, ProjectSnap} from "./ProjectPage.element";
import SwipeableViews from "react-swipeable-views";
import { autoPlay } from "react-swipeable-views-utils";
import Pagination from "../Common/Pagination";
import ProjectPageIntro from "./ProjectPageIntro";
import { FlexColumn } from "common";
import Rating from "@material-ui/lab/Rating";
import {
  IntroParagraph,
  Container,
  SectionHeadlines,
  SubSectionParagraph,
} from "globalstyles";
import { Box } from "@material-ui/core";
import projectsData from "../../data/projectsData.json";
import { useTheme } from "@material-ui/styles";

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

function ProjectPage1(props) {
  const [project, setProject] = useState({
    id: "",
    projectTitle: "",
    slug: "",
    projectImage: "",
    projectText: "",
    beforeDexterio: "",
    results: "",
    resultImages: "",
    accolades: "",
  });
  // eslint-disable-next-line no-unused-vars
  const [slug, setSlug] = useState("");

  useEffect(() => {
    const slug = props.match.params.slug;
    const projectData = projectsData.data.find(
      (project) => project.slug === slug
    );
    setProject(projectData);
    setSlug(slug);
  }, [project, props.match.params.slug]);

  const theme = useTheme();
  const [activeStep, setActiveStep] = useState(0);

  const handleStepChange = (step) => {
    setActiveStep(step);
  };

  if (project.projectImage === "") return null;

  return (
    <section style={{ overflow: "hidden" }}>
      <ProjectPageIntro
        data={{
          projectTitle: project.projectTitle,
          projectText: project.beforeDexterio,
          projectImage: project.projectImage,
          mobileImage: project.mobileImage,
        }}
      />
      <SectionHeadlines style={{ textAlign: "center" }}>
        <span>{project.projectTitle}</span>
      </SectionHeadlines>
      <Container>
        <ProjectSnap>
          <FlexAlwaysRow>
            <FlexColumn style={{"justifyContent":"center"}}>
              {project.snapImages[0].map((step, index) => (
                <FlexColumn style={{ padding: "10px" }}>
                  <img
                    src={step}
                    alt=""
                    style={{
                      maxWidth: "100%",
                      objectFit: "contain",
                      borderRadius: "8px",
                    }}
                  />
                </FlexColumn>
              ))}
            </FlexColumn>
            <FlexColumn style={{"justifyContent":"center"}}>
              {project.snapImages[1].map((step, index) => (
                <FlexColumn style={{ padding: "10px" }}>
                  <img
                    src={step}
                    alt=""
                    style={{
                      maxWidth: "100%",
                      objectFit: "contain",
                      borderRadius: "8px",
                    }}
                  />
                </FlexColumn>
              ))}
            </FlexColumn>
          </FlexAlwaysRow>
        </ProjectSnap>
        <PreDexterioStatus
          backgroundcolor="#f5f3ed"
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <FlexRow
            style={{
              justifyContent: "space-between",
              width: "100%",
              alignItems: "flex-start",
              flex: 1,
              position: "relative"
            }}
          >
            <FlexRow style={{ "justifyContent":"space-between", width: "100%"}}>
              <div>
                <InvertedQoutes src="/svg/Inverted.svg" alt="" stlye={{flex: 0.9}}/>
              </div>
              <ProjectFlexRow style={{flex: 0.1}}>
                <Rating name="read-only" value={5} readOnly />
              </ProjectFlexRow>
              <ProfileImageWrapper style={{"position":"absolute", "left":"50%", transform: "translate(-50%, -50%)" }}>
                <ProfileImage src={project.accolades.customer_image} alt="" />
              </ProfileImageWrapper>
            </FlexRow>
          </FlexRow>
          <SubSectionParagraph
            style={{ display: "flex", alignItems: "center", maxWidth: "90%" }}
          >
            {project.accolades.text}
          </SubSectionParagraph>
          <IntroParagraph style={{ marginBottom: "2vw", textAlign: "center" }}>
            {project.accolades.customername}
          </IntroParagraph>
        </PreDexterioStatus>
        <Box>
          <AutoPlaySwipeableViews
            axis={theme.direction === "rtl" ? "x-reverse" : "x"}
            index={activeStep}
            onChangeIndex={handleStepChange}
            enableMouseEvents
          >
            {project.carouselImages.map((step, index) => (
            <div key={step.label} style={{"display":"flex","justifyContent":"center","alignItems":"center", "margin": "auto"}}>
              {Math.abs(activeStep - index) <= 2 ? (
                  <CarouselImages
                    src={step}
                    alt="Interior Designer Project"
                  />
                ) : null}
              </div>
            ))}
          </AutoPlaySwipeableViews>
          <Pagination
            dots={project.carouselImages.length}
            index={activeStep}
            onChangeIndex={handleStepChange}
          />
        </Box>
      </Container>

      <BackGroundImageWithText
        data={{ page: "homecalltoaction", imagePath: "estimate_5.webp" }}
      />
    </section>
  );
}

export default ProjectPage1;
