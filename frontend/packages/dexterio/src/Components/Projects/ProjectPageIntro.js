import React from "react";
import { FlexColumn, TextWrapper } from "common";
import { Link } from "react-router-dom";
import { IntroParagraph, IntroHeadlines, CurvedButton } from "globalstyles";
import { SvgWrapperRectangles } from "common";
import { LeftPanel, ImageWrapper, ImgDesktop, ImgMobile } from "common";

function ProjectPageIntro(props) {
  const { projectTitle, projectText, projectImage, mobileImage } = { ...props.data };

  return (
    <div style={{ position: "relative", overflow: "hidden" }}>
      <LeftPanel>
        <div>
          <TextWrapper style={{ paddingBottom: "0" }}>
            <IntroHeadlines>{projectTitle}</IntroHeadlines>
            <IntroParagraph>{projectText}</IntroParagraph>
            <Link to="/hello">
              <CurvedButton
                style={{
                  background: "rgba(255,92,92,1)",
                  padding:"6px 29px",
                  fontSize:"1.10vw"
                }}
                light
                curve
                big
                fontBig
                margin="0px"
              >
                Book Online Consultation
              </CurvedButton>
            </Link>
          </TextWrapper>
        </div>
      </LeftPanel>
      <FlexColumn style={{ zIndex: "-100" }}>
        <ImageWrapper>
          <ImgDesktop src={projectImage} alt="Projects Introduction" />
          <ImgMobile src={mobileImage} alt="Projects Introduction" />
        </ImageWrapper>
        <div style={{ height: "50px" }}></div>
      </FlexColumn>
      <SvgWrapperRectangles style={{ transform: "translateY(-50%)" }}>
        <img src="/Rectangles.svg" width="125" height="125" alt="" />
      </SvgWrapperRectangles>
    </div>
  );
}

export default ProjectPageIntro;
