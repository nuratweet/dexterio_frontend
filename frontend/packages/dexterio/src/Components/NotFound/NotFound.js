import React from 'react';
import {Container} from '../../globalStyles';

const NotFound = () => {
  return(
    <div style={{"background-image": "linear-gradient(rgb(255, 255, 255), rgba(255, 235, 222, 0.42))"}}>
      <Container>
        <div style={{"padding":"250px"}}>
          <img src="/page_not_found.svg" alt="Page Not Found"/>
        </div>
      </Container>
    </div>
  );
}

export default NotFound;