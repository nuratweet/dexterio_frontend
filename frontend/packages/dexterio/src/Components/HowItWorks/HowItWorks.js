import React, { useState } from "react";
import { Link } from "react-router-dom";
import { data } from "../../data/HowItWorksAccordianData";
import BackGroundImageWithText from "../Common/BackGroundImageWithText";
import AccordionItem from "../Common/HowItWorksAccordionItem";
import "./HowItWorks.css";

function HowItWorks(props) {
  const [button, setbutton] = useState(true);
  const showButton = () => {
    if (window.innerWidth <= 768) {
      setbutton(false);
    } else {
      setbutton(true);
    }
  };

  const { mobileLeadFormData, setMobileLeadFormData } = {
    ...props.mobileLeadFormData,
  };

  React.useEffect(() => {
    showButton();
    return () => {
      window.removeEventListener("resize", () => {});
    };
  });

  window.addEventListener("resize", showButton);

  const [step1Clicked, setStep1Clicked] = useState("0");
  const [step3Clicked, setStep3Clicked] = useState("0");
  const [step5Clicked, setStep5Clicked] = useState("0");
  const [step1Active, setStep1Active] = useState("react-tabs-0");
  const [step3Active, setStep3Active] = useState("react-tabs-0");
  const [step5Active, setStep5Active] = useState("react-tabs-0");

  return (
    <>
      <section
        className="style__LayoutWrap-sc-17sdrek-0 iboecf"
        style={{ opacity: "1" }}
      >
        <div className="style__ContentInner-sc-17sdrek-1 jGEAmd">
          <section className="how-it-works__HowItWorksPageWrap-sc-3z546x-0 gyYQtw">
            <header className="style__HowItWorksHeroWrap-sc-a1div4-0 bXeJDg">
              <span
                className="style__HeroBG-sc-a1div4-1 bZrjDG"
                style={{ transform: "translateX(0%) translateZ(0px)" }}
              />
              <span
                className="style__HeroPattern-sc-a1div4-2 hQhXEr"
                style={{ opacity: "1", transform: "none" }}
              />
              <div className="Container-sc-b4hb0m-0 doSWzV">
                <div className="sc-gsTEea kKQBVH row">
                  <div className="sc-dlfnuX ljBSYu">
                    <div className="style__HeroText-sc-a1div4-3 bJNniz">
                      <div style={{ opacity: "1", transform: "none" }}>
                        <img
                          src="/svg/slash.svg"
                          alt="slash"
                          className="slash"
                        />
                      </div>
                      <div style={{ opacity: "1", transform: "none" }}>
                        <h1 className="Heading-sc-1dlwch6-0 itdznI">
                          How it <span>Works</span>
                        </h1>
                      </div>
                      <div className="text">
                        <p
                          className="text-xl"
                          style={{ opacity: "1", transform: "none" }}
                        >
                          Create a home you love with Dexterio. Get started in a
                          few easy steps.
                        </p>
                        <p style={{ opacity: "1", transform: "none" }}>
                          {button ? (
                            <Link to="/hello">
                              <button className="Button-sc-g0wer1-1 ewyUxR">
                                Get an Estimate <i>→</i>
                              </button>
                            </Link>
                          ) : (
                            <button
                              className="Button-sc-g0wer1-1 ewyUxR"
                              onClick={() => {
                                props.mobileLeadFormData.setMobileLeadFormData(
                                  true
                                );
                              }}
                            >
                              Get an Estimate <i>→</i>
                            </button>
                          )}
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="sc-dlfnuX kaZyOD">
                    <div
                      className="style__HeroImage-sc-a1div4-4 kLZUNV"
                      style={{ opacity: "1", transform: "none" }}
                    >
                      <div className="img-wrap">
                        <picture>
                          <source
                            media="(max-width: 767px)"
                            srcSet="/img/work-hero-img2-mobile.webp"
                            type="image/webp"
                          />
                          <source
                            media="(max-width: 767px)"
                            srcSet="/img/work-hero-img2-mobile.jpg"
                            type="image/jpeg"
                          />
                          <img
                            className="hero_img"
                            src="/img/work-hero-img2.jpg"
                            srcSet="/img/work-hero-img2.jpg 1x, /img/work-hero-img2@2x.jpg 2x"
                            alt="hero"
                          />
                        </picture>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </header>
            <section className="style__HowItWorksWelcomeWrap-sc-p40e0y-0 lbmIxI">
              <div className="Container-sc-b4hb0m-0 doSWzV">
                <div style={{ opacity: "1", transform: "none" }}>
                  <h2 className="Heading-sc-1dlwch6-0 gRClMq">
                    Welcome to a streamlined home renovation <br />
                    platform to plan, design, and build
                    <br /> <span>all in one place.</span>
                  </h2>
                </div>
              </div>
            </section>
            <section className="HowItWorksInfo__HowItWorksInfosWrap-sc-1dxvi2w-0 hsdkmN">
              <div className="style__HowItWorksWrap-sc-1fzc1wr-0 jNfiDm">
                <div className="style__HowItWorksSteps-sc-1fzc1wr-1 dIOrBv">
                  <div className="style__HowItWorksStepItem-sc-1fzc1wr-2 grJclU">
                    <div className="Container-sc-b4hb0m-0 doSWzV translateContent">
                      <div className="step-heading">
                        <span
                          className="number"
                          style={{ opacity: "1", transform: "none" }}
                        >
                          1
                        </span>
                        <div style={{ opacity: "1", transform: "none" }}>
                          <h3 className="Heading-sc-1dlwch6-0 cXrisn">
                            Personalized <span>Interior Design</span>
                          </h3>
                        </div>
                        <p
                          className="lead"
                          style={{ opacity: "1", transform: "none" }}
                        >
                          Take our online Design Quiz to help us understand your
                          personalized choices.
                          <br /> Dexterio's Design Consultant will schedule a
                          call to further understand your renovation goals and
                          provide a detailed, all-inclusive estimate.
                        </p>
                        <p
                          className="btn-cta"
                          style={{ opacity: "1", transform: "none" }}
                        >
                          {button ? (
                            <Link to="/hello">
                              <button className="Button-sc-g0wer1-1 ewyUxR">
                                Book Online Consultation <i>→</i>
                              </button>
                            </Link>
                          ) : (
                            <button
                              className="Button-sc-g0wer1-1 ewyUxR"
                              onClick={() => {
                                props.mobileLeadFormData.setMobileLeadFormData(
                                  true
                                );
                              }}
                            >
                              Book Online Consultation <i>→</i>
                            </button>
                          )}
                        </p>
                      </div>
                      <div className="style__Step3Tabs-sc-1fzc1wr-3 fA-dNms">
                        <div className="react-tabs" data-tabs="true">
                          <div className="sc-gsTEea kKQBVH row">
                            <div className="sc-dlfnuX eQxpxE">
                              <div>
                                <ul
                                  className="react-tabs__tab-list"
                                  role="tablist"
                                >
                                  <li
                                    className={
                                      step1Active === "react-tabs-0"
                                        ? "react-tabs__tab  react-tabs__tab--selected"
                                        : "react-tabs__tab"
                                    }
                                    role="tab"
                                    id="react-tabs-0"
                                    aria-selected="false"
                                    aria-disabled="false"
                                    aria-controls="react-tabs-1"
                                    onMouseEnter={() =>
                                      setStep1Active("react-tabs-0")
                                    }
                                  >
                                    <div
                                      style={{
                                        opacity: "1",
                                        transform: "none",
                                      }}
                                    >
                                      <span className="line"></span>
                                      <span className="check">
                                        <svg
                                          viewBox="0 0 1024 1024"
                                          style={{
                                            display: "inline-block",
                                            stroke: "currentcolor",
                                            fill: "currentcolor",
                                            width: "10px",
                                            height: "10px",
                                          }}
                                        >
                                          <path d="M1024 267.832l-162.909-158.118-465.455 451.765-232.727-225.882-162.909 158.118 395.636 384z"></path>
                                        </svg>
                                      </span>
                                      <h4>
                                        Design Quiz <span>→</span>
                                      </h4>
                                      <p>
                                        Let’s analyze your situation first. Go
                                        through our onboarding quiz & let our
                                        interior designers know what plan suits
                                        you the best.
                                      </p>
                                    </div>
                                  </li>
                                  <li
                                    className={
                                      step1Active === "react-tabs-2"
                                        ? "react-tabs__tab  react-tabs__tab--selected"
                                        : "react-tabs__tab"
                                    }
                                    role="tab"
                                    id="react-tabs-2"
                                    aria-selected="false"
                                    aria-disabled="false"
                                    aria-controls="react-tabs-3"
                                    onMouseEnter={() =>
                                      setStep1Active("react-tabs-2")
                                    }
                                  >
                                    <div
                                      style={{
                                        opacity: "1",
                                        transform: "none",
                                      }}
                                    >
                                      <span className="line"></span>
                                      <span className="check">
                                        <svg
                                          viewBox="0 0 1024 1024"
                                          style={{
                                            display: "inline-block",
                                            stroke: "currentcolor",
                                            fill: "currentcolor",
                                            width: "10px",
                                            height: "10px",
                                          }}
                                        >
                                          <path d="M1024 267.832l-162.909-158.118-465.455 451.765-232.727-225.882-162.909 158.118 395.636 384z"></path>
                                        </svg>
                                      </span>
                                      <h4>
                                        Get free virtual consultation{" "}
                                        <span>→</span>
                                      </h4>
                                      <p>
                                        Talk to our design consultant to discuss
                                        your ideas , needs and budget to enliven
                                        your space instantly. Get personalized
                                        consultancy and an all-inclusive
                                        estimate for your interior project.
                                      </p>
                                    </div>
                                  </li>
                                </ul>
                              </div>
                            </div>
                            <div className="sc-dlfnuX eQxpxE">
                              <div style={{ opacity: "1", transform: "none" }}>
                                <div
                                  className={
                                    step1Active === "react-tabs-0"
                                      ? "react-tabs__tab-panel  react-tabs__tab-panel--selected"
                                      : "react-tabs__tab-panel"
                                  }
                                  role="tabpanel"
                                  id="react-tabs-1"
                                  aria-labelledby="react-tabs-0"
                                >
                                  <div className="img-wrap">
                                    <picture>
                                      <source
                                        srcSet="/img/home-howitworks-step1-1.webp 1x, /img/home-howitworks-step1-1@2x.webp 2x"
                                        type="image/webp"
                                      />
                                      <source
                                        srcSet="/img/home-howitworks-step1-1.jpg 1x, /img/home-howitworks-step1-1@2x.jpg 2x"
                                        type="image/jpeg"
                                      />
                                      <img
                                        src="/img/home-howitworks-step1-1.jpg"
                                        srcSet="/img/home-howitworks-step1-1.jpg 1x, /img/home-howitworks-step1-1@2x.jpg 2x"
                                        style={{ borderRadius: "10px" }}
                                        sizes="100%"
                                        alt="How it works"
                                      />
                                    </picture>
                                  </div>
                                </div>
                                <div
                                  className={
                                    step1Active === "react-tabs-2"
                                      ? "react-tabs__tab-panel  react-tabs__tab-panel--selected"
                                      : "react-tabs__tab-panel"
                                  }
                                  role="tabpanel"
                                  id="react-tabs-3"
                                  aria-labelledby="react-tabs-2"
                                >
                                  <div className="img-wrap">
                                    <picture>
                                      <source
                                        srcSet="/img/home-howitworks-step1-2.webp 1x, /img/home-howitworks-step1-2@2x.webp 2x"
                                        type="image/webp"
                                      />
                                      <source
                                        srcSet="/img/home-howitworks-step1-2.jpg 1x, /img/home-howitworks-step1-2@2x.jpg 2x"
                                        type="image/jpeg"
                                      />
                                      <img
                                        src="/img/home-howitworks-step1-2.jpg"
                                        srcSet="/img/home-howitworks-step1-2.jpg 1x, /img/home-howitworks-step1-2@2x.jpg 2x"
                                        style={{ borderRadius: "10px" }}
                                        sizes="100%"
                                        alt="How it works"
                                      />
                                    </picture>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <ul className="style__Step3Accordion-sc-1fzc1wr-4 aPJIY">
                        <div style={{ opacity: "1", transform: "none" }}>
                          {data.Step1.value.map((faq, index) => (
                            <div key={index}>
                              <AccordionItem
                                onToggle={() => setStep1Clicked(index)}
                                active={step1Clicked === index}
                                index={index}
                                item={faq}
                              />
                            </div>
                          ))}
                        </div>
                      </ul>
                    </div>
                  </div>
                  <div className="style__HowItWorksStepItem-sc-1fzc1wr-2 eVdiAH">
                    <div className="Container-sc-b4hb0m-0 doSWzV translateContent">
                      <div className="step-heading">
                        <span
                          className="number"
                          style={{ opacity: "1", transform: "none" }}
                        >
                          2
                        </span>
                        <div style={{ opacity: "1", transform: "none" }}>
                          <h3 className="Heading-sc-1dlwch6-0 cXrisn">
                            Let's Seal the Deal
                          </h3>
                        </div>
                        <p
                          className="lead"
                          style={{ opacity: "1", transform: "none" }}
                        >
                          Happy with our proposal. Pay 5% of the estimated
                          project cost as a booking amount and kickstart your
                          planning.
                        </p>
                      </div>
                      <div className="image">
                        <div
                          className="img-wrap"
                          style={{ opacity: "1", transform: "none" }}
                        >
                          <picture>
                            <source
                              media="(max-width: 767px)"
                              srcSet="/img/home-howitworks-step2-1-mobile.webp 1x, /img/home-howitworks-step2-1-mobile@2x.webp 2x"
                              type="image/webp"
                            />
                            <source
                              media="(max-width: 767px)"
                              srcSet="/img/home-howitworks-step2-1-mobile.jpg 1x, /img/home-howitworks-step2-1-mobile@2x.jpg 2x"
                              type="image/jpeg"
                            />
                            <source
                              media="(min-width: 768px)"
                              srcSet="/img/home-howitworks-step2-1.webp 1x, /img/home-howitworks-step2-1@2x.webp 2x"
                              type="image/webp"
                            />
                            <source
                              media="(min-width: 768px)"
                              srcSet="/img/home-howitworks-step2-1.jpg 1x, /img/home-howitworks-step2-1@2x.jpg 2x"
                              type="image/jpeg"
                            />
                            <img
                              src="/img/home-howitworks-step2-1.jpg"
                              srcSet="/img/home-howitworks-step2-1.jpg 1x, /img/home-howitworks-step2-1@2x.jpg 2x"
                              sizes="100%"
                              alt="How it Works"
                              width=""
                              height=""
                            />
                          </picture>
                        </div>
                        <div
                          className="estimate"
                          style={{ opacity: "1", transform: "none" }}
                        >
                          <h4>Your Project Estimate</h4>
                          <ul className="prices">
                            <li style={{ opacity: "1", transform: "none" }}>
                              <h5>Design</h5>
                              <p>₹ XXX</p>
                            </li>
                            <li style={{ opacity: "1", transform: "none" }}>
                              <h5>Project Support</h5>
                              <p>₹ XXX</p>
                            </li>
                            <li style={{ opacity: "1", transform: "none" }}>
                              <h5>Construction</h5>
                              <p>₹ XXX</p>
                            </li>
                            <li style={{ opacity: "1", transform: "none" }}>
                              <h5>
                                <strong>Estimated Total</strong>
                              </h5>
                              <p>
                                <strong>₹ XXX</strong>
                              </p>
                            </li>
                          </ul>
                          <div
                            className="logo"
                            style={{ opacity: "1", transform: "none" }}
                          >
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              viewBox="0 0 100 23"
                              width="100"
                              height="23"
                            >
                              <text x="0" y="15" fill="black">
                                DEXTERIO
                              </text>
                            </svg>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="style__HowItWorksStepItem-sc-1fzc1wr-2 gORjmq">
                    <div className="Container-sc-b4hb0m-0 doSWzV translateContent">
                      <div className="step-heading">
                        <span
                          className="number"
                          style={{ opacity: "1", transform: "none" }}
                        >
                          3
                        </span>
                        <div style={{ opacity: "1", transform: "none" }}>
                          <h3 className="Heading-sc-1dlwch6-0 cXrisn" style = {{paddingTop:"0px"}}>
                            Place the <br /> <span>Partial Order</span>
                          </h3>
                        </div>
                      </div>
                      <div className="style__Step3Tabs-sc-1fzc1wr-3 fA-dNms">
                        <div className="react-tabs" data-tabs="true">
                          <div className="sc-gsTEea kKQBVH row">
                            <div className="sc-dlfnuX eQxpxE">
                              <div>
                                <ul
                                  className="react-tabs__tab-list"
                                  role="tablist"
                                >
                                  <li
                                    className={
                                      step3Active === "react-tabs-0"
                                        ? "react-tabs__tab  react-tabs__tab--selected"
                                        : "react-tabs__tab"
                                    }
                                    role="tab"
                                    id="react-tabs-0"
                                    aria-selected="false"
                                    aria-disabled="false"
                                    aria-controls="react-tabs-1"
                                    onMouseEnter={() =>
                                      setStep3Active("react-tabs-0")
                                    }
                                  >
                                    <div
                                      style={{
                                        opacity: "1",
                                        transform: "none",
                                      }}
                                    >
                                      <span className="line"></span>
                                      <span className="check">
                                        <svg
                                          viewBox="0 0 1024 1024"
                                          style={{
                                            display: "inline-block",
                                            stroke: "currentcolor",
                                            fill: "currentcolor",
                                            width: "10px",
                                            height: "10px",
                                          }}
                                        >
                                          <path d="M1024 267.832l-162.909-158.118-465.455 451.765-232.727-225.882-162.909 158.118 395.636 384z"></path>
                                        </svg>
                                      </span>
                                      <h4>
                                        Kickstart Execution <span>→</span>
                                      </h4>
                                      <p>
                                        Pay 50% of the quoted amount. Our
                                        procurement team orders, inspects and
                                        warehouses the required products and
                                        materials Meanwhile, civil work begins
                                        on the site.
                                      </p>
                                    </div>
                                  </li>
                                  <li
                                    className={
                                      step3Active === "react-tabs-2"
                                        ? "react-tabs__tab  react-tabs__tab--selected"
                                        : "react-tabs__tab"
                                    }
                                    role="tab"
                                    id="react-tabs-2"
                                    aria-selected="false"
                                    aria-disabled="false"
                                    aria-controls="react-tabs-3"
                                    onMouseEnter={() =>
                                      setStep3Active("react-tabs-2")
                                    }
                                  >
                                    <div
                                      style={{
                                        opacity: "1",
                                        transform: "none",
                                      }}
                                    >
                                      <span className="line"></span>
                                      <span className="check">
                                        <svg
                                          viewBox="0 0 1024 1024"
                                          style={{
                                            display: "inline-block",
                                            stroke: "currentcolor",
                                            fill: "currentcolor",
                                            width: "10px",
                                            height: "10px",
                                          }}
                                        >
                                          <path d="M1024 267.832l-162.909-158.118-465.455 451.765-232.727-225.882-162.909 158.118 395.636 384z"></path>
                                        </svg>
                                      </span>
                                      <h4>
                                        Pay 40% of quoted amount <span>→</span>
                                      </h4>
                                      <p>
                                        We’re in the middle of your project. Pay
                                        40% of the quoted amount. The on site
                                        civil work is 50% done and the off site
                                        manufacturing of furniture & modular
                                        items begins at our warehouse.
                                      </p>
                                    </div>
                                  </li>
                                </ul>
                              </div>
                            </div>
                            <div className="sc-dlfnuX eQxpxE">
                              <div style={{ opacity: "1", transform: "none" }}>
                                <div
                                  className={
                                    step3Active === "react-tabs-0"
                                      ? "react-tabs__tab-panel  react-tabs__tab-panel--selected"
                                      : "react-tabs__tab-panel"
                                  }
                                  role="tabpanel"
                                  id="react-tabs-1"
                                  aria-labelledby="react-tabs-0"
                                >
                                  <div className="img-wrap">
                                    <picture>
                                      <source
                                        srcSet="/img/home-howitworks-step3-1.webp 1x, /img/home-howitworks-step3-1@2x.webp 2x"
                                        type="image/webp"
                                      />
                                      <source
                                        srcSet="/img/home-howitworks-step3-1.jpg 1x, /img/home-howitworks-step3-1@2x.jpg 2x"
                                        type="image/jpeg"
                                      />
                                      <img
                                        src="/img/home-howitworks-step3-1.jpg"
                                        srcSet="/img/home-howitworks-step3-1.jpg 1x, /img/home-howitworks-step3-1@2x.jpg 2x"
                                        style={{ borderRadius: "10px" }}
                                        sizes="100%"
                                        alt="How it works"
                                      />
                                    </picture>
                                  </div>
                                </div>
                                <div
                                  className={
                                    step3Active === "react-tabs-2"
                                      ? "react-tabs__tab-panel  react-tabs__tab-panel--selected"
                                      : "react-tabs__tab-panel"
                                  }
                                  role="tabpanel"
                                  id="react-tabs-3"
                                  aria-labelledby="react-tabs-2"
                                >
                                  <div className="img-wrap">
                                    <picture>
                                      <source
                                        srcSet="/img/home-howitworks-step3-2.webp 1x, /img/home-howitworks-step3-2@2x.webp 2x"
                                        type="image/webp"
                                      />
                                      <source
                                        srcSet="/img/home-howitworks-step3-2.jpg 1x, /img/home-howitworks-step3-2@2x.jpg 2x"
                                        type="image/jpeg"
                                      />
                                      <img
                                        src="/img/home-howitworks-step3-2.jpg"
                                        srcSet="/img/home-howitworks-step3-2.jpg 1x, /img/home-howitworks-step3-2@2x.jpg 2x"
                                        style={{ borderRadius: "10px" }}
                                        sizes="100%"
                                        alt="How it works"
                                      />
                                    </picture>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <ul className="style__Step3Accordion-sc-1fzc1wr-4 aPJIY">
                        <div style={{ opacity: "1", transform: "none" }}>
                          {data.Step3.value.map((faq, index) => (
                            <AccordionItem
                              onToggle={() => setStep3Clicked(index)}
                              active={step3Clicked === index}
                              key={index}
                              item={faq}
                            />
                          ))}
                        </div>
                      </ul>
                    </div>
                  </div>
                  <div className="style__HowItWorksStepItem-sc-1fzc1wr-2 eVdiAH">
                    <div className="Container-sc-b4hb0m-0 doSWzV translateContent">
                      <div className="step-heading">
                        <span
                          className="number"
                          style={{ opacity: "1", transform: "none" }}
                        >
                          4
                        </span>
                        <div style={{ opacity: "1", transform: "none" }}>
                          <h3 className="Heading-sc-1dlwch6-0 cXrisn">
                            Installation
                          </h3>
                        </div>
                        <p
                          className="lead"
                          style={{ opacity: "1", transform: "none" }}
                        >
                          Ready to assemble furniture gets delivered to your
                          home from our warehouse and installation work begins
                          as per design.
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="style__HowItWorksStepItem-sc-1fzc1wr-2 gORjmq">
                    <div className="Container-sc-b4hb0m-0 doSWzV translateContent">
                      <div className="step-heading">
                        <span
                          className="number"
                          style={{ opacity: "1", transform: "none" }}
                        >
                          5
                        </span>
                        <div style={{ opacity: "1", transform: "none" }}>
                          <h3 className="Heading-sc-1dlwch6-0 cXrisn" style = {{paddingTop:"0px"}}>
                            Move in to your <br /> <span>New Home</span>
                          </h3>
                        </div>
                      </div>
                      <div className="style__Step3Tabs-sc-1fzc1wr-3 fA-dNms">
                        <div className="react-tabs" data-tabs="true">
                          <div className="sc-gsTEea kKQBVH row">
                            <div className="sc-dlfnuX eQxpxE">
                              <div>
                                <ul
                                  className="react-tabs__tab-list"
                                  role="tablist"
                                >
                                  <li
                                    className={
                                      step5Active === "react-tabs-0"
                                        ? "react-tabs__tab  react-tabs__tab--selected"
                                        : "react-tabs__tab"
                                    }
                                    role="tab"
                                    id="react-tabs-0"
                                    aria-selected="false"
                                    aria-disabled="false"
                                    aria-controls="react-tabs-1"
                                    onMouseEnter={() =>
                                      setStep5Active("react-tabs-0")
                                    }
                                  >
                                    <div
                                      style={{
                                        opacity: "1",
                                        transform: "none",
                                      }}
                                    >
                                      <span className="line"></span>
                                      <span className="check">
                                        <svg
                                          viewBox="0 0 1024 1024"
                                          style={{
                                            display: "inline-block",
                                            stroke: "currentcolor",
                                            fill: "currentcolor",
                                            width: "10px",
                                            height: "10px",
                                          }}
                                        >
                                          <path d="M1024 267.832l-162.909-158.118-465.455 451.765-232.727-225.882-162.909 158.118 395.636 384z"></path>
                                        </svg>
                                      </span>
                                      <h4>
                                        Settle in <span>→</span>
                                      </h4>
                                      <p>
                                        Your home is ready to live and 95% of
                                        the payment has been made. Pay the rest
                                        5% amount, 7 days after you settle in.
                                      </p>
                                    </div>
                                  </li>
                                  <li
                                    className={
                                      step5Active === "react-tabs-2"
                                        ? "react-tabs__tab  react-tabs__tab--selected"
                                        : "react-tabs__tab"
                                    }
                                    role="tab"
                                    id="react-tabs-2"
                                    aria-selected="false"
                                    aria-disabled="false"
                                    aria-controls="react-tabs-3"
                                    onMouseEnter={() =>
                                      setStep5Active("react-tabs-2")
                                    }
                                  >
                                    <div
                                      style={{
                                        opacity: "1",
                                        transform: "none",
                                      }}
                                    >
                                      <span className="line"></span>
                                      <span className="check">
                                        <svg
                                          viewBox="0 0 1024 1024"
                                          style={{
                                            display: "inline-block",
                                            stroke: "currentcolor",
                                            fill: "currentcolor",
                                            width: "10px",
                                            height: "10px",
                                          }}
                                        >
                                          <path d="M1024 267.832l-162.909-158.118-465.455 451.765-232.727-225.882-162.909 158.118 395.636 384z"></path>
                                        </svg>
                                      </span>
                                      <h4>
                                        The Dexterio Support <span>→</span>
                                      </h4>
                                      <p>
                                        True peace of mind kicks in with our 2
                                        year of maintenance support. Also most
                                        of the products in your interior come
                                        with upto 10 years of warranty period.
                                      </p>
                                    </div>
                                  </li>
                                </ul>
                              </div>
                            </div>
                            <div className="sc-dlfnuX eQxpxE">
                              <div style={{ opacity: "1", transform: "none" }}>
                                <div
                                  className={
                                    step5Active === "react-tabs-0"
                                      ? "react-tabs__tab-panel  react-tabs__tab-panel--selected"
                                      : "react-tabs__tab-panel"
                                  }
                                  role="tabpanel"
                                  id="react-tabs-1"
                                  aria-labelledby="react-tabs-0"
                                >
                                  <div className="img-wrap">
                                    <picture>
                                      <source
                                        srcSet="/img/home-howitworks-step5-1.webp 1x, /img/home-howitworks-step5-1@2x.webp 2x"
                                        type="image/webp"
                                      />
                                      <source
                                        srcSet="/img/home-howitworks-step5-1.jpg 1x, /img/home-howitworks-step5-1@2x.jpg 2x"
                                        type="image/jpeg"
                                      />
                                      <img
                                        src="/img/home-howitworks-step5-1.jpg"
                                        srcSet="/img/home-howitworks-step5-1.jpg 1x, /img/home-howitworks-step5-1@2x.jpg 2x"
                                        style={{ borderRadius: "10px" }}
                                        sizes="100%"
                                        alt="How it works"
                                      />
                                    </picture>
                                  </div>
                                </div>
                                <div
                                  className={
                                    step5Active === "react-tabs-2"
                                      ? "react-tabs__tab-panel  react-tabs__tab-panel--selected"
                                      : "react-tabs__tab-panel"
                                  }
                                  role="tabpanel"
                                  id="react-tabs-3"
                                  aria-labelledby="react-tabs-2"
                                >
                                  <div className="img-wrap">
                                    <picture>
                                      <source
                                        srcSet="/img/home-howitworks-step5-2.webp 1x, /img/home-howitworks-step5-2@2x.webp 2x"
                                        type="image/webp"
                                      />
                                      <source
                                        srcSet="/img/home-howitworks-step5-2.jpg 1x, /img/home-howitworks-step5-2@2x.jpg 2x"
                                        type="image/jpeg"
                                      />
                                      <img
                                        src="/img/home-howitworks-step5-2.jpg"
                                        srcSet="/img/home-howitworks-step5-2.jpg 1x, /img/home-howitworks-step5-2@2x.jpg 2x"
                                        style={{ borderRadius: "10px" }}
                                        sizes="100%"
                                        alt="How it works"
                                      />
                                    </picture>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <ul className="style__Step3Accordion-sc-1fzc1wr-4 aPJIY">
                        <div style={{ opacity: "1", transform: "none" }}>
                          {data.Step5.value.map((faq, index) => (
                            <AccordionItem
                              onToggle={() => setStep5Clicked(index)}
                              active={step5Clicked === index}
                              key={index}
                              item={faq}
                            />
                          ))}
                        </div>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </section>
        </div>
      </section>
      <BackGroundImageWithText
        mobileLeadFormData={{ mobileLeadFormData, setMobileLeadFormData }}
        data={{ page: "homecalltoaction", imagePath: "estimate_5.webp" }}
      />
    </>
  );
}

export default HowItWorks;
