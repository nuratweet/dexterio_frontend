import React from "react";
import styled from "styled-components";
import { CurvedButton } from "../../globalStyles";
import ClientSignUpForm from "../Common/ClientSignUpForm";
import "./ClientSignup1.css";
import NewImage from '../../assets/estimate_5.webp';

export default function ClientSignUp1(props) {
  return (
    <div className="parent-signup">
      <div className="sibling-overlay"></div>
      <div className="component-parent" style={{ background: {NewImage}}}>
        <div className="content-wrapper-bottom">
          <div className="text-content-wrapper">
            <div className="bottom-header-content">
              <span>Get Expert Advise</span>
              <br />
              <span style={{ color: "rgba(255,92,92,1)" }}>Right Now</span>
            </div>
            <div className="bottom-para-content">
              <span>
                {" "}
                Join Dexterio's community of vetted Home Professionals. Get the
                all-in-one tool for CRM and project management
              </span>
            </div>
          </div>
          <div className="mobile-button-container">
            <CurvedButton primary="rgb(255,92,92)" light curve margin="0px">
              <span
                style={{
                  color: "white",
                  fontSize: "20px",
                  fontWeight: "bold",
                  fontFamily: "Poppins",
                  padding: "10px 0px",
                }}
              >
                Book Online Consultation
              </span>
            </CurvedButton>
          </div>
          <div className="box-wrapper">
            <LeadFormContainer>
              <LeadFormSection>
                <LeadFormWrapper>
                  <LeadFormArticle>
                    <ClientSignUpForm
                      headerText="Connect with the Team"
                      auth={props.auth}
                    />
                  </LeadFormArticle>
                </LeadFormWrapper>
              </LeadFormSection>
            </LeadFormContainer>
          </div>
        </div>
      </div>
    </div>
  );
}

const LeadFormContainer = styled.div`
  max-width: 422px;

  @media only screen and (max-width: 768px) {
    display: none;
  }
`;

const LeadFormSection = styled.div``;

const LeadFormWrapper = styled.div`
  max-width: 422px;
`;

const LeadFormArticle = styled.article`
  max-width: 422px;
  padding: 44px;
  border-radius: 8px;
  text-align: left;
  position: relative;
  margin-top: 0;
  background-color: #fff;
`;
