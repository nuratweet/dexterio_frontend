import styled from 'styled-components';


export const InfoSec = styled.div`
    color: #fff;
    padding: 42px 0;
    background: '#fff';
`;

export const InfoRow = styled.div`
    display: flex;
    margin: 0 -15px -15px -15px;
    flex-wrap: wrap;
    align-items: center;
    flex-direction: row-reverse;
`;

export const InfoColumn = styled.div`
    margin-bottom: 15px;
    padding-right: 15px;
    padding-left: 15px;
    flex: 1;
    max-width: 50%;
    flex-basis: 50%;

    @media (max-width: 768px) {
        max-width:100%;
        flex-basis: 100%;
        display: flex;
        justify-content: center;
    }
`;

export const TextWrapper = styled.div`
    max-width: 540px;
    padding-top: 0;
    padding-bottom: 60px;

    @media(max-width: 768px) {
        padding-bottom: 65px;
    }
`;

export const ImgWrapper = styled.div`
    max-width:555px;
    display: flex;
    justify-content: center;
`;

export const Img =styled.img`
    padding-right: 0;
    border: 0;
    max-width:100%;
    vertical-align: middle;
    display: inline-block;
    max-height: 500px;
    z-index: 100;
    @media(max-width: 768px) {
        display: none;
    }

`;

export const SpacerItem= styled.div`
    padding: 20px;
`;

export const Input = styled.input`
  color: #0B4C76;
  font-size: 1.5em;
  border: 2px solid #e5efef;
  border-radius: 3px;

`;

export const Heading = styled.div`
    color: #0B4C76;
    font-size: 35px;
    font-weight: 600;
    justify-content: center;

`;