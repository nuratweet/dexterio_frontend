import {InfoSec, InfoColumn, InfoRow, TextWrapper, Input, 
            Img, ImgWrapper, SpacerItem, Heading} from './SignIn.element';
import { CurvedButton , Container} from '../../globalStyles';
import designImage from '../../assets/login.svg';
import FormErrors from "../Errors/FormErrors";
import React, {useState} from 'react';
import { Auth } from 'aws-amplify'

const ClientSignIn = (props) => {
    console.log('ClientSignIn');
    const [loginId, setLoginId] = useState("");
    const error = false;
    const [cognitoUser, setCognitoUser] = useState();
    const [step, setStep] = useState(0);
    const [errors, setErrors] = useState({cognito: null,blankfield: false,passwordmatch: false});
    const [authenticationCode, setAuthenticationCode] = useState("");

    async function handleSubmit(event) {
        event.preventDefault();

        try {            
            let temp = await Auth.signIn(loginId);
            setCognitoUser(temp);
            setStep(1);
        } catch (error) {
            setErrors({
            cognito: 'Please consider signing up.',
            blankfield: error.blankfield,
            passwordmatch: error.passwordmatch
            });
        }
    }

    async function confirmLogin(event){
        event.preventDefault();
        const user = await Auth.sendCustomChallengeAnswer(cognitoUser, authenticationCode);
        try {
            await Auth.currentSession();
            props.auth.setAuthStatus(true);
            props.auth.setUser(user);
            props.history.push("/home");
        } catch(err) {
          if (err === 'No current user') {
            setErrors({
              cognito: 'Wrong OTP, Please enter correct OTP',
              blankfield: error.blankfield,
              passwordmatch: error.passwordmatch
              }
            );  
          }
            console.log(err);
            props.auth.setAuthStatus(false);
            props.auth.setUser(null);
            props.history.push("/hello");
        }
      }
    

    return(
        <>
            <InfoSec>
                <Container>
                    <InfoRow>
                        <InfoColumn>
                            <TextWrapper>
                                <Heading>
                                    Sign In
                                </Heading>
                                <SpacerItem/>
                                <FormErrors formerrors={errors} />
                                <SpacerItem/>
                                { step === 0 && 
                                (
                                    <form onSubmit={handleSubmit} noValidate>
                                        <Input name="email"
                                            value={loginId}
                                            onChange={(event) => setLoginId(event.target.value)}
                                            id="LoginId"
                                            label="LoginId"
                                        />
                                        <SpacerItem/>                                    
                                        <CurvedButton primary='#0B4C76' light fontBig curve margin='0px'>
                                            Continue <i>→</i>
                                        </CurvedButton>
                                    </form>
                                )}
                                { step === 1 && 
                                (
                                    <form onSubmit={confirmLogin} noValidate>
                                        <Input name="email"
                                            value={loginId}
                                            onChange={(event) => setLoginId(event.target.value)}
                                            id="street"
                                            label="Street"
                                        />
                                        <SpacerItem/>
                                        <Input name="email"
                                            value={authenticationCode}
                                            onChange={(event) => setAuthenticationCode(event.target.value)}
                                            id="street"
                                            label="Street"
                                            />
                                        <SpacerItem/>                                    
                                        <CurvedButton primary='#0B4C76' light fontBig curve margin='0px'>
                                            Continue <i>→</i>
                                        </CurvedButton>
                                    </form>
                                )}
                            </TextWrapper>
                        </InfoColumn>
                        <InfoColumn>
                            <ImgWrapper>
                                <Img src={designImage} alt="Image"/>
                            </ImgWrapper>
                        </InfoColumn>
                    </InfoRow>
                </Container>
            </InfoSec>
        </>
    );
}

export default ClientSignIn;