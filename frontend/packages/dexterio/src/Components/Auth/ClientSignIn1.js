import React, {useState} from "react";
import { FlexColumn, TextWrapper, LeftPanel } from "common";
import {
    SubSectionHeadlines,
  SubSubSubSectionHeadlines,
  SubSubSubSectionParagraph,
  CurvedButton,
  VerticalSpacing
} from "globalstyles";
import { FlexRow } from "common";
import { Input, SpacerItem } from "./SignIn.element";

function ClientSignIn1() {
    // eslint-disable-next-line no-unused-vars
    const [step, setStep] = useState(0);
    const [loginId, setLoginId] = useState("");

  return (
    <div style={{ position: "relative", overflow: "hidden" }}>
      <LeftPanel>
        <TextWrapper style={{ paddingBottom: "0" }}>
          <FlexColumn>
            <FlexRow>
              <SubSectionHeadlines>
                D E X T E R I O
              </SubSectionHeadlines>
            </FlexRow>
            <FlexRow>
                <VerticalSpacing height="20px" />
            </FlexRow>
            <FlexRow>
              <div>
                <FlexRow>
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <img
                      src="/icons/suitcase_do.png"
                      srcSet="/icons/suitcase_do.png 1x, /icons/suitcase_do@2x.png 2x"
                      alt=""
                      style={{"width":"60px"}}
                    />
                  </div>
                  <FlexColumn style={{ "paddingLeft":"10px" }}>
                    <SubSubSubSectionHeadlines
                      style={{ color: "rgba(123,131,97,1)" }}
                    >
                      Create a wishlist
                    </SubSubSubSectionHeadlines>
                    <SubSubSubSectionParagraph>
                      Beautiful home interiors to seek inspiration from
                    </SubSubSubSectionParagraph>
                  </FlexColumn>
                </FlexRow>
              </div>
            </FlexRow>
            <FlexRow>
                <VerticalSpacing height="20px" />
            </FlexRow>
            <FlexRow>
              <div>
                <FlexRow>
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <img
                      src="/icons/suitcase_do.png"
                      srcSet="/icons/suitcase_do.png 1x, /icons/suitcase_do@2x.png 2x"
                      alt=""
                      style={{"width":"60px"}}
                    />
                  </div>
                  <FlexColumn style={{ "paddingLeft":"10px" }}>
                    <SubSubSubSectionHeadlines
                      style={{ color: "rgba(123,131,97,1)" }}
                    >
                      Browse catalogue
                    </SubSubSubSectionHeadlines>
                    <SubSubSubSectionParagraph>
                      Widest range of furniture, decor and modular products
                    </SubSubSubSectionParagraph>
                  </FlexColumn>
                </FlexRow>
              </div>
            </FlexRow>
            <FlexRow>
                <VerticalSpacing height="20px" />
            </FlexRow>
            <FlexRow>
              <div>
                <FlexRow>
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <img
                      src="/icons/suitcase_do.png"
                      srcSet="/icons/suitcase_do.png 1x, /icons/suitcase_do@2x.png 2x"
                      alt=""
                      style={{"width":"60px"}}
                    />
                  </div>
                  <FlexColumn style={{ "paddingLeft":"10px"  }}>
                    <SubSubSubSectionHeadlines
                      style={{ color: "rgba(123,131,97,1)" }}
                    >
                      Get free quote
                    </SubSubSubSectionHeadlines>
                    <SubSubSubSectionParagraph>
                      Review quotes for your home interiors
                    </SubSubSubSectionParagraph>
                  </FlexColumn>
                </FlexRow>
              </div>
            </FlexRow>
          </FlexColumn>
        </TextWrapper>
      </LeftPanel>
      <FlexColumn style={{ height: "80vh", "width":"100%", "paddingLeft":"50vw", "justifyContent":"center","alignItems":"center" }}>
        {step === 0 && (
          <form noValidate>
            <Input
              name="email"
              value={loginId}
              onChange={(event) => setLoginId(event.target.value)}
              id="LoginId"
              label="LoginId"
            />
            <SpacerItem />
            <CurvedButton primary="#0B4C76" light fontBig curve margin="0px">
              Continue <i>→</i>
            </CurvedButton>
          </form>
        )}
        {step === 1 && (
          <form  noValidate>
            <Input
              name="email"
              value={loginId}
              onChange={(event) => setLoginId(event.target.value)}
              id="street"
              label="Street"
            />
            <SpacerItem />
            <Input
              name="email"
              id="street"
              label="Street"
            />
            <SpacerItem />
            <CurvedButton primary="#0B4C76" light fontBig curve margin="0px">
              Continue <i>→</i>
            </CurvedButton>
          </form>
        )}
      </FlexColumn>
    </div>
  );
}

export default ClientSignIn1;
