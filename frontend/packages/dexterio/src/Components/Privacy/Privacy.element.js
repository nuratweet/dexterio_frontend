import styled from 'styled-components'
import { FontSizeHeader1, FontSizeSubHeader1 } from '../../globalStyles';

export const StaticPage = styled.div`
    max-width: 1300px;
    padding: 0 20px 40px;
    margin: auto;

    @media only screen and (min-width: 601px) and (max-width: 1024px) {
        padding: 0 30px 30px;
    }

    @media only screen and (max-width: 60px) {
        padding: 30px 25px;
    }

`;

export const StaticBlock = styled.div`
    color: #000707DE;
    font-family: "Open Sans", sans-serif;
    font-size: 18px;
    font-weight: 600;
    line-height: 3.1em;
    letter-spacing: 0.8px;
    transition: background .3s,border .3s,border-radius .3s,box-shadow .3s,-webkit-border-radius .3s,-webkit-box-shadow .3s;

    @media(max-width: 1023px){
        font-family: "Open Sans", sans-serif;
        font-size: 14px;
        font-weight: 400;
        line-height: 1.5;
    }
`;

export const PrivacyPolicyHeader = styled(FontSizeHeader1)`
    display: flex;
    justify-content: center;

`;

export const PrivacyPolicySubHeading1 = styled(FontSizeSubHeader1)`
    font-family: "Open Sans", sans-serif;
    font-size: 18px;
    font-weight: 600;
    line-height: 3.1em;

    @media(max-width: 1023px){
        font-family: "Open Sans", sans-serif;
        font-size: 17px;
        font-weight: 400;
        line-height: 1.85714285714286;
    }
`;
