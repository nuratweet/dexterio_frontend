import React from 'react'
import { PrivacyPolicyHeader, StaticBlock, StaticPage , PrivacyPolicySubHeading1 } from './Privacy.element';
import privacyData from '../../data/privacy.json';

function Privacy() {

    return (
        <>
        <div style={{"height":"45px"}}></div>
        <PrivacyPolicyHeader>Privacy Policy</PrivacyPolicyHeader>
        <div style={{"height":"35px"}}></div>
        <StaticPage>
            <div>
            {
                privacyData.data.map((paragraph, index) => {
                    return(
                        <StaticBlock key={index}>
                            <PrivacyPolicySubHeading1><strong>{paragraph.blockBold}</strong></PrivacyPolicySubHeading1>
                            <p key= {index}>{paragraph.block}</p>
                            <ol style={{"listStyle":"disc inside", "paddingLeft": "20px"}}>
                                {
                                    paragraph.list.map((inner, innerIndex) => {
                                        return (
                                            <li key={innerIndex}>
                                                {inner}
                                            </li>)
                                    })
                                }
                            </ol>
                        </StaticBlock>
                    );
                })
            }
            </div>
        </StaticPage>
        </>
    )
}

export default Privacy;


