import styled, { keyframes } from "styled-components";
import { Link } from "react-router-dom";

export const NavbarContainer = styled.div`
  height: 70px;

  @media (max-width: 768px) {
    background: #fff;
    margin: 0px 0px 0px 0px;
    text-align: center;
    padding: 0px 0px;
    max-width: 100%;
    height: 50px;
  }
`;

export const Nav = styled.nav`
  background: #ffffff;
  width: 100%;
  height: 70px;
  display: flex;
  position: fixed;
  top: 0;
  padding-left: calc(15px + (100vw - 700px) * (15 - 30) / (700 - 1000));
  padding-right: calc(15px + (100vw - 700px) * (15 - 30) / (700 - 1000));
  z-index: 9999;
  align-items: center;
  box-shadow: rgba(0, 0, 0, 0.05) 0px 1px 2px 0px;

  @media (max-width: 768px) {
    height: 50px;
    position: relative;
  }
`;

export const LogoWrapper = styled.div`
  width: 250px;
  height: 100%;
  display: flex;
  align-items: center;

  @media only screen and (max-width: 1350px) {
    width: 100%;
  }

  @media only screen and (max-width: 600px) {
    justify-content: center;
  }
`;

export const NavLogo = styled(Link)`
  cursor: pointer;
  text-decoration: none;
  display: ${({ display }) => (!display ? "flex" : "none")};
  white-space: nowrap;
  text-align: left;
  font-family: "Montserrat";
  font-style: normal;
  font-weight: 600;

  font-size: 30px;
  color: black;
  letter-spacing: 6px;

  @media only screen and (max-width: 1350px) {
    font-size: 1.6rem;
  }

  @media only screen and (max-width: 768px) {
    font-size: 2rem;
    justify-content: center;
  }

  @media only screen and (max-width: 768px) {
    display: flex;
    background: #fff;
    margin: 0px 0px 0px 0px;
    text-align: center;
    max-width: 400px;
  }
`;

const rotateIn = keyframes`
    0% {
        transform-origin: center;
        transform: rotate3d(0,0,1,-200deg);
        opacity: 0;
    }
    100% {
        transform-origin: center;
        transform: none;
        opacity: 1;
    }
`;

export const NavLogoImg = styled(NavLogo)`
  overflow: hidden;
  animation: 1s ${rotateIn} ease-out;
  transform-origin: center;
  width: 70px;
  height: 70px;

  @media (max-width: 430px) {
    margin-right: 0px;
  }
`;

export const Divider = styled.div`
  height: 47px;
  position: relative;
  margin-left: 1em;
  border-left: 2px solid #e5efef;

  @media (max-width: 768px) {
    display: none;
  }
`;

export const NavMenu = styled.div`
  display: flex;
  height: 100%;
  margin: 0 auto;

  @media (max-width: 1024px) {
    display: none;
  }
`;

export const SignUpMenu = styled.div`
  display: flex;

  @media (max-width: 1024px) {
    display: none;
  }
`;

export const NavLinks = styled(Link)`
  color: #000011ee;
  display: flex;
  align-items: center;
  text-decoration: none;
  padding: 0.5rem 1rem;
  height: 100%;

  &: hover {
    color: ${({ light }) => (light ? "#fff" : "rgba(123,131,97,1)")};
  }

  @media (max-width: 1024px) {
    text-align: center;
    padding: 2rem;
    width: 100%;
    display: table;

    &:hover {
      color: #01070b;
      transition: all 0.3s ease;
    }
  }
`;

export const NavItem = styled.div`
    white-space: nowrap;
    text-align: left;
    font-size: 1vw;
    font-family: "Poppins", sans-serif;
    font-weight: normal;
    font-style: normal;
    color: ${({ light }) => (light ? "#fff" : "rgba(0,0,0,1)")};
    letter-spacing: 1.2px;
    margin: 0 10px;

    &:focus {
        ${NavLinks}{
            background:black;
        }
        outline:none;
        background: rgba(0, 0, 0, 0.1);
    }

    &::after {
        content: '';
        display: block;
        height: 2px;
        width: 0;
        background: ${({ light }) => (light ? "#fff" : "rgba(123,131,97,1)")};
        transition: width 0.5s;
    }

    &:hover::after {
        font-size: 1vw;
        color: ${({ light }) => (light ? "#fff" : "rgba(123,131,97,1)")};
        letter-spacing: 1.2px
        transform: width;
        width: 100%
    }

    @media (max-width: 1024px) {
        width: 100%;

        &:hover {
            border: none;
        }
    }
`;

export const NavItemFloatRight = styled(NavItem)`
  margin-left: auto;
`;

export const NavSpacerItem = styled.div`
  height: 70px;
  border-bottom: 2px solid transparent;
  flex: 1;

  @media (max-width: 1024px) {
    width: 100%;
    display: none;

    &:hover {
      border: none;
    }
  }
`;

export const NavItemBtn = styled.div`
  @media (max-width: 1024px) {
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 120px;
  }
`;

export const NavBtnLink = styled(Link)`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
  text-decoration: none;
  padding: 8px 16px;
  width: 100%;
  border: none;
`;

export const StyledUl = styled.ul`
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
`;

export const StyledLi = styled.li`
  float: left;
`;

export const Dropbtn = styled.button`
  background: #f4ff99a1;
  border-radius: 9px;
  display: inline-block;
  color: #01070b;
  text-align: center;
  padding: 10px 20px;
  font-family: "Open Sans", sans-serif;
  font-size: 16px;
  outline: none;
  border: none;

  &:hover {
    background-color: #f4ff99;
  }
`;

export const DropDownContent = styled.div`
  display: none;
  position: absolute;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
  z-index: 10010;
`;

export const DropDownLi = styled(StyledLi)`
  display: inline-block;
  &:hover {
    background-color: #dee5ed;
  }
  &:hover ${DropDownContent} {
    display: block;
  }
`;

export const SubA = styled.a`
  color: black;
  background: #f4ff99a1;
  padding: 10px 20px;
  text-decoration: none;
  display: block;
  text-align: left;
  &:hover {
    background-color: #f4ff99;
  }
`;
