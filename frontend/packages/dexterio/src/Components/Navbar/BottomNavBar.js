import { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import './BottomNavBar.css';
import {DexterioHand, Home, Blog, Projects, HowItWorks} from '../Common/Icons';

const BottomNavBar = props => {
    const history = useHistory()
    const [activeTabs, setActiveTabs] = useState(props.name);
    const [animation, setAnimation] = useState('false');

    useEffect(() => {
        switch (activeTabs) {
            case 'home':
                history.push('/')
                break;
            case 'blogs':
                history.push('/blogs')
                break;
            case 'stylequiz':
                history.push('/quiz')
                break;
            case 'projects':
                history.push('/portfolio')
                break;
            case 'howitworks':
                history.push('/how-it-works')
                break;
            default:
                break;
        }
    }, [activeTabs, history])

    return (
        <div className='bottom-nav-wrapper'>
            <div className='bottom-nav'>
                <div className='bn-tab'>
                    <div>
                        {activeTabs === 'home' ?
                            <Home
                                color='#0B4C76'
                                onClick={() => setActiveTabs('home')}
                            /> :
                            <Home
                                color='#000'
                                onClick={() => setActiveTabs('home')}
                            />}
                    </div>
                    <div className = "bottom-nav-helptext">HOME</div>
                </div>
                <div className='bn-tab'>
                    <div>
                        {activeTabs === 'blogs' ?
                            <Blog
                                color='#0B4C76'
                                onClick={() => setActiveTabs('blogs')}
                            /> :
                            <Blog
                                color='#000'
                                onClick={() => setActiveTabs('blogs')}
                            />}
                    </div>
                    <div className = "bottom-nav-helptext">MAGAZINE</div>
                </div>
                <div className='bn-tab'>
                    <div className={`style-quiz ${animation ? 'active' : ''}`} style={{"transform": "translateY(-40%)"}}>
                        {activeTabs === 'stylequiz' ?
                            <DexterioHand
                                size='35'
                                color='#0B4C76'
                                onClick={() => { setActiveTabs('stylequiz'); setAnimation('true')}}
                                onAnimationEnd={() => setAnimation('false')}
                            /> :
                            <DexterioHand
                                size='35'
                                color='#000'
                                onClick={() => { setActiveTabs('stylequiz'); setAnimation('true')}}
                                onAnimationEnd={() => setAnimation('false')}
                            />}
                    </div>
                </div>
                <div className='bn-tab'>
                    <div>
                        {activeTabs === 'projects' ?
                            <Projects
                                color='#0B4C76'
                                onClick={() => setActiveTabs('projects')}
                            /> :
                            <Projects
                                color='#000'
                                onClick={() => setActiveTabs('projects')}
                            />}
                    </div>
                    <div className = "bottom-nav-helptext">PORTFOLIO</div>
                </div>
                <div className='bn-tab'>
                    <div>
                        {activeTabs === 'howitworks' ?
                            <HowItWorks
                                color='#0B4C76'
                                onClick={() => setActiveTabs('howitworks')}
                            /> :
                            <HowItWorks
                                color='#000'
                                onClick={() => setActiveTabs('howitworks')}
                            />}
                    </div>
                    <div className = "bottom-nav-helptext">HOW IT WORKS</div>
                </div>
            </div>
        </div>
    )
}

export default BottomNavBar