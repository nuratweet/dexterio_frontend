import React , { useState } from 'react';
import { DesktopPane, TermsOfUsePage, MobilePane, TermsOfUseHeading, TermsOfUseSubHeading , TermsOfUseSubHeading1 } from './TermsOfUse.element';
import termData from '../../data/terms.json';
import './TermsOfUse.css';
import { FaAngleDown , FaAngleUp } from 'react-icons/fa';
import { Helmet } from 'react-helmet';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import {Link} from 'react-scroll';

const TermsOfUse = () => {
	const [angleOpen, setAngleOpen] = useState(true);
	const [expanded, setExpanded] = React.useState(false);
	const [activeLink, setActiveLink] = useState('Basic Design Consultation');

	const handleChange = (panel) => (event, isExpanded) => {
		setExpanded(isExpanded ? panel : false);
	};

	return (
		<>
		<Helmet>
			<title>Terms &amp; Conditions | Dexterio</title>
		</Helmet>
        <TermsOfUsePage>
			<DesktopPane width="30%" padding="10px">
				<div style = {{"position":"fixed"}}>
					<div className="dexterio-toc-widget-container" style={{"minHeight": "0px"}}>
						<div className="dexterio-toc__header">
							<h2 className="dexterio-toc__header-title">Terms &amp; Conditions</h2>
							{
								angleOpen ? (
									<div className="dexterio-toc__toggle-button" onClick={()=> setAngleOpen(false)}>
										<FaAngleDown/>
									</div>
								) : 
								(
									<div className="dexterio-toc__toggle-button"  onClick={()=> setAngleOpen(true)}>
										<FaAngleUp/>
									</div>
								)
							}
						</div>
						<div className="dexterio-toc__body" style={{"--show": (angleOpen ? "block" : "none")}}>
							<ul className="dexterio-toc__list-wrapper">
								<li className={`dexterio-toc__list-item ${activeLink === 'Basic Design Consultation' ? "active" : ""}`}>
									<div className="dexterio-toc__list-item-text-wrapper">
										<svg  x="0px" y="0px" width="10px" height="10px" viewBox="0 0 20 20" enableBackground="new 0 0 10 10"><circle cx="10" cy="10" r="5"/></svg>
										<Link  to="basic_design" spy={true} smooth={true} className="dexterio-toc__list-item-text dexterio-toc__top-level dexterio-item-active"  onClick={()=>{setActiveLink('Basic Design Consultation')}}>Basic Design Consultation</Link>
									</div>
								</li>
								<li className={`dexterio-toc__list-item ${activeLink === 'Pro Design Consultation' ? "active" : ""}`} >
									<div className="dexterio-toc__list-item-text-wrapper">
										<svg  x="0px" y="0px" width="10px" height="10px" viewBox="0 0 20 20" enableBackground="new 0 0 10 10"><circle cx="10" cy="10" r="5"/></svg>
										<Link  to="pro_design" spy={true} smooth={true} className="dexterio-toc__list-item-text dexterio-toc__top-level" onClick={()=>{setActiveLink('Pro Design Consultation')}}>Pro Design Consultation</Link>
									</div>
								</li>
								<li className={`dexterio-toc__list-item ${activeLink === 'Turnkey Interiors' ? "active" : ""}`} >
									<div className="dexterio-toc__list-item-text-wrapper">
										<svg  x="0px" y="0px" width="10px" height="10px" viewBox="0 0 20 20" enableBackground="new 0 0 10 10"><circle cx="10" cy="10" r="5"/></svg>
										<Link  to="turnkey" spy={true} smooth={true} className="dexterio-toc__list-item-text dexterio-toc__top-level" onClick={()=>{setActiveLink('Turnkey Interiors')}}>Turnkey Interiors</Link>
									</div>
								</li>
								<li className={`dexterio-toc__list-item ${activeLink === 'Prefab Interiors' ? "active" : ""}`} >
									<div className="dexterio-toc__list-item-text-wrapper">
										<svg  x="0px" y="0px" width="10px" height="10px" viewBox="0 0 20 20" enableBackground="new 0 0 10 10"><circle cx="10" cy="10" r="5"/></svg>
										<Link  to="prefab" spy={true} smooth={true} className="dexterio-toc__list-item-text dexterio-toc__top-level" onClick={()=>{setActiveLink('Prefab Interiors')}}>Prefab Interiors</Link>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</DesktopPane>
			<DesktopPane width="70%" padding="0px 0px 0px 90px">
				<>
					<div style={{"height":"15px"}}></div>
					<TermsOfUseHeading>Terms &amp; Conditions</TermsOfUseHeading>
					{
						termData.data.map((item, index) => {
							return(
								<div id={item.id} key= {index}  style={{"paddingTop":"60px"}}>
									<TermsOfUseSubHeading>{item.key}</TermsOfUseSubHeading>
									<TermsOfUseSubHeading1>
										<ol style={{"listStyle":"number inside"}}>
										{
											item.value.map((inner, innerIndex) => {
												return(
													<li key={innerIndex}>
														{inner.pre}
														<ol style={{"listStyleType":"disc", "paddingLeft": "20px"}}>
															{
																inner.midlist.length > 0 && inner.midlist.map((innerInner, innerInnerIndex) => {
																	return (<li key = {innerInnerIndex}>{innerInner}</li>);
																})
															}
														</ol>
														{inner.post.length > 0 && inner.post}
													</li>)
											})
										}
										</ol>
									</TermsOfUseSubHeading1>
								</div>
							)
						})
					}
				</>
			</DesktopPane>
			<MobilePane>
				<div style={{"height":"45px"}}></div>
				<TermsOfUseHeading>Terms &amp; Conditions</TermsOfUseHeading>
				<div style={{"height":"45px"}}></div>
					{
						termData.data.map((item, index) => {

							return(	
								<Accordion key = {index} expanded={expanded === index} onChange={handleChange(index)} >
									<AccordionSummary
									expandIcon={<FaAngleDown/>}
									>
										<TermsOfUseSubHeading>{item.key}</TermsOfUseSubHeading>
									</AccordionSummary>
									<AccordionDetails>
										<TermsOfUseSubHeading1>
											<ol style={{"listStyle":"number inside"}}>
												{
													item.value.map((inner, innerIndex) => {
														return(
															<li key= {innerIndex} className="dexterio-toc__sections">
																{inner.pre}
																<ol style={{"listStyleType":"disc", "paddingLeft": "20px"}}>
																	{
																		inner.midlist.length > 0 && inner.midlist.map((innerInner, innerInnerIndex) => {
																			return (<li className="dexterio-toc__sections" key={innerInnerIndex}>{innerInner}</li>);
																		})
																	}
																</ol>
																{inner.post.length > 0 && inner.post}
															</li>)
													})
												}
											</ol>
										</TermsOfUseSubHeading1>
									</AccordionDetails>
								</Accordion>
							)
						})
					}
			</MobilePane>
		</TermsOfUsePage>
		</>
    );
};

export default TermsOfUse;