import React from 'react';
import './ConsultPage.css';
import '../HowItWorks/HowItWorks.css';
import ConsultImage from '../../assets/livingRoom.jpg'
import { Link } from 'react-router-dom';
import styled from 'styled-components';

const ConsultPage = ({props}) => {

    return (
        <div className = "outer-wrapper" style={{"--color": "#faefe6"}}>
           <Container>
                <div className="sc-gsTEea kKQBVH row">
                    <div className="sc-dlfnuX ljBSYu">
                        <div className="style__HeroText-sc-a1div4-3 bJNniz">
                            <div style={{"opacity": "1", "transform": "none"}}>
                                <h1 className="Heading-sc-1dlwch6-0 itdznI">Ready for your new Home?</h1>
                            </div>
                            <div className="text">
                                <p className="text-xl" style={{"opacity": "1", "transform": "none"}}>
                                Book a free consultation with one of our interior designers and get your dream apartment furnished.
                                </p>
                                <p style={{"opacity": "1", "transform": "none"}}>
                                    <Link to="/hello">
                                        <button className="Button-sc-g0wer1-1 ewyUxR">Get Started For free <i>→</i></button>
                                    </Link>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="sc-dlfnuX kaZyOD">
                        <div className="style__HeroImage-sc-a1div4-4 kLZUNV" style={{"opacity": "1", "transform": "none"}}>
                            <div className="img-wrap">
                                <img src = {ConsultImage} alt = "" className = "imgStyle" style={{"width":"100%","objectFit":"fill","objectPosition":"center"}}/>
                            </div>
                        </div>
                    </div>
                </div>
            </Container>
        </div>
    );
};

export const Container = styled.div`
    max-width: 90vw;
    margin: 0 auto;
    overflow-x: hidden;
    margin-bottom: 30px;
    @media only screen and (max-width: 419px) {
        max-width: 98vw;
    }
`;


export default ConsultPage;