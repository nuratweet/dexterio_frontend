import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";
import {
  ScrollToTop,
  ScrollToTopButton,
  IndicatorFallback,
  IndicatorProvider,
  Indicator,
  EZYRedirect,
} from "common";
import ClientSignUp from "./Components/Auth/ClientSignUp1";
import Home from "./Components/Home/Home";
import React, { Component } from "react";
import { Auth } from "aws-amplify";
import "./App.css";
import MobileLeads from "./MobileLeads";
import ComingSoon from "./Components/ComingSoon/Coming_Soon";

import Layout from "./Layout";
import ThankYou from "./Components/Common/ThankYou";

const DesignQuiz = React.lazy(() =>
  import("./Components/DesignQuiz/DesignQuiz2")
);
const HowItWorks = React.lazy(() =>
  import("./Components/HowItWorks/HowItWorks")
);
const AboutUs = React.lazy(() => import("./Components/AboutUs/AboutUs"));
const Privacy = React.lazy(() => import("./Components/Privacy/Privacy"));
const Projects = React.lazy(() => import("./Components/Projects/Projects3"));
const ProjectPage = React.lazy(() =>
  import("./Components/Projects/ProjectPage1")
);
const InventoryManagementView = React.lazy(() =>
  import("./Components/VendorManagement/2/VendorManagementView")
);
const CreateDexterioProjects = React.lazy(() =>
  import("./Components/DexterioProjects/CreateDexterioProjects")
);
const MilestoneList = React.lazy(() =>
  import("./Components/Client/Home/Milestone/MilestoneList")
);
const ProjectDocuments = React.lazy(() =>
  import("./Components/Client/Home/ProjectsDocuments/Documents")
);
const CustomerList = React.lazy(() =>
  import("./Components/Client/Home/Customer/CustomerList")
);
const ProjectProgress = React.lazy(() =>
  import("./Components/Client/Home/Progress/Progress")
);
const ProjectImages = React.lazy(() =>
  import("./Components/Client/Home/ProjectImages/ProjectImage")
);
const WarrantyPolicy = React.lazy(() =>
  import("./Components/WarrantyPolicy/WarrantyPolicy")
);
const DashboardLayout = React.lazy(() =>
  import("./Components/Client/Home/DashboardLayout")
);
const ClientSettings = React.lazy(() =>
  import("./Components/Client/ClientSettings")
);
const Account = React.lazy(() =>
  import("./Components/Client/Home/Account/Account")
);
const TermsOfUse = React.lazy(() =>
  import("./Components/TermsOfUse/TermsOfUse")
);
const ClientSignIn = React.lazy(() =>
  import("./Components/Auth/ClientSignIn1")
);
const Updates = React.lazy(() => import("./Components/Client/Updates/Updates"));
const ClientGrid = React.lazy(() => import("./Components/Client/ClientGrid"));
const IDSignUp = React.lazy(() => import("./Components/Auth/IDSignUp"));
const BlogHome = React.lazy(() => import("./Components/Blogs/BlogHome2"));
const Blog = React.lazy(() => import("./Components/Blogs/Blog"));
const BlogCreation = React.lazy(() =>
  import("./Components/Blogs/BlogCreation")
);

const PrivateRoute = ({
  component: Component,
  authed,
  forward,
  authProps,
  ...rest
}) => {
  return (
    <Route
      {...rest}
      render={(props) =>
        authed === true ? (
          <Component {...props} auth={authProps} />
        ) : (
          <Redirect
            to={{ pathname: forward, state: { from: props.location } }}
          />
        )
      }
    />
  );
};

class App extends Component {
  state = {
    isAuthenticated: false,
    isAuthenticating: true,
    user: null,
    mobileLeadFormData: false,
  };

  setAuthStatus = (authenticated) => {
    this.setState({ isAuthenticated: authenticated });
  };

  setUser = (user) => {
    this.setState({ user: user });
  };

  setMobileLeadFormData = (mobileLeadFormData) => {
    this.setState({ mobileLeadFormData: mobileLeadFormData });
  };

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.pathname) {
      window.gtag("config", "G-0769NW9GED", {
        page_title: this.props.location.pathname,
        page_path: this.props.location.pathname,
      });
    }
  }

  async componentDidMount() {
    try {
      await Auth.currentSession();
      this.setAuthStatus(true);
      const user = await Auth.currentAuthenticatedUser();
      this.setUser(user);
    } catch (error) {
      console.log(error);
    }
    this.setState({ isAuthenticating: false });
  }

  pattern = /^(\/c|\/id|\/quiz|\/designer)+(\/[a-z0-9]*)$/gm;

  render() {
    const authProps = {
      isAuthenticated: this.state.isAuthenticated,
      user: this.state.user,
      setAuthStatus: this.setAuthStatus,
      setUser: this.setUser,
    };

    const mobileLeadFormData = {
      mobileLeadFormData: this.state.mobileLeadFormData,
      setMobileLeadFormData: this.setMobileLeadFormData,
    };

    return (
      !this.state.isAuthenticating && (
        <div className="App">
          <Router>
            <ScrollToTop />
            <MobileLeads mobileLeadFormData={mobileLeadFormData} />
            <IndicatorProvider>
              <Indicator />
              <div>
                <div className="body">
                  <Switch>
                    <React.Suspense fallback={<IndicatorFallback />}>
                      <Layout newProps={authProps}>
                        <PrivateRoute
                          authed={!authProps.isAuthenticated}
                          exact
                          path="/id/login"
                          forward="/id/createProfile"
                          component={IDSignUp}
                          authProps={authProps}
                        />
                        {/* <PrivateRoute authed = {authProps.isAuthenticated} exact path = '/idprofile' forward = '/idhello' component={IDProfile} authProps={authProps}/> */}

                        <Route
                          exact
                          path="/prefab"
                          render={(props) => <ComingSoon />}
                        />
                        <Route
                          exact
                          path="/how-it-works"
                          render={(props) => (
                            <HowItWorks
                              mobileLeadFormData={mobileLeadFormData}
                            />
                          )}
                        />
                        <Route
                          exact
                          path="/about"
                          render={(props) => <AboutUs />}
                        />
                        <Route
                          exact
                          path="/thankyou"
                          render={(props) => <ThankYou {...props} />}
                        />
                        <Route
                          exact
                          path="/privacy"
                          render={(props) => <Privacy />}
                        />
                        <Route
                          exact
                          path="/portfolio"
                          render={(props) => <Projects auth={authProps} />}
                        />
                        <Route
                          exact
                          path="/project/:slug"
                          render={(props) => <ProjectPage {...props} />}
                        />
                        <Route
                          exact
                          path="/warranty-policy"
                          render={(props) => <WarrantyPolicy />}
                        />
                        <Route
                          exact
                          path="/terms-conditions"
                          render={(props) => <TermsOfUse />}
                        />
                        <Route
                          exact
                          path="/vendor"
                          render={(props) => <InventoryManagementView />}
                        />
                        <Route
                          exact
                          path="/blogs"
                          render={(props) => <BlogHome {...props} />}
                        />
                        <Route
                          exact
                          path="/blog/:slug"
                          render={(props) => <Blog {...props} />}
                        />
                        <Route
                          exact
                          path="/createBlog/:slug"
                          render={(props) => <BlogCreation {...props} />}
                        />
                        <Route
                          exact
                          path="/documentation"
                          render={(props) => <CreateDexterioProjects />}
                        />
                        <Route
                          exact
                          path="/ezy"
                          render={(props) => <EZYRedirect />}
                        />
                        <PrivateRoute
                          authed={!authProps.isAuthenticated}
                          exact
                          path="/login"
                          forward="/home"
                          component={ClientSignIn}
                          authProps={authProps}
                        />
                        <PrivateRoute
                          authed={authProps.isAuthenticated}
                          exact
                          path="/home"
                          forward="/hello"
                          component={ClientGrid}
                        />
                        <PrivateRoute
                          authed={authProps.isAuthenticated}
                          exact
                          path="/project/settings"
                          forward="/hello"
                          component={ClientSettings}
                          authProps={authProps}
                        />
                        <PrivateRoute
                          authed={authProps.isAuthenticated}
                          exact
                          path="/project/updates"
                          forward="/hello"
                          component={Updates}
                          authProps={authProps}
                        />

                        <Route
                          exact
                          path="/instagram"
                          render={() =>
                            (window.location =
                              "https://www.instagram.com/dexterio.in/")
                          }
                        />
                        <Route
                          exact
                          path="/whatsapp"
                          render={() =>
                            (window.location =
                              "https://api.whatsapp.com/message/PUOQSGNGWOJ5A1")
                          }
                        />
                        <Route
                          exact
                          path="/linkedin"
                          render={() =>
                            (window.location =
                              "https://www.linkedin.com/company/dexterio")
                          }
                        />

                        <PrivateRoute
                          authed={!authProps.isAuthenticated}
                          path="/hello"
                          forward="/home"
                          component={ClientSignUp}
                          authProps={authProps}
                        />
                        <Route
                          exact
                          path="/"
                          render={(props) => (
                            <Home
                              {...props}
                              auth={authProps}
                              mobileLeadFormData={mobileLeadFormData}
                            />
                          )}
                        />
                        {/* <Redirect to="/" /> */}
                      </Layout>

                      {/* header and footer not required in the below routes */}
                      <Route
                        exact
                        path="/c/account"
                        render={(props) => (
                          <DashboardLayout
                            {...props}
                            authProps={authProps}
                            component={Account}
                          />
                        )}
                      />
                      <Route
                        exact
                        path="/c/customer"
                        render={(props) => (
                          <DashboardLayout
                            {...props}
                            authProps={authProps}
                            component={CustomerList}
                          />
                        )}
                      />
                      <Route
                        exact
                        path="/c/milestone"
                        render={(props) => (
                          <DashboardLayout
                            {...props}
                            authProps={authProps}
                            component={MilestoneList}
                          />
                        )}
                      />
                      {/* <Route exact path = '/c/uploadProject' render = {(props) => <AddAndUploadProjects {...props} auth={authProps}/>}/>
                            <Route exact path = '/c/updateImage/:projectKey' render = {(props) => <UpdateProjectImages {...props} auth={authProps} />}/>
                            <Route exact path = "/c/details/:id" render={(props) => <ProfilePage {...props} auth={authProps} />} /> */}
                      <Route
                        exact
                        path="/c/progress"
                        render={(props) => (
                          <DashboardLayout
                            {...props}
                            authProps={authProps}
                            component={ProjectProgress}
                          />
                        )}
                      />
                      <Route
                        exact
                        path="/c/images"
                        render={(props) => (
                          <DashboardLayout
                            {...props}
                            authProps={authProps}
                            component={ProjectImages}
                          />
                        )}
                      />
                      <Route
                        exact
                        path="/c/documents"
                        render={(props) => (
                          <DashboardLayout
                            {...props}
                            authProps={authProps}
                            component={ProjectDocuments}
                          />
                        )}
                      />

                      <Route
                        exact
                        path="/quiz"
                        render={(props) => <DesignQuiz />}
                      />
                    </React.Suspense>
                  </Switch>
                </div>
                <ScrollToTopButton />
              </div>
            </IndicatorProvider>
          </Router>
        </div>
      )
    );
  }
}

export default App;
