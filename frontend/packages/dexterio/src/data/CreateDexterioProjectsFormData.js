export const createProjectFormData = {
    stepOne: {
      projectName: {
        value: '',
        required: true,
        type: 'input',
        placeholder: 'Profile Name',
        width: '100%',
      },
      projectLocation: {
        value: '',
        required: true,
        type: 'input',
        placeholder: 'Studio/Company Name',
        width: '100%',
      },
      squareFoot: {
        value: '',
        required: true,
        type: 'input',
        placeholder: 'Address Line 1',
        width: '100%'
      },
    milestones: {
        value: [],
        type: 'select',
        placeholder: 'Milestone',
        choices: [
          'Site Detailed Measurements',
          'Electrical and Plumbing Labour',
          'False Ceiling and Civil for Kitchen',
          'Paiting Work for Base and First Coat',
          'Order Wooden material and Allocate Carpenter',
          'Electrician work for Light placements',
          'Painting Job for last 2 coats',
          'Cushioning Labour',
          'Place the decor Items',
          'Final cleaning and Project Handover'
        ] 
    }
  }
 }