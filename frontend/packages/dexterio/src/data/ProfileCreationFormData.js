export const profileFormData = {
    stepOne: {
      profilename: {
        value: '',
        required: true,
        type: 'input',
        placeholder: 'Profile Name',
        width: '100%',
      },
      name: {
        value: '',
        required: true,
        type: 'input',
        placeholder: 'Studio/Company Name',
        width: '100%',
      },
      addressLine1: {
        value: '',
        required: true,
        type: 'input',
        placeholder: 'Address Line 1',
        width: '100%'
      },
      addressLine2: {
        value: '',
        required: false,
        type: 'input',
        placeholder: 'Address Line 2',
        width: '100%'
      },
      city: {
        value: '',
        required: true,
        type: 'input',
        placeholder: 'City',
        width: '50%'
      },
      state: {
        value: '',
        required: true,
        type: 'input',
        placeholder: 'State',
        width: '50%'
      },
      country: {
        value: '',
        required: true,
        type: 'input',
        placeholder: 'Country',
        width: '50%'
      },
      pinCode: {
        value: '',
        required: true,
        type: 'input',
        placeholder: 'PIN Code',
        width: '50%'
      },
      email: {
        value: '',
        required: true,
        type: 'input',
        placeholder: 'Email',
        width: '50%'
      },
      phone: {
        value: '',
        required: true,
        type: 'input',
        placeholder: 'Phone Number',
        width: '50%'
      }
    },
  stepTwo: {
    briefDescription: {
        value: '',
        email: true,
        type: 'multilineInput',
        placeholder: 'Brief Description',
        width: '100%',
      },
    skills: {
        value: '',
        type: 'select',
        placeholder: 'Skills',
        choices: [
          'BedRoom',
          'Commercial',
          'Office'
        ],
        width: '100%',
      },
    themes: {
        value: [],
        type: 'select',
        placeholder: 'Themes',
        choices: [
          'Residential',
          'Commercial',
          'Office'
        ] 
    },
    image: {
      value: {},
      required: true,
      file: true,
      fileName: 'No file chosen',
      type: 'file',
      allowedTypes: ['png', 'jpg', 'jpeg'],
      maxFileSize: 1024
    }
  }
 }