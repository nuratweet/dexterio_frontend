export const data = {
  Step1 : {
    key : "Personalized Interior Design",
    value : [
       {
        heading: "Design Quiz",
        subheading: "Let’s analyze your situation first. Go through our onboarding quiz & let our interior designers know what plan suits you the best.",
        webpXMobile : "/img/home-howitworks-step1-1-mobile.webp",
        webp2XMobile : "/img/home-howitworks-step1-1-mobile@2X.webp",
        jpgXMobile : "/img/home-howitworks-step1-1-mobile.jpg",
        jpg2XMobile : "/img/home-howitworks-step1-1-mobile@2X.jpg",
        imgX : "/img/home-howitworks-step1-1.jpg",
        img2X : "/img/home-howitworks-step1-1@2X.jpg"
      },
      {
          heading: "Get free virtual consultation",
          subheading: "Talk to our design consultant to discuss your ideas , needs and budget to enliven your space instantly. Get personalized consultancy and an all-inclusive estimate for your interior project.",
          webpXMobile : "/img/home-howitworks-step1-2-mobile.webp",
          webp2XMobile : "/img/home-howitworks-step1-2-mobile@2X.webp",
          jpgXMobile : "/img/home-howitworks-step1-2-mobile.jpg",
          jpg2XMobile : "/img/home-howitworks-step1-2-mobile@2X.jpg",
          imgX : "/img/home-howitworks-step1-2.jpg",
          img2X : "/img/home-howitworks-step1-2@2X.jpg"
      }
    ]
  },
  Step3 : {
    key : "Place the Partial Order",
    value : [
      {
        heading: "Kickstart Execution",
        subheading: "Pay 50% of the quoted amount. Our procurement team orders, inspects and warehouses the required products and materials Meanwhile, civil work begins on the site.",
        webpXMobile : "/img/home-howitworks-step3-1-mobile.webp",
        webp2XMobile : "/img/home-howitworks-step3-1-mobile@2X.webp",
        jpgXMobile : "/img/home-howitworks-step3-1-mobile.jpg",
        jpg2XMobile : "/img/home-howitworks-step3-1-mobile@2X.jpg",
        imgX : "/img/home-howitworks-step3-1.jpg",
        img2X : "/img/home-howitworks-step3-1@2X.jpg"
      },
      {
        heading: "Pay 40% of quoted amount",
        subheading: "We’re in the middle of your project. Pay 40% of the quoted amount. The on site civil work is 50% done and the off site manufacturing of furniture & modular items begins at our warehouse.",
        webpXMobile : "/img/home-howitworks-step3-2-mobile.webp",
        webp2XMobile : "/img/home-howitworks-step3-2-mobile@2X.webp",
        jpgXMobile : "/img/home-howitworks-step3-2-mobile.jpg",
        jpg2XMobile : "/img/home-howitworks-step3-2-mobile@2X.jpg",
        imgX : "/img/home-howitworks-step3-2.jpg",
        img2X : "/img/home-howitworks-step3-2@2X.jpg"
      }
    ]
  },
  Step5 : {
    key : "Move in to your new Home",
    value : [
      {
        heading: "Settle in",
        subheading: "Your home is ready to move in and 95% of the payment has been made. Pay the rest 5% amount, 7 days after you settle in.",
        webpXMobile : "/img/home-howitworks-step5-1-mobile.webp",
        webp2XMobile : "/img/home-howitworks-step5-1-mobile@2X.webp",
        jpgXMobile : "/img/home-howitworks-step5-1-mobile.jpg",
        jpg2XMobile : "/img/home-howitworks-step5-1-mobile@2X.jpg",
        imgX : "/img/home-howitworks-step5-1.jpg",
        img2X : "/img/home-howitworks-step5-1@2X.jpg"
      },
      {
        heading: "The Dexterio Support",
        subheading: "True peace of mind kicks in with our 2 year of maintenance support. Also most of the products in your interior come with upto 10 years of warranty period.",
        webpXMobile : "/img/home-howitworks-step5-2-mobile.webp",
        webp2XMobile : "/img/home-howitworks-step5-2-mobile@2X.webp",
        jpgXMobile : "/img/home-howitworks-step5-2-mobile.jpg",
        jpg2XMobile : "/img/home-howitworks-step5-2-mobile@2X.jpg",
        imgX : "/img/home-howitworks-step5-2.jpg",
        img2X : "/img/home-howitworks-step5-2@2X.jpg"
      }
    ]
  }
};