import styled, {createGlobalStyle} from "styled-components";


const GlobalStyle = createGlobalStyle`
* {
    box-sizing: border-box;
    margin: 0;
    padding: 0;
    font-family: 'Source Sans Pro', sans-serif;
}
`;

export const Container = styled.div`

    z-index : 1;
    width : 100%;
    margin-right : auto;
    margin-left : auto;

    @media only screen and (min-width: 1536px) {
        max-width: 1300px;
    }

    @media only screen and (max-width: 1440px) {
        max-width: 1300px;
    }

    @media only screen and (max-width: 1366px) {
        max-width: 1150px;
    }

    @media only screen and (max-width: 1280px) {//Bad
        max-width: 1024px;
    }

    @media only screen and (max-width: 768px) {//Check
        max-width: 700px;
    }

    @media only screen and (max-width: 576px) {//Check
        max-width: 540px;
    }

`;

export const CurvedButton = styled.button`
    border-radius: ${({curve}) => (curve ? '25px' : '0px')};
    background: ${({primary}) => (primary)};
    white-space: nowrap;
    padding: ${({big}) => (big ? '15px 55px' : '14px 31px')};
    color: ${({light}) => (light ? '#fff' : '#01070b')};;
    font-size : ${({fontBig}) => (fontBig ? '20px' : '16px')};
    outline: none;
    border: none;
    cursor: pointer;
    margin-left: ${({margin}) => (margin)};
    font-family: "Poppins";

    &:hover {
        transition: all 0.3s ease-out;
        background: red;
    }
    
    @media (max-width: 768px) {
        padding: ${({big}) => (big ? '10px 10px' : '10px 20px')};
    }
`;

export const BorderedEmptyCurvedButton = styled(CurvedButton)`
    background: none;
    color: #111121;
    border: 1px solid #111121;
    padding: ${({padding}) => padding};
    
    &:hover{
        transition: all 0.3s ease-out;
        background: red;
        color: red;
    }
`;

export const FontSizeHeader1 = styled.div`
    font-size: 54px;
    font-family: "Poppins", sans-serif;
    color: #111121;
    font-weight: 600;
    line-height: 1.4em;
    letter-spacing: 0px;

    @media screen and (max-width: 1024px) {
        font-size: 46px;
    }
    
    @media screen and (max-width: 768px) {
        font-size: 40px;
    }

    @media screen and (max-width: 430px) {
        font-size: 35px;
    }

    @media screen and (max-width: 375px) {
        font-size: 30px;
    }
`;

export const FontSizeHeader2 = styled.div`
    font-size: 46px;
    font-family: "Poppins", sans-serif;
    color: #111121;
    font-weight: 600;
    line-height: 1.4em;
    letter-spacing: 0px;
`;

export const FontSizeHeader3 = styled.div`
    font-size: 30px;
    font-family: "Poppins", sans-serif;
    color: #111121;
    font-weight: 600;
    line-height: 1.4em;
    letter-spacing: 0px;
`;

export const FontSizeHeader4 = styled.div`
    font-size: 26px;
    font-family: "Poppins", sans-serif;
    color: #111121;
    font-weight: 600;
    line-height: 1.4em;
    letter-spacing: 0px;
`;

export const FontSizeSubHeader1 = styled.div`
    font-size: 22px;
    font-family: "Open Sans", sans-serif;
    color: #111121;
    font-weight: 700;
    line-height: 1.2em;
    letter-spacing: 0px;
`;

export default GlobalStyle;