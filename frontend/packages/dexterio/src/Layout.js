import React, { useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import { withRouter } from 'react-router-dom';
import Navbar from './Components/Navbar/Navbar';
import styled from 'styled-components';
import Footer from './Components/Footer/Footer';
import BottomNavBar from './Components/Navbar/BottomNavBar';

const Header = styled.header`
  height: 70px;
  box-sizing: border-box;
  @media(max-width: 768px) {
       height: 50px;
  }
`;

const Layout = (props) => {
    const location = useLocation();
    const [showHeaderFooter, setshowHeaderFooter] = useState(true);

    useEffect(()=> {
        validateHeaderFooter(location.pathname.split("/")[1]);
    },[location.pathname])

    const {newProps} = props;

    const validateHeaderFooter = (key) =>{
        const arr = ['c','id','designer','quiz','ezy'];  
        if(arr.includes(key)){
            setshowHeaderFooter(false);
        }
    } 

    return (
        <div>
            {showHeaderFooter&& <Header><Navbar auth = {newProps}/></Header> }
            {props.children}
            {showHeaderFooter && <><Footer/><BottomNavBar/></>}
        </div>
    );
}

export default withRouter(Layout);