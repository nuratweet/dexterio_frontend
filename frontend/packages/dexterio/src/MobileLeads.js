import React, { useEffect, useState } from "react";
import "./MobileLeads.css";
import ClientSignUpForm from "./Components/Common/ClientSignUpForm";

const MobileLeads = (props) => {
  let mobileLeadFormData = props.mobileLeadFormData;
  const [mobileLeadFormShowClasses, setShowClasses] = useState(["ivMMT"]);

  useEffect(() => {
    if (
      mobileLeadFormData.mobileLeadFormData &&
      mobileLeadFormShowClasses.length === 1
    ) {
      setShowClasses(["ivMMT", "jxIcUk"]);
    }

    if (
      !mobileLeadFormData.mobileLeadFormData &&
      mobileLeadFormShowClasses.length === 2
    ) {
      setShowClasses(["ivMMT"]);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [mobileLeadFormData.mobileLeadFormData]);

  return (
    <section className={mobileLeadFormShowClasses.join(" ")}>
      <div className="navMobile">
        <button
          className="dHKbCn"
          onClick={() => {
            mobileLeadFormData.setMobileLeadFormData(false);
          }}
        >
          <span className="toggle-bar"></span>
          <span className="toggle-bar"></span>
        </button>
        Get Expert Advice
      </div>
      <div className="hxpTmi">
        <div className="dAKhBr">
          <ClientSignUpForm headerText="Meet a Designer" {...props}/>
        </div>
      </div>
    </section>
  );
};

export default MobileLeads;
