import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import  { ScrollToTop , ScrollToTopButton, IndicatorFallback, IndicatorProvider, Indicator} from 'common';
import ComingSoon from './Components/ComingSoon/Coming_Soon';

import './App.css';
import React from 'react';

// const InteriorDesignerLandingPage = React.lazy(() => import('./Components/InteriorDesignerLandingPage/InteriorDesignerLandingPage'));
const AddAndUploadProjects = React.lazy(() => import('./Components/InteriorDesigner/AddAndUpdateProjects/AddAndUpdateProjects'));
const UpdateProjectImages = React.lazy(() => import('./Components/InteriorDesigner/AddAndUpdateProjects/UpdateProjectImages'));
const IDProfile = React.lazy(() => import('./Components/InteriorDesigner/ProfileCreation/IDProfileCreation'));
const ProfilePage = React.lazy(() => import('./Components/ProfilePage/ProfilePage2'));
const IDProfileLeadCreation = React.lazy(() => import('./Components/InteriorDesigner/ProfileCreation/IDProfileLeadCreation'));


function App() {
  return (
    <div className="App">
      <Router>
        <ScrollToTop />
        <IndicatorProvider>
          <Indicator />
          <div>
            <div className="body">
              <Switch>
                <React.Suspense fallback={<IndicatorFallback />}>
                  <Route exact path="/" render={(props) => <ComingSoon />} />
                  <Route
                    exact
                    path="/interior-designers/createProfile"
                    render={(props) => <IDProfile {...props} />}
                  />
                  <Route
                    exact
                    path="/interior-designers/welcome"
                    render={(props) => <IDProfileLeadCreation {...props} />}
                  />
                  <Route
                    exact
                    path="/interior-designers/uploadProject"
                    render={(props) => <AddAndUploadProjects {...props} />}
                  />
                  <Route
                    exact
                    path="/interior-designers/updateImage/:projectKey"
                    render={(props) => <UpdateProjectImages {...props} />}
                  />
                  <Route
                    exact
                    path="/interior-designers/profile/:id"
                    render={(props) => <ProfilePage {...props} />}
                  />
                </React.Suspense>
              </Switch>
            </div>
            <ScrollToTopButton />
          </div>
        </IndicatorProvider>
      </Router>
    </div>
  );
}

export default App;
