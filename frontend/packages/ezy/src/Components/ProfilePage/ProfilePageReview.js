import React from 'react'
import { Avatar } from '@material-ui/core';
import {Margin, TextDetailsSmall, TextDetailsBold, TextDetails} from './ProfilePage2.element';
import { FlexColumn, FlexAlwaysRow } from "common";


function ProfilePageReview({review, timestamp, rating, user}) {
    return (
        <Margin margin="0 0 20px 10px" style={{"maxWidth":"90%", "borderBottom": "1px solid #7c8361", "paddingBottom":"10px"}}>
            <FlexAlwaysRow>
                <div>
                    <Avatar
                        sx={{
                            cursor: 'pointer',
                            width: 64,
                            height: 64
                        }}
                        />
                </div>
                <FlexColumn>
                    <Margin margin="0 10px">
                        <FlexAlwaysRow>
                            <TextDetailsBold>
                                {user} 
                            </TextDetailsBold>
                            <div style={{"marginLeft":"10px"}}>
                                {/* <GoldenStar/> */}
                            </div>
                            <div style={{"marginLeft":"10px"}}>
                            <TextDetails style={{"color":"rgba(255,196,0,1)"}}>{rating} Stars</TextDetails>
                            </div>
                        </FlexAlwaysRow>
                        <div>
                            <TextDetailsSmall style={{"opacity":"0.4"}}>Published {timestamp}</TextDetailsSmall>
                        </div>
                        <div>
                            <TextDetails>"{review}"</TextDetails>
                        </div>
                    </Margin>
                </FlexColumn>
            </FlexAlwaysRow>
        </Margin>
    )
}

export default ProfilePageReview
