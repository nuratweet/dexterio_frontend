import SwipeableTextMobileStepper from "../SwipeableTextMobileStepper/SwipeableTextMobileStepper";
import React, { useEffect, useState } from "react";
import { BottomSheet } from "react-spring-bottom-sheet";
import {
  ProfileContainer,
  ProfileRightPanel,
  Margin,
  ProfileLeftPanel,
  DesktopProjectsView,
  MobileProjectsView,
  TextDetails,
  TextDetailsHeader4,
  TextDetailsHeader3,
  SwipeableWrapper,
  HideMobileView,
  SocialMediaButton,
  SocialIcons,
  ShareColumn,
} from "./ProfilePage2.element";
import {
  FlexRow,
  FlexAlwaysRow,
  ProfileImage,
  ProfileImageWrapper,
  FlexColumn,
  CarouselSnapper,
  CarouselSlide,
  CarouselViewPort,
  Carousel,
} from "common";
import ProfilePageReview from "./ProfilePageReview";
import Modal from "@material-ui/core/Modal";
import { FaClone } from "react-icons/fa";
import { IconContext } from "react-icons/lib";
import { Helmet } from "react-helmet";
import { CurvedButton } from "globalstyles";

const config = require("../../config_prod.json");

function ProfilePage2(props) {
  const [username] = useState(props.match.params.id);
  const [name, setName] = useState("");
  const [profileImage, setProfileImage] = useState();
  const [profileDesc, setProfileDesc] = useState("");
  const [skills, setSkills] = useState([]);
  const [state, setState] = useState([]);
  const [city, setCity] = useState("");
  const [projectData, setProjectData] = useState([]);
  const [perProjectData, setPerProjectData] = useState([{}]);
  const [projectCount, setProjectCount] = useState("");
  const [experience, setExperience] = useState();
  const [certifications, setCertifications] = useState([]);
  const [reviews, setReviews] = useState([]);
  const [isModalOpened, setIsModalOpened] = useState(false);
  const [activeProjectKey, setActiveProjectKey] = useState();
  const [shareCard, setShareCard] = useState(false);
  const [reviewOpen, setReviewOpen] = useState(false);
  const reviewToggle = document.getElementById("reviews");
  const [open, setOpen] = useState(false);

  function onDismiss() {
    setOpen(false);
  }

  useEffect(() => {
    projectData.length = 0;
    perProjectData.length = 0;
    fetch(`${config.api.invokeUrl}/designerprofile/${username}`)
      .then((resp) => resp.json())
      .then((data) => {
        if (data.Item) {
          setName(data.Item.name);
          setProfileImage(data.Item.profileImage);
          setProfileDesc(data.Item.briefDescription);
          setSkills(data.Item.skills);
          setState(data.Item.state);
          setCity(data.Item.city);
          setProjectCount(data.Item.projectCount);
          setExperience(data.Item.experience);
          setCertifications(data.Item.certifications);
        }
      });

    fetch(`${config.api.invokeUrl}/designerprofile/projects/${username}`)
      .then((resp) => resp.json())
      .then((data) => {
        let projectDataApiRes = [];
        let perProjectDataTemp = [];
        let reviewTemp = [];
        if (data.Items) {
          data.Items.forEach((item) => {
            if (item.toBeShownOnProfile === "1") {
              projectDataApiRes.push({
                image: item.thumbnail,
                room: item.projectName,
                content: "bladj vaijk naij voialmfcdmaoein voekafdgsfdgkkds",
                key: item.projectKey,
              });

              for (let index in item.reviews) {
                if (parseInt(item.reviews[index].rating) >= 4) {
                  reviewTemp.push(item.reviews[index]);
                }
              }
              let values = [];
              for (let index in item.images) {
                if (item.images[index].isActive) {
                  values.push(item.images[index]);
                }
              }
              perProjectDataTemp.push({
                key: item.projectKey,
                values: values,
              });
            }
          });
        }
        setReviews(reviewTemp);
        setPerProjectData(perProjectDataTemp);
        setProjectData(projectDataApiRes);
    
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const toggleReviews = () => {
    if (reviewToggle.style.display === "block") {
      reviewToggle.style.display = "none";
    } else {
      reviewToggle.style.display = "block";
    }
  };

  return (
    <>
      <Helmet>
        <title>{name} - Interior Designer : Dexterio</title>
        <meta name="author" content="Dexterio" />
        <meta name="twitter:domain" content="dexterio.in" />
        <meta name="robots" content="index,follow" />
        <meta
          property="og:description"
          content="Home renovation made easy fast affordable. Welcome to the one stop solution for all your home interior needs. Get expert interior designers and create a beautiful- living room, toilet, kitchen, dining, bedrooms &amp; more."
        />
        <meta property="og:locale" content="en_IN" />
        <meta
          property="og:title"
          content={`${name} - Interior Designer : Dexterio`}
        />
        <meta
          property="og:site_name"
          content="Dexterio - Redefining Interiors"
        />
        <meta property="og:url" content={`/designer/${username}`} />
        <meta property="og:type" content="website" />
        <meta
          name="twitter:description"
          content="Home renovation made easy fast affordable. Welcome to the one stop solution for all your home interior needs. Get expert interior designers and create a beautiful- living room, toilet, kitchen, dining, bedrooms &amp; more."
        />
        <meta
          name="twitter:title"
          content={`${name} - Interior Designer : Dexterio`}
        />
        <meta name="twitter:card" content="summary" />
        <meta
          name="description"
          content="Home renovation made easy fast affordable. Welcome to the one stop solution for all your home interior needs. Get expert interior designers and create a beautiful- living room, toilet, kitchen, dining, bedrooms &amp; more."
        />
      </Helmet>
      <ProfileContainer>
        <Margin margin="100px 0 0 0">
          <FlexRow>
            <ProfileLeftPanel>
              <FlexColumn style={{ height: "100%" }}>
                <FlexColumn style={{ transform: "translateY(-10%)" }}>
                  <FlexColumn>
                    <Margin margin="0 auto">
                      <ProfileImageWrapper translate="-10%">
                        <ProfileImage src={profileImage} alt="" />
                      </ProfileImageWrapper>
                    </Margin>
                    <Margin margin="20px auto 0 auto">
                      <TextDetails>{name}</TextDetails>
                    </Margin>
                    <Margin margin="10px auto 0 auto">
                      <span>
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="24"
                          height="24"
                          viewBox="0 0 24 24"
                        >
                          <path d="M12 0c-4.198 0-8 3.403-8 7.602 0 4.198 3.469 9.21 8 16.398 4.531-7.188 8-12.2 8-16.398 0-4.199-3.801-7.602-8-7.602zm0 11c-1.657 0-3-1.343-3-3s1.343-3 3-3 3 1.343 3 3-1.343 3-3 3z" />
                        </svg>
                      </span>
                      <TextDetailsHeader4>
                        {city}, {state}
                      </TextDetailsHeader4>
                    </Margin>
                  </FlexColumn>
                  <Margin margin="40px 0 0 0">
                    <FlexAlwaysRow
                      style={{
                        alignItems: "center",
                        backgroundColor: "rgba(123,131,97,1)",
                        padding: "0 20px",
                        justifyContent: "space-between",
                      }}
                    >
                      <div style={{ margin: "20px 0px", textAlign: "center" }}>
                        <TextDetailsHeader3 style={{ color: "#fff" }}>
                          {projectCount}
                        </TextDetailsHeader3>{" "}
                        <br />
                        <TextDetails style={{ color: "#fff" }}>
                          Projects
                        </TextDetails>
                      </div>
                      <div style={{ margin: "20px 0px", textAlign: "center" }}>
                        <TextDetailsHeader3 style={{ color: "#fff" }}>
                          {experience} Yrs
                        </TextDetailsHeader3>{" "}
                        <br />{" "}
                        <TextDetails style={{ color: "#fff" }}>
                          Experience
                        </TextDetails>
                      </div>
                      <div style={{ margin: "20px 0px", textAlign: "center" }}>
                        <TextDetailsHeader3 style={{ color: "#fff" }}>
                          100
                        </TextDetailsHeader3>{" "}
                        <br />
                        <TextDetails style={{ color: "#fff" }}>
                          References
                        </TextDetails>
                      </div>
                    </FlexAlwaysRow>
                  </Margin>
                  <Margin margin="40px auto" style={{ maxWidth: "90%" }}>
                    <div style={{ textAlign: "center" }}>
                      <TextDetails>{profileDesc}</TextDetails>
                    </div>
                  </Margin>
                </FlexColumn>
                <FlexColumn>
                  <HideMobileView>
                    <Margin margin="0">
                      <FlexColumn>
                        <div style={{ textAlign: "center" }}>
                          <TextDetailsHeader4>Skills</TextDetailsHeader4>
                        </div>
                        <div style={{ maxWidth: "80%", margin: "0 auto" }}>
                          {skills?.map((skill, index) => {
                            return (
                              <div
                                style={{
                                  backgroundColor: "#dadbce",
                                  borderRadius: "15px",
                                  padding: "5px",
                                  display: "inline-block",
                                  margin: "10px",
                                }}
                                key={index}
                              >
                                <TextDetails>{skill}</TextDetails>
                              </div>
                            );
                          })}
                        </div>
                      </FlexColumn>
                    </Margin>
                    <Margin margin="40px 0 0 0">
                      <FlexColumn>
                        <div style={{ textAlign: "center" }}>
                          <TextDetailsHeader4>Achievements</TextDetailsHeader4>
                        </div>
                        <div style={{ maxWidth: "90%", margin: "0 auto" }}>
                          {certifications?.map((certificate, index) => {
                            return (
                              <div
                                style={{
                                  backgroundColor: "#dadbce",
                                  borderRadius: "15px",
                                  padding: "5px",
                                  display: "inline-block",
                                  margin: "10px",
                                }}
                                key={index}
                              >
                                <TextDetails>{certificate}</TextDetails>
                              </div>
                            );
                          })}
                        </div>
                      </FlexColumn>
                    </Margin>
                  </HideMobileView>
                </FlexColumn>
                <ShareColumn>
                  <Margin margin="2vw auto">
                    <CurvedButton
                      id="shareButton"
                      background="rgba(255,92,92,1)"
                      light
                      curve
                      fontBig
                      margin="0px"
                      onClick={() => {
                        const shareButton =
                          document.getElementById("shareButton");
                        shareButton.innerText = "Link Copied";

                        setTimeout(() => {
                          // button && setShareCard(!shareCard);
                          // !button && setOpen(true);
                          setShareCard(!shareCard);
                        }, 1000);

                        navigator.clipboard.writeText(
                          "https://ezy.dexterio.in/interior-designers/profile/" + username
                        );

                        setTimeout(() => {
                          shareButton.innerText = "Share Profile";
                        }, 3000);
                      }}
                    >
                      Share Profile
                    </CurvedButton>
                  </Margin>
                </ShareColumn>
              </FlexColumn>
            </ProfileLeftPanel>
            <ProfileRightPanel>
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  margin: "20px",
                  gridGap: "20px",
                }}
              >
                <div style={{ textAlign: "left" }}>
                  <Margin margin="0 0 0 10px">
                    <TextDetailsHeader3>Top Projects</TextDetailsHeader3>
                  </Margin>
                  <DesktopProjectsView>
                    {projectData.map((item, index) => (
                      <div
                        style={{
                          margin: "10px",
                          display: "flex",
                          flex: "0 0 22em",
                          backgroundColor: "rgba(123,131,97,1)",
                          borderRadius: "8px",
                          overflow: "hidden",
                        }}
                        key={index}
                        onClick={() => {
                          setActiveProjectKey({ item });
                          setIsModalOpened(true);
                        }}
                      >
                        <img
                          src={`${item.image}`}
                          srcSet={`${item.image}`}
                          alt={item.room}
                          style={{
                            width: "100%",
                            cursor: "pointer",
                            objectFit: "cover",
                            objectPosition: "center",
                          }}
                          loading="lazy"
                        />
                      </div>
                    ))}
                  </DesktopProjectsView>
                  <MobileProjectsView>
                    {perProjectData != null &&
                      perProjectData?.length > 0 &&
                      perProjectData?.map((item1, index) => {
                        return (
                          <div
                            style={{
                              marginTop: "5vw",
                              display: "flex",
                              justifyContent: "center",
                            }}
                            key={index}
                          >
                            <Carousel>
                              <CarouselViewPort>
                                {item1 != null &&
                                  item1 &&
                                  item1?.values?.length > 0 &&
                                  item1?.values?.map((step, stepIndex) => {
                                    return (
                                      <CarouselSlide
                                        key={stepIndex}
                                        height="200px"
                                      >
                                        <CarouselSnapper>
                                          <div
                                            style={{
                                              width: "100%",
                                              height: "100%",
                                              position: "relative",
                                            }}
                                          >
                                            <div
                                              style={{
                                                position: "absolute",
                                                top: "0.75rem",
                                                right: "0.75rem",
                                                textShadow:
                                                  "0.2rem 0.2rem 0.2rem rgba(0, 0, 0, 0.1)",
                                              }}
                                            >
                                              <IconContext.Provider
                                                value={{
                                                  color: "#fff",
                                                  size: "20px",
                                                }}
                                              >
                                                {" "}
                                                {stepIndex === 0 && (
                                                  <FaClone />
                                                )}{" "}
                                              </IconContext.Provider>
                                            </div>
                                            <img
                                              src={step.projectUrl}
                                              alt=""
                                              style={{
                                                width: "100%",
                                                height: "100%",
                                                cursor: "pointer",
                                                objectFit: "cover",
                                                objectPosition: "center",
                                              }}
                                            />
                                          </div>
                                        </CarouselSnapper>
                                      </CarouselSlide>
                                    );
                                  })}
                              </CarouselViewPort>
                            </Carousel>
                          </div>
                        );
                      })}
                  </MobileProjectsView>
                </div>
                <div style={{ textAlign: "left" }}>
                  <FlexAlwaysRow>
                    <div style={{ marginLeft: "10px" }}>
                      <TextDetailsHeader3>Top Reviews</TextDetailsHeader3>
                    </div>
                    <div
                      className="collapsable"
                      style={{
                        marginLeft: "1vw",
                        display: "flex",
                        alignItems: "center",
                      }}
                    >
                      <button
                        onClick={() => {
                          setReviewOpen(!reviewOpen);
                          toggleReviews();
                        }}
                        style={{ border: "none", background: "inherit" }}
                      >
                        {!reviewOpen && (
                          <img
                            src="/svg/plus_circle.svg"
                            alt="Plus Icon"
                            style={{ width: "18px" }}
                          />
                        )}
                        {reviewOpen && (
                          <img
                            src="/svg/minus_circle.svg"
                            alt="Plus Icon"
                            style={{ width: "20px" }}
                          />
                        )}
                      </button>
                    </div>
                  </FlexAlwaysRow>
                  <div id="reviews" style={{ display: "none" }}>
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        flexWrap: "wrap",
                        marginTop: "20px",
                      }}
                    >
                      {reviews.map((update, index) => (
                        <div key={index}>
                          <ProfilePageReview
                            review={update.review}
                            timestamp={update.published}
                            rating={update.rating}
                            user={update.customer}
                          />
                        </div>
                      ))}
                    </div>
                  </div>
                </div>
              </div>
            </ProfileRightPanel>
            <BottomSheet
              open={open}
              onDismiss={onDismiss}
              defaultSnap={({ snapPoints, lastSnap }) =>
                lastSnap ?? Math.min(...snapPoints)
              }
              snapPoints={({ maxHeight }) => [
                maxHeight - maxHeight / 5,
                maxHeight * 0.6,
              ]}
              header={
                <h1 className="flex items-center text-xl justify-center font-bold text-gray-800">
                  Share Profile
                </h1>
              }
            >
              <div>
                <p>
                  Hey! Checkout this designer on Dexterio
                </p>
                <FlexRow style={{ alignItems: "center" }}>
                  <FlexColumn style={{ paddingRight: "50px" }}>
                    <img
                      src="/svg/whatsapp.svg"
                      alt="Whatsapp"
                      style={{ width: "30px" }}
                    />
                  </FlexColumn>
                  <FlexColumn>
                    <TextDetailsHeader4>
                      {" "}
                      Share to Whatsapp
                    </TextDetailsHeader4>
                  </FlexColumn>
                </FlexRow>
              </div>
            </BottomSheet>
          </FlexRow>
        </Margin>
        {isModalOpened && (
          <div>
            <Modal
              open={isModalOpened}
              onClose={() => setIsModalOpened(false)}
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                margin: "10% 20% !important",
              }}
            >
              <SwipeableWrapper>
                <SwipeableTextMobileStepper
                  projectSelected={activeProjectKey}
                />
              </SwipeableWrapper>
            </Modal>
          </div>
        )}
        {shareCard && (
          <div>
            <Modal
              open={shareCard}
              onClose={() => setShareCard(false)}
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                margin: "10% 20% !important",
                borderRadius: "5px",
              }}
            >
              <div
                style={{
                  display: "flex",
                  justifyContent: "center",
                  backgroundColor: "white",
                  flexDirection: "column",
                  borderRadius: "8px",
                }}
              >
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    flexDirection: "column",
                    borderBottom: "1px solid #7c8361",
                    margin: "10px",
                  }}
                >
                  <TextDetailsHeader4>Share Profile</TextDetailsHeader4>
                  <TextDetails>
                    Hey! Checkout this designer on Dexterio
                  </TextDetails>
                </div>

                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    margin: "10px 20px",
                  }}
                >
                  <SocialIcons style={{ justifyContent: "left" }}>
                    <SocialMediaButton
                      onClick={() => {
                        window.open(
                          "https://api.whatsapp.com//send?text=Hey! Checkout this designer on Dexterio. https://ezy.dexterio.in/interior-designers/profile/" +
                            username
                        );
                      }}
                      target="_blank"
                      aria-label="Youtube"
                    >
                      <FlexAlwaysRow style={{ alignItems: "center" }}>
                        <FlexColumn style={{ paddingRight: "50px" }}>
                          <img
                            src="/svg/whatsapp.svg"
                            alt="Whatsapp"
                            style={{ width: "30px" }}
                          />
                        </FlexColumn>
                        <FlexColumn>
                          <TextDetailsHeader4>
                            {" "}
                            Share to Whatsapp
                          </TextDetailsHeader4>
                        </FlexColumn>
                      </FlexAlwaysRow>
                    </SocialMediaButton>
                  </SocialIcons>
                </div>
              </div>
            </Modal>
          </div>
        )}
      </ProfileContainer>
    </>
  );
}

export default ProfilePage2;
