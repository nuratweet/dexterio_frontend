import React from 'react'
import styled from 'styled-components';

function ShareCard(props) {
    const share = {...props};
    console.log(share);
    return (
        <ShareCardContainer >
            <ShareCardSlide share={share}>

            </ShareCardSlide>
        </ShareCardContainer>
    )
}

export default ShareCard

const ShareCardContainer = styled.div`
    position: relative;
    width:100%;
    height: 35px;
`;

const ShareCardSlide = styled.div`
    position: absolute;
    bottom: ${({share}) => (share ? '0' : '-35px')};
    width: 100%;
    height: 35px;
    background: #0d0d0d;
    transition: 1s;
`;