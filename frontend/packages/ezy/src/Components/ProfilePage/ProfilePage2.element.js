import styled from 'styled-components';
import {FlexAlwaysRow, FlexColumn} from 'common';

export const ProfileContainer = styled.div`
    max-width: 90vw;
    margin: 0 auto;
    overflow-x: hidden;
    @media only screen and (max-width: 419px) {
        max-width: 98vw;
    }

`;

export const FlexAlwaysRowMobile = styled(FlexAlwaysRow)`
    justify-content: space-between;
    flex-wrap: wrap;
    @media only screen and (max-width: 419px) {
        justify-content: space-around;
    }
`;

export const SocialIcons = styled.div`
    display: flex;
    justify-content: flex-start;
    align-items: center;
    width: 100%;
`;

export const Margin = styled.div`
    margin: ${({margin}) => margin}
`;

export const ProfileLeftPanel = styled.div`
    background-color: #f4f3ee;
    border-radius: 10px;
    flex: 0 0 25em;
    margin: 10px;

    @media only screen and (max-width:767px) {
        flex: 1;
    }
`;

export const ProfileRightPanel = styled.div`
    display: flex;
    background-color: #f4f3ee;
    border-radius: 10px;
    margin: 10px;
    flex-grow: 1
`;

export const TimeStamp = styled.div`
    color: gray;
    font-weight: 300;
    font-size: 13px;
`;

export const SaveButton = styled.button`

    background: #fff;
    border: 0;
    border-radius: 50%;
    box-shadow: 0 18px 24px rgb(0 0 0 / 13%);
    cursor: pointer;
    outline: none!important;
    padding: 0;
    position: relative;
    right: 10px;
    float: right;
    height: 40px!important;
    margin-left: 15px;
    width: 40px!important;

    & > span  {
        & > svg {
            height: 20px!important;
            width: 17px!important;

            & > path {
                stroke: #171717;
                stroke-width: 1.3px;
                fill: #f77a64;
                fill-opacity: 0;
            }
        }
    }
`;

export const ShareButton = styled.button`

    cursor: pointer;
    height: 40px!important;
    width: 40px!important;
    border-radius: 50%;
    background-color: #fff;
    box-shadow: 0 8px 12px 2px rgba(0,0,0,.1)!important;
    padding-left: 0;
    padding-right: 0;
    display: inline-block;
    font-weight: 400;
    color: #212529;
    text-align: center;
    vertical-align: middle;
    user-select: none;
    border: 1px solid transparent;
    padding: 0.5rem 0.5rem;
    font-size: .875rem;
    line-height: 1.5;
    transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;

    & > i  {
        & > svg {
            height: 20px!important;
            width: 17px!important;
            
            & > path {
                stroke: #171717;
                stroke-width: 1.3px;
                fill: #f77a64;
                fill-opacity: 0;
            }
        }
    }
`;

export const DesktopProjectsView = styled.div`
    @media only screen and (max-width: 767px) {
        display: none;
    }
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    margin-top: 20px;
    justify-content: space-evenly;
`;

export const MobileProjectsView = styled.div`
    @media only screen and (min-width: 768px) {
        display: none;
    }
    display: flex;
    flex-direction: column; 
    justify-content: center;
    margin-top: 20px;
`;

export const TextDetails = styled.span`
    font-family: Poppins;
    font-style: normal;
    font-weight: normal;
    font-size: 18px;
    color: rgba(0,0,0,1);
    letter-spacing: 1.2px;

    @media only screen and (max-width:500px) {
        font-size: 15px;
    }
`;

export const TextDetailsBold = styled(TextDetails)`
    font-weight: bold;

    @media only screen and (max-width:500px) {
        font-size: 16.5px;
    }
`;

export const TextDetailsHeader3 = styled(TextDetails)`
    font-weight: bold;
    font-size: 24px;

    @media only screen and (max-width:500px) {
        font-size: 20px;
    }
`;

export const TextDetailsHeader4 = styled(TextDetails)`
    font-weight: bold;
    font-size: 18px;
    
    @media only screen and (max-width:500px) {
        font-size: 15px;
    }
`;


export const TextDetailsSmall = styled(TextDetails)`
    font-size: 12px;
`;


export const HideMobileView = styled.div`
    @media only screen and (max-width:500px) {
        display: none;
    }
`;

export const CarouselNavigation = styled.aside`
    position: absolute;
    right: 0;
    bottom: 0;
    left: 0;
    text-align: center;
    scrollbar-color: transparent transparent;
    scrollbar-width: 0px;
`;

export const CarouselNavigationButton = styled.a`
    display: inline-block;
    width: 1rem;
    height: 1rem;
    background-color: #333;
    background-clip: content-box;
    border: 0.25rem solid transparent;
    border-radius: 50%;
    font-size: 0;
    transition: transform 0.1s;
`;

export const CarouselNavigationItem = styled.li`
    display: inline-block;
    list-style: none;
    margin: 0;
    padding: 0;
`;

export const CarouselNavigationList = styled.ol`
    display: inline-block;
    list-style: none;
    margin: 0;
    padding: 0;
`;

export const CarouselPrevious = styled.a`
    left: -1rem;
    position: absolute;
    top: 0;
    margin-top: 37.5%;
    width: 4rem;
    height: 4rem;
    transform: translateY(-50%);
    border-radius: 50%;
    font-size: 0;
    outline: 0;

`;

export const CarouselNext = styled.a`
    right: -1rem;
    position: absolute;
    top: 0;
    margin-top: 37.5%;
    width: 4rem;
    height: 4rem;
    transform: translateY(-50%);
    border-radius: 50%;
    font-size: 0;
    outline: 0;
        
`;

export const SwipeableWrapper = styled.div`
    position: relative;
    /* display: block; */
    background: white;
    max-width: 90vw;
    height: 95vh;
    padding-top: 7.5vh;
`;

export const SocialMediaButton = styled.button`
    color: #000000;
    font-size: 24px;
    margin-right: 20px;
    margin-left: 20px;
    border: none;
    background: inherit;
    cursor: pointer;
    width: 100%
`;

export const ShareColumn = styled(FlexColumn)`
    align-items: center;
    justify-content: center;

    @media only screen and (max-width: 600px) {
        transform: translateY(-100%);
    }
`;