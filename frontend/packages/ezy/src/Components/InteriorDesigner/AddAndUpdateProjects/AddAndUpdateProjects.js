import {
  Grid,
  FormControl,
  ListSubheader,
  MenuItem,
  Paper,
  Select,
  TextField,
} from "@material-ui/core";
import { FlexAlwaysRow, FlexColumn, FlexRow } from "common";
import { Autocomplete } from '@material-ui/core';
import React, { useState, useEffect } from "react";
import {
  MainArea,
  Container,
  PanelLeft,
  PanelRight,
  Key,
  Description,
  InputWrapper,
  DescriptionListItem,
} from "./AddAndUpdateProjects.element";
import styled from "styled-components";
import { FileDropHandler } from "common";
import axios from "axios";
import { useLocation, Link } from "react-router-dom";
import { CurvedButton } from "globalstyles";

const config = require("../../../config_prod.json");
const year = new Date().getFullYear();
const years = Array.from(new Array(17), (val, index) => {
  if (index === 16) {
    return "Pre -" + (year - index);
  } else {
    return (year - index).toString();
  }
});

export const Button = styled.button`
  background-color: #0b4c76;
  color: #fff;
  border-width: 1px;
  cursor: pointer;
  padding: calc(0.5em - 1px) 1em;
  white-space: nowrap;
  -webkit-appearance: none;
  border: 1px solid transparent;
  border-radius: 4px;
  display: inline-flex;
  font-size: 1rem;
  height: 2.5em;
  line-height: 1.5;
  margin: 43px;
  position: absolute;
  right: 0;
  bottom: 0;
`;

const choices = ["Residential", "Commercial", "Office"];

const styles = [
  { key: "Indian Traditional", value: "Indian Traditional" },
  { key: "Zen", value: "Zen" },
  { key: "Indian Contemporary", value: "Indian Contemporary" },
  { key: "Coastal", value: "Coastal" },
  { key: "Industrial", value: "Industrial" },
  { key: "Mediterranean", value: "Mediterranean" },
  { key: "Mid Modern", value: "Mid Modern" },
  { key: "Rustic", value: "Rustic" },
  { key: "Scandinavian", value: "Scandinavian" },
  { key: "Luxurious", value: "Luxurious" },
  { key: "Transitional", value: "Transitional" },
  { key: "Tropical", value: "Tropical" },
  { key: "British Colonial", value: "British Colonial" },
  { key: "Modern French", value: "Modern French" },
  { key: "Eclectic", value: "Eclectic" },
];

const AddAndUpdateProjects = (props) => {
  const [action, setAction] = useState("Create a new project");
  const [projectName, setProjectName] = useState("");
  const [projectImages, setProjectImages] = useState([]);
  const [projectLocation, setProjectLocation] = useState("");
  const [style, setStyle] = useState("");
  const [projectYear, setProjectYear] = useState(year.toString());
  const [keywords, setKeywords] = useState([]);
  const location = useLocation();
  const { username } = location.state;

  const upload = async (image) => {
    const formData = new FormData();
    formData.append("image", image);
    formData.append("description", "ProjectImage");

    const data = await axios.post("/images", formData, {
      headers: {
        "Content-Type": "multipart/form-data",
        "Access-Control-Allow-Origin": "true",
      },
    });
    return data.data.imagePath.toString();
  };

  const postImage = async () => {
    const localImageUrls = [];
    if (projectImages.length > 0) {
      for (const image of projectImages) {
        localImageUrls.push(await upload(image));
      }
    }
    return localImageUrls;
  };

  useEffect(
    () => () => {
      // Make sure to revoke the data uris to avoid memory leaks
      projectImages.forEach((file) => URL.revokeObjectURL(file.preview));
    },
    [projectImages]
  );

  const handleAddIDProfile = async (event) => {
    try {
      const data = await postImage();

      const params = {
        username: username,
        projectName: projectName,
        projectLocation: projectLocation,
        projectStyle: style,
        projectYear: projectYear,
        projectUrls: data,
        keywords: keywords,
      };

      await axios
        .post(
          `${config.api.invokeUrl}designerprofile/projects/${username}`,
          params
        )
        .then((data) => {
          const projectKey = data.data.projectKey;
          props.history.push(`/interior-designers/updateImage/${projectKey}`);
        })
        .catch((error) => {
          console.log(error);
        });
    } catch (error) {
      console.log(`Error occured: ${error}`);
    }
  };

  return (
    <MainArea>
      <Container>
        <PanelLeft>
          <p>Upload Content to a Project</p>
          <Grid component={Paper} elevation={3}>
            <FlexColumn style={{ alignItems: "center", margin: "0 2vw" }}>
              <form
                onSubmit={(event) => {
                  event.preventDefault();
                  handleAddIDProfile();
                }}
              >
                <InputWrapper width="95%">
                  <Description>
                    A Project is a collection of photos and videos of your own
                    work or products. Projects are usually best organized by
                    client, job site, or product line.{" "}
                  </Description>
                </InputWrapper>
                <InputWrapper width="95%">
                  <FormControl fullWidth>
                    <Select
                      defaultValue="Create a new project"
                      value={action}
                      onChange={(event) => setAction(event.target.value)}
                      name="Action"
                      variant="outlined"
                      margin="dense"
                      size="small"
                      required
                    >
                      <MenuItem value="Create a new project">
                        Create a new project
                      </MenuItem>
                      <ListSubheader>Your existing projects</ListSubheader>
                    </Select>
                  </FormControl>
                </InputWrapper>
                <InputWrapper width="95%">
                  <TextField
                    variant="outlined"
                    margin="dense"
                    size="small"
                    required
                    fullWidth
                    label="New Project Name"
                    autoComplete="off"
                    value={projectName}
                    onChange={(event) => setProjectName(event.target.value)}
                  />
                </InputWrapper>
                <InputWrapper width="95%">
                  <Description>
                    Increase your visibility to homeowners looking for
                    professionals who have worked in their area.
                  </Description>
                  <TextField
                    variant="outlined"
                    margin="dense"
                    size="small"
                    required
                    fullWidth
                    placeholder="Enter Project Locality/Area, City (will not be shared publicly)"
                    autoComplete="off"
                    value={projectLocation}
                    onChange={(event) => setProjectLocation(event.target.value)}
                  />
                </InputWrapper>
                <InputWrapper width="95%">
                  <FlexAlwaysRow style={{ gridGap: "1vw" }}>
                    <FlexColumn style={{ flex: "0.7", alignItems: "left" }}>
                      <Description style={{flexGrow: 3}}>
                        What is the style of this space/product?
                      </Description>
                      <FormControl fullWidth>
                        <Select
                          value={style}
                          onChange={(event) => setStyle(event.target.value)}
                          variant="outlined"
                          margin="dense"
                          size="small"
                          defaultValue={styles[0].key}
                        >
                          {styles.map((style) => {
                            return (
                              <MenuItem value={style.key} key={style.key}>
                                {style.value}
                              </MenuItem>
                            );
                          })}
                        </Select>
                      </FormControl>
                    </FlexColumn>
                    <FlexColumn style={{ flex: "0.3" }}>
                      <Description style={{flexGrow: 3}}>Project Year</Description>
                      <FormControl fullWidth>
                        <Select
                          placeholder="Year"
                          value={projectYear}
                          onChange={(event) =>
                            setProjectYear(event.target.value)
                          }
                          variant="outlined"
                          margin="dense"
                          size="small"
                        >
                          {years.map((year, index) => {
                            return (
                              <MenuItem value={year} key={index}>
                                {year}
                              </MenuItem>
                            );
                          })}
                        </Select>
                      </FormControl>
                    </FlexColumn>
                  </FlexAlwaysRow>
                </InputWrapper>
                <InputWrapper width="95%">
                  <Autocomplete
                    multiple
                    id="tags-outlined"
                    options={choices}
                    getOptionLabel={(option) => option}
                    value={keywords}
                    onChange={(event, newValue) => setKeywords(newValue)}
                    filterSelectedOptions
                    margin="dense"
                    size="small"
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label="Keywords"
                        variant="outlined"
                        margin="dense"
                      />
                    )}
                  />
                </InputWrapper>
                <InputWrapper width="95%">
                  <Key>Upload Project Images</Key>
                  <FileDropHandler setProjectImages={setProjectImages} />
                  <Description>
                    By uploading photos, you are approving their display by
                    Dexterio, saying that you either own the rights to the image
                    or that you have permission or a license to do so from the
                    copyright holder, and agreeing to abide by Dexterio's{" "}
                    <Link to="/term-of-use">terms of use</Link>.
                  </Description>
                </InputWrapper>
                <InputWrapper width="100%" style={{ position: "relative" }}>
                  <FlexRow
                    style={{
                      alignItems: "center",
                      justifyContent: "flex-end",
                    }}
                  >
                    <CurvedButton
                      background="rgba(255,92,92,1)"
                      light
                      curve
                      fontBig
                      margin="0px"
                      type="submit"
                    >
                      Upload Project
                    </CurvedButton>
                  </FlexRow>
                </InputWrapper>
              </form>
            </FlexColumn>
          </Grid>
        </PanelLeft>
        <PanelRight>
          <p>Photo Guidelines</p>
          <Grid component={Paper} elevation={3}>
            <FlexColumn style={{ alignItems: "left" }}>
              <section className="hz-carded guidelines-card photo parked">
                <Description>
                  Photos that do not meet these guidelines will be removed.
                </Description>
                <FlexAlwaysRow style={{margin:"1vw"}}>
                  <div>
                    <Description>Do's</Description>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="35"
                      height="35"
                      fill="#0B4C76"
                      className="bi bi-check2-square"
                      viewBox="0 0 16 16"
                    >
                      <path d="M3 14.5A1.5 1.5 0 0 1 1.5 13V3A1.5 1.5 0 0 1 3 1.5h8a.5.5 0 0 1 0 1H3a.5.5 0 0 0-.5.5v10a.5.5 0 0 0 .5.5h10a.5.5 0 0 0 .5-.5V8a.5.5 0 0 1 1 0v5a1.5 1.5 0 0 1-1.5 1.5H3z" />
                      <path d="m8.354 10.354 7-7a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0z" />
                    </svg>
                  </div>
                  <ul style={{ textAlign: "left", listStyle: "none" }}>
                    <DescriptionListItem>
                      Photos/3D renders of your own work
                    </DescriptionListItem>
                    <DescriptionListItem>
                      Minimum 2 MP photographs
                    </DescriptionListItem>
                    <DescriptionListItem>
                      JPEG, PNG file formats
                    </DescriptionListItem>
                    <DescriptionListItem>
                      Good Quality Photos
                    </DescriptionListItem>
                  </ul>
                </FlexAlwaysRow>
                <FlexAlwaysRow style={{margin:"1vw"}}>
                  <div>
                    <Description>Dont's</Description>
                    <img alt="" src="/svg/Dont.svg"  style= {{height: "35px", width: "35px"}}/>
                  </div>
                  <ul style={{ textAlign: "left", listStyle: "none" }}>
                    <DescriptionListItem>
                      Low quality or small size photos
                    </DescriptionListItem>
                    <DescriptionListItem>
                      PDF, Multi-Page TIFF, or EPS file formats
                    </DescriptionListItem>
                  </ul>
                </FlexAlwaysRow>
              </section>
            </FlexColumn>
          </Grid>
        </PanelRight>
      </Container>
    </MainArea>
  );
};

export default AddAndUpdateProjects;
