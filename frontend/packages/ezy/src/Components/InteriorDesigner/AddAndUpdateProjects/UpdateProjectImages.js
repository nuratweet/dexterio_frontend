import {
  Box,
  TextField,
  Autocomplete,
  Checkbox,
  FormControlLabel,
} from "@material-ui/core";
import React, { useEffect, useState, useRef } from "react";
import "./UpdateProjectImages.css";
import {
  ArrowLeft,
  ArrowRight,
  BannerFront,
  LeadForm,
  LeadFormArticle,
  LeadFormContainer,
  LeadFormSection,
  LeadFormTitle,
  LeadFormWrapper,
  ProjectImage,
  TextWrapper,
  Wrap,
} from "./UpdateProjectImages.element";
import { CurvedButton } from "globalstyles";
import {
  FlexRow
} from "common";

const config = require("../../../config_prod.json");

const UpdateProjectImages = (props) => {
  const [current, setCurrent] = useState(0);
  const nodes = useRef([]);

  function reset() {
    for (let i = 0; i < nodes.current.length; i++) {
      nodes.current[i].style.display = "none";
    }
  }

  function slide() {
    reset();
    nodes.current[current].style.display = "block";
  }

  const [projectKey] = useState(props.match.params.projectKey);

  const choices = ["Kids Bedroom", "Study Room", "Kitchen Modular", ""];

  const [disableInput, setDisableInput] = React.useState(false);

  const changeHandler = (event, newValue) => {
    setDisableInput(newValue.length >= 3);
    // dropdownChange(event, newValue);
  };

  let [projectImages, setProjectImages] = useState([]);

  useEffect(() => {
    fetch(`${config.api.invokeUrl}/designerprofile/project/${projectKey}`)
      .then((resp) => resp.json())
      .then((data) => {
        const projectDataApiRes = [];
        if (data.Items) {
          data.Items.forEach((element) => {
            projectDataApiRes.push(element.projectUrl);
          });
        }

        nodes.current = nodes.current.slice(0, projectDataApiRes.length);

        setProjectImages(projectDataApiRes);

        slide();
      });

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Wrap>
      <ArrowLeft
        onClick={() => {
          console.log(current);
          if (current === nodes.current.length - 1) {
            setCurrent(-1);
          } else {
            setCurrent(current + 1);
          }
          slide();
        }}
      />
      <div id="slider">
        {projectImages.map((image, index) => {
          return (
            <ProjectImage
              ref={(node) => (nodes.current[index] = node)}
              key={index}
              backgroundImage={image}
            >
              <div className="slide-content" />
            </ProjectImage>
          );
        })}
      </div>
      <BannerFront>
        <TextWrapper>
          <LeadFormContainer>
            <LeadFormSection>
              <LeadFormWrapper>
                <LeadFormArticle>
                  <LeadForm>
                    <LeadFormTitle>Image Details</LeadFormTitle>
                    <form noValidate>
                      <Autocomplete
                        getOptionDisabled={() => disableInput}
                        multiple
                        options={choices}
                        getOptionLabel={(option) => option}
                        onChange={changeHandler}
                        filterSelectedOptions
                        margin="dense"
                        size="small"
                        renderInput={(params) => (
                          <TextField
                            {...params}
                            label="Related Tags"
                            variant="outlined"
                            margin="dense"
                            required
                            background="#fff"
                          />
                        )}
                      />
                      <FormControlLabel
                        control={
                          <Checkbox
                            sx={{
                              color: "rgba(123,131,97,1)",
                              "&.Mui-checked": { color: "rgba(123,131,97,1)" },
                            }}
                          />
                        }
                        label="Show as Project Thumbnail"
                      />
                      <TextField
                        variant="outlined"
                        margin="dense"
                        required
                        fullWidth
                        id="email"
                        label="Email Address"
                        name="email"
                        autoComplete="email"
                        // value={email}
                        // onChange={(event) => setEmail(event.target.value)}
                      />

                      <FlexRow
                        style={{
                          alignItems: "center",
                          justifyContent: "flex-end",
                        }}
                      >
                        <CurvedButton
                          background="rgba(255,92,92,1)"
                          light
                          curve
                          fontBig
                          margin="0px"
                          type="submit"
                        >
                          Submit
                        </CurvedButton>
                      </FlexRow>

                      <Box mt={5}></Box>
                    </form>
                  </LeadForm>
                </LeadFormArticle>
              </LeadFormWrapper>
            </LeadFormSection>
          </LeadFormContainer>
        </TextWrapper>
      </BannerFront>
      <ArrowRight
        onClick={() => {
          if (current === 0) {
            setCurrent(nodes.current.length - 1);
          } else {
            setCurrent(current - 1);
          }
          slide();
        }}
      />
    </Wrap>
  );
};

export default UpdateProjectImages;
