import { TextField } from "@material-ui/core";
import React from "react";
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng
} from "react-places-autocomplete";
import { InputWrapper } from "./AddAndUpdateProjects.element";

class LocationSearchInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = { address: "" };
  }

  handleChange = address => {
    this.setState({ address });
  };

  handleSelect = address => {
    geocodeByAddress(address)
      .then(results => getLatLng(results[0]))
      .then(latLng => console.log("Success", latLng))
      .catch(error => console.error("Error", error));
  };

  render() {
    return (
      <PlacesAutocomplete
        value={this.state.address}
        onChange={this.handleChange}
        onSelect={this.handleSelect}
      >
        {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
          <div>
            {/* <input
              {...getInputProps({
                placeholder: "Enter Project Locality/Area, City (will not be shared publicly)",
                className: "location-search-input"
              })}
            /> */}
            <InputWrapper>
                <TextField
                variant="outlined"
                margin="dense"
                size="small"
                required
                fullWidth
                label='New Project Name'
                placeholder="Enter Project Locality/Area, City (will not be shared publicly)"
                // name={name}
                // autoComplete="off"
                // value={value}
                // onChange={onChange}
                // helperText={error && <div style={{"color":"red"}}>{error}</div>}
                inputProps={{
                    style: { backgroundColor: '#fff' },
                }} 
                />
            </InputWrapper>
            <div className="autocomplete-dropdown-container">
              {loading && <div>Loading...</div>}
              {suggestions.map(suggestion => {
                const className = suggestion.active
                  ? "suggestion-item--active"
                  : "suggestion-item";
                // inline style for demonstration purpose
                const style = suggestion.active
                  ? { backgroundColor: "#fafafa", cursor: "pointer" }
                  : { backgroundColor: "#ffffff", cursor: "pointer" };
                return (
                  <div
                    {...getSuggestionItemProps(suggestion, {
                      className,
                      style
                    })}
                  >
                    <span>{suggestion.description}</span>
                  </div>
                );
              })}
            </div>
          </div>
        )}
      </PlacesAutocomplete>
    );
  }
}

export default LocationSearchInput;
