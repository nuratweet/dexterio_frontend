import styled from 'styled-components';
import PropTypes from 'prop-types';

export const MainArea = styled.div`
`;

export const Container = styled.div`
    margin-right: auto;
    margin-left: auto;
    max-width: 100%;
    display: flex;
    margin: 1vw 2vw;
    grid-gap: 1.5vw;

    @media only screen and (max-width: 600px) {
      flex-direction: column;
    }
`;

export const InputWrapper = styled.div`
    /* float: left; */
    text-align: left;
    width: ${({width}) => (width)};
    padding: 5px;
`;

export const InputWrapperCombobox = styled.div`

  width: 100%;
  border: 1px solid #434343;
  /* background-color: #141414; */
  border-radius: 4px;
  padding: 1px;
  display: flex;
  flex-wrap: wrap;

  &:hover {
    border-color: '#177ddc';
  }

  &.focused {
    border-color: '#177ddc';
    box-shadow: 0 0 0 2px rgba(24, 144, 255, 0.2);
  }

  & input {
    background-color: '#141414';
    color: 'rgba(255,255,255,0.65)';
    height: 30px;
    box-sizing: border-box;
    padding: 4px 6px;
    width: 0;
    min-width: 30px;
    flex-grow: 1;
    border: 0;
    margin: 0;
    outline: 0;
  }
`;
  
function Tag(props) {
  const { label, onDelete, ...other } = props;
  return (
    <div {...other}>
      <span>{label}</span>
      <div onClick={onDelete}>
        <svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0z" fill="none"/><path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/></svg>
      </div>
    </div>
  );
}

Tag.propTypes = {
  label: PropTypes.string.isRequired,
  onDelete: PropTypes.func.isRequired,
};
  
export const StyledTag = styled(Tag)`
  display: flex;
  align-items: center;
  height: 24px;
  margin: 2px;
  line-height: 22px;
  background-color: lightgray;
  border: 1px solid '#303030';
  border-radius: 9px;
  box-sizing: content-box;
  padding: 0 4px 0 10px;
  outline: 0;
  overflow: hidden;

  &:focus {
    border-color: '#177ddc';
    background-color: '#003b57';
  }

  & span {
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
  }

  & svg {
    font-size: 12px;
    cursor: pointer;
    padding: 4px;
  }
`;
  
export  const Listbox = styled.ul`
  width: 75%;
  margin: 2px 0 0;
  padding: 0;
  position: absolute;
  list-style: none;
  background-color: '#fff';
  overflow: auto;
  max-height: 150px;
  border-radius: 4px;
  box-shadow: 0 2px 8px rgba(0, 0, 0, 0.15);
  z-index: 1000;

  & li {
    padding: 5px 12px;
    display: flex;
    z-index: 100;
    background-color: '#fff';

    & span {
      flex-grow: 1;
      z-index: 100;
      background-color: '#fff';
    }

    & svg {
      color: transparent;
    }
  }

  & li[aria-selected='true'] {
    background-color: '#2b2b2b';
    font-weight: 600;

    & svg {
      color: #1890ff;
    }
  }

  & li[data-focus='true'] {
    background-color: '#003b57';
    cursor: pointer;

    & svg {
      color: currentColor;
    }
  }
`;

export const Label = styled.label`
  padding: 0 0 4px;
  line-height: 1.5;
  display: block;
`;



export const UploadCard = styled.section`
    border-top: none;
    background-color: #fff;
    padding: 16px;
    margin-bottom: 16px;
    box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 8%);
`;

export const ProjectForm = styled.form`
    width: 100%
`;

export const Key = styled.div`
    margin-top: 20px;
    margin-bottom: 20px;
    color: #000707DE;
    font-family: "Open Sans",sans-serif;
    font-size: 18px;
    font-weight: 600;
    line-height: 1.6em;
`;

export const Description = styled.div`
    margin-top: 10px;
    margin-bottom: 10px;
    color: #000707DE;
    font-family: "Open Sans",sans-serif;
    font-size: 16px;
    font-weight: 500;
    line-height: 1.6em;
`;

export const DescriptionListItem = styled.li`
    margin-top: 5px;
    margin-bottom: 5px;
    color: #000707DE;
    font-family: "Open Sans",sans-serif;
    font-size: 14px;
    font-weight: 500;
    line-height: 1.6em;
`;

export const PanelLeft = styled.div`
    padding: 0 8px;
    position: relative;
    min-height: 1px;
    background-color: #fff;
    min-height : 100vh;
    flex: 0.75;
    & > p {
      background-color: rgba(123,131,97,1);
      color: #fff;
      text-align: center;
      border-radius: 5px 5px 0 0;
      font-size: 1.25em;
      font-weight: 700;
      line-height: 1.25;
      font-family: "Poppins", sans-serif;
      padding: 5px;
      margin-bottom: 0px;
    }
`;


export const PanelRight = styled.div`
    padding: 0 8px;
    position: relative;
    min-height: 1px;
    background-color: #fff;
    height : 100vh;
    flex: 0.25;
    & > p {
      background-color: rgba(123,131,97,1);
      color: #fff;
      text-align: center;
      border-radius: 5px 5px 0 0;
      font-size: 1.25em;
      font-weight: 700;
      line-height: 1.25;
      font-family: "Poppins", sans-serif;
      padding: 5px;
      margin-bottom: 0px;
    }
`;