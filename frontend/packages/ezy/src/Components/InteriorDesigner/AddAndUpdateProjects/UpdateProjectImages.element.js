import styled from "styled-components";

export const Wrap = styled.div`
  position: relative;
`;

export const Arrow = styled.div`
  cursor: pointer;
  position: absolute;
  top: 50%;
  margin-top: -35px;
  width: 0;
  height: 0;
  border-style: solid;
`;

export const ArrowLeft = styled(Arrow)`
  border-width: 30px 40px 30px 0;
  border-color: transparent #fff transparent transparent;
  left: 0;
  margin-left: 30px;
`;

export const ArrowRight = styled(Arrow)`
  border-width: 30px 0 30px 40px;
  border-color: transparent transparent transparent #fff;
  right: 0;
  margin-right: 30px;
`;

export const ImageDetails = styled.div`
  margin-left: auto;
  margin-right: 70px;
  width: 35%;
  z-index: 100;
`;

export const ProjectImage = styled.div`
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
  background-image: url(${({ backgroundImage }) => backgroundImage});
`;

export const LeadFormTitle = styled.div`
  font-size: 35px;
  font-family: "Poppins", sans-serif;
  line-height: 42px;
  padding-bottom: 20px;
`;
export const LeadForm = styled.div`
  text-align: left;
`;
export const LeadFormContainer = styled.div`
  max-width: 422px;
`;

export const LeadFormSection = styled.div``;

export const LeadFormWrapper = styled.div`
  max-width: 422px;
`;

export const LeadFormArticle = styled.article`
  max-width: 422px;
  padding: 44px;
  border-radius: 8px;
  text-align: left;
  min-height: 80vh;
  position: relative;
  margin-top: 0;
  background-color: #fff;
`;

export const HeaderText = styled.h1`
  color: #fff;
  font-family: "Poppins", sans-serif;
  font-size: 54px;
  font-weight: 600;
  line-height: 1.4em;
  letter-spacing: 0px;
`;

export const HeaderSubText = styled.p`
  color: #fff;
  font-size: 23px;
  font-weight: 500;
  line-height: 45px;
  font-family: "Open-Sans", sans-serif;
`;

export const HeaderTextWrapper = styled.div`
  max-width: 85%;
`;
export const LandingPageHeaderWrapper = styled.div`
  margin-top: 5rem;
`;

export const TextWrapper = styled.div`
  max-width: 90%;
  margin: auto;
  padding: 0 20px;
  display: flex;
  justify-content: flex-end;
`;

export const BannerImage = styled.img`
  min-height: 550px;
  width: 100%;
  max-width: 100%;
  height: auto;
`;

export const HireDesigner = styled.div``;

export const SignUpPageBanner = styled.div`
  position: relative;
`;

export const Banner = styled.div`
  position: relative;
`;

export const LandingPageBanner = styled.div``;

export const BannerFront = styled.div`
  width: 100%;
  height: 100%;
  position: absolute;
  top: 0;
  padding-top: 65px;
`;
