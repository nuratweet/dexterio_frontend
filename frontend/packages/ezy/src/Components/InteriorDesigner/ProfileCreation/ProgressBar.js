import React from "react";
import Styled from "styled-components";

const Container = Styled.div`

  progress[value] {
    width: ${props => props.width};
    height: 20px;
    display: flex;
    position: fixed;
    top: 0;
    z-index: 9999;
    -webkit-appearance: none;
    appearance: none;
  }

  progress[value]::-webkit-progress-bar {
    height: 20px;
    background-color: #eee;
    display: flex;
    position: fixed;
    top: 0;
    z-index: 9999;
  }  

  progress[value]::-webkit-progress-value {
    height: 20px;
    background-color: ${props => props.color};
    display: flex;
    position: fixed;
    top: 0;
    z-index: 9999;
  }
`;

const ProgressBar = ({ value, max, color, width }) => {
  
  return (
    <Container color={color} width={width}>
      <progress value={value} max={max} />
    </Container>
  );
};

export default ProgressBar;