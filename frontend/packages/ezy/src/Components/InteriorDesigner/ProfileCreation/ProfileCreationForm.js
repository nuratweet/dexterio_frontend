import React, { useState } from "react";

import { Step, Preview, validate } from "common";
import axios from "axios";
import AddFirstProject from "./AddFirstProject";
import { profileFormData } from "../../../data/ProfileCreationFormData";
const config = require("../../../config_prod.json");

const Form = ({ props, step, setStep }) => {
  const [formData, setFormData] = useState(profileFormData);
  const [errors, setErrors] = useState({});

  const updateProfileLink = (username) => {
    document.getElementById("profileLink").innerText =
      "https://ezy.dexterio.in/interior-designers/profile/" + username;
  };

  const updateChildChoices = (event, step, childName, childChoices) => {
    event.preventDefault();

    setFormData((prev) => ({
      ...prev,
      [step]: {
        ...prev[step],
        [childName]: {
          ...prev[step][childName],
          choices: childChoices,
        },
      },
    }));
  };

  const avoidSpace = (event) => {
    var k = event.keyCode || event.charCode || event.which;
    if (k === 32) {
      event.preventDefault();
    }
  };

  const dropdownChangeHandler = (event, step, name, newValue) => {
    event.preventDefault();

    setFormData((prev) => ({
      ...prev,
      [step]: {
        ...prev[step],
        [name]: {
          ...prev[step][name],
          value: newValue,
        },
      },
    }));
  };

  const changeHandler = (step, e) => {
    e.preventDefault();

    setFormData((prev) => ({
      ...prev,
      [step]: {
        ...prev[step],
        [e.target.name]: {
          ...prev[step][e.target.name],
          value: e.target.value,
        },
      },
    }));
  };

  const sliderChangeHandler = (step, e, newValue) => {
    e.preventDefault();
    
    setFormData((prev) => ({
      ...prev,
      [step]: {
        ...prev[step],
        [e.target.name]: {
          ...prev[step][e.target.name],
          value: newValue,
        },
      },
    }));
  };

  const fileChangeHandler = (name, file, step) => {
    setFormData((prev) => ({
      ...prev,
      [step]: {
        ...prev[step],
        [name]: {
          ...prev[step][name],
          value: file,
          fileName: file.name ? file.name : "No file chosen",
        },
      },
    }));
  };

  const stepChangeHandler = (values, e) => {
    e.preventDefault();
    const newErrors = validate(values);
    setErrors(newErrors);
    
    if (Object.keys(newErrors).length === 0) {
      setStep(step + 1);
    }
  };

  const submitHandler = async (e) => {
    e.preventDefault();

    try {
      console.log("Start uploading the profile Image");
      console.log(formData.stepTwo.image.value);
      const axiosForm = new FormData();
      axiosForm.append("image", formData.stepTwo.image.value);
      axiosForm.append("description", "ProjectImage");

      const data = await axios.post("/images", axiosForm, {
        headers: {
          "Content-Type": "multipart/form-data",
          "Access-Control-Allow-Origin": "true",
        },
      });

      const params = {
        username: formData.stepOne.profilename.value,
        name: formData.stepOne.name.value,
        addressLine1: formData.stepOne.addressLine1.value,
        addressLine2: formData.stepOne.addressLine2.value,
        city: formData.stepOne.city.value,
        state: formData.stepOne.state.value,
        country: formData.stepOne.country.value,
        pinCode: formData.stepOne.pinCode.value,
        email: formData.stepOne.email.value,
        phone: formData.stepOne.phone.value,
        experience: formData.stepTwo.experience.value,
        genre: formData.stepTwo.genre.value,
        projectCount: formData.stepTwo.projectCount.value,
        projectRange: formData.stepTwo.projectRange.value,
        briefDescription: formData.stepTwo.briefDescription.value,
        skills: formData.stepTwo.skills.value,
        themes: formData.stepTwo.themes.value,
        profileImage: data.data.imagePath,
      };
      await axios
        .post(
          `${config.api.invokeUrl}/designerprofile/${formData.stepOne.profilename.value}`,
          params
        )
        .then((data) => {
          console.log(data.status);
          if (data.status === 201) {
            setStep(step + 1);
          } else {
            //TODO Forward to Error Screen
          }
        });
    } catch (error) {
      console.log(`Error occured: ${error}`);
    }
  };

  return (
    <>
      <form onSubmit={submitHandler}>
        {step === 1 && (
          <Step
            data={formData.stepOne}
            onChange={changeHandler}
            onStepChange={stepChangeHandler}
            onProfileLinkChange={updateProfileLink}
            avoidSpace={avoidSpace}
            errors={errors}
            stepKey="stepOne"
            stepName="Basic Info"
            step={1}
          />
        )}
        {step === 2 && (
          <Step
            data={formData.stepTwo}
            onChange={changeHandler}
            onSlideChange={sliderChangeHandler}
            onFileChange={fileChangeHandler}
            onStepChange={stepChangeHandler}
            dropdownChange={dropdownChangeHandler}
            updateChildChoices={updateChildChoices}
            errors={errors}
            stepKey="stepTwo"
            stepName="Business Details"
            onPrevStep={(step) => setStep(step)}
            step={2}
          />
        )}
        {step === 3 && (
          <Preview
            onPrevStep={(step) => setStep(step)}
            step={3}
            data={[
              {
                label: "Profile Name",
                value: formData.stepOne.profilename.value,
                width: "100%",
              },
              {
                label: "Studio/Company Name",
                value: formData.stepOne.name.value,
                width: "100%",
              },
              {
                label: "Address Line1",
                value: formData.stepOne.addressLine1.value,
                width: "100%",
              },
              {
                label: "Address Line2",
                value: formData.stepOne.addressLine2.value,
                width: "100%",
              },
              {
                label: "City",
                value: formData.stepOne.city.value,
                width: "50%",
              },
              {
                label: "State",
                value: formData.stepOne.state.value,
                width: "50%",
              },
              {
                label: "Country",
                value: formData.stepOne.country.value,
                width: "50%",
              },
              {
                label: "PinCode",
                value: formData.stepOne.pinCode.value,
                width: "50%",
              },
              {
                label: "Email",
                value: formData.stepOne.email.value,
                width: "50%",
              },
              {
                label: "Phone",
                value: formData.stepOne.phone.value,
                width: "50%",
              },
              {
                label: "Brief Description",
                value: formData.stepTwo.briefDescription.value,
                width: "100%",
              },
              {
                label: "Skills",
                value: formData.stepTwo.skills.value.join(", "),
                width: "100%",
              },
              {
                label: "Themes",
                value: formData.stepTwo.themes.value.join(", "),
                width: "100%",
              },
              {
                label: "Profile Image",
                value: URL.createObjectURL(formData.stepTwo.image.value),
                image: true,
                width: "100%",
              },
            ]}
          />
        )}
      </form>
      {step === 4 && (
        <AddFirstProject
          step={4}
          data={formData.stepOne.profilename.value}
          onChange={changeHandler}
          errors={errors}
          stepKey="stepFour"
        />
      )}
    </>
  );
};

export default Form;
