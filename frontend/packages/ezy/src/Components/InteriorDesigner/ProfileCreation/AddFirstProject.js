import styled from 'styled-components';
import React from 'react';
import {Link} from "react-router-dom";
import { PanelWrapper } from "common";

function AddFirstProject({data, onChange, step, errors, onFileChange, stepKey}) {

    return (
        <PanelWrapper backgroundColor="rgba(123,131,97,1)">
            <p>Add your First Project</p>
            <Subtitle>
                Congratulations! Your profile has been created.<br/><br/>
                A project is a collection of photos of your work and one of the key ways homeowners can find you on Dexterio.<br/>
                For a better profile and lead generation, please add your projects.
            </Subtitle>
            
            <Link to={{
              pathname: '/interior-designers/uploadProject',
              state: {username : data}
            }}>
              <CreateNewProject>
                <div>
                  <p>Create your first project </p>
                </div>
              </CreateNewProject>
            </Link>
      </PanelWrapper>
    )
}

export default AddFirstProject

const CreateNewProject = styled.div`
    & > div {
      width: 50%;
      margin: auto;
      min-height: 200px;
      background-color: rgba(255,92,92,1);
      border-radius: 5px;
      position : relative;
      box-shadow: 0 0 0 4px rgb(0 0 0 / 15%), 0 0 0 rgb(0 0 0 / 20%);
      cursor: pointer;

      &:hover {
        transition: all 0.3s ease-out;
        background: rgba(123,131,97,1);
    }

      & > p {
        margin-top: 0px;
        margin-left:10%;
        width: 80%;
        font-size: 30px;
        font-family: "Poppins", sans-serif;
        color : #fff;
        font-weight: 600;
        position: absolute;
        top: 50%;
        -ms-transform: translateY(-50%);
        transform: translateY(-50%);
      }

    }
`;

const Subtitle = styled.div`
    max-width:100%;
    margin-bottom: 35px;
    color: #000707DE;
    font-family: "Open Sans", sans-serif;
    font-size: 16px;
    font-weight: 600;
    line-height: 1.6em;
    margin: 35px auto;


    @media(max-width: 768px) {
        font-size: 16px;
    }
`;