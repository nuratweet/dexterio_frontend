import React, { Fragment , useState, useEffect } from 'react';
import styled from 'styled-components';

import ProfileCreationForm from './ProfileCreationForm';
import ProgressBar from './ProgressBar';

const IDProfileCreation = (props) => {
  const [value, setValue] = useState(0);
  const [step, setStep] = useState(1);

  useEffect(() => {
    const interval = () => {
      setValue(oldValue => {
          if (step === 1) return 0;
          if (step === 2) return 25;
          if (step === 3) return 50;
          if (step === 4) return 75;
      });
    };
    interval();
  }, [step]);

  return (
    <Fragment>
      <ProgressBar color={"lightblue"} width={"100%"} value={value} max={100} />
      <Container>
        <Columns>
          <Column>
            <ProfileCreationForm 
              step={step}
              setStep={setStep}
            />
          </Column>
        </Columns>
      </Container>
    </Fragment>
  );
}

const Container = styled.div`
  padding: 0.75rem 0 0 0;
  
  text-align: center;
  
  flex-grow: 1;
  position: relative;
`;

const Columns = styled.div`
    @media only screen and (min-width: 768px) {
      display: flex;
      justify-content: center;
      background-color: #f6f8fa;
    }

`;

const Column = styled.div`
    @media only screen and (min-width: 768px) {
      flex: none;
      width: 75%;
      
    }
    display: block;
`;

export default IDProfileCreation;