import { FlexRow } from "common/components/StyledComponents";
import { IntroHeadlines, SubSectionHeadlines } from "globalstyles/globalStyles";
import React from "react";

function Thankyou() {
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#f4f3ee",
        textAlign: "center",
        height: "100vh",
        flexDirection: "column",
      }}
    >
      <FlexRow>
        <IntroHeadlines>THANK YOU FOR YOUR SUBMISSION</IntroHeadlines>
      </FlexRow>
      <FlexRow>
        <SubSectionHeadlines>We will get back to you</SubSectionHeadlines>
      </FlexRow>
    </div>
  );
}

export default Thankyou;
