import React, { useState } from "react";
import { idProfileLeadData } from "../../../data/IDProfileLeadsData";
import { Step, Preview, validate } from "common";
import axios from "axios";
import Thankyou from "./Thankyou";

const config = require("../../../config_prod.json");

function IDProfileLeads({ props, step, setStep }) {
  const [formData, setFormData] = useState(idProfileLeadData);
  const [errors, setErrors] = useState({});
  const [dependantSection, setDependantSection] = useState("");

  const changeHandler = (step, e) => {
    e.preventDefault();

    setFormData((prev) => ({
      ...prev,
      [step]: {
        ...prev[step],
        [e.target.name]: {
          ...prev[step][e.target.name],
          value: e.target.value,
        },
      },
    }));
  };

  const stepChangeHandler = (values, e) => {
    e.preventDefault();
    const newErrors = validate(values);
    setErrors(newErrors);
    console.log(values);
    if (Object.keys(newErrors).length === 0) {
      setStep(step + 1);
    }
  };

  const dropdownChangeHandler = (event, step, name, newValue) => {
    event.preventDefault();
    if(name === 'WhoAreYou') {
        setDependantSection(newValue);
    }
    setFormData((prev) => ({
      ...prev,
      [step]: {
        ...prev[step],
        [name]: {
          ...prev[step][name],
          value: newValue,
        },
      },
    }));
  };

  const submitHandler = async (e) => {
    e.preventDefault();
    try {
      const params = {
        name: formData.stepOne.name.value,
        contactNumber: formData.stepOne.contactNumber.value,
        email: formData.stepOne.email.value,
        WhoAreYou: formData.stepOne.WhoAreYou.value,
        cityOfOperations: formData.stepOne.cityOfOperations.value,
        education: formData.stepOne.education.value,
        instagram: formData.stepOne.instagram.value,
        linkedin: formData.stepOne.linkedin.value,
        nameOfStudio: formData.stepTwo.nameOfStudio.value,
        numberOfEmployees: formData.stepTwo.numberOfEmployees.value,
        studioStarted: formData.stepTwo.studioStarted.value,
        projectsCompletedStudio: formData.stepTwo.projectsCompletedStudio.value,
        averageRangeStudio: formData.stepTwo.averageRangeStudio.value,
        experienceFreelance: formData.stepThree.experienceFreelance.value,
        projectsCompletedFreelance:
          formData.stepThree.projectsCompletedFreelance.value,
        averageRangeFreelance: formData.stepThree.averageRangeFreelance.value,
        certificationsFreelance:
          formData.stepThree.certificationsFreelance.value,
        year: formData.stepFour.year.value,
        college: formData.stepFour.college.value,
        certificationsStudent: formData.stepFour.certificationsStudent.value,
        havedonefreelance: formData.stepFour.havedonefreelance.value,
      };

      console.log(params);
      await axios
        .post(`${config.api.invokeUrl}/designerlead/`, params)
        .then((data) => {
          console.log(data.status);
          if (data.status === 201) {
            setStep(step + 1);
          } else {
            //TODO Forward to Error Screen
          }
        });
    } catch (error) {
      console.log(`Error occured: ${error}`);
    }
  };

  return (
    <>
      <form onSubmit={submitHandler}>
        {step === 1 && (
          <Step
            data={formData.stepOne}
            onChange={changeHandler}
            dropdownChange={dropdownChangeHandler}
            onStepChange={stepChangeHandler}
            errors={errors}
            stepKey="stepOne"
            stepName="Onboarding Form"
            step={1}
          />
        )}
        {step === 2 && dependantSection[0] === "Studio" && (
          <Step
            data={formData.stepTwo}
            onChange={changeHandler}
            dropdownChange={dropdownChangeHandler}
            onStepChange={stepChangeHandler}
            onPrevStep={(step) => setStep(step)}
            errors={errors}
            stepKey="stepTwo"
            stepName="Onboarding Form"
            step={2}
          />
        )}
        {step === 2 && dependantSection[0] === "Freelancer" && (
          <Step
            data={formData.stepThree}
            onChange={changeHandler}
            dropdownChange={dropdownChangeHandler}
            onStepChange={stepChangeHandler}
            onPrevStep={(step) => setStep(step)}
            errors={errors}
            stepKey="stepThree"
            stepName="Onboarding Form"
            step={2}
          />
        )}
        {step === 2 && dependantSection[0] === "College Student" && (
          <Step
            data={formData.stepFour}
            onChange={changeHandler}
            dropdownChange={dropdownChangeHandler}
            onStepChange={stepChangeHandler}
            onPrevStep={(step) => setStep(step)}
            errors={errors}
            stepKey="stepFour"
            stepName="Onboarding Form"
            step={2}
          />
        )}

        {step === 3 && (
          <Preview
            onPrevStep={(step) => setStep(step)}
            step={3}
            data={[
              {
                label: "Name",
                value: formData.stepOne.name.value,
                width: "100%",
              },
              {
                label: "Contact Number",
                value: formData.stepOne.contactNumber.value,
                width: "100%",
              },
              {
                label: "Your Email",
                value: formData.stepOne.email.value,
                width: "100%",
              },
            ]}
          />
        )}
      </form>
      {step === 4 && <Thankyou />}
    </>
  );
}

export default IDProfileLeads;
