import React, {Component} from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';

class FullPageLoader extends Component {
    state = {  }


    render() { 
        const {loading} = this.props;

        if(!loading) return null;

        return ( 
            <LoaderContainer>
                <Loader>
                    <img src="/loader.gif" alt="Interior Designing Profile submission"/>
                </Loader>
            </LoaderContainer>
         );
    }
}

const LoaderContainer = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: #F8F8F8AD;
`;

const Loader = styled.div`
  left: 50%;
  top: 30%;
  z-index: 1000;
  position: absolute;
`;

const mapStateToProps = state => ({ loading: state.application.loading })

export default connect(mapStateToProps)(FullPageLoader);
