import React from "react";
import { CurvedButton, IntroHeadlines, SubSectionHeadlines } from "globalstyles";
import { Margin } from "../ProfilePage/ProfilePage2.element";
import { FlexColumn, FlexRow } from "common";
import {
  Container,
  LeftPanel,
  RightPanel,
} from "./IntreriorDesigningLandingPage.element";

const InteriorDesignerLandingPage = (props) => {
  return (
    <Container>
      <Margin margin="100px 0 0 0">
        <FlexRow>
          <LeftPanel
            style={{
              display: "flex",
              alignItems: "left",
              justifyContent: "left",
            }}
          >
            <div>
              <FlexColumn>
                <IntroHeadlines
                  style={{
                    alignItems: "left",
                    padding: "20px 0px",
                    lineHeight: "1",
                  }}
                >
                  Unlock the global<br/> remote job opportunities.
                </IntroHeadlines>
                <SubSectionHeadlines
                  style={{
                    alignItems: "left",
                    padding: "20px 0px",
                    fontWeight: "300",
                  }}
                >
                  Rethink and relearn your engineering
                  <br />
                  know-how for the jobs of tomorrow.
                </SubSectionHeadlines>
                <FlexRow
                  style={{
                    width: "50%",
                    alignItems: "left",
                    padding: "20px 0px",
                  }}
                >
                  <CurvedButton
                    primary="#1b222b"
                    light
                    curve
                    big
                    margin="0px"
                    onClick={() => {
                      props.mobileLeadFormData.setMobileLeadFormData(true);
                    }}
                  >
                    Get Early Access
                  </CurvedButton>
                </FlexRow>
              </FlexColumn>
            </div>
          </LeftPanel>
          <RightPanel style={{ minHeight: "450px" }}>
            <FlexRow>
              <div
                style={{
                  display: "flex",
                  justifyContent: "flex-end",
                  margin: "15px",
                  maxHeight: "350px",
                }}
              >
                <img
                  alt="interior designer 1"
                  src="./interiordesigner1.webp"
                  style={{
                    borderRadius: "10px",
                    display: "block",
                    verticalAlign: "center",
                    width: "100%",
                    height: "100%",
                    objectFit: "cover",
                    objectPosition: "center",
                  }}
                />
              </div>
              <div
                style={{
                  display: "flex",
                  justifyContent: "flex-start",
                  margin: "15px",
                  maxHeight: "350px",
                }}
              >
                <img
                  alt="interior designer 2"
                  src="./interiordesigner2.webp"
                  style={{
                    borderRadius: "10px",
                    display: "block",
                    verticalAlign: "center",
                    width: "100%",
                    height: "100%",
                    objectFit: "cover",
                    objectPosition: "center",
                  }}
                />
              </div>
            </FlexRow>
          </RightPanel>
        </FlexRow>
      </Margin>
    </Container>
  );
};

export default InteriorDesignerLandingPage;
