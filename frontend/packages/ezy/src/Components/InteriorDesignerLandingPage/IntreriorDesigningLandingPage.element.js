import styled from 'styled-components';

export const Container = styled.div`
    max-width: 90vw;
    margin: 0 auto;
    overflow-x: hidden;
    @media only screen and (max-width: 419px) {
        max-width: 98vw;
    }

`;

export const LeftPanel = styled.div`
    flex: 0 0 40em;
    margin: 10px;

    @media only screen and (max-width:767px) {
        flex: 1;
    }
`;

export const RightPanel = styled.div`
    display: flex;
    margin: 10px;
    flex-grow: 1;

    @media only screen and (max-width:767px) {
        display: none;
    }
`;
