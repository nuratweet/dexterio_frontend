import React from 'react';
import { Container } from 'globalstyles';

const ComingSoon = () => {
  return(
    <div style={{"background-image": "linear-gradient(rgb(255, 248, 248), rgba(255, 235, 222, 0.42))"}}>
      <Container>
        <div style={{"padding":"100px"}}>
          <img src="/Coming_Soon.svg" alt="Page Not Found"/>
        </div>
      </Container>
    </div>
  );
}

export default ComingSoon;