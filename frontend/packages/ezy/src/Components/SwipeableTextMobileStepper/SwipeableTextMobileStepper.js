import React, { useEffect } from 'react';
import { useTheme } from '@material-ui/styles';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import { MdKeyboardArrowRight, MdKeyboardArrowLeft } from 'react-icons/md';
import SwipeableViews from 'react-swipeable-views';
import { autoPlay } from 'react-swipeable-views-utils';
import './SwipeableTextMobileStepper.css';
import Pagination from "../Common/Pagination";
import {
  VerticalSpacing,
} from "globalstyles";

const config = require('../../config_prod.json');

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

function SwipeableTextMobileStepper(props) {
  const theme = useTheme();
  const [activeStep, setActiveStep] = React.useState(0);
  const [maxSteps, setMaxSteps] = React.useState(0);
  const projectKey = props.projectSelected.item.key;
  const [projectImages, setProjectImages] = React.useState([]);

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleStepChange = (step) => {
    setActiveStep(step);
  };

  useEffect(() => {
    fetch(`${config.api.invokeUrl}/designerprofile/project/${projectKey}`)
      .then(resp => resp.json())
      .then(data => {
        const projectDataApiRes = [];
        if (data.Items) {
          data.Items.forEach(element => {
            projectDataApiRes.push(element.projectUrl);
          });
        }
        setProjectImages(projectDataApiRes);
        setMaxSteps(projectDataApiRes.length);
      }
      )
    return () => {

    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <div>
      <div className="left-button">
        <Button size="small"
          onClick={handleBack}
          disabled={activeStep === 0}
          style={{ fontSize: "40px", color: "black" }}>
          {theme.direction === 'rtl' ? (
            <MdKeyboardArrowRight />
          ) : (
            <MdKeyboardArrowLeft />
          )}
        </Button>
      </div>
      <div className="right-button" >
        <Button
          size="small"
          onClick={handleNext}
          disabled={activeStep === maxSteps - 1}
          style={{ fontSize: "40px", color: "black" }}
        >
          {theme.direction === 'rtl' ? (
            <MdKeyboardArrowLeft />
          ) : (
            <MdKeyboardArrowRight />
          )}
        </Button>
      </div>
      <Box >

        <AutoPlaySwipeableViews
          axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
          index={activeStep}
          onChangeIndex={handleStepChange}
          enableMouseEvents
        >
          {projectImages.map((step, index) => (
            <div key={step.label} style={{ "display": "flex", "justifyContent": "center", "alignItems": "center", "margin": "auto" }}>
              {Math.abs(activeStep - index) <= 2 ? (
                <img src={step} alt="Interior Designer Project" style={{ "alignSelf": "center", "overflow": "hidden", "width": "95%", "height":"90vh", "objectFit": "contain", "objectPosition": "center" }} />
              ) : null}
            </div>
          ))}
        </AutoPlaySwipeableViews>
        <VerticalSpacing height="1vw" />
        <Pagination
          dots={maxSteps}
          index={activeStep}
          onChangeIndex={handleStepChange}
        />

      </Box>
    </div>
  );
}

export default SwipeableTextMobileStepper;