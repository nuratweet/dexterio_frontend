import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import  { ScrollToTop , ScrollToTopButton, IndicatorFallback, IndicatorProvider, Indicator} from 'common';
import ComingSoon from './Components/ComingSoon/Coming_Soon';

import './App.css';
import React from 'react';

function App() {
  return (
      <div className="App">
        <Router>
          <ScrollToTop />
          <IndicatorProvider>
            <Indicator />
            <div>
              <div className = "body">
                <Switch>
                  <React.Suspense fallback={<IndicatorFallback />}>
                    <Route exact path = "/" render = {(props) => <ComingSoon/>} />
                  </React.Suspense>
                </Switch>
              </div>
              <ScrollToTopButton />
            </div>
          </IndicatorProvider>
        </Router>
      </div>
    );
}

export default App;
