import styled, { createGlobalStyle, keyframes } from "styled-components";

const GlobalStyle = createGlobalStyle`
    * {
        box-sizing: border-box;
        margin: 0;
        padding: 0;
        font-family: 'Poppins', sans-serif;
    }
`;

export const Container = styled.div`
  z-index: 1;
  width: 100%;
  margin-right: auto;
  margin-left: auto;
  padding-bottom: 4vw;
  padding-top: 4vw;

  @media only screen and (min-width: 1536px) {
    max-width: 80%;
  }

  @media only screen and (max-width: 1440px) {
    max-width: 1300px;
  }

  @media only screen and (max-width: 1366px) {
    max-width: 1150px;
  }

  @media only screen and (max-width: 1280px) {
    //Bad
    max-width: 1024px;
  }

  @media only screen and (max-width: 600px) {
    //Check
    max-width: 90%;
    padding-bottom: 9vw;
    padding-top: 9vw;
  }
`;

export const DexterioEdgeContainer = styled(Container)`
  @media only screen and (max-width: 600px) {
    max-width: 100%;
    padding-bottom: 9vw;
    padding-top: 9vw;
  }
`;

export const CurvedButton = styled.button`
  border-radius: ${({ curve }) => (curve ? "25px" : "0px")};
  background: ${({ background }) => background};
  white-space: nowrap;
  padding: ${({ big }) => (big ? "15px 55px" : "6px 29px ")};
  color: ${({ light }) => (light ? "#fff" : "#01070b")};
  font-size: 1.10vw ;
  font-weight: bold;
  font-style: normal;
  outline: none;
  border: none;
  cursor: pointer;
  margin-left: ${({ margin }) => margin};
  font-family: "Poppins", sans-serif;
  width: max-content;

  &:hover {
    transition: all 0.3s ease-out;
    background: rgba(123, 131, 97, 1);
  }

  @media (max-width: 600px) {
    padding: ${({ big }) => (big ? "10px 10px" : "10px 20px")};
    font-size: 4vw;
  }
`;

export const BorderedEmptyCurvedButton = styled(CurvedButton)`
  background: none;
  color: #111121;
  border: 1px solid #111121;
  padding: ${({ padding }) => padding};

  &:hover,
  &:focus {
    transition: all 0.3s ease-out;
    background: ${({ hoverBackground }) => hoverBackground};
    color: ${({ hoverTextColor }) => hoverTextColor};
  }
`;

const blink = keyframes`
    0%  { background-color: #ccc; }
    49% { background-color: #ccc; }
    50% { background-color: transparent; }
    99% { background-color: transparent; }
    100%  { background-color: #ccc; }
`;

export const IntroHeadlines = styled.h1`
    font-size: 3.5vw;
    font-family: "Poppins", sans-serif;
    color: ${({ light }) => (light ? "#ffffff" : "#111121")};;
    font-weight: bold;
    line-height: 4.5vw;
    letter-spacing: 0.5px;
    white-space: nowrap;
    font-style: normal;
    margin-top: 0px;
    margin-bottom: 2.5vw;

    & > span {
        color:${({ spanColor }) => spanColor};
        font-size: 3.5vw;
        font-family: "Poppins", sans-serif;
        font-weight: bold;
        line-height: 4.5vw;
        letter-spacing: 0.5px;
        white-space: nowrap;
        font-style: normal;
    }

    & > span.typed-text {
        color:${({ spanColor }) => spanColor};
        font-size: 3.5vw;
        font-family: "Poppins", sans-serif;
        font-weight: bold;
        line-height: 4.5vw;
        letter-spacing: 0.5px;
        white-space: nowrap;
        font-style: normal;
    }

    & > span.cursor {
        background:${({ spanColor }) => spanColor};
        font-weight: bold;
        line-height: 3.5vw;
        font-size: 3.5vw;
        animation: ${blink} 1s infinite;
        border: 1px solid {spanColor}
    }

    & > span.cursor.typing {
        background:${({ spanColor }) => spanColor};
        line-height: 3.5vw;
        font-size: 3.5vw;
        font-weight: bold;
        animation: none;
        border: 1px solid {spanColor}
    }

    @media only screen and (max-width: 600px) {
        font-size: 12vw;
        line-height: 13vw;
        margin-bottom: 4vw;

        & > span {
            font-size: 12vw;
            line-height: 13vw;
        }
        & > span.typed-text {
            font-size: 12vw;
            line-height: 13vw;
        }
    
        & > span.cursor {
            font-size: 10vw;
            line-height: 10vw;
        }
    
        & > span.cursor.typing {
            font-size: 10vw;
            line-height: 10vw;
        }
    
    }
`;

export const IntroParagraph = styled.p`
  font-size: 1vw;
  font-family: "Poppins", sans-serif;
  color: #111121;
  font-weight: normal;
  font-style: normal;
  width: 450px;
  margin-top: 0px;
  margin-bottom: 4vw;

  @media screen and (max-width: 600px) {
    font-size: 4.5vw;
    width: 80vw;
  }
`;

export const HighlightHeadings = styled.h2`
  font-size: 52px;
  font-family: "Poppins", sans-serif;
  color: #111121;
  font-weight: bold;
  line-height: 70px;
  font-style: normal;
  margin-top: 20px;
  margin-bottom: 30px;

  & > span {
    color: rgba(123, 131, 97, 1);
    font-size: 52px;
    font-family: "Poppins", sans-serif;
    font-weight: bold;
    line-height: 70px;
    font-style: normal;
  }
`;

export const SectionHeadlines = styled.h2`
  font-size: 3vw;
  font-family: "Poppins", sans-serif;
  color: ${({ light }) => (light ? "#ffffff" : "#111121")};
  font-weight: bold;
  font-style: normal;
  white-space: nowrap;
  letter-spacing: 0.5px;
  margin-top: 1vw;
  margin-bottom: 1vw;
  line-height: 4vw;

  & > span {
    color: rgba(123, 131, 97, 1);
    font-size: 3vw;
    font-family: "Poppins", sans-serif;
    font-weight: bold;
    font-style: normal;
    white-space: nowrap;
    letter-spacing: 0.5px;
    line-height: 3.5vw;
  }

  @media only screen and (max-width: 600px) {
    font-size: 9.5vw;
    line-height: 10vw;
    & > span {
      font-size: 9.5vw;
    }
    margin-top: 4vw;
    margin-bottom: 4vw;
  }
`;

export const SectionParagraph = styled.p`
  font-size: 1.5vw;
  font-family: "Poppins", sans-serif;
  color: ${({ light }) => (light ? "#ffffff" : "#111121")};
  font-weight: normal;
  font-style: normal;
  letter-spacing: 0.5px;
  margin-top: 0px;
  margin-bottom: 30px;
  text-align: justify;
  text-justify: inter-word;
  line-height: 2vw;

  @media only screen and (max-width: 600px) {
    font-size: 5vw;
    line-height: 6.5vw;
  }
`;

export const SubSectionHeadlines = styled.h2`
  font-size: 1.5vw;
  font-family: "Poppins", sans-serif;
  color: #111121;
  font-weight: bold;
  font-style: normal;
  white-space: wrap;
  margin-top: 1vw;
  margin-bottom: 1vw;
  line-height: 2vw;

  @media only screen and (max-width: 600px) {
    font-size: 5.5vw;
    line-height: 6vw;
    margin-top: 2vw;
    margin-bottom: 2vw;
    & > span {
      font-size: 5.5vw;
    }
  }
`;

export const DexterioEdgeSubSectionheadlines = styled(SubSectionHeadlines)`
  @media only screen and (max-width: 600px) {
    margin-bottom: 8vw;
  }
`;

export const SubSectionParagraph = styled.p`
  font-size: 1vw;
  font-family: "Poppins", sans-serif;
  color: #111121;
  font-weight: normal;
  font-style: normal;
  letter-spacing: 1px;
  line-height: 1.5vw;
  white-space: wrap;

  @media only screen and (max-width: 600px) {
    font-size: 3.5vw;
    line-height: 4.25vw;
    & > span {
      font-size: 3.5vw;
    }
  }
`;

export const SubSubSectionHeadlines = styled.h2`
  font-size: 1.15vw;
  font-family: "Poppins", sans-serif;
  color: #111121;
  font-weight: bold;
  font-style: normal;
  margin: 0;

  @media only screen and (max-width: 600px) {
    font-size: 4.5vw;
    line-height: 5vw;
    & > span {
      font-size: 4.5vw;
    }
  }
`;

export const CompareSubText = styled(SubSubSectionHeadlines)`
  font-size: 3vw;
  text-align: left;
  width: 100%;
  position: absolute;
  top: 0;
  left: 0;
  transform: translateY(-3vw);
  background: ${({ background }) => background};
  width: max-content;

  @media only screen and (min-width: 500px) {
    display: none;
  }
`;

export const SubSubSectionParagraph = styled.p`
  font-size: 0.75vw;
  font-family: "Poppins", sans-serif;
  color: #111121;
  font-weight: normal;
  font-style: normal;
  letter-spacing: 0.5px;
  line-height: 1.25vw;
  margin: 0;

  @media only screen and (max-width: 600px) {
    font-size: 4vw;
    line-height: 4.5vw;
    & > span {
      font-size: 4vw;
    }
  }
`;

export const SubSubSubSectionHeadlines = styled.h2`
  font-size: 20px;
  font-family: "Poppins", sans-serif;
  color: #111121;
  font-weight: bold;
  font-style: normal;
  margin: 0;
`;

export const SubSubSubSectionParagraph = styled.p`
  font-size: 14px;
  font-family: "Poppins", sans-serif;
  color: #111121;
  font-weight: normal;
  font-style: normal;
  letter-spacing: 0.5px;
  line-height: 30px;
  margin: 0;
`;

export const BannersHeadlines = styled.h2`
  font-size: 3.25vw;
  font-family: "Poppins", sans-serif;
  color: ${({ light }) => (light ? "#ffffff" : "#111121")};
  font-weight: bold;
  font-style: normal;
  white-space: nowrap;
  letter-spacing: 0.5px;
  margin-top: 1vw;
  margin-bottom: 1vw;
  line-height: 4.5vw;

  & > span {
    color: rgba(123, 131, 97, 1);
    font-size: 3.25vw;
    font-family: "Poppins", sans-serif;
    font-weight: bold;
    font-style: normal;
    white-space: nowrap;
    letter-spacing: 0.5px;
    line-height: 4.5vw;
  }

  @media only screen and (max-width: 600px) {
    font-size: 9vw;
    line-height: 11.5vw;
    & > span {
      font-size: 9vw;
      line-height: 11.5vw;
    }
    margin-top: 4vw;
    margin-bottom: 4vw;
  }
`;

export const VerticalSpacing = styled.div`
  height: ${({ height }) => height};
`;

export default GlobalStyle;
