import React from "react";
import Progress from "./Progress";

const IndicatorContext = React.createContext();

export const IndicatorProvider = ({ children }) => {
  const [active, setActive] = React.useState(false);
  const value = React.useMemo(() => ({ active, setActive }), [active, setActive]);
  return (
    <IndicatorContext.Provider value={value}>
      {children}
    </IndicatorContext.Provider>
  );
}

export const Indicator = () => {
  const { active } = React.useContext(IndicatorContext);
  const [percent, setPercent] = React.useState(0);
  React.useEffect(() => {
    setTimeout(() => {
      setPercent(percent => (percent < 100 ? percent + 10 : 100));
    }, 200);
  });
  return active ? <Progress percent={percent} /> : null;
}

export const IndicatorFallback = () => {
  const { setActive } = React.useContext(IndicatorContext);
  React.useEffect(() => {
    setActive(true);
    return () => setActive(false);
  });
  return null;
}