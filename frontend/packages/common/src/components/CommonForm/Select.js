import PropTypes from "prop-types";
import React from "react";
import { Select, MenuItem, FormControl, InputLabel } from "@material-ui/core";
import { InputWrapper } from "./ProfileCreation.element";

const CommonSelect = ({
  name,
  value,
  onChange,
  choices,
  error,
  placeholder,
  updateChildChoices,
}) => {
  return (
    <InputWrapper width="100%">
      <FormControl fullWidth>
        <InputLabel id="demo-simple-select-label">{placeholder}</InputLabel>
        <Select
          name={name}
          value={value}
          onChange={(e) => {
            onChange(e);
            if (name === "genre") {
              updateChildChoices(e);
            }
          }}
          variant="outlined"
          margin="dense"
          size="small"
          label={placeholder}
        >
          {choices.map((style) => {
            return (
              <MenuItem value={style} key={style}>
                {style}
              </MenuItem>
            );
          })}
        </Select>
      </FormControl>
    </InputWrapper>
  );
};

CommonSelect.propTypes = {
  type: PropTypes.string,
  placeholder: PropTypes.string,
  name: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  onChange: PropTypes.func.isRequired,
};

export default CommonSelect;
