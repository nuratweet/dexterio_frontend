import React from "react";
import PropTypes from "prop-types";
import { IconContext } from "react-icons/lib";
import Select from "./Select";
import {
  PanelWrapper,
  PanelLeft,
  PanelRight,
  Panels,
  SuggestionBoxWrapper,
  SuggestionBox,
  SuggestionBoxLine,
  SuggestionBoxHeader,
} from "./ProfileCreation.element";
import Input from "./Input";
import MultilineInput from "./MultilineInput";
import MultiDropdownApp from "./MultiDropdownApp";
import FileInput from "./FileInput";
// import TagInput from './TagInput';
import { IoBriefcase } from "react-icons/io5";
import { FaRuler, FaPhone } from "react-icons/fa";
import Display from "./Display";
import { CurvedButton } from "globalstyles";
import { FlexRow , FlexAlwaysRow } from "../StyledComponents";
import CommonSlider from "./Slider";

const Step = ({
  data,
  onChange,
  onFileChange,
  onStepChange,
  onSlideChange,
  avoidSpace,
  onProfileLinkChange,
  updateChildChoices,
  dropdownChange,
  errors,
  stepKey,
  step,
  onPrevStep,
  stepName,
}) => {
  let output = [];

  for (const [key, val] of Object.entries(data)) {
    if (val.type.split(":")[0] === "input") {
      output.push(
        <Input
          key={key}
          placeholder={val.placeholder}
          name={key}
          value={val.value}
          mandatory="true"
          onChange={(e) => {
            onChange(stepKey, e);
            if (val.placeholder === "Profile ID") {
              onProfileLinkChange(e.target.value);
            }
          }}
          onKeyDown={(e) => {
            if (val.placeholder === "Profile ID") {
              avoidSpace(e);
            }
          }}
          error={errors[key]}
          width={val.width}
          type={val.type.split(":")[1]}
        />
      );
    } else if (val.type === "display") {
      output.push(
        <Display
          key={key}
          placeholder={val.placeholder}
          name={key}
          value={val.value}
          onChange={(e) => onChange(stepKey, e)}
          dropdownChange={(e, value) => dropdownChange(e, stepKey, key, value)}
          error={errors[key]}
          choices={val.choices}
        />
      );
    } else if (val.type === "slider") {
      output.push(
        <CommonSlider
          key={key}
          placeholder={val.placeholder}
          name={key}
          value={val.value}
          onChange={(e) => onSlideChange(stepKey, e , val.choices.find((choice) => choice.value === e.target.value).text)}
          error={errors[key]}
          choices={val.choices}
          textAround={val.textaround}
        />
      );
    } else if (val.type === "select") {
      output.push(
        <Select
          key={key}
          placeholder={val.placeholder}
          name={key}
          value={val.value}
          onChange={(e) => onChange(stepKey, e)}
          updateChildChoices={(e) =>
            updateChildChoices(
              e,
              stepKey,
              val.childName,
              val.childChoices.find((genre) => genre.key === e.target.value)
                .value
            )
          }
          dropdownChange={(e, value) => dropdownChange(e, stepKey, key, value)}
          error={errors[key]}
          choices={val.choices}
          width={val.width}
        />
      );
    } else if (val.type === "multiselect") {
      output.push(
        <MultiDropdownApp
          key={key}
          placeholder={val.placeholder}
          name={key}
          value={val.value}
          onChange={(e) => onChange(stepKey, e)}
          dropdownChange={(e, value) => dropdownChange(e, stepKey, key, value)}
          error={errors[key]}
          choices={val.choices}
          limit={val.limit}
        />
      );
    } else if (val.type === "dependentmultiselect") {
      output.push(
        <MultiDropdownApp
          key={key}
          placeholder={val.placeholder}
          name={key}
          value={val.value}
          onChange={(e) => onChange(stepKey, e)}
          dropdownChange={(e, value) => dropdownChange(e, stepKey, key, value)}
          error={errors[key]}
          choices={val.choices}
          limit={val.limit}
        />
      );
    } else if (val.type === "file") {
      output.push(
        <FileInput
          key={key}
          onChange={onFileChange}
          error={errors[key]}
          name={key}
          stepKey={stepKey}
          fileName={val.fileName}
        />
      );
    } else if (val.type.split(":")[0] === "multilineInput") {
      output.push(
        <MultilineInput
          key={key}
          placeholder={val.placeholder}
          name={key}
          value={val.value}
          mandatory="true"
          onChange={(e) => onChange(stepKey, e)}
          error={errors[key]}
          width={val.width}
          type={val.type.split(":")[1]}
        />
      );
    }
  }

  return (
    <PanelWrapper backgroundColor="rgba(123,131,97,1)">
      <p>{stepName}</p>
      <Panels>
        <PanelLeft>
          {output}
          <FlexAlwaysRow
            style={{
              alignItems: "center",
              justifyContent: "space-between",
              padding: "2em 2em",
              textAlign: "right",
            }}
          >
            <FlexRow
              style={{
                alignItems: "center",
                justifyContent: "flex-start",
              }}
            >
              <IconContext.Provider value={{ size: "3rem" }}>
                {step > 1 && (
                  <CurvedButton
                    background="rgba(255,92,92,1)"
                    light
                    curve
                    fontBig
                    margin="0px"
                    onClick={() => onPrevStep(step - 1)}
                  >
                    Go Back
                  </CurvedButton>
                )}
              </IconContext.Provider>
            </FlexRow>
            <FlexRow
              style={{
                alignItems: "center",
                justifyContent: "flex-end",
              }}
            >
              <CurvedButton
                background="rgba(255,92,92,1)"
                light
                curve
                fontBig
                margin="0px"
                onClick={(e) => onStepChange(data, e)}
              >
                Continue
              </CurvedButton>
            </FlexRow>
          </FlexAlwaysRow>
        </PanelLeft>
        <PanelRight>
          <SuggestionBoxWrapper>
            <SuggestionBox>
              <SuggestionBoxHeader>Complete your Profile</SuggestionBoxHeader>
              <SuggestionBoxLine iconColor="rgba(123,131,97,1)">
                <span>
                  <IoBriefcase />
                </span>
                <span>
                  Include your business name as it will appear in the directory.
                </span>
              </SuggestionBoxLine>
              <SuggestionBoxLine iconColor="rgba(123,131,97,1)">
                <span>
                  <FaRuler />
                </span>
                <span>
                  Choose the category and company type that best fit your
                  business.
                </span>
              </SuggestionBoxLine>
              <SuggestionBoxLine iconColor="rgba(123,131,97,1)">
                <span>
                  <FaPhone />
                </span>
                <span>
                  Provide your complete contact information in the space
                  provided.
                </span>
              </SuggestionBoxLine>
            </SuggestionBox>
          </SuggestionBoxWrapper>
        </PanelRight>
      </Panels>
    </PanelWrapper>
  );
};

Step.propTypes = {
  data: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  onFileChange: PropTypes.func,
  onStepChange: PropTypes.func.isRequired,
  errors: PropTypes.object,
  stepKey: PropTypes.string.isRequired,
  step: PropTypes.number.isRequired,
  onPrevStep: PropTypes.func,
};

export default Step;
