import React, { useRef , useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Header2, ImgWrapper, ProfilePhotoUploadContainer, ProfilePhotoUploadForm, SeperatorBox } from './ProfileCreation.element';

const FileInput = ({ onChange, name, error, stepKey, fileName }) => {
  const fileInput = useRef();
  const [file, setFile] = useState("/profileUpload.png");

  const openFilePicker = () => {
    fileInput.current.click();
  }

  const fileChangeHandler = (e) => {
    if(e.target.files[0]) {
      setFile(URL.createObjectURL(e.target.files[0]))
      onChange(name, e.target.files[0], stepKey);
    } else {
      setFile("/profileUpload.png")
      onChange(name, {}, stepKey);
    }
  }

  return(
    <ImgWrapper width="100%">
      <Header2>Upload Your Photo</Header2>
      <SeperatorBox></SeperatorBox>
      <ProfilePhotoUploadForm>
          <ProfilePhotoUploadContainer>
              <input type="file" name={name} ref={fileInput} onChange={fileChangeHandler} style={{display: 'none'}} />
              <ProfileImage onClick={openFilePicker} width="115" height="115" alt= "" src={file}/>
              <span>Upload Profile Image.</span>
          </ProfilePhotoUploadContainer>
      </ProfilePhotoUploadForm>
      <span>We recommend a photo of you or your team. If not available, upload your business logo.</span>
    </ImgWrapper>
    );
}

export const ProfileImage = styled.img`

`;

FileInput.propTypes = {
  onChange: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  error: PropTypes.string,
  stepKey: PropTypes.string.isRequired,
  fileName: PropTypes.string.isRequired
}

export default FileInput;