import { InputWrapper } from './ProfileCreation.element';
import TextField from '@material-ui/core/TextField';
import PropTypes from 'prop-types';
import React from 'react';

const MultilineInput = ({ type = 'text', placeholder, width, name, value, onChange, error, mandatory }) => {
  return(

    <InputWrapper width={width}>
      <TextField
      multiline
      maxRows={6}
      variant="outlined"
      margin="dense"
      size="small"
      fullWidth
      label={placeholder}
      name={name}
      autoComplete="off"
      value={value}
      onChange={onChange}
      helperText={error && <div style={{"color":"red"}}>{error}</div>}
      inputProps={{
        style: { backgroundColor: '#fff' },
        maxLength: "300" 
      }} 
      />
    </InputWrapper>
  );
}

MultilineInput.propTypes = {
  type: PropTypes.string,
  placeholder: PropTypes.string,
  name: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object
  ]),
  onChange: PropTypes.func.isRequired
}

export default MultilineInput;