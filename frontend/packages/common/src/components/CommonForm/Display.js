import { InputWrapper } from "./ProfileCreation.element";
import React from "react";
import { SubSubSectionParagraph } from "globalstyles/globalStyles";
const Display = ({
  type = "text",
  placeholder,
  width,
  name,
  value,
  onChange,
  error,
  mandatory,
}) => {
  return (
    <InputWrapper width={width} style={{"textAlign":"left"}}>
      <SubSubSectionParagraph id="profileLink">
        https://ezy.dexterio.in/interior-designers/profile/
      </SubSubSectionParagraph>
    </InputWrapper>
  );
};

export default Display;
