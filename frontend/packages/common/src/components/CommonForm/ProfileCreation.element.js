import styled from 'styled-components'
import { IntroHeadlines } from 'globalstyles';

export const InputWrapper = styled.div`
  
  float: left;
  width: ${({width}) => width};
  padding: 5px;
`;

export const Menu = styled.ul`
  width: 50%;
  padding: 0;
  margin-top: 0;
  position: absolute;
  background-color: white;
  width: 100%;
  max-height: 20rem;
  overflow-y: auto;
  overflow-x: hidden;
  outline: 0;
  transition: opacity 0.1s ease;
  border-radius: 0 0 0.28571429rem 0.28571429rem;
  box-shadow: 0 2px 3px 0 rgba(34, 36, 38, 0.15);
  border-color: #96c8da;
  border-top-width: 0;
  border-right-width: 1px;
  border-bottom-width: 1px;
  border-left-width: 1px;
  border-style: solid;
  z-index: 10;
  border: ${props => (props.isOpen ? null : 'none')};
`

export const ControllerButton = styled.button`
  background-color: transparent;
  border: none;
  position: absolute;
  right: 0;
  top: 0;
  cursor: pointer;
  width: 47px;
  display: flex;
  flex-direction: column;
  height: 100%;
  justify-content: center;
  align-items: center;
`

export const Item = styled.li`
  position: relative;
  cursor: pointer;
  display: block;
  border: none;
  height: auto;
  text-align: left;
  border-top: none;
  line-height: 1em;
  color: rgba(0, 0, 0, 0.87);
  font-size: 1rem;
  text-transform: none;
  font-weight: 400;
  box-shadow: none;
  padding: 0.8rem 1.1rem;
  white-space: normal;
  word-wrap: normal;
  width: 50%;

  ${({isActive}) =>
    isActive &&
    `
    color: rgba(0,0,0,.95);
    background: rgba(0,0,0,.03);
  `}

  ${({isSelected}) =>
    isSelected &&
    `
    color: rgba(0,0,0,.95);
    font-weight: 700;
  `}
`

export const Dropdown = styled.div`
  cursor: pointer;
  position: relative;
  border-radius: 6px;
  border-bottom-right-radius: ${props => (props.isOpen ? '0' : '6px')};
  border-bottom-left-radius: ${props => (props.isOpen ? '0' : '6px')};
  box-shadow: 0 2px 3px 0 rgba(34, 36, 38, 0.15);
  border-color: #96c8da;
  border-top-width: 1px;
  border-right-width: 1px;
  border-bottom-width: 1px;
  border-left-width: 1px;
  border-style: solid;
  width: 50%;
`

export const TextInputContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
`

export const SelectedPill = styled.div`
  margin: 2px;
  padding: 2px 8px 2px 8px;
  display: inline-block;
  word-wrap: none;
  background-color: #ccc;
  border-radius: 2px;
`

export const SelectedPillContentWrap = styled.div`
  display: grid;
  grid-gap: 6px;
  grid-auto-flow: column;
  align-items: center;
`

export const SelectedPillX = styled.button`
  cursor: pointer;
  line-height: 0.8;
  border: none;
  background-color: transparent;
  padding: 0;
  font-size: 16px;
`

export const TextInput = styled.input`
  border: none;
  flex: 1;
  font-size: 14px;
  min-height: 27px;
`

export const ArrowIcon = ({isOpen}) => (
  <svg
    viewBox="0 0 20 20"
    preserveAspectRatio="none"
    width={16}
    fill="transparent"
    stroke="#979797"
    strokeWidth="1.1px"
    transform={isOpen ? 'rotate(180)' : null}
  >
    <path d="M1,6 L10,15 L19,6" />
  </svg>
)

export const Description = styled.div`
    margin-top: 10px;
    margin-bottom: 10px;
    color: #000707DE;
    font-family: "Open Sans",sans-serif;
    font-size: 16px;
    font-weight: 500;
    line-height: 1.6em;
`;

export const Button = styled.button`
  background-color: #0B4C76;
  color: #fff;
  border-width: 1px;
  cursor: pointer;
  padding: calc(.5em - 1px) 1em;
  white-space: nowrap;
  -webkit-appearance: none;
  border: 1px solid transparent;
  border-radius: 4px;
  display: inline-flex;
  font-size: 1rem;
  height: 2.5em;
  line-height: 1.5;
  margin: 43px;
  position: absolute;
  right:    0;
  bottom:   0;
`;

export const GoBackButton = styled.button`
  position: absolute;
  left:     0;
  bottom:   0;
  margin: 43px;
  cursor: pointer;
`;

export const SuggestionBoxWrapper = styled.div`
    margin-left: -8px;
    margin-right: -8px;

    &::before {
      content: " ";
      display: table;
    }

    &::after {
      content: " ";
      display: table;
    }
`;

export const SuggestionBox = styled.div`
    border-radius: 3px;
    border: dotted 1px #d7d7d7;
    padding: 15px;
    text-align: left;
    position: relative;
`;

export const SuggestionBoxHeader = styled(IntroHeadlines)`
    font-size: 18px;
    display: block;
    padding-bottom: 10px;
    margin-top: 0;
    border-bottom: dotted 1px #d7d7d7;
    margin-bottom: 20px;
    margin: 16px 0;
    line-height: 1.2;
`;
export const SuggestionBoxLine = styled.div`
    margin-bottom: 15px;
    &::before {
      content: " ";
      display: table;
    }
    & > span {
      &:nth-child(1) {
        font-size: 35px;
        color: ${({iconColor}) => iconColor};
        display: table-cell;
        width: 50px;
        padding: 0px 5px;
        font-family: 'Houzz-Icon-Glyphs' !important;
        font-style: normal;
        font-weight: normal;
        font-variant: normal;
        text-transform: none;
        line-height: 1;
        -webkit-font-smoothing: antialiased;
        zoom: 1;
        vertical-align: middle;
      }

      &:nth-child(2) {
        padding-top: 3px;
        padding-left: 15px;
        display: table-cell;
        font-size: 16px;
        font-family: "Open Sans", sans-serif;
        vertical-align: middle;
      }
    }

    &::after {
      content: " ";
      display: table;
    }
`;
export const PanelWrapper = styled.div`
    background-color: #fff;
    box-shadow:  5px 5px 15px #d2d3d5,
             -5px -5px 15px #ffffff;
    min-height : 90vh;
    position: relative;
    margin-top: 2vw;
    margin-bottom: 2vw;
    border-radius: 8px 8px 8px 8px;


    & > p {
      background-color: ${({backgroundColor}) => backgroundColor};
      color: #fff;
      text-align: center;
      border-radius: 8px 8px 0 0;
      font-size: 1.25em;
      font-weight: 700;
      line-height: 1.25;
      font-family: "Poppins", sans-serif;
      padding: 5px;
      margin-top: 0;
      margin-bottom: 0;
    }
`;

export const Subtitle = styled.div`
    max-width:100%;
    margin-bottom: 35px;
    color: #000707DE;
    font-family: "Open Sans", sans-serif;
    font-size: 16px;
    font-weight: 600;
    line-height: 1.6em;
    margin: 35px auto;


    @media(max-width: 768px) {
        font-size: 16px;
    }
`;

export const Panels = styled.div`
  display : flex;
  flex-direction: row;
`;

export const PanelLeft = styled.div` 
  width: 58%;
  border-bottom-left-radius: 6px;
  border-bottom-right-radius: 6px;
  color: #363636;
  display: block !important;
  padding: .5em 2em .5em 2em;
  @media only screen and (max-width:1024px) {
    width: 100%;
  }
`;

export const PanelRight = styled.div` 
  width: 42%;
  position: relative;
  min-height: 1px;
  padding-top: 1em;
  padding-left: 16x;
  padding-right: 50px;

  @media only screen and (max-width:1024px) {
    display: none;
  }
`;

export const PreviewContainer = styled.div`
    @media only screen and (min-width: 768px) {
      flex: 1;
      margin-left:auto;
      margin-right:auto;
      width: 50%;
      text-align: left;
    }
    display: block;
`;

export const ProfilePhotoUploadContainer = styled.div`
    padding: 20px;
    border: dashed 1px #d7d7d7;
    cursor: pointer;
    text-align: center;

    & > span {
        margin-left: 30px;
    }
    & > img {
        vertical-align: middle;
        text-align: left;
    }
`;

export const ProfilePhotoUploadForm = styled.div`
    margin-bottom: 15px;
    position: relative;
    &::before {
        content: " ";
        display: table;
    }
    &::after {
        content: " ";
        display: table;
    }
`;
export const ImgWrapper = styled.div`
    float: left;
    width: ${({width}) => width};
    padding: 5px;
`;

export const SeperatorBox = styled.div`
    border-bottom: dotted 1px #d7d7d7;
    height: 1px;
    margin-top: 15px;
    margin-bottom: 15px;
`;

export const Header2 = styled.div`
    margin-top: 30px;
    margin-bottom: 10px;
    font-family: "Open Sans", sans-serif;
    font-size : 24px;
    font-weight: 500;
    line-height: 1.2;
    text-align: left;
`;
