function validate (fields) {
    let errors = {};
    let mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    for(let field in fields) {
        const currentField = fields[field];

        if(currentField.required && currentField.value === '') {
            errors[field] = 'This field is required!';
        }

        if(currentField.email  && !currentField.value.match(mailformat)) {
            errors[field] = 'Please enter a valid Email Id';
        }

        if(currentField.required && currentField.file && !currentField.value.name) {
            errors[field] = 'This field is required!';
        }

        if(!errors[field] && currentField.minLength && currentField.value.trim().length < currentField.minLength) {
            errors[field] = `This field must have at least ${currentField.minLength} characters`;
        }

        if(!errors[field] && currentField.file && currentField.allowedTypes && !currentField.allowedTypes.includes(currentField.value.type.split('/')[1])) {
            errors[field] = 'Invalid file type!';
        }

        if(!errors[field] && currentField.file && currentField.maxFileSize && (currentField.maxFileSize * 1024) < Math.round(currentField.value.size)) {
            errors[field] = `File is too large(${Math.round(currentField.value.size / 1024)}KB), it cannot be larger than ${currentField.maxFileSize}KB`;
        }
    }

    return errors;
}

export default validate;