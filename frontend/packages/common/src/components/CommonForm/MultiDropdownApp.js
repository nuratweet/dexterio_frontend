import { InputWrapper } from './ProfileCreation.element';
import { Autocomplete, TextField } from '@material-ui/core';
import React from 'react';

function MultiDropdownApp({ name, value, dropdownChange, choices, error, placeholder, limit }) {

    const [disableInput, setDisableInput] = React.useState(false);
    
    const changeHandler = (event, newValue) => {
        setDisableInput(newValue.length >= limit);
        dropdownChange(event, newValue);
    }

    return (
        <InputWrapper width='100%'>
            <Autocomplete
                getOptionDisabled={()=> disableInput}
                multiple
                options={choices}
                getOptionLabel={(option) => option}
                name={name}
                onChange={changeHandler}
                filterSelectedOptions
                margin="dense"
                size="small"
                renderInput={(params) => (
                <TextField
                    {...params}
                    label={placeholder}
                    variant="outlined"
                    margin="dense"
                    required
                    name={name}
                    helperText={error && <div style={{"color":"red"}}>{error}</div>}
                />
                )}
            />
        </InputWrapper>
    )
}

export default MultiDropdownApp;