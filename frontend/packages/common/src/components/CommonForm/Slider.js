import React from "react";
import { InputWrapper } from "./ProfileCreation.element";
import { Slider } from "@material-ui/core";
import { FlexAlwaysRow, FlexColumn } from "../StyledComponents";

const CommonSlider = ({
  placeholder,
  onKeyDown,
  width,
  name,
  value,
  onChange,
  error,
  choices,
  textAround
}) => {

  function valueLabelFormat(value) {
    return textAround + ' ' + choices.find((mark) => mark.value === value).text;
  }

  return (
    <InputWrapper width="100%">
      <FlexAlwaysRow style={{justifyContent:"center", alignItems:"center"}}>
        <FlexColumn>{placeholder}</FlexColumn>
        <FlexColumn style={{flex:"1", "marginLeft":"1vw"}}>
          <InputWrapper width="95%">
            <Slider
              name={name}
              aria-label="Restricted values"
              valueLabelFormat={valueLabelFormat}
              step={null}
              valueLabelDisplay="auto"
              marks={choices}
              onChange={onChange}
              sx={{
                color: "rgba(255,92,92,1)",
                '& .MuiSlider-thumb': {
                  height: 12,
                  width: 12,
                  backgroundColor: '#fff',
                  border: '2px solid currentColor',
                  '&:focus, &:hover, &.Mui-active, &.Mui-focusVisible': {
                    boxShadow: 'inherit',
                  },
                  '&:before': {
                    display: 'none',
                  },
                }
              }}
            />
          </InputWrapper>
        </FlexColumn>
      </FlexAlwaysRow>
    </InputWrapper>
  );
};

export default CommonSlider;
