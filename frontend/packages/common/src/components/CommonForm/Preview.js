import { PanelWrapper , Description, Panels, PanelLeft, PanelRight, 
          SuggestionBoxWrapper, SuggestionBox, SuggestionBoxHeader, SuggestionBoxLine, 
          InputWrapper } from './ProfileCreation.element';
import { FaPhone, FaRuler } from 'react-icons/fa';
import { IoBriefcase } from 'react-icons/io5';
import React from 'react';
import { CurvedButton } from "globalstyles";
import { FlexRow , FlexAlwaysRow } from "../StyledComponents";
import { IconContext } from "react-icons/lib";

const Preview = ({ step , data, onPrevStep }) => {
  return(
    <PanelWrapper backgroundColor="rgba(123,131,97,1)"> 
      <p>Confirm and Submit</p>
      <Panels>
        <PanelLeft>
          <FlexAlwaysRow>
            <ul style={{"listStyle": "none"}}>
              {data.map((input, index) => (
                <li key={index}>
                  <Description>
                  {!input.image
                    ? <InputWrapper width={input.width} style={{"textAlign":"left"}}><strong>{input.label}:</strong> {input.value}</InputWrapper>
                    : <InputWrapper width={input.width} style={{"textAlign":"left"}}><strong>{input.label}:</strong> <img src={input.value} alt="" style={{maxWidth: '100px'}} /></InputWrapper>
                  }
                  </Description>
                </li>
              ))}
            </ul>
          </FlexAlwaysRow>
          <FlexAlwaysRow
            style={{
              alignItems: "center",
              justifyContent: "space-between",
              padding: "2em 2em",
              textAlign: "right",
            }}
          >
            <FlexRow
              style={{
                alignItems: "center",
                justifyContent: "flex-start",
              }}
            >
              <IconContext.Provider value={{ size: "3rem" }}>
                {step > 1 && (
                  <CurvedButton
                    background="rgba(255,92,92,1)"
                    light
                    curve
                    fontBig
                    margin="0px"
                    onClick={() => onPrevStep(step - 1)}
                  >
                    Go Back
                  </CurvedButton>
                )}
              </IconContext.Provider>
            </FlexRow>
            <FlexRow
              style={{
                alignItems: "center",
                justifyContent: "flex-end",
              }}
            >
              <CurvedButton
                background="rgba(255,92,92,1)"
                light
                curve
                fontBig
                margin="0px"
                type="submit"
              >
                Submit
              </CurvedButton>
            </FlexRow>
          </FlexAlwaysRow>

        </PanelLeft>
        <PanelRight>
          <SuggestionBoxWrapper>
            <SuggestionBox>
                <SuggestionBoxHeader>Complete your Profile</SuggestionBoxHeader>
                <SuggestionBoxLine iconColor="rgba(123,131,97,1)">
                    <span><IoBriefcase/></span>
                    <span>Include your business name as it will appear in the directory.</span>
                </SuggestionBoxLine>
                <SuggestionBoxLine iconColor="rgba(123,131,97,1)">
                    <span><FaRuler/></span>
                    <span>Choose the category and company type that best fit your business.</span>
                </SuggestionBoxLine>
                <SuggestionBoxLine iconColor="rgba(123,131,97,1)">
                    <span><FaPhone/></span>
                    <span>Provide your complete contact information in the space provided.</span>
                </SuggestionBoxLine>
            </SuggestionBox>
          </SuggestionBoxWrapper>
        </PanelRight>
      </Panels>
    </PanelWrapper>
  );
}

export default Preview;