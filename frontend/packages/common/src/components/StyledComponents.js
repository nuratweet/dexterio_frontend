import styled, {keyframes} from 'styled-components';

export const SvgWrapperRectangles = styled.div`
    z-index:9999;
    margin-left: 5%;
    

    @media only screen and (max-width: 768px) {
        display:none;
    }

    @media only screen and (max-width:1366) {
        transform:translateY(-22%);
    }


`;

export const FlexRow = styled.div`
    display: flex;

    @media only screen and (max-width:767px) {
        flex-direction: column;
    }
`;

export const FlexAlwaysRow = styled.div`
    display: flex;
`;

export const FlexColumn = styled.div`
    display: flex;
    flex-direction: column;
`;

export const TypicalExperienceFlexColumn = styled(FlexColumn)`
    width: 100%;
    margin-top: 5%;
    margin-bottom: 5%;
    padding: 10%;
    justify-content: center;
    align-items: center;
    background: ${({background}) => background};

    &> div {
        &:nth-child(2) {
            border-top: 1px solid rgba(123, 131, 97, 1);
        }
    }

    @media only screen and (min-width: 501px) {
        border-radius: 8px;
    }

    @media only screen and (max-width: 600px) {
        padding: 5%;
    }
`;

export const HideCompareFlexColumn = styled.div `
    display: flex;
    flex-direction: column;

    @media only screen and (max-width:600px) {
        display: none;
    }
`;
export const ProfileImageWrapper = styled.div`
    width: 200px;
    height: 200px;
    border-radius: 50%;
    overflow: hidden;
    padding: 3px;
    transform: translateY(${({translate}) => translate});

    @media only screen and (max-width: 600px) {
        width: 150px;
        height: 150px;
    }
`;

export const ProfileImage = styled.img`

    border-radius: 50%;
    width: 100%;
    height: 100%;
    object-fit: cover;
    object-position: center;
    padding: 1px;
    /* border: 1px solid #fff;//; */
`;

export const CarouselImages = styled.img`
    align-self: center;
    overflow: hidden;
    max-height: 80vh;
    object-fit: contain;
    object-position: center;
    border-radius:8px;

    @media only screen and (max-width: 600px) {
        max-height: 50vh;
    }
`;

export const BackgroundImage = styled.div`
    background: url(/${({image}) => image});
    height: 100vh;
    background-size: cover;
    position: relative;
 
    @media screen and (min-width: 769px) {
        & > div {
            position: absolute;
            top: 0px;
            left: 0px;
            transform: translate(50%, 50%);
        }
    }
    @media screen and (max-width: 768px) {
        height: 500px;
        & > div {
            position: absolute;
            top: 0px;
            left: 0px;
            transform: translate(10%, 20%);
        }
    }
`;

const floating = keyframes`

    0% {
        top: -70vh;
    }
    70% {
        top: 0px;
    }
    100% {
        top: -10vh;
    }
`;

export const LeftPanel = styled.div`
  height: calc(80vh + 50px);
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding: 12em 0em;
  padding-left: calc(15px + (100vw - 700px) * (15 - 30) / (700 - 1000));
  padding-right: calc(15px + (100vw - 700px) * (15 - 30) / (700 - 1000));
  background-color: #f4f3ee;
  position: absolute;
  top: 0;
  left: 0;

  @media screen and (max-width: 768px) {
    height: calc(70vh);
    padding-left: 2.5vw;
    padding-top: 30vh;

    animation-name: ${floating};
    animation-duration: 2s;
    animation-fill-mode: forwards;
    animation-delay: 0.5s;
    top: -70vh;
  }

  &::after {
    content: " ";
    position: absolute;
    display: block;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    z-index: -1;

    background: #f4f3ee;

    transform: skew(-5deg, 0deg);
    transform-origin: bottom left;
  }
`;

const tonext = keyframes`
  75% {
    left: 0;
  }
  95% {
    left: 100%;
  }
  98% {
    left: 100%;
  }
  99% {
    left: 0;
  }
`;

const snap = keyframes`
  96% {
    scroll-snap-align: center;
  }
  97% {
    scroll-snap-align: none;
  }
  99% {
    scroll-snap-align: none;
  }
  100% {
    scroll-snap-align: center;
  }
`;

export const DesktopView = styled.div`
    @media only screen and (max-width: 767px) {
        display: none;
    }
`;

export const MobileView = styled.div`
    @media only screen and (min-width: 768px) {
        display: none;
    }
`;

export const Carousel = styled.div`
    perspective: 100px;
    border-radius: 10px;
    overflow: hidden;
    width: 100%;
    &::-webkit-scrollbar {
        display: none;
    }
`;

export const CarouselViewPort = styled.ol`
    display: flex;
    overflow-x: scroll;
    counter-reset: item;
    scroll-behavior: smooth;
    scroll-snap-type: x mandatory;
    list-style: none;
    margin: 0;
    padding: 0;
    &::-webkit-scrollbar {
        display: none;
    }
`;

export const CarouselSlide = styled.li`
    flex: 0 0 100%;
    width: 100%;
    counter-increment: item;
    list-style: none;
    margin: 0;
    padding: 0;
    height:${({height}) => height};

`;

export const CarouselSnapper = styled.div`
    display: flex;
    justify-content: center;
    align-items: stretch;
    width: 100%;
    height: 100%;
    scroll-snap-align: center;

    &:hover {
        animation-name: ${tonext} , ${snap} ;
        animation-timing-function: ease;
        animation-duration: 4s;
        animation-iteration-count: infinite;
    }
`;


export const TextWrapper = styled.div`
    padding-top: 0;
    padding-bottom: 60px;

    @media screen and (max-width: 768px) {
        width: 90%;
        display: flex;
        justify-content: center;
        flex-direction: column;
        padding-bottom: 30px;
    }
`;

export const ImageWrapper = styled.div`
    max-height: 80vh;
    overflow: hidden;
    text-align: right;
    z-index: -100;
    @media screen and (max-width: 600px) {
        max-height: 100vh;
    }
`;

export const ImgDesktop = styled.img`
    z-index: -100;
    height: 100%;
    object-fit: fill;
    object-position: bottom;
    max-width:100%;
    vertical-align: middle;
    display: inline-block;

    @media screen and (min-width: 1279px) {
        max-width: 70%;
        object-position: right;
    }

    @media screen and (max-width: 768px) {
        display: none;
    }

`;

export const ImgMobile = styled.img`
    z-index: -100;
    height: 100%;
    object-fit: fill;
    object-position: bottom;
    max-width:100%;
    vertical-align: middle;
    display: inline-block;

    @media screen and (min-width: 1279px) {
        max-width: 70%;
        object-position: right;
    }
    @media screen and (min-width: 768px) {
        display: none;
    }

`;