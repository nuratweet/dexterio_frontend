import React, { useEffect, useState } from "react";
import styled from "styled-components";

export default function ScrollToTop() {
  const [isVisible, setIsVisible] = useState(false);

  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  useEffect(() => {
    const toggleVisibility = () => {
      if (window.pageYOffset > 500) {
        setIsVisible(true);
      } else {
        setIsVisible(false);
      }
    };

    window.addEventListener("scroll", toggleVisibility);

    return () => window.removeEventListener("scroll", toggleVisibility);
  }, []);

  return (
    <>
      {isVisible && (
        <ScrollToTopButton onClick={scrollToTop}>
          <svg
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
            style={{ zIndex: "1000" }}
          >
            <path d="M10 2.5L16.5 9H13v8H7V9H3.5L10 2.5z" />
          </svg>
        </ScrollToTopButton>
      )}
    </>
  );
}

const ScrollToTopButton = styled.div`
  display: flex;
  position: fixed;
  bottom: 0px;
  right: 0px;
  margin: 50px;
  width: 60px;
  height: 60px;
  border-radius: 50%;
  border: 1px solid #111121;
  cursor: pointer;
  justify-content: center;
  align-items: center;
  z-index: 999;

  &:hover,
  &:focus {
    transition: all 0.3s ease-out;
    background: #111121;
  }

  & > svg > path {
    &:hover,
    &:focus {
      fill: #fff;
    }
  }

  @media only screen and (max-width: 768px) {
    display: none;
  }
`;
