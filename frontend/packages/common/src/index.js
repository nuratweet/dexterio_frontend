import ScrollToTop from "./components/ScrollToTop";
import ScrollToTopButton from "./components/ScrollToTopButton";
import ComingSoon from "./components/Coming_Soon";
import { PanelWrapper } from "./components/CommonForm/ProfileCreation.element"; 
import {
  IndicatorProvider,
  Indicator,
  IndicatorFallback,
} from "./components/IndicatorProvider";
import FileDropHandler from './components/FileDropHandler'; 
import EZYRedirect from "./components/Redirects/EZYRedirect";
import NESTRedirect from "./components/Redirects/NESTRedirect";
import {
  SvgWrapperRectangles,
  FlexRow,
  ProfileImageWrapper,
  ProfileImage,
  BackgroundImage,
  LeftPanel,
  FlexColumn,
  TextWrapper,
  ImageWrapper,
  ImgDesktop,
  ImgMobile,
  HideCompareFlexColumn,
  FlexAlwaysRow,
  CarouselSnapper,
  Carousel,
  CarouselSlide,
  CarouselViewPort,
  DesktopView,
  MobileView,
  TypicalExperienceFlexColumn,
  CarouselImages
} from "./components/StyledComponents";
import FileInput from "./components/CommonForm/FileInput";
import Input from "./components/CommonForm/Input";
import MultiDropdownApp from "./components/CommonForm/MultiDropdownApp";
import MultilineInput from "./components/CommonForm/MultilineInput";
import Preview from "./components/CommonForm/Preview";
import Step from "./components/CommonForm/Step";
import validate from "./components/CommonForm/validate";
import MaterialUiPhoneNumber from "./components/PhoneNumberTextField/MaterialUiPhoneNumber";

export {
  FileInput,
  Input,
  MultiDropdownApp,
  validate,
  MultilineInput,
  Preview,
  Step,
  SvgWrapperRectangles,
  FlexRow,
  EZYRedirect,
  NESTRedirect,
  ScrollToTop,
  ComingSoon,
  ScrollToTopButton,
  IndicatorProvider,
  Indicator,
  IndicatorFallback,
  ProfileImageWrapper,
  ProfileImage,
  BackgroundImage,
  LeftPanel,
  FlexColumn,
  TextWrapper,
  ImageWrapper,
  ImgDesktop,
  ImgMobile,
  PanelWrapper,
  FileDropHandler,
  FlexAlwaysRow,
  HideCompareFlexColumn,
  CarouselSnapper,
  Carousel,
  CarouselSlide,
  CarouselViewPort,
  DesktopView,
  MobileView,
  TypicalExperienceFlexColumn,
  CarouselImages,
  MaterialUiPhoneNumber
};
