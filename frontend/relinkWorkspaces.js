(async () => {
    const { linkWorkspaces } = require("./packages/project-utils/lib");
    await linkWorkspaces();
})();